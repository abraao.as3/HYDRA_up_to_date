/**
 * @license almond 0.3.2 Copyright jQuery Foundation and other contributors.
 * Released under MIT license, http://github.com/requirejs/almond/LICENSE
 */
//Going sloppy to avoid 'use strict' string cost, but strict practices should
//be followed.
/*global setTimeout: false */

var requirejs, require, define;
(function (undef) {
    var main, req, makeMap, handlers,
        defined = {},
        waiting = {},
        config = {},
        defining = {},
        hasOwn = Object.prototype.hasOwnProperty,
        aps = [].slice,
        jsSuffixRegExp = /\.js$/;

    function hasProp(obj, prop) {
        return hasOwn.call(obj, prop);
    }

    /**
     * Given a relative module name, like ./something, normalize it to
     * a real name that can be mapped to a path.
     * @param {String} name the relative name
     * @param {String} baseName a real name that the name arg is relative
     * to.
     * @returns {String} normalized name
     */
    function normalize(name, baseName) {
        var nameParts, nameSegment, mapValue, foundMap, lastIndex,
            foundI, foundStarMap, starI, i, j, part, normalizedBaseParts,
            baseParts = baseName && baseName.split("/"),
            map = config.map,
            starMap = (map && map['*']) || {};

        //Adjust any relative paths.
        if (name) {
            name = name.split('/');
            lastIndex = name.length - 1;

            // If wanting node ID compatibility, strip .js from end
            // of IDs. Have to do this here, and not in nameToUrl
            // because node allows either .js or non .js to map
            // to same file.
            if (config.nodeIdCompat && jsSuffixRegExp.test(name[lastIndex])) {
                name[lastIndex] = name[lastIndex].replace(jsSuffixRegExp, '');
            }

            // Starts with a '.' so need the baseName
            if (name[0].charAt(0) === '.' && baseParts) {
                //Convert baseName to array, and lop off the last part,
                //so that . matches that 'directory' and not name of the baseName's
                //module. For instance, baseName of 'one/two/three', maps to
                //'one/two/three.js', but we want the directory, 'one/two' for
                //this normalization.
                normalizedBaseParts = baseParts.slice(0, baseParts.length - 1);
                name = normalizedBaseParts.concat(name);
            }

            //start trimDots
            for (i = 0; i < name.length; i++) {
                part = name[i];
                if (part === '.') {
                    name.splice(i, 1);
                    i -= 1;
                } else if (part === '..') {
                    // If at the start, or previous value is still ..,
                    // keep them so that when converted to a path it may
                    // still work when converted to a path, even though
                    // as an ID it is less than ideal. In larger point
                    // releases, may be better to just kick out an error.
                    if (i === 0 || (i === 1 && name[2] === '..') || name[i - 1] === '..') {
                        continue;
                    } else if (i > 0) {
                        name.splice(i - 1, 2);
                        i -= 2;
                    }
                }
            }
            //end trimDots

            name = name.join('/');
        }

        //Apply map config if available.
        if ((baseParts || starMap) && map) {
            nameParts = name.split('/');

            for (i = nameParts.length; i > 0; i -= 1) {
                nameSegment = nameParts.slice(0, i).join("/");

                if (baseParts) {
                    //Find the longest baseName segment match in the config.
                    //So, do joins on the biggest to smallest lengths of baseParts.
                    for (j = baseParts.length; j > 0; j -= 1) {
                        mapValue = map[baseParts.slice(0, j).join('/')];

                        //baseName segment has  config, find if it has one for
                        //this name.
                        if (mapValue) {
                            mapValue = mapValue[nameSegment];
                            if (mapValue) {
                                //Match, update name to the new value.
                                foundMap = mapValue;
                                foundI = i;
                                break;
                            }
                        }
                    }
                }

                if (foundMap) {
                    break;
                }

                //Check for a star map match, but just hold on to it,
                //if there is a shorter segment match later in a matching
                //config, then favor over this star map.
                if (!foundStarMap && starMap && starMap[nameSegment]) {
                    foundStarMap = starMap[nameSegment];
                    starI = i;
                }
            }

            if (!foundMap && foundStarMap) {
                foundMap = foundStarMap;
                foundI = starI;
            }

            if (foundMap) {
                nameParts.splice(0, foundI, foundMap);
                name = nameParts.join('/');
            }
        }

        return name;
    }

    function makeRequire(relName, forceSync) {
        return function () {
            //A version of a require function that passes a moduleName
            //value for items that may need to
            //look up paths relative to the moduleName
            var args = aps.call(arguments, 0);

            //If first arg is not require('string'), and there is only
            //one arg, it is the array form without a callback. Insert
            //a null so that the following concat is correct.
            if (typeof args[0] !== 'string' && args.length === 1) {
                args.push(null);
            }
            return req.apply(undef, args.concat([relName, forceSync]));
        };
    }

    function makeNormalize(relName) {
        return function (name) {
            return normalize(name, relName);
        };
    }

    function makeLoad(depName) {
        return function (value) {
            defined[depName] = value;
        };
    }

    function callDep(name) {
        if (hasProp(waiting, name)) {
            var args = waiting[name];
            delete waiting[name];
            defining[name] = true;
            main.apply(undef, args);
        }

        if (!hasProp(defined, name) && !hasProp(defining, name)) {
            throw new Error('No ' + name);
        }
        return defined[name];
    }

    //Turns a plugin!resource to [plugin, resource]
    //with the plugin being undefined if the name
    //did not have a plugin prefix.
    function splitPrefix(name) {
        var prefix,
            index = name ? name.indexOf('!') : -1;
        if (index > -1) {
            prefix = name.substring(0, index);
            name = name.substring(index + 1, name.length);
        }
        return [prefix, name];
    }

    /**
     * Makes a name map, normalizing the name, and using a plugin
     * for normalization if necessary. Grabs a ref to plugin
     * too, as an optimization.
     */
    makeMap = function (name, relName) {
        var plugin,
            parts = splitPrefix(name),
            prefix = parts[0];

        name = parts[1];

        if (prefix) {
            prefix = normalize(prefix, relName);
            plugin = callDep(prefix);
        }

        //Normalize according
        if (prefix) {
            if (plugin && plugin.normalize) {
                name = plugin.normalize(name, makeNormalize(relName));
            } else {
                name = normalize(name, relName);
            }
        } else {
            name = normalize(name, relName);
            parts = splitPrefix(name);
            prefix = parts[0];
            name = parts[1];
            if (prefix) {
                plugin = callDep(prefix);
            }
        }

        //Using ridiculous property names for space reasons
        return {
            f: prefix ? prefix + '!' + name : name, //fullName
            n: name,
            pr: prefix,
            p: plugin
        };
    };

    function makeConfig(name) {
        return function () {
            return (config && config.config && config.config[name]) || {};
        };
    }

    handlers = {
        require: function (name) {
            return makeRequire(name);
        },
        exports: function (name) {
            var e = defined[name];
            if (typeof e !== 'undefined') {
                return e;
            } else {
                return (defined[name] = {});
            }
        },
        module: function (name) {
            return {
                id: name,
                uri: '',
                exports: defined[name],
                config: makeConfig(name)
            };
        }
    };

    main = function (name, deps, callback, relName) {
        var cjsModule, depName, ret, map, i,
            args = [],
            callbackType = typeof callback,
            usingExports;

        //Use name if no relName
        relName = relName || name;

        //Call the callback to define the module, if necessary.
        if (callbackType === 'undefined' || callbackType === 'function') {
            //Pull out the defined dependencies and pass the ordered
            //values to the callback.
            //Default to [require, exports, module] if no deps
            deps = !deps.length && callback.length ? ['require', 'exports', 'module'] : deps;
            for (i = 0; i < deps.length; i += 1) {
                map = makeMap(deps[i], relName);
                depName = map.f;

                //Fast path CommonJS standard dependencies.
                if (depName === "require") {
                    args[i] = handlers.require(name);
                } else if (depName === "exports") {
                    //CommonJS module spec 1.1
                    args[i] = handlers.exports(name);
                    usingExports = true;
                } else if (depName === "module") {
                    //CommonJS module spec 1.1
                    cjsModule = args[i] = handlers.module(name);
                } else if (hasProp(defined, depName) ||
                           hasProp(waiting, depName) ||
                           hasProp(defining, depName)) {
                    args[i] = callDep(depName);
                } else if (map.p) {
                    map.p.load(map.n, makeRequire(relName, true), makeLoad(depName), {});
                    args[i] = defined[depName];
                } else {
                    throw new Error(name + ' missing ' + depName);
                }
            }

            ret = callback ? callback.apply(defined[name], args) : undefined;

            if (name) {
                //If setting exports via "module" is in play,
                //favor that over return value and exports. After that,
                //favor a non-undefined return value over exports use.
                if (cjsModule && cjsModule.exports !== undef &&
                        cjsModule.exports !== defined[name]) {
                    defined[name] = cjsModule.exports;
                } else if (ret !== undef || !usingExports) {
                    //Use the return value from the function.
                    defined[name] = ret;
                }
            }
        } else if (name) {
            //May just be an object definition for the module. Only
            //worry about defining if have a module name.
            defined[name] = callback;
        }
    };

    requirejs = require = req = function (deps, callback, relName, forceSync, alt) {
        if (typeof deps === "string") {
            if (handlers[deps]) {
                //callback in this case is really relName
                return handlers[deps](callback);
            }
            //Just return the module wanted. In this scenario, the
            //deps arg is the module name, and second arg (if passed)
            //is just the relName.
            //Normalize module name, if it contains . or ..
            return callDep(makeMap(deps, callback).f);
        } else if (!deps.splice) {
            //deps is a config object, not an array.
            config = deps;
            if (config.deps) {
                req(config.deps, config.callback);
            }
            if (!callback) {
                return;
            }

            if (callback.splice) {
                //callback is an array, which means it is a dependency list.
                //Adjust args if there are dependencies
                deps = callback;
                callback = relName;
                relName = null;
            } else {
                deps = undef;
            }
        }

        //Support require(['a'])
        callback = callback || function () {};

        //If relName is a function, it is an errback handler,
        //so remove it.
        if (typeof relName === 'function') {
            relName = forceSync;
            forceSync = alt;
        }

        //Simulate async callback;
        if (forceSync) {
            main(undef, deps, callback, relName);
        } else {
            //Using a non-zero value because of concern for what old browsers
            //do, and latest browsers "upgrade" to 4 if lower value is used:
            //http://www.whatwg.org/specs/web-apps/current-work/multipage/timers.html#dom-windowtimers-settimeout:
            //If want a value immediately, use require('id') instead -- something
            //that works in almond on the global level, but not guaranteed and
            //unlikely to work in other AMD implementations.
            setTimeout(function () {
                main(undef, deps, callback, relName);
            }, 4);
        }

        return req;
    };

    /**
     * Just drops the config on the floor, but returns req in case
     * the config return value is used.
     */
    req.config = function (cfg) {
        return req(cfg);
    };

    /**
     * Expose module registry for debugging and tooling
     */
    requirejs._defined = defined;

    define = function (name, deps, callback) {
        if (typeof name !== 'string') {
            throw new Error('See almond README: incorrect module build, no module name');
        }

        //This module may not have dependencies
        if (!deps.splice) {
            //deps is not an array, so probably means
            //an object literal or factory function for
            //the value. Adjust args.
            callback = deps;
            deps = [];
        }

        if (!hasProp(defined, name) && !hasProp(waiting, name)) {
            waiting[name] = [name, deps, callback];
        }
    };

    define.amd = {
        jQuery: true
    };
}());
define("../lib/almond", function(){});

define('core/text/char/KerningLibrary',['require','exports','module'],function (require, exports, module) {/**
* this class is used to adjust the positioning of the Char
* she stores values and adds an index for future requests
*
* @class
* @memberof type.text
**/
function KerningLibrary(){

}

KerningLibrary.prototype.constructor = KerningLibrary;
module.exports = KerningLibrary;

KerningLibrary._map = {};

KerningLibrary.getRightKerning = function(fontFamily, right, char){
    if(fontFamily === undefined || right === undefined || char === undefined)
    {
        throw 'you need to specify fontFamily, right end char. See the documentation.';
    }

    if(KerningLibrary._map[fontFamily] === undefined){
        KerningLibrary._map[fontFamily] = {};
    }

    if(KerningLibrary._map[fontFamily][char + right] === undefined){
        var test = new PIXI.Text(right, {fontFamily: fontFamily});
        var rw = test.width;
        // console.log("right width...." + rw);

        test.text = char + right;
        var ww = test.width;
        // console.log("word width..." + ww);

        test.text = char;
        var cw = test.width;
        // console.log("char width..." + cw);

        var diff = ww - rw;
        // console.log("diff....." + diff);

        KerningLibrary._map[fontFamily][char + right] = 1 - diff / cw;
    }

    return KerningLibrary._map[fontFamily][char + right];
};

/**
*Calculate the correct kerning acording to the font and store the request
*
* @param fontFamily {String} the font family for the requested char
* @param left {String} The char on the left of the requested char
* @param char {String} The Char to be generated an object char
* @static
*
*/
KerningLibrary.getLeftKerning = function(fontFamily, left, char){
    if(fontFamily === undefined || left === undefined || char === undefined)
    {
        throw 'you need to specify fontFamily, left end char. See the documentation.';
    }

    if(KerningLibrary._map[fontFamily] === undefined){
        KerningLibrary._map[fontFamily] = {};
    }

    if(KerningLibrary._map[fontFamily][left + char] === undefined){
        var test = new PIXI.Text(left, {fontFamily: fontFamily});
        var lw = test.width;


        test.text = left + char;
        var ww = test.width;


        test.text = char;
        var cw = test.width;


        var diff = ww - lw;


        KerningLibrary._map[fontFamily][left + char] = diff / cw;
    }



    return KerningLibrary._map[fontFamily][left + char];
};

});

define('core/text/char/Char',['require','exports','module','./KerningLibrary'],function (require, exports, module) {var KerningLibrary = require('./KerningLibrary');
var PIXI = require('PIXI');
/**
 * Represents a character to form a Word many character are needed
 *
 * @class
 * @extends PIXI.Container
 * @memberof type.text
 * @param text {String} the character in String
 * @param [style] {object|PIXI.TextStyle} the text style
 * @param [debug=false] {boolean} turn on/off objects to support
 * @see type.text.TextField
 */
function Char(text, style, debug) {
    PIXI.Container.call(this);
    var self = this;

    this._text = text || '';
    if (this._text.length > 1) this._text = this._text.charAt(0);

    this._style = new PIXI.TextStyle({
        fill: '#000000',
        fontSize: 20,
        fontFamily: 'Arial'
    });

    this._undeline = false;

    if (style !== undefined && style !== null) {
        for (var p in style) {
            if (p == 'underscore') {
                if (style[p] === true) {
                    this._underscore = true;
                }
                continue;
            }
            this._style[p] = style[p];
        }
    }

    this._textObject = new PIXI.Text(this._text, this._style);
    this.addChild(this._textObject);
    if (this._underscore === true) {
        this._underscoreObj = new PIXI.Graphics();
        this._underscoreObj.beginFill(this._style.fill);
        var size = this._style.fontSize < 40 ? 1 : 2;
        this._underscoreObj.drawRect(0, 0, this._textObject.width, size);
        this._underscoreObj.endFill();

        this._underscoreObj.x = this._textObject.x;
        this._underscoreObj.y = this._textObject.y + this._textObject.height - this._underscoreObj.height;

        this.addChild(this._underscoreObj);
    }

    this._bounds = new PIXI.Graphics();
    this._bounds.beginFill(0xff0000);
    this._bounds.drawRect(0, 0, this._textObject.width, this._textObject.height);
    this._bounds.endFill();
    this.addChildAt(this._bounds, 0);
    this._debug = false;
    this.debug = debug;
}

Char.prototype = Object.create(PIXI.Container.prototype);
Char.prototype.constructor = Char;
module.exports = Char;

Object.defineProperties(Char.prototype, {

    /**
     * Draw objects to help visualize char bounds
     *
     * @member {boolean}
     * @memberof type.text.Char#
     */
    debug: {
        get: function() {
            return this._debug;
        },
        set: function(value) {
            var d = value || false;
            if (d === true) this._bounds.alpha = 0.5;
            else if (d === false) this._bounds.alpha = 0;
        }
    },

    /**
     * return the x of the Char
     *
     * @member {number}
     * @memberof type.text.Char#
     */
    x: {
        get: function() {
            return this.position.x;
        },
        set: function(value) {

            this.position.x = value;

        }
    },



    /**
     * return the width of the Char
     *
     * @member {number}
     * @memberof type.text.Char#
     */
    width: {
        get: function() {
            return this.scale.x * this._textObject.getLocalBounds().width;
        },
        set: function(value) {
            var width = this._textObject.getLocalBounds().width;

            if (width !== 0) {
                this.scale.x = value / width;
            } else {
                this.scale.x = 1;
            }

            this._width = value;
        }
    },

    /**
     * return the width minus the kerning of the char to calculate width of words or lines
     *
     * @member {number}
     * @memberof type.text.Char#
     */
    widthMinusKerning: {
        get: function() {
            return this.scale.x * this._textObject.width + this._bounds.x;
        }
    },

    /**
     * Return the Text of the Char
     *
     * @member {String}
     * @memberof type.text.Char#
     */
    text: {
        get: function() {
            return this._textObject.text;
        }
    }
});



/**
 * Calculate the kerning from the character to the left
 *
 * @param char {String} the character to the left of the current one
 */
Char.prototype.setSideCharL = function(char) {

    if (char.length === 0) return;

    var k = KerningLibrary.getLeftKerning(this._style.fontFamily, char, this._textObject.text);
    this._bounds.x = this._textObject.width * k - this._textObject.width;
};


/**
 * Change the style of the char
 *
 * @param char {Object} object containing all the style properties
 */
Char.prototype.setStyle = function(style) {

    for (var p in style) {
        if (p == 'underscore') {
            if (style[p] === true) {
                this._underscore = true;
            }
            continue;
        }
        this._style[p] = style[p];
        this._textObject.style.p = style[p];
    }
};

});

define('core/text/word/Word',['require','exports','module'],function (require, exports, module) {/**
 * Used to order Chars and keep track of size of the Word
 *
 * @class
 * @memberof type.text
 */


function Word(text) {

    /**
     * Store the chars to keep track of width height and positioning
     *
     * @member {Array}
     */
    this.chars = [];

    this.indexWord = -1;

    this._width = 0;
    this._height = 0;
    this._x = 0;
    this._y = 0;


}


Word.prototype.constructor = Word;
module.exports = Word;

Object.defineProperties(Word.prototype, {

    /**
     * x position of the Word
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    x: {
        get: function() {
            return this._x;
        },
        set: function(value) {
            this._x = value;
        }
    },

    /**
     * y position of the Word
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    y: {
        get: function() {
            return this._y;
        },
        set: function(value) {
            this._y = value;
        }
    },


    /**
     * width of the Word
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    width: {
        get: function() {
            return this._width;
        },
        set: function(value) {
            this._width = value;
        }
    },


    /**
     * height of the Word
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    height: {
        get: function() {
            return this._height;
        },
        set: function(value) {
            this._height = value;
        }
    }
});

});

define('core/text/phrase/Phrase',['require','exports','module'],function (require, exports, module) {/**
 * Used to order words and keep track of size of the line
 *
 * @class
 * @memberof type.text
 */

function Phrase(text) {

    /**
     * Store the words to keep track of width height and positioning
     *
     * @member {Array}
     */
    this.words = [];
    this._width = 0;
    this._height = 0;
    this._x = 0;
    this._y = 0;

}

Phrase.prototype.constructor = Phrase;
module.exports = Phrase;

Object.defineProperties(Phrase.prototype, {

    /**
     * x position of the line
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    x: {
        get: function() {
            return this._x;
        },
        set: function(value) {
            this._x = value;
        }
    },

    /**
     * y position of the line
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    y: {
        get: function() {
            return this._y;
        },
        set: function(value) {
            this._y = value;
        }
    },

    /**
     * width of the line
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    width: {
        get: function() {
            return this._width;
        },
        set: function(value) {
            this._width = value;
        }
    },

    /**
     * height of the line
     *
     * @member {number}
     * @default 0
     * @memberof type.text.Phrase#
     */
    height: {
        get: function() {
            return this._height;
        },
        set: function(value) {
            this._height = value;
        }
    }
});

});

define('core/../../lib/Jsdiff',['require','exports','module'],function (require, exports, module) {/* See LICENSE file for terms of use */

/*
 * Text diff implementation.
 *
 * This library supports the following APIS:
 * JsDiff.diffChars: Character by character diff
 * JsDiff.diffWords: Word (as defined by \b regex) diff which ignores whitespace
 * JsDiff.diffLines: Line based diff
 *
 * JsDiff.diffCss: Diff targeted at CSS content
 *
 * These methods are based on the implementation proposed in
 * "An O(ND) Difference Algorithm and its Variations" (Myers, 1986).
 * http://citeseerx.ist.psu.edu/viewdoc/summary?doi=10.1.1.4.6927
 */
var JsDiff = (function () {
    /*jshint maxparams: 5*/
    function clonePath(path) {
        return {
            newPos: path.newPos,
            components: path.components.slice(0)
        };
    }

    function removeEmpty(array) {
        var ret = [];
        for (var i = 0; i < array.length; i++) {
            if (array[i]) {
                ret.push(array[i]);
            }
        }
        return ret;
    }

    function escapeHTML(s) {
        var n = s;
        n = n.replace(/&/g, '&amp;');
        n = n.replace(/</g, '&lt;');
        n = n.replace(/>/g, '&gt;');
        n = n.replace(/"/g, '&quot;');

        return n;
    }

    var Diff = function (ignoreWhitespace) {
        this.ignoreWhitespace = ignoreWhitespace;
    };
    Diff.prototype = {
        diff: function (oldString, newString) {
            // Handle the identity case (this is due to unrolling editLength == 0
            if (newString === oldString) {
                return [{
                    value: newString
                }];
            }
            if (!newString) {
                return [{
                    value: oldString,
                    removed: true
                }];
            }
            if (!oldString) {
                return [{
                    value: newString,
                    added: true
                }];
            }

            newString = this.tokenize(newString);
            oldString = this.tokenize(oldString);

            var newLen = newString.length,
                oldLen = oldString.length;
            var maxEditLength = newLen + oldLen;
            var bestPath = [{
                newPos: -1,
                components: []
            }];

            // Seed editLength = 0
            var oldPos = this.extractCommon(bestPath[0], newString, oldString, 0);
            if (bestPath[0].newPos + 1 >= newLen && oldPos + 1 >= oldLen) {
                return bestPath[0].components;
            }

            for (var editLength = 1; editLength <= maxEditLength; editLength++) {
                for (var diagonalPath = -1 * editLength; diagonalPath <= editLength; diagonalPath += 2) {
                    var basePath;
                    var addPath = bestPath[diagonalPath - 1],
                        removePath = bestPath[diagonalPath + 1];
                    oldPos = (removePath ? removePath.newPos : 0) - diagonalPath;
                    if (addPath) {
                        // No one else is going to attempt to use this value, clear it
                        bestPath[diagonalPath - 1] = undefined;
                    }

                    var canAdd = addPath && addPath.newPos + 1 < newLen;
                    var canRemove = removePath && 0 <= oldPos && oldPos < oldLen;
                    if (!canAdd && !canRemove) {
                        bestPath[diagonalPath] = undefined;
                        continue;
                    }

                    // Select the diagonal that we want to branch from. We select the prior
                    // path whose position in the new string is the farthest from the origin
                    // and does not pass the bounds of the diff graph
                    if (!canAdd || (canRemove && addPath.newPos < removePath.newPos)) {
                        basePath = clonePath(removePath);
                        this.pushComponent(basePath.components, oldString[oldPos], undefined, true);
                    } else {
                        basePath = clonePath(addPath);
                        basePath.newPos++;
                        this.pushComponent(basePath.components, newString[basePath.newPos], true, undefined);
                    }

                    var oldPos = this.extractCommon(basePath, newString, oldString, diagonalPath);

                    if (basePath.newPos + 1 >= newLen && oldPos + 1 >= oldLen) {
                        return basePath.components;
                    } else {
                        bestPath[diagonalPath] = basePath;
                    }
                }
            }
        },

        pushComponent: function (components, value, added, removed) {
            var last = components[components.length - 1];
            if (last && last.added === added && last.removed === removed) {
                // We need to clone here as the component clone operation is just
                // as shallow array clone
                components[components.length - 1] = {
                    value: this.join(last.value, value),
                    added: added,
                    removed: removed
                };
            } else {
                components.push({
                    value: value,
                    added: added,
                    removed: removed
                });
            }
        },
        extractCommon: function (basePath, newString, oldString, diagonalPath) {
            var newLen = newString.length,
                oldLen = oldString.length,
                newPos = basePath.newPos,
                oldPos = newPos - diagonalPath;
            while (newPos + 1 < newLen && oldPos + 1 < oldLen && this.equals(newString[newPos + 1], oldString[oldPos + 1])) {
                newPos++;
                oldPos++;

                this.pushComponent(basePath.components, newString[newPos], undefined, undefined);
            }
            basePath.newPos = newPos;
            return oldPos;
        },

        equals: function (left, right) {
            var reWhitespace = /\S/;
            if (this.ignoreWhitespace && !reWhitespace.test(left) && !reWhitespace.test(right)) {
                return true;
            } else {
                return left === right;
            }
        },
        join: function (left, right) {
            return left + right;
        },
        tokenize: function (value) {
            return value;
        }
    };

    var CharDiff = new Diff();

    var WordDiff = new Diff(true);
    WordDiff.tokenize = function (value) {
        return removeEmpty(value.split(/(\s+|\b)/));
    };

    var CssDiff = new Diff(true);
    CssDiff.tokenize = function (value) {
        return removeEmpty(value.split(/([{}:;,]|\s+)/));
    };

    var LineDiff = new Diff();
    LineDiff.tokenize = function (value) {
        return value.split(/^/m);
    };

    return {
        Diff: Diff,

        diffChars: function (oldStr, newStr) {
            return CharDiff.diff(oldStr, newStr);
        },
        diffWords: function (oldStr, newStr) {
            return WordDiff.diff(oldStr, newStr);
        },
        diffLines: function (oldStr, newStr) {
            return LineDiff.diff(oldStr, newStr);
        },

        diffCss: function (oldStr, newStr) {
            return CssDiff.diff(oldStr, newStr);
        },

        createPatch: function (fileName, oldStr, newStr, oldHeader, newHeader) {
            var ret = [];

            ret.push('Index: ' + fileName);
            ret.push('===================================================================');
            ret.push('--- ' + fileName + (typeof oldHeader === 'undefined' ? '' : '\t' + oldHeader));
            ret.push('+++ ' + fileName + (typeof newHeader === 'undefined' ? '' : '\t' + newHeader));

            var diff = LineDiff.diff(oldStr, newStr);
            if (!diff[diff.length - 1].value) {
                diff.pop(); // Remove trailing newline add
            }
            diff.push({
                value: '',
                lines: []
            }); // Append an empty value to make cleanup easier

            function contextLines(lines) {
                return lines.map(function (entry) {
                    return ' ' + entry;
                });
            }

            function eofNL(curRange, i, current) {
                var last = diff[diff.length - 2],
                    isLast = i === diff.length - 2,
                    isLastOfType = i === diff.length - 3 && (current.added !== last.added || current.removed !== last.removed);

                // Figure out if this is the last line for the given file and missing NL
                if (!/\n$/.test(current.value) && (isLast || isLastOfType)) {
                    curRange.push('\\ No newline at end of file');
                }
            }

            var oldRangeStart = 0,
                newRangeStart = 0,
                curRange = [],
                oldLine = 1,
                newLine = 1;
            for (var i = 0; i < diff.length; i++) {
                var current = diff[i],
                    lines = current.lines || current.value.replace(/\n$/, '').split('\n');
                current.lines = lines;

                if (current.added || current.removed) {
                    if (!oldRangeStart) {
                        var prev = diff[i - 1];
                        oldRangeStart = oldLine;
                        newRangeStart = newLine;

                        if (prev) {
                            curRange = contextLines(prev.lines.slice(-4));
                            oldRangeStart -= curRange.length;
                            newRangeStart -= curRange.length;
                        }
                    }
                    curRange.push.apply(curRange, lines.map(function (entry) {
                        return (current.added ? '+' : '-') + entry;
                    }));
                    eofNL(curRange, i, current);

                    if (current.added) {
                        newLine += lines.length;
                    } else {
                        oldLine += lines.length;
                    }
                } else {
                    if (oldRangeStart) {
                        // Close out any changes that have been output (or join overlapping)
                        if (lines.length <= 8 && i < diff.length - 2) {
                            // Overlapping
                            curRange.push.apply(curRange, contextLines(lines));
                        } else {
                            // end the range and output
                            var contextSize = Math.min(lines.length, 4);
                            ret.push(
                                '@@ -' + oldRangeStart + ',' + (oldLine - oldRangeStart + contextSize) + ' +' + newRangeStart + ',' + (newLine - newRangeStart + contextSize) + ' @@');
                            ret.push.apply(ret, curRange);
                            ret.push.apply(ret, contextLines(lines.slice(0, contextSize)));
                            if (lines.length <= 4) {
                                eofNL(ret, i, current);
                            }

                            oldRangeStart = 0;
                            newRangeStart = 0;
                            curRange = [];
                        }
                    }
                    oldLine += lines.length;
                    newLine += lines.length;
                }
            }

            return ret.join('\n') + '\n';
        },

        applyPatch: function (oldStr, uniDiff) {
            var diffstr = uniDiff.split('\n');
            var diff = [];
            var remEOFNL = false,
                addEOFNL = false;

            for (var i = (diffstr[0][0] === 'I' ? 4 : 0); i < diffstr.length; i++) {
                if (diffstr[i][0] === '@') {
                    var meh = diffstr[i].split(/@@ -(\d+),(\d+) \+(\d+),(\d+) @@/);
                    diff.unshift({
                        start: meh[3],
                        oldlength: meh[2],
                        oldlines: [],
                        newlength: meh[4],
                        newlines: []
                    });
                } else if (diffstr[i][0] === '+') {
                    diff[0].newlines.push(diffstr[i].substr(1));
                } else if (diffstr[i][0] === '-') {
                    diff[0].oldlines.push(diffstr[i].substr(1));
                } else if (diffstr[i][0] === ' ') {
                    diff[0].newlines.push(diffstr[i].substr(1));
                    diff[0].oldlines.push(diffstr[i].substr(1));
                } else if (diffstr[i][0] === '\\') {
                    if (diffstr[i - 1][0] === '+') {
                        remEOFNL = true;
                    } else if (diffstr[i - 1][0] === '-') {
                        addEOFNL = true;
                    }
                }
            }

            var str = oldStr.split('\n');
            for (var i = diff.length - 1; i >= 0; i--) {
                var d = diff[i];
                for (var j = 0; j < d.oldlength; j++) {
                    if (str[d.start - 1 + j] !== d.oldlines[j]) {
                        return false;
                    }
                }
                Array.prototype.splice.apply(str, [d.start - 1, +d.oldlength].concat(d.newlines));
            }

            if (remEOFNL) {
                while (!str[str.length - 1]) {
                    str.pop();
                }
            } else if (addEOFNL) {
                str.push('');
            }
            return str.join('\n');
        },

        convertChangesToXML: function (changes) {
            var ret = [];
            for (var i = 0; i < changes.length; i++) {
                var change = changes[i];
                if (change.added) {
                    ret.push("<ins class='diff'>");
                } else if (change.removed) {
                    ret.push("<del class='diff'>");
                }

                ret.push(escapeHTML(change.value));

                if (change.added) {
                    ret.push('</ins>');
                } else if (change.removed) {
                    ret.push('</del>');
                }
            }
            return ret.join('');
        },

        // See: http://code.google.com/p/google-diff-match-patch/wiki/API
        convertChangesToDMP: function (changes) {
            var ret = [],
                change;
            for (var i = 0; i < changes.length; i++) {
                change = changes[i];
                ret.push([(change.added ? 1 : change.removed ? -1 : 0), change.value]);
            }
            return ret;
        }
    };
})();

if (typeof module !== 'undefined') {
    module.exports = JsDiff;
}

/*var a = document.getElementById('a');
var b = document.getElementById('b');
var result = document.getElementById('result');
var diffType = "diffChars";

function check(){
    var oldStr = a.value;
    var newStr = b.value;
    var changes = JsDiff[diffType](oldStr, newStr);
    console.log(changes)
    result.innerHTML = JsDiff.convertChangesToXML(changes);
}

a.onkeyup = b.onkeyup =
a.onpaste = a.onchange =
b.onpaste = b.onchange = check;
check();

var radio = document.getElementsByName('diff_type');
for (var i = 0; i < radio.length; i++) {
	radio[i].onchange = function(e) {
		diffType = this.value;
		check();
	}
}*/
});

define('core/text/textfield/align/horizontalModule/HorizontalModule',['require','exports','module','../../../phrase/Phrase','../../../word/Word'],function (require, exports, module) {var Phrase = require("../../../phrase/Phrase"),
    Word = require("../../../word/Word");




/**
 * Used to order alocate and align chars in a horizontal manner
 *
 * @class
 * @memberof type.text
 */

function HorizontalModule() {

    /**
     * width to be used to calculate
     *
     * @member {number}
     * @default 0
     */
    this._width = 0;

    /**
     * height to be used to calculate
     *
     * @member {number}
     * @default 0
     */
    this._height = 0;

    /**
     * define starting position left or right
     *
     * @member {boolean}
     * @default true
     */
    this._textLeftToRight = true;

    /**
     * define starting position top or bottom
     *
     * @member {boolean}
     * @default true
     */
    this._textTopToBottom = true;

    /**
     * define space between lines
     *
     * @member {number}
     * @default 0
     */
    this._spaceBetweenLines = 0;

    /**
     * define space between words
     *
     * @member {number}
     * @default -1
     */
    this._spaceBetweenWords = -1;

    /**
     * define the form of alignment
     *
     * @member {String}
     * @default left
     */
    this._textAlign = "left";


}

//HorizontalModule.prototype = Object.create(PIXI.Container.prototype);
HorizontalModule.prototype.constructor = HorizontalModule;
module.exports = HorizontalModule;


/**
 *   Organiza os caracteres em palavras e frases para fazer o alinhamento depois
 *
 *  @param letters [array of objects to be align] {Array}
 *  @param width [width used to calculate de align borders] {number}
 *  @param height [height used to calculate de align borders] {number}
 */
HorizontalModule.prototype._relocate = function(letters, width, height) {

    this._width = width;
    this._height = height;

    this._lines = [];


    this._lines.push(new Phrase());
    this._lines[0].words.push(new Word());
    this._lines[0].words[0].indexWord = 0;
    var isSpace = false;
    var breakMultiControl = false;

    for (var i = 0; i < letters.length; i++) {

        var line = this._lines[this._lines.length - 1];
        var word = line.words[line.words.length - 1];

        //c é o caracter a ser tratado
        var c = letters[i];


        //seta as propriedades do char
        if (i !== 0) {
            c.setSideCharL(letters[i - 1].text);
        }


        //se for um /n  organizar a linha atual, criar nova linha e setar a primeira palavra
        if (c.text == "\n") {

            this._lines.push(new Phrase());
            line = this._lines[this._lines.length - 1];
            var w = new Word();
            w.indexWord = 0;
            line.words.push(w);

            if (breakMultiControl) {
                line.height = c.height;
                w.height = c.height;
            } else{
                c.height = 0;
            }

            c.width = 0;

            w.chars.push(c);

            breakMultiControl = true;
            isSpace = false;

            continue;
        }
        breakMultiControl = false;

        //se for espaço adicionar o caracter criar uma nova palavra e adicionar a frase
        if (c.text == " ") {
            isSpace = true;
            if (this._spaceBetweenWords != -1) {
                c.width = this._spaceBetweenWords;
            } else{
                c.width = 8;
            }
        }


        if (isSpace && c.text != " ") {
            isSpace = false;

            var wordAtual = this._arrangeLines(line, c.width);
            line = this._lines[this._lines.length - 1];

            var w = null;
            if (this._lines[this._lines.length - 1].words[this._lines[this._lines.length - 1].words.length - 1].chars.length !== 0) {
                //adiciona nova palavra, com a proxima index
                var index = word.indexWord;
                w = new Word();
                w.indexWord = index + 1;
                line.words.push(w);

            } else {
                w = wordAtual;
            }

            w.chars.push(c);


            w.width = c.widthMinusKerning;
            w.height = c.height;
            line.width += c.widthMinusKerning;

            if (c.height > line.height) {
                line.height = c.height;
            }

            continue;
        }

        var word = this._arrangeLines(line, c.width);

        line = this._lines[this._lines.length - 1];

        word.width += c.widthMinusKerning;
        line.width += c.widthMinusKerning;


        if (c.height > word.height) {
            word.height = c.height;
        }

        if (c.height > line.height) {
            line.height = c.height;
        }

        //coloca o caracter no final da palavra
        word.chars.push(c);



    }
    //alinha as linhas e coloca o espaçamento em Y
    this._alignLines(this._lines);
};

/**
 *  Break the line if the phrase is bigger then the field
 *
 *  @param line [line to be analised] {Array}
 */
HorizontalModule.prototype._arrangeLines = function(line, charWidth) {

    var self = this;

    if (line.words.length == 1) {
        if (line.width + charWidth > self._width) {
            this._lines.push(new Phrase());

            novaWord = new Word();
            novaWord.indexWord = 0;

            this._lines[this._lines.length -1].words.push(novaWord);


        }
    } else {
        if (line.width + charWidth > self._width) {


            this._lines.push(new Phrase());

            var widthTotal = 0;
            for (var i = 0; i < line.words.length; i++) {

                widthTotal += line.words[i].width;


                if (self._width < widthTotal + charWidth) {
                    // arruma a largura e altura da linha antiga
                    var wordAntiga = line.words[line.words.length - 1];

                    line.width -= wordAntiga.width;
                    line.words[line.words.length - 2].chars[line.words[line.words.length - 2].chars.length - 1].width = 0;


                    var spliceWord = line.words.splice(line.words.length - 1, 1);
                    var word = spliceWord[0];
                    word.indexWord = 0;

                    this._lines[this._lines.length - 1].words.push(word);

                    //arruma a largura e altura da linha
                    var linhaNova = this._lines[this._lines.length - 1];
                    linhaNova.width = wordAntiga.width;
                    linhaNova.height = wordAntiga.height;

                    break;
                }
            }
        }
    }
    return this._lines[this._lines.length - 1].words[this._lines[this._lines.length - 1].words.length - 1];

};





/**
 *  Responsible for the decision of which direction to write(between all 4 vertical types) and which align to use (4 align types)
 *
 *  @param lines [lines to repositioned] {Array}
 */
HorizontalModule.prototype._alignLines = function(lines) {

    //chooses vertical direction
    if (this._textTopToBottom) {
        //start top


        //chooses horizontal direction
        if (this._textLeftToRight) {
            //left to right start top

            if (this._textAlign == "center") {

                this._centerAlignLRUD(lines);
                return;
            }

            if (this._textAlign == "right") {

                this._rightAlignLRUD(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignLRUD(lines);
                return;
            }

            this._leftAlignLRUD(lines);
            return;
        } else {
            //right to left start top
            if (this._textAlign == "center") {

                this._centerAlignRLUD(lines);
                return;
            }

            if (this._textAlign == "right") {

                this._rightAlignRLUD(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignRLUD(lines);
                return;
            }

            this._leftAlignRLUD(lines);
            return;
        }
    } else {
        //start Bottom

        //chooses horizontal direction
        if (this._textLeftToRight) {
            //left to right start Bottom

            if (this._textAlign == "center") {

                this._centerAlignLRDU(lines);
                return;
            }

            if (this._textAlign == "right") {

                this._rightAlignLRDU(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignLRDU(lines);
                return;
            }

            this._leftAlignLRDU(lines);
            return;
        } else {
            //right to left start Bottom
            if (this._textAlign == "center") {

                this._centerAlignRLDU(lines);
                return;
            }

            if (this._textAlign == "right") {

                this._rightAlignRLDU(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignRLDU(lines);
                return;
            }

            this._leftAlignRLDU(lines);
            return;
        }

    }

};


//////// Iniciando alinhamentos


/// primeiro LRUD

/**
 * Align on the left starting from the top left going right then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._leftAlignLRUD = function(lines) {

    var self = this;
    var y = 0;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];



        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y += lines[i - 1].height;
        }

        line.y = y;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].x = 0;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);

                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].widthMinusKerning;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].widthMinusKerning;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;

            }

        }

        y += this._spaceBetweenLines;

    }

};

/**
 * Align on the right starting from the top left going right then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._rightAlignLRUD = function(lines) {

    var self = this;
    var y = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y += lines[i - 1].height;
        }


        line.y = y;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].x = line.words[j + 1].chars[0].x - line.words[j].chars[k].widthMinusKerning;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k + 1].x - line.words[j].chars[k].widthMinusKerning;

                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
            }

        }

        y += this._spaceBetweenLines;

    }

};

/**
 * Align on the center starting from the top left going right then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._centerAlignLRUD = function(lines) {

    var self = this;
    var y = 0;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y += lines[i - 1].height;
        }


        line.y = y;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].x = self._width / 2 - lines[i]._width / 2;
                    lines[i].words[j].chars[k].y = y + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].x = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].x + lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].widthMinusKerning;
                    lines[i].words[j].chars[k].y = y + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }


                lines[i].words[j].chars[k].x = lines[i].words[j].chars[k - 1].x + lines[i].words[j].chars[k - 1].widthMinusKerning;
                lines[i].words[j].chars[k].y = y + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
            }

        }

        y += this._spaceBetweenLines;

    }

};

/**
 * Align justified starting from the top left going right then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._justifyAlignLRUD = function(lines) {

    var self = this;
    var y = 0;

    //se for mais de uma linha
    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //soma ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                y += lines[i - 1].height;
            }


            line.y = y;
            var difEspacoLinha = (self._width - line.width) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {

                //se for mais de uma palavra e for a ultima palavra alinha na direita
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k == line.words[j].chars.length - 1) {
                                line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                                continue;
                            }

                            line.words[j].chars[k].x = line.words[j].chars[k + 1].x - line.words[j].chars[k].widthMinusKerning;
                            line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);


                        }
                        break;
                    }
                }

                for (var k = 0; k < line.words[j].chars.length; k++) {

                    if (k === 0 && j === 0) {
                        line.words[j].chars[k].x = 0;
                        line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].widthMinusKerning + difEspacoLinha;
                        line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }


                    line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].widthMinusKerning;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;


                }

            }

            y += this._spaceBetweenLines;

        }


        y += lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].x = 0;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].widthMinusKerning;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }

            line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].widthMinusKerning;
            line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
            continue;

        }

    }

};




//
//
//iniciando RLUD  (direita esquerda começando do topo)
//
//


/**
 * Align on the left starting from the top right going left then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._leftAlignRLUD = function(lines) {

    var self = this;
    var y = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y += lines[i - 1].height;
        }


        line.y = y;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].x = 0;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].x = line.words[j + 1].chars[0].x + line.words[j + 1].chars[0].widthMinusKerning;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k + 1].x + line.words[j].chars[k + 1].widthMinusKerning;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
            }

        }

        y += this._spaceBetweenLines;

    }

};

/**
 * Align on the center starting from the top right going left then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._centerAlignRLUD = function(lines) {

    var self = this;
    var y = 0;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y += lines[i - 1].height;
        }


        line.y = y;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].x = self._width / 2 + lines[i]._width / 2 - lines[i].words[j].chars[k].width;
                    lines[i].words[j].chars[k].y = y + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].x = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].x - lines[i].words[j].chars[k].widthMinusKerning;
                    lines[i].words[j].chars[k].y = y + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }


                lines[i].words[j].chars[k].x = lines[i].words[j].chars[k - 1].x - lines[i].words[j].chars[k].widthMinusKerning;
                lines[i].words[j].chars[k].y = y + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
            }

        }

        y += this._spaceBetweenLines;

    }

};


/**
 * Align on the right starting from the top right going left then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._rightAlignRLUD = function(lines) {

    var self = this;
    var y = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y += lines[i - 1].height;
        }


        line.y = y;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x - line.words[j].chars[k].widthMinusKerning;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k - 1].x - line.words[j].chars[k].widthMinusKerning;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;

            }

        }

        y += this._spaceBetweenLines;

    }

};



/**
 * Align justified starting from the top right going left then down
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._justifyAlignRLUD = function(lines) {

    var self = this;
    var y = 0;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //soma ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                y += lines[i - 1].height;
            }


            line.y = y;
            var difEspacoLinha = (self._width - line.width) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {


                //se for mais de uma palavra e for a ultima palavra alinha na esquerda
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k === line.words[j].chars.length - 1) {
                                line.words[j].chars[k].x = 0;
                                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                                continue;
                            }

                            line.words[j].chars[k].x = line.words[j].chars[k + 1].x + line.words[j].chars[k + 1].widthMinusKerning;
                            line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);


                        }
                        break;
                    }
                }


                //se n for a ultima palavra alinha da direita


                // if (k === 0 && j === 0) {
                //     line.words[j].chars[k].x = 0;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                // if (k === 0) {
                //     line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].width + difEspacoLinha;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                //
                //
                // line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].width;
                // line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                // continue;

                for (var k = 0; k < line.words[j].chars.length; k++) {
                    if (j === 0 && k === 0) {
                        line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                        line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x - line.words[j].chars[k].widthMinusKerning - difEspacoLinha;
                        line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }

                    line.words[j].chars[k].x = line.words[j].chars[k - 1].x - line.words[j].chars[k].widthMinusKerning;
                    line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;

                }




            }

            y += this._spaceBetweenLines;

        }


        y += lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x - line.words[j].chars[k].widthMinusKerning;
                line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }

            line.words[j].chars[k].x = line.words[j].chars[k - 1].x - line.words[j].chars[k].widthMinusKerning;
            line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
            continue;

        }

    }

};

//FIM do justify




//
//
//iniciando LRDU  (esquerda direita começando da base)
//
//


/**
 * Align on the left starting from the bottom left going right then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._leftAlignLRDU = function(lines) {

    var self = this;
    var y = this._height;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y -= lines[i - 1].height;
        }


        line.y = y;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].x = 0;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].widthMinusKerning;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].widthMinusKerning;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;

            }

        }

        y -= this._spaceBetweenLines;

    }

};


/**
 * Align on the right starting from the bottom left going right then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._rightAlignLRDU = function(lines) {

    var self = this;
    var y = this._height;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y -= lines[i - 1].height;
        }



        line.y = y;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].x = line.words[j + 1].chars[0].x - line.words[j].chars[k].widthMinusKerning;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k + 1].x - line.words[j].chars[k].widthMinusKerning;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
            }

        }

        y -= this._spaceBetweenLines;

    }

};


/**
 * Align on the center starting from the bottom left going right then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._centerAlignLRDU = function(lines) {

    var self = this;
    var y = this._height;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y -= lines[i - 1].height;
        }


        line.y = y;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].x = self._width / 2 - lines[i]._width / 2;
                    lines[i].words[j].chars[k].y = y - line.height + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].x = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].x + lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].widthMinusKerning;
                    lines[i].words[j].chars[k].y = y - line.height + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }


                lines[i].words[j].chars[k].x = lines[i].words[j].chars[k - 1].x + lines[i].words[j].chars[k - 1].widthMinusKerning;
                lines[i].words[j].chars[k].y = y - line.height + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
            }

        }

        y -= this._spaceBetweenLines;

    }

};

/**
 * Align justified starting from the bottom left going right then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._justifyAlignLRDU = function(lines) {

    var self = this;
    var y = this._height;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //subtrai ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                y -= lines[i - 1].height;
            }


            line.y = y;
            var difEspacoLinha = (self._width - line.width) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {

                //se for mais de uma palavra e for a ultima palavra alinha na direita
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k == line.words[j].chars.length - 1) {
                                line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                                continue;
                            }

                            line.words[j].chars[k].x = line.words[j].chars[k + 1].x - line.words[j].chars[k].widthMinusKerning;
                            line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);


                        }
                        break;
                    }
                }

                for (var k = 0; k < line.words[j].chars.length; k++) {

                    if (k === 0 && j === 0) {
                        line.words[j].chars[k].x = 0;
                        line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].widthMinusKerning + difEspacoLinha;
                        line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }


                    line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].widthMinusKerning;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;


                }

            }

            y -= this._spaceBetweenLines;

        }


        y -= lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].x = 0;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].widthMinusKerning;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }

            line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].widthMinusKerning;
            line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
            continue;

        }

    }

};
//FIM justify




//
//
//iniciando RLDU  (direita esquerda começando da base)
//
//


/**
 * Align on the left starting from the bottom right going left then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._leftAlignRLDU = function(lines) {

    var self = this;
    var y = this._height;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y -= lines[i - 1].height;
        }


        line.y = y;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].x = 0;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].x = line.words[j + 1].chars[0].x + line.words[j + 1].chars[0].widthMinusKerning;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k + 1].x + line.words[j].chars[k + 1].widthMinusKerning;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
            }

        }

        y -= this._spaceBetweenLines;

    }

};

/**
 * Align on the center starting from the bottom right going left then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._centerAlignRLDU = function(lines) {

    var self = this;
    var y = this._height;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y -= lines[i - 1].height;
        }


        line.y = y;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].x = self._width / 2 + lines[i]._width / 2 - lines[i].words[j].chars[k].width;
                    lines[i].words[j].chars[k].y = y - line.height + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].x = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].x - lines[i].words[j].chars[k].widthMinusKerning;
                    lines[i].words[j].chars[k].y = y - line.height + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
                    continue;
                }


                lines[i].words[j].chars[k].x = lines[i].words[j].chars[k - 1].x - lines[i].words[j].chars[k].widthMinusKerning;
                lines[i].words[j].chars[k].y = y - line.height + ((line.height - lines[i].words[j].chars[k].height) * 0.80);
            }

        }

        y -= this._spaceBetweenLines;

    }

};


/**
 * Align on the right starting from the bottom right going left then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._rightAlignRLDU = function(lines) {

    var self = this;
    var y = this._height;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            y -= lines[i - 1].height;
        }


        line.y = y;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x - line.words[j].chars[k].widthMinusKerning;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;
                }

                line.words[j].chars[k].x = line.words[j].chars[k - 1].x - line.words[j].chars[k].widthMinusKerning;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;

            }

        }

        y -= this._spaceBetweenLines;

    }

};



/**
 * Align justified starting from the bottom right going left then up
 *
 * @param lines Array of lines to be repositioned {Array}
 */
HorizontalModule.prototype._justifyAlignRLDU = function(lines) {

    var self = this;
    var y = this._height;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //subtrai ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                y -= lines[i - 1].height;
            }


            line.y = y;
            var difEspacoLinha = (self._width - line.width) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {


                //se for mais de uma palavra e for a ultima palavra alinha na esquerda
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k === line.words[j].chars.length - 1) {
                                line.words[j].chars[k].x = 0;
                                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                                continue;
                            }

                            line.words[j].chars[k].x = line.words[j].chars[k + 1].x + line.words[j].chars[k + 1].widthMinusKerning;
                            line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);


                        }
                        break;
                    }
                }


                //se n for a ultima palavra alinha da direita


                // if (k === 0 && j === 0) {
                //     line.words[j].chars[k].x = 0;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                // if (k === 0) {
                //     line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].width + difEspacoLinha;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                //
                //
                // line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].width;
                // line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                // continue;

                for (var k = 0; k < line.words[j].chars.length; k++) {
                    if (j === 0 && k === 0) {
                        line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                        line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x - line.words[j].chars[k].widthMinusKerning - difEspacoLinha;
                        line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                        continue;
                    }

                    line.words[j].chars[k].x = line.words[j].chars[k - 1].x - line.words[j].chars[k].widthMinusKerning;
                    line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                    continue;

                }




            }

            y -= this._spaceBetweenLines;

        }


        y -= lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].x = self._width - line.words[j].chars[k].width;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x - line.words[j].chars[k].widthMinusKerning;
                line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
                continue;
            }

            line.words[j].chars[k].x = line.words[j].chars[k - 1].x - line.words[j].chars[k].widthMinusKerning;
            line.words[j].chars[k].y = y - line.height + ((line.height - line.words[j].chars[k].height) * 0.80);
            continue;

        }

    }

};

//FIM do justify


////// fim do alinhamento

});

define('core/text/textfield/align/verticalModule/VerticalModule',['require','exports','module','../../../phrase/Phrase','../../../word/Word'],function (require, exports, module) {var Phrase = require("../../../phrase/Phrase"),
    Word = require("../../../word/Word");




/**
 * Used to order alocate and align chars in a vertical manner
 *
 * @class
 * @memberof type.text
 */

function VerticalModule() {

    /**
     * width to be used to calculate
     *
     * @member {number}
     * @default 0
     */
    this._width = 0;


    /**
     * height to be used to calculate
     *
     * @member {number}
     * @default 0
     */
    this._height = 0;

    /**
     * define starting position left or right
     *
     * @member {boolean}
     * @default true
     */
    this._textLeftToRight = true;

    /**
     * define starting position top or bottom
     *
     * @member {boolean}
     * @default true
     */
    this._textTopToBottom = true;

    /**
     * define space between lines
     *
     * @member {number}
     * @default 0
     */
    this._spaceBetweenLines = 0;

    /**
     * define space between words
     *
     * @member {number}
     * @default -1
     */
    this._spaceBetweenWords = -1;

    /**
     * define the form of alignment
     *
     * @member {String}
     * @default top
     */
    this._textAlign = "top";

}

//HorizontalModule.prototype = Object.create(PIXI.Container.prototype);
VerticalModule.prototype.constructor = VerticalModule;
module.exports = VerticalModule;

/**
 *   Organiza os caracteres em palavras e frases para fazer o alinhamento depois
 *
 *  @param letters [array of objects to be align] {Array}
 *  @param width [width used to calculate de align borders] {number}
 *  @param height [height used to calculate de align borders] {number}
 */
VerticalModule.prototype._relocate = function(letters, width, height) {

    this._width = width;
    this._height = height;

    this._lines = [];

    this._lines.push(new Phrase());
    this._lines[0].words.push(new Word());
    this._lines[0].words[0].indexWord = 0;
    var isSpace = false;
    var breakMultiControl = false;

    for (var i = 0; i < letters.length; i++) {

        var line = this._lines[this._lines.length - 1];
        var word = line.words[line.words.length - 1];

        //c é o caracter a ser tratado
        var c = letters[i];

        //seta as propriedades do char
        if (i !== 0) {
            c.setSideCharL(letters[i - 1].text);
        }


        //se for um /n  organizar a linha atual, criar nova linha e setar a primeira palavra
        if (c.text == "\n") {

            this._lines.push(new Phrase());
            line = this._lines[this._lines.length - 1];
            var w = new Word();
            w.indexWord = 0;
            line.words.push(w);

            if (breakMultiControl) {
                line.width = c.width;
                w.width = c.width;
            }else{
                c.width = 0;

            }
            c.height = 0;


            w.chars.push(c);

            breakMultiControl = true;
            isSpace = false;

            continue;
        }
        breakMultiControl = false;

        //se for espaço adicionar o caracter criar uma nova palavra e adicionar a frase
        if (c.text == " ") {
            isSpace = true;
            if (this._spaceBetweenWords != -1) {
                if (this._spaceBetweenWords != -1) {
                    c.height = this._spaceBetweenWords;
                }
            }
        }

        if (isSpace && c.text != " ") {
            isSpace = false;



            var wordAtual = this._arrangeLinesVertical(line, c.height);
            line = this._lines[this._lines.length - 1];

            var w = null;
            if (this._lines[this._lines.length - 1].words[this._lines[this._lines.length - 1].words.length - 1].chars.length !== 0) {
                //adiciona nova palavra, com a proxima index
                var index = word.indexWord;
                w = new Word();
                w.indexWord = index + 1;
                line.words.push(w);

            } else {
                w = wordAtual;
            }

            w.chars.push(c);

            w.width = c.width;
            w.height = c.height;
            line.height += c.height;

            if (c.width > line.width) {
                line.width = c.width;
            }

            continue;
        }

        var word = this._arrangeLinesVertical(line, c.height);

        line = this._lines[this._lines.length - 1];

        word.height += c.height;
        line.height += c.height;


        if (c.width > word.width) {

            word.width = c.width;
        }

        if (c.width > line.width) {
            line.width = c.width;
        }

        //coloca o caracter no final da palavra
        word.chars.push(c);



    }
    //alinha as linhas e coloca o espaçamento em Y
    this._alignLines(this._lines);



    //TO DO emitir a ultima posição para atualizar o cursor

    // if (self.children.length > 0) {
    //     self.emit("textUpdated", {
    //         x: self.getChildAt(self.children.length - 1).x + self.getChildAt(self.children.length - 1).width,
    //         y: self.getChildAt(self.children.length - 1).y
    //     });
    // } else {
    //     if (self._textAlign == 'left') {
    //         self.emit("textUpdated", {
    //             x: this.x,
    //             y: this.y
    //         });
    //     }
    //     if (self._textAlign == 'center' || self._textAlign == 'justify') {
    //         self.emit("textUpdated", {
    //             x: this.x + this._width / 2,
    //             y: this.y
    //         });
    //     }
    //     if (self._textAlign == 'right') {
    //         self.emit("textUpdated", {
    //             x: this.x + this._width,
    //             y: this.y
    //         });
    //     }
    // }

};



/**
 *  Break the line if the phrase is bigger then the field
 *
 *  @param line [line to be analised] {Array}
 */
VerticalModule.prototype._arrangeLinesVertical = function(line, charHeight) {

    var self = this;

    if (line.words.length == 1) {
        if (line.height + charHeight > self._height) {
            this._lines.push(new Phrase());


            novaWord = new Word();
            novaWord.indexWord = 0;

            this._lines[this._lines.length -1].words.push(novaWord);


        }
    } else {
        if (line.height + charHeight > self._height) {


            this._lines.push(new Phrase());

            var heightTotal = 0;
            for (var i = 0; i < line.words.length; i++) {

                heightTotal += line.words[i].height;


                if (self._height < heightTotal + charHeight) {
                    // arruma a largura e altura da linha antiga
                    var wordAntiga = line.words[line.words.length - 1];

                    line.height -= wordAntiga.height;
                    line.words[line.words.length - 2].chars[line.words[line.words.length - 2].chars.length - 1].height = 0;


                    var spliceWord = line.words.splice(line.words.length - 1, 1);
                    var word = spliceWord[0];
                    word.indexWord = 0;

                    this._lines[this._lines.length - 1].words.push(word);

                    //arruma a largura e altura da linha
                    var linhaNova = this._lines[this._lines.length - 1];
                    linhaNova.width = wordAntiga.width;
                    linhaNova.height = wordAntiga.height;

                    break;
                }
            }
        }
    }
    return this._lines[this._lines.length - 1].words[this._lines[this._lines.length - 1].words.length - 1];

};



/**
 *  Responsible for the decision of which direction to write(between all 4 vertical types) and which align to use (4 align types)
 *
 *  @param lines [lines to repositioned] {Array}
 */
VerticalModule.prototype._alignLines = function(lines) {

    //vertical Priority

    //chooses vertical direction
    if (this._textTopToBottom) {
        //start top

        //chooses horizontal direction
        if (this._textLeftToRight) {
            //left to right start top

            if (this._textAlign == "center") {

                this._centerAlignUDLR(lines);
                return;
            }

            if (this._textAlign == "bottom") {

                this._bottomAlignUDLR(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignUDLR(lines);
                return;
            }

            this._topAlignUDLR(lines);
            return;
        } else {
            //right to left start top
            if (this._textAlign == "center") {

                this._centerAlignUDRL(lines);
                return;
            }

            if (this._textAlign == "bottom") {

                this._bottomAlignUDRL(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignUDRL(lines);
                return;
            }

            this._topAlignUDRL(lines);
            return;
        }
    } else {
        //start Bottom

        //chooses horizontal direction
        if (this._textLeftToRight) {
            //left to right start Bottom

            if (this._textAlign == "center") {

                this._centerAlignDULR(lines);
                return;
            }

            if (this._textAlign == "bottom") {

                this._bottomAlignDULR(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignDULR(lines);
                return;
            }

            this._topAlignDULR(lines);
            return;
        } else {
            //right to left start Bottom
            if (this._textAlign == "center") {

                this._centerAlignDURL(lines);
                return;
            }

            if (this._textAlign == "bottom") {

                this._bottomAlignDURL(lines);
                return;
            }

            if (this._textAlign == "justify") {

                this._justifyAlignDURL(lines);
                return;
            }

            this._topAlignDURL(lines);
            return;
        }

    }

};



//////// Iniciando alinhamentos



/// primeiro UPLR

/**
 * Align on the top starting from the top left going down then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._topAlignUDLR = function(lines) {

    var self = this;
    var x = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x += lines[i - 1].width;
        }


        line.x = x;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].y = 0;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k - 1].y + line.words[j].chars[k - 1].height;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;

            }

        }

        x += this._spaceBetweenLines;

    }

};


/**
 * Align on the bottom starting from the top left going down then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._bottomAlignUDLR = function(lines) {
    var self = this;
    var x = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x += lines[i - 1].width;
        }


        line.x = x;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].y = line.words[j + 1].chars[0].y - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k + 1].y - line.words[j].chars[k].height;

                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
            }

        }


        x += this._spaceBetweenLines;

    }

};


/**
 * Align on the center starting from the top left going down then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._centerAlignUDLR = function(lines) {

    var self = this;
    var x = 0;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x += lines[i - 1].width;
        }


        line.x = x;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].y = self._height / 2 - lines[i]._height / 2;
                    lines[i].words[j].chars[k].x = x + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].y = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].y + lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].height;
                    lines[i].words[j].chars[k].x = x + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }


                lines[i].words[j].chars[k].y = lines[i].words[j].chars[k - 1].y + lines[i].words[j].chars[k - 1].height;
                lines[i].words[j].chars[k].x = x + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
            }

        }

        x += this._spaceBetweenLines;

    }

};


/**
 * Align justified starting from the top left going down then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._justifyAlignUDLR = function(lines) {

    var self = this;
    var x = 0;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //soma ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                x += lines[i - 1].width;
            }


            line.x = x;
            var difEspacoLinha = (self._height - line.height) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {

                //se for mais de uma palavra e for a ultima palavra alinha na direita
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k == line.words[j].chars.length - 1) {
                                line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                                continue;
                            }

                            line.words[j].chars[k].y = line.words[j].chars[k + 1].y - line.words[j].chars[k].height;
                            line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);


                        }
                        break;
                    }
                }

                for (var k = 0; k < line.words[j].chars.length; k++) {

                    if (k === 0 && j === 0) {
                        line.words[j].chars[k].y = 0;
                        line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].height + difEspacoLinha;
                        line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }


                    line.words[j].chars[k].y = line.words[j].chars[k - 1].y + line.words[j].chars[k - 1].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;


                }

            }

            x += this._spaceBetweenLines;

        }


        x += lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].y = 0;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].height;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }

            line.words[j].chars[k].y = line.words[j].chars[k - 1].y + line.words[j].chars[k - 1].height;
            line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
            continue;

        }

    }

};





//
//
//iniciando DULR  (baixo topo começando da esquerda)
//
//


/**
 * Align on the top starting from the down left going up then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._topAlignDULR = function(lines) {


    var self = this;
    var x = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x += lines[i - 1].width;
        }


        line.x = x;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].y = 0;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].y = line.words[j + 1].chars[0].y + line.words[j + 1].chars[0].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k + 1].y + line.words[j].chars[k + 1].height;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
            }

        }

        x += this._spaceBetweenLines;

    }

};

/**
 * Align on the center starting from the down left going up then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._centerAlignDULR = function(lines) {

    var self = this;
    var x = 0;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x += lines[i - 1].width;
        }


        line.x = x;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].y = self._height / 2 + lines[i]._height / 2 - lines[i].words[j].chars[k].height;
                    lines[i].words[j].chars[k].x = x + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].y = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].y - lines[i].words[j].chars[k].height;
                    lines[i].words[j].chars[k].x = x + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }


                lines[i].words[j].chars[k].y = lines[i].words[j].chars[k - 1].y - lines[i].words[j].chars[k].height;
                lines[i].words[j].chars[k].x = x + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
            }

        }

        x += this._spaceBetweenLines;

    }

};


/**
 * Align on the bottom starting from the down left going up then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._bottomAlignDULR = function(lines) {

    var self = this;
    var x = 0;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //soma ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x += lines[i - 1].width;
        }


        line.x = x;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k - 1].y - line.words[j].chars[k].height;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;

            }

        }

        x += this._spaceBetweenLines;

    }

};



/**
 * Align justified starting from the down left going up then right
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._justifyAlignDULR = function(lines) {

    var self = this;
    var x = 0;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //soma ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                x += lines[i - 1].width;
            }


            line.x = x;
            var difEspacoLinha = (self._height - line.height) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {


                //se for mais de uma palavra e for a ultima palavra alinha na esquerda
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k === line.words[j].chars.length - 1) {
                                line.words[j].chars[k].y = 0;
                                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                                continue;
                            }

                            line.words[j].chars[k].y = line.words[j].chars[k + 1].y + line.words[j].chars[k + 1].height;
                            line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);


                        }
                        break;
                    }
                }


                //se n for a ultima palavra alinha da direita


                // if (k === 0 && j === 0) {
                //     line.words[j].chars[k].x = 0;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                // if (k === 0) {
                //     line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].width + difEspacoLinha;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                //
                //
                // line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].width;
                // line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                // continue;

                for (var k = 0; k < line.words[j].chars.length; k++) {
                    if (j === 0 && k === 0) {
                        line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                        line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y - line.words[j].chars[k].height - difEspacoLinha;
                        line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }

                    line.words[j].chars[k].y = line.words[j].chars[k - 1].y - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;

                }




            }

            x += this._spaceBetweenLines;

        }


        x += lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y - line.words[j].chars[k].height;
                line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }

            line.words[j].chars[k].y = line.words[j].chars[k - 1].y - line.words[j].chars[k].height;
            line.words[j].chars[k].x = x + ((line.width - line.words[j].chars[k].width) * 0.50);
            continue;

        }

    }

};

//FIM do justify




//
//
//iniciando UDRL  (cima baixo começando da direita)
//
//


/**
 * Align on the top starting from the top right going down then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._topAlignUDRL = function(lines) {

    var self = this;
    var x = this._width;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x -= lines[i - 1].width;
        }


        line.x = x;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].y = 0;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k - 1].y + line.words[j].chars[k - 1].height;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;

            }

        }

        x -= this._spaceBetweenLines;

    }

};


/**
 * Align on the bottom starting from the top right going down then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._bottomAlignUDRL = function(lines) {

    var self = this;
    var x = this._width;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x -= lines[i - 1].width;
        }



        line.x = x;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].y = line.words[j + 1].chars[0].y - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k + 1].y - line.words[j].chars[k].height;

                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
            }

        }

        x -= this._spaceBetweenLines;

    }

};


/**
 * Align on the center starting from the top right going down then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._centerAlignUDRL = function(lines) {

    var self = this;
    var x = this._width;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x -= lines[i - 1].width;
        }


        line.x = x;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].y = self._height / 2 - lines[i]._height / 2;
                    lines[i].words[j].chars[k].x = x - line.width + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].y = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].y + lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].height;
                    lines[i].words[j].chars[k].x = x - line.width + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }


                lines[i].words[j].chars[k].y = lines[i].words[j].chars[k - 1].y + lines[i].words[j].chars[k - 1].height;
                lines[i].words[j].chars[k].x = x - line.width + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
            }

        }

        x -= this._spaceBetweenLines;

    }

};

/**
 * Align justified starting from the top right going down then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._justifyAlignUDRL = function(lines) {

    var self = this;
    var x = this._width;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //subtrai ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                x -= lines[i - 1].width;
            }


            line.x = x;
            var difEspacoLinha = (self._height - line.height) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {

                //se for mais de uma palavra e for a ultima palavra alinha na direita
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k == line.words[j].chars.length - 1) {
                                line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                                continue;
                            }

                            line.words[j].chars[k].y = line.words[j].chars[k + 1].y - line.words[j].chars[k].height;
                            line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);


                        }
                        break;
                    }
                }

                for (var k = 0; k < line.words[j].chars.length; k++) {

                    if (k === 0 && j === 0) {
                        line.words[j].chars[k].y = 0;
                        line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].height + difEspacoLinha;
                        line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }


                    line.words[j].chars[k].y = line.words[j].chars[k - 1].y + line.words[j].chars[k - 1].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;


                }

            }

            x -= this._spaceBetweenLines;

        }


        x -= lines[lines.length - 2].height;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].y = 0;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].height;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }

            line.words[j].chars[k].y = line.words[j].chars[k - 1].y + line.words[j].chars[k - 1].height;
            line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
            continue;

        }

    }

};
//FIM justify




//
//
//iniciando DURL  (baixo  cima começando da direita)
//
//


/**
 * Align on the top starting from the bottom right going up then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._topAlignDURL = function(lines) {

    var self = this;
    var x = this._width;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x -= lines[i - 1].width;
        }


        line.x = x;



        for (var j = line.words.length - 1; j >= 0; j--) {

            for (var k = line.words[j].chars.length - 1; k >= 0; k--) {

                if (k == line.words[j].chars.length - 1 && j == line.words.length - 1) {
                    line.words[j].chars[k].y = 0;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k == line.words[j].chars.length - 1) {
                    line.words[j].chars[k].y = line.words[j + 1].chars[0].y + line.words[j + 1].chars[0].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k + 1].y + line.words[j].chars[k + 1].height;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
            }

        }

        x -= this._spaceBetweenLines;

    }

};

/**
 * Align on the center starting from the bottom right going up then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._centerAlignDURL = function(lines) {

    var self = this;
    var x = this._width;

    for (var i = 0; i < lines.length; i++) {
        var line = lines[i];

        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x -= lines[i - 1].width;
        }


        line.x = x;



        for (var j = 0; j < lines[i].words.length; j++) {
            for (var k = 0; k < lines[i].words[j].chars.length; k++) {

                if (k === 0 && j === 0) {
                    lines[i].words[j].chars[k].y = self._height / 2 + lines[i]._height / 2 - lines[i].words[j].chars[k].height;
                    lines[i].words[j].chars[k].x = x - line.width + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    lines[i].words[j].chars[k].y = lines[i].words[j - 1].chars[lines[i].words[j - 1].chars.length - 1].y - lines[i].words[j].chars[k].height;
                    lines[i].words[j].chars[k].x = x - line.width + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
                    continue;
                }


                lines[i].words[j].chars[k].y = lines[i].words[j].chars[k - 1].y - lines[i].words[j].chars[k].height;
                lines[i].words[j].chars[k].x = x - line.width + ((line.width - lines[i].words[j].chars[k].width) * 0.50);
            }

        }

        x -= this._spaceBetweenLines;

    }

};


/**
 * Align on the bottom starting from the bottom right going up then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._bottomAlignDURL = function(lines) {

    var self = this;
    var x = this._width;

    for (var i = 0; i < lines.length; i++) {

        var line = lines[i];


        //subtrai ao y o tamanho das linhas para alinha-las
        if (i !== 0) {
            x -= lines[i - 1].width;
        }


        line.x = x;


        for (var j = 0; j < line.words.length; j++) {
            for (var k = 0; k < line.words[j].chars.length; k++) {
                if (j === 0 && k === 0) {
                    line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }
                if (k === 0) {
                    line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;
                }

                line.words[j].chars[k].y = line.words[j].chars[k - 1].y - line.words[j].chars[k].height;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;

            }

        }

        x -= this._spaceBetweenLines;

    }

};



/**
 * Align justified starting from the bottom right going up then left
 *
 * @param lines Array of lines to be repositioned {Array}
 */
VerticalModule.prototype._justifyAlignDURL = function(lines) {

    var self = this;
    var x = this._width;

    if (lines.length > 1) {

        for (var i = 0; i < lines.length - 1; i++) {

            var line = lines[i];

            //subtrai ao y o tamanho das linhas para alinha-las
            if (i !== 0) {
                x -= lines[i - 1].width;
            }


            line.x = x;
            var difEspacoLinha = (self._height - line.height) / (line.words.length - 1);

            for (var j = 0; j < line.words.length; j++) {


                //se for mais de uma palavra e for a ultima palavra alinha na esquerda
                if (line.words.length != 1) {
                    if (j == line.words.length - 1) {
                        for (var k = line.words[j].chars.length - 1; k >= 0; k--) {
                            if (k === line.words[j].chars.length - 1) {
                                line.words[j].chars[k].y = 0;
                                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                                continue;
                            }

                            line.words[j].chars[k].y = line.words[j].chars[k + 1].y + line.words[j].chars[k + 1].height;
                            line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);


                        }
                        break;
                    }
                }


                //se n for a ultima palavra alinha da direita


                // if (k === 0 && j === 0) {
                //     line.words[j].chars[k].x = 0;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                // if (k === 0) {
                //     line.words[j].chars[k].x = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].x + line.words[j - 1].chars[line.words[j - 1].chars.length - 1].width + difEspacoLinha;
                //     line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                //     continue;
                // }
                //
                //
                // line.words[j].chars[k].x = line.words[j].chars[k - 1].x + line.words[j].chars[k - 1].width;
                // line.words[j].chars[k].y = y + ((line.height - line.words[j].chars[k].height) * 0.80);
                // continue;

                for (var k = 0; k < line.words[j].chars.length; k++) {
                    if (j === 0 && k === 0) {
                        line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                        line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }
                    if (k === 0) {
                        line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y - line.words[j].chars[k].height - difEspacoLinha;
                        line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                        continue;
                    }

                    line.words[j].chars[k].y = line.words[j].chars[k - 1].y - line.words[j].chars[k].height;
                    line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                    continue;

                }




            }

            x -= this._spaceBetweenLines;

        }


        x -= lines[lines.length - 2].width;
    }
    var line = lines[lines.length - 1];

    for (var j = 0; j < line.words.length; j++) {
        for (var k = 0; k < line.words[j].chars.length; k++) {
            if (j === 0 && k === 0) {
                line.words[j].chars[k].y = self._height - line.words[j].chars[k].height;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }
            if (k === 0) {
                line.words[j].chars[k].y = line.words[j - 1].chars[line.words[j - 1].chars.length - 1].y - line.words[j].chars[k].height;
                line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
                continue;
            }

            line.words[j].chars[k].y = line.words[j].chars[k - 1].y - line.words[j].chars[k].height;
            line.words[j].chars[k].x = x - line.width + ((line.width - line.words[j].chars[k].width) * 0.50);
            continue;

        }

    }

};

//FIM do justify


////// fim do alinhamento

});

define('core/text/textfield/align/customModule/CustomModule',['require','exports','module'],function (require, exports, module) {/**
 * Used to order alocate and align chars in a Any manner
 *
 * @class
 * @memberof type.text
 */

function CustomModule() {

    /*
     * Type Align Object, object that have the properties of aligment
     *
     * @member {Object | TypeALign}
     * @memberof type.text.CustomModule#
     */
    this._typeAlign = null;

    /*
     * define the use of rotation or not on the custom align
     *
     * @member {Boolean}
     * @default false
     */
    this._useRotation = false;


}

//HorizontalModule.prototype = Object.create(PIXI.Container.prototype);
CustomModule.prototype.constructor = CustomModule;
module.exports = CustomModule;


Object.defineProperties(CustomModule.prototype, {

    /**
     * Type Align Object, object that have the properties of aligment
     *
     * @member {Object | TypeALign}
     * @memberof type.text.CustomModule#
     */
    typeAlign: {
        get: function() {
            return this._typeAlign;
        },
        set: function(value) {
            this._typeAlign = value;
        }
    }


});



/**
 *  position the Chars on the screen
 *
 *  @param chars [chars to be positioned] {Array}
 */
CustomModule.prototype._align = function(chars) {

    chars[0].x = this._typeAlign.startingX;
    chars[0].y = this._typeAlign.startingY;

    var stepCount = 0;

    for (var i = 1; i < chars.length; i++) {

        if (stepCount == this._typeAlign.steps.length) {
            stepCount = 0;
        }

        chars[i].x = chars[i - 1].x + this._typeAlign.steps[stepCount].x;
        chars[i].y = chars[i - 1].y + this._typeAlign.steps[stepCount].y;

        if(this._typeAlign.steps[stepCount].rotation !== undefined)
        chars[i].rotation = Math.PI / 180 * this._typeAlign.steps[stepCount].rotation;


        stepCount++;
    }

};

});

define('core/text/textfield/TextField',['require','exports','module','../../../../lib/Jsdiff','../char/Char','./align/horizontalModule/HorizontalModule','./align/verticalModule/VerticalModule','./align/customModule/CustomModule'],function (require, exports, module) {var JsDiff = require("../../../../lib/Jsdiff"),
    Char = require("../char/Char"),
    HorizontalModule = require("./align/horizontalModule/HorizontalModule"),
    VerticalModule = require("./align/verticalModule/VerticalModule");
    CustomModule = require("./align/customModule/CustomModule");

var PIXI = require('PIXI');
/**
 * The TextField is a class that shows a String of text and has multiple ways of showing it
 *
 * @class
 * @memberof type.text
 * @extends PIXI.Container
 * @param [width=2048] {number} the width of the textBox, it's used for all aligns
 * @param [height=1152] {number} the height of the textBox, it's used for all aligns
 *
 * @example
 * txField.align = "justify";
 *
 * @example
 * txField.spaceBetweenLines = 20;
 * txField.textLeftToRight = true;
 * txField.textTopToBottom = true;
 * txField.alignHorizontalPriority = false;
 *
 *
 *
 */
function TextField(width, height) {

    var self = this;

    PIXI.Container.call(this);

    horizontalModule = new HorizontalModule();
    verticalModule = new VerticalModule();
    customModule = new CustomModule();

    this._width = width || 2048;
    this._height = height || 1152;

    this.hitArea = new PIXI.Rectangle(0,0,this._width, this._height);

    this._box = null;
    this._text = "";
    this._map = {};

    this._customAlign = false;

    this._spaceBetweenLines = 0;
    this._spaceBetweenWords = -1;
    this._textAlign = "left";
    this._textLeftToRight = true;
    this._textTopToBottom = true;
    this._alignHorizontalPriority = true;
    this._defaultStyle = {
        fontFamily: 'Arial',
        fontSize: 20,
        fill: "#000000"
    };

    this._mapStyle = [];

    this._customStyle = {};
    this._textSup = document.createElement('div');
}

TextField.prototype = Object.create(PIXI.Container.prototype);
TextField.prototype.constructor = TextField;
module.exports = TextField;



Object.defineProperties(TextField.prototype, {

    /**
     * define if the text is writen starting from the left or right
     *
     * @member {boolean}
     * @default true
     * @memberof type.text.TextField#
     */
    textLeftToRight: {
        get: function() {
            return this._textLeftToRight;
        },
        set: function(value) {
            this._textLeftToRight = value;

            this._relocate();
        }
    },

    /**
     * define if the text is writen starting from the top or bottom
     *
     * @member {boolean}
     * @default true
     * @memberof type.text.TextField#
     */
    textTopToBottom: {
        get: function() {
            return this._textTopToBottom;
        },
        set: function(value) {
            this._textTopToBottom = value;
            horizontalModule._textTopToBottom = value;
            verticalModule._textTopToBottom = value;

            this._relocate();
        }
    },

    /**
     * define if the priority of writing is vertical or horizontal
     *
     * @member {boolean}
     * @default true
     * @memberof type.text.TextField#
     */
    alignHorizontalPriority: {
        get: function() {
            return this._alignHorizontalPriority;
        },
        set: function(value) {
            this._alignHorizontalPriority = value;

            this._relocate();
        }
    },



    /**
     * define if the align mode will be a normal one or a customized one
     *
     * @member {Boolean}
     * @default false
     * @memberof type.text.TextField#
     */
    customAlign: {
        get: function() {
            return this._customAlign;
        },
        set: function(value) {
            this._customAlign = value;

            this._relocate();
        }
    },



    /**
     * set Type Align Object to be used in custom align
     *
     * @member {Object | TypeAlign}
     * @default null
     * @memberof type.text.TextField#
     */
    typeAlign: {
        get: function() {
            return customModule.typeAlign;
        },
        set: function(value) {
            customModule.typeAlign = value;
        }
    },

    /**
     * define the align method for textField (left, right, top, bottom, center, justify)
     *
     * @member {String}
     * @default left
     * @memberof type.text.TextField#
     */
    align: {
        get: function() {
            return this._textAlign;
        },
        set: function(value) {
            this._textAlign = value;
            horizontalModule._textAlign = value;
            verticalModule._textAlign = value;

            this._relocate();
        }
    },

    /**
     * define the space between lines vertical or horizontal, default 0
     *
     * @member {number}
     * @default 0
     * @memberof type.text.TextField#
     */
    spaceBetweenLines: {
        get: function() {
            return this._spaceBetweenLines;
        },
        set: function(value) {
            this._spaceBetweenLines = value;
            horizontalModule._spaceBetweenLines = value;
            verticalModule._spaceBetweenLines = value;

            this._relocate();
        }
    },

    /**
     * alter the space between words vertical or horizontal, default -1 which uses space font width
     *
     * @member {number}
     * @default -1
     * @memberof type.text.TextField#
     */
    spaceBetweenWords: {
        get: function() {
            return this._spaceBetweenWords;
        },
        set: function(value) {
            this._spaceBetweenWords = value;
            horizontalModule._spaceBetweenWords = value;
            verticalModule._spaceBetweenWords = value;

            this._relocate();
        }
    },


    tint: {
        get: function() {
            return this._tint;
        },
        set: function(value) {
            this._tint = value;

            for (var i = 0; i < this.children.length; i++) {
                this.children[i]._textObject.tint = value;
            }
        }

    }
});


/**
 * Used to alter the text of the TextField, automatically align afterwards
 *
 * @param text to be show by the field {String}
 */
TextField.prototype.setText = function(text, style) {
    this._customStyle = style || {};

    this._textSup.innerHTML = text;

    this._mapStyle.splice(0, this._mapStyle.length);

    if (this._textSup.children.length > 0) {
        for (var i = 0; i < this._textSup.childNodes.length; i++) {
            var _node = this._textSup.childNodes[i];
            var _name = _node.nodeName;
            _name = _name.toLowerCase();
            var len = 0;
            if (_name == "#text") {
                _name = "text";
                len = _node.length;
            } else
                len = _node.innerText.length;

            for (var j = 0; j < len; j++)
                this._mapStyle.push(_name);
        }
    }


    var oldText = this._text;
    this._text = this._textSup.innerText;

    var diff = JsDiff.diffChars(oldText, this._text);
    this._change(JsDiff.convertChangesToDMP(diff));
};

/**
 * Used to alter the style of the TextField, automatically align afterwards
 *
 * @param style Object containing all the atributes to be changed {Object}
 */
TextField.prototype.setStyle = function(style) {

    for (var i = 0; i < this.children.length; i++) {
        this.children[i].setStyle(style);
    }

    this._customStyle = style || {};

    this._relocate();

};


/**
 * Used to alter the style of a limited String, automatically align afterwards
 *
 * @param word String to be altered with a custom style{String}
 * @param style Object containing all the atributes to be changed {Object}
 */
TextField.prototype.setWordStyle = function(word, style) {

    var index = this._text.indexOf(word);

    if (index === -1)
        return;

    for (var i = index; i < (index + word.length); i++) {
        this.children[i].setStyle(style);
    }

    this._relocate();

};


/**
 * Used to detect all changes on the new string to show, so we don't need to delete and recreate the same char
 *
 * @private
 * @param diff {Array}
 */
TextField.prototype._change = function(diff) {
    var count = 0;


    for (var i = 0; i < diff.length; i++) {
        if (diff[i][0] === 0) {
            for (var z = 0; z < diff[i][1].length; z++) {

                if (this._mapStyle[count] == "text") {
                    count++;
                    continue;
                }

                this.children[count].setStyle(this._customStyle[this._mapStyle[count]]);
                count++;
            }
        }

        if (diff[i][0] == 1) {
            for (var j = 0; j < diff[i][1].length; j++) {
                var st = this._mapStyle[count] === undefined ? this._defaultStyle : this._customStyle[this._mapStyle[count]];
                if (st === undefined) st = this._defaultStyle;
                var char = new Char(diff[i][1].charAt(j), st, false);

                this.addChildAt(char, count);
                count++;
            }
        }

        if (diff[i][0] == -1) {
            for (var k = 0; k < diff[i][1].length; k++) {
                var c = this.children[count];
                this.removeChild(c);
                c.destroy(true);
                c = null;
            }
        }
    }

    this._relocate();
};

/**
 * Used to to alocate the chars into phrases and words so they can be arranged in position
 *
 * @private
 */
TextField.prototype._relocate = function() {

    var self = this;

    if (this._customAlign) {
        customModule._align(this.children);
        return;
    }

    if (self._alignHorizontalPriority) {
        horizontalModule._textAlign = this._textAlign;
        horizontalModule._relocate(this.children, self._width, self._height);
        if (this.children.length !== 0) {
            this.emit("textUpdated", this.children[this.children.length - 1]);
        }else{
            this.emit("textUpdated", null);
        }

    } else {
        verticalModule._textAlign = this._textAlign;
        verticalModule._relocate(this.children, self._width, self._height);
    }

};



// TextField.prototype._clearWords = function() {
//     for (var i = 0; i < this._words.length; i++) {
//         var r = this._words[i];
//         r.splice(0, r.length);
//         r = null;
//     }
//
//     this._words.splice(0, this._words.length);
//     this._words.push([]);
// };

TextField.prototype.getSelectionCoordinates = function(charinicial, charfinal) {

    var self = this;

    var coordinates = [];

    var iniCoord = {};
    var endCoord = {};

    if (charinicial < charfinal) {
        letraIni = this.children[charinicial];
        letraFinal = this.children[charfinal];
    } else {
        letraIni = this.children[charfinal];
        letraFinal = this.children[charinicial];
    }



    //coordenadas iniciais
    for (var i = 0; i < horizontalModule._lines.length; i++) {
        line = horizontalModule._lines[i];
        if (letraIni.y >= line.y && letraIni.y < line.y + line.height) {
            iniCoord = {
                x: letraIni.x,
                up: line.y,
                down: line.height,
                lineIndex: i
            };

            break;
        }
    }

    //coordenadas finais
    for (i = 0; i < horizontalModule._lines.length; i++) {
        line = horizontalModule._lines[i];
        if (letraFinal.y >= line.y && letraFinal.y < line.y + line.height) {
            endCoord = {
                x: letraFinal.x + letraFinal.width,
                up: line.y,
                down: line.height,
                lineIndex: i
            };
            break;
        }
    }

    //se for uma seleção na mesma linha desenha um retangulo
    if (iniCoord.lineIndex == endCoord.lineIndex) {
        coordinates.push({
            x: iniCoord.x,
            y: iniCoord.up,
            width: endCoord.x - iniCoord.x,
            height: endCoord.down
        });
        return coordinates;
    }

    //se for uma seleção de duas linhas desenha 2 retangulos
    if (iniCoord.lineIndex == endCoord.lineIndex - 1) {
        coordinates.push({
            x: iniCoord.x,
            y: iniCoord.up,
            width: self._width - iniCoord.x,
            height: iniCoord.down
        });
        coordinates.push({
            x: 0,
            y: endCoord.up,
            width: endCoord.x,
            height: endCoord.down
        });
        return coordinates;
    }


    //se for uma seleção de mais de duas linhas desenha 3 retangulos
    coordinates.push({
        x: iniCoord.x,
        y: iniCoord.up,
        width: self._width - iniCoord.x,
        height: iniCoord.down
    });
    coordinates.push({
        x: 0,
        y: iniCoord.up + iniCoord.down,
        width: self._width,
        height: endCoord.up - iniCoord.up - iniCoord.down
    });
    coordinates.push({
        x: 0,
        y: endCoord.up,
        width: endCoord.x,
        height: endCoord.down
    });



    return coordinates;

};


TextField.prototype.getCharAt = function(x, y) {

    var position = this.toLocal({
        x: x,
        y: y
    });

    for (var i = 0; i < this.children.length; i++) {
        if (this.children[i].x < position.x &&
            this.children[i].x + this.children[i].width > position.x &&
            this.children[i].y < position.y &&
            this.children[i].y + this.children[i].height > position.y) {

            return i;
        }
    }
};

/**
 * Used to remove all chars from the Textfield
 *
 *
 */
TextField.prototype.reset = function() {

    this.removeChildren();

};



TextField.prototype.cursosPosition = function() {


    //TO DO emitir a ultima posição para atualizar o cursor

    if (self.children.length > 0) {
        self.emit("textUpdated", {
            x: self.getChildAt(self.children.length - 1).x + self.getChildAt(self.children.length - 1).width,
            y: self.getChildAt(self.children.length - 1).y
        });
    } else {
        if (self._textAlign == 'left') {
            self.emit("textUpdated", {
                x: this.x,
                y: this.y
            });
        }
        if (self._textAlign == 'center' || self._textAlign == 'justify') {
            self.emit("textUpdated", {
                x: this.x + this._width / 2,
                y: this.y
            });
        }
        if (self._textAlign == 'right') {
            self.emit("textUpdated", {
                x: this.x + this._width,
                y: this.y
            });
        }
    }
};

});

define('core/text/textfield/align/customModule/TypeAlign',['require','exports','module'],function (require, exports, module) {/**
 * Used to Define a custom aligment for the chars in the textfield
 *
 * @class
 * @memberof type.text
 *
 * @example
 * var typeAlign = new type.text.TypeAlign();
 * typeAlign.startingX = 50;
 * typeAlign.startingY = 250;
 *typeAlign.steps = [{
 *     "x": 20,
 *     "y": 70,
 *     "rotation": 90
 *}, {
 *     "x": 15,
 *     "y": -20
 *}, {
 *     "x": 15,
 *     "y": +20,
 *     "rotation": 45
 *}, {
 *     "x": 15,
 *     "y": -70
 *},
 *{
 *     "x": 20,
 *     "y": -70,
 *     "rotation": 45
 *}, {
 *     "x": 15,
 *     "y": +20
 *}, {
 *     "x": 15,
 *     "y": -20
 *}, {
 *     "x": 15,
 *     "y": +70
 * }];
 *
 *
 *txField.typeAlign = typeAlign;
 *
 *txField.customAlign = true;
 */

function TypeAlign() {

    /*
     * Starting x point of the chars
     *
     * @member {number}
     * @memberof type.text.TypeAlign#
     */
    this._startingX = 0;

    /*
     * Starting y point of the chars
     *
     * @member {number}
     * @memberof type.text.TypeAlign#
     */
    this._startingY = false;

    /*
     * steps to follow after the starting point to align the chars each object inside must contain x, y and rotation
     *
     * @member {Array}
     * @memberof type.text.TypeAlign#
     */
    this._steps = [];


}

//HorizontalModule.prototype = Object.create(PIXI.Container.prototype);
TypeAlign.prototype.constructor = TypeAlign;
module.exports = TypeAlign;


Object.defineProperties(TypeAlign.prototype, {

    /**
     * Starting x point of the chars
     *
     * @member {number}
     * @memberof type.text.TypeAlign#
     */
    startingX: {
        get: function() {
            return this._startingX;
        },
        set: function(value) {
            this._startingX = value;
        }
    },

    /**
     * Starting y point of the chars
     *
     * @member {number}
     * @memberof type.text.TypeAlign#
     */
    startingY: {
        get: function() {
            return this._startingY;
        },
        set: function(value) {
            this._startingY = value;
        }
    },

    /**
     * steps to follow after the starting point to align the chars each object inside must contain x, y and rotation
     *
     * @member {Array}
     * @memberof type.text.TypeAlign#
     */
    steps: {
        get: function() {
            return this._steps;
        },
        set: function(value) {
            this._steps = value;
        }
    }



});

});

define('core/text/input/Input',['require','exports','module','../textfield/TextField','../char/Char'],function (require, exports, module) {var TextField = require('../textfield/TextField');
var Char = require('../char/Char');
var PIXI = require('PIXI');
function Input(_width, _height, align, maxChars) {
    PIXI.Container.call(this);

    var self = this;

    this._width = _width || 2048;
    this._height = _height || 1152;

    this.hitArea = new PIXI.Rectangle(0,0,this._width, this._height);


    this.debug = false;

    this.maxChars = maxChars || 0;

    this.field = new TextField(_width, _height);
    this.field.align = align;
    this.addChild(this.field);

    this.cursorStyle = {
        fontFamily: 'arial',
        fontSize: 20,
        fill: "#000000"
    };


    //configurando o cursor
    this.cursor = new Char("I", this.cursorStyle);
    this.addChild(this.cursor);

    this.cursor.alpha = 0;

    switch (align) {
        case "center":
            self.cursor.x = (self.x + self._width) / 2;
            break;
        case "right":
            self.cursor.x = (self.x + self._width);
            break;
        case "justify":
            self.cursor.x = (self.x + self._width) / 2;
            break;
        default:
            self.cursor.x = self.x;

    }

    this.selectionGraphics = new PIXI.Graphics();
    this.addChild(this.selectionGraphics);


    this.field.on("textUpdated", function(char) {
        self.positionCursor(char);
    });


}


Input.prototype = Object.create(PIXI.Container.prototype);
Input.prototype.constructor = Input;
module.exports = Input;

Object.defineProperties(Input.prototype, {
    text: {
        get: function() {
            return this.field._text;
        },

        set: function(text) {

            this.field.setText(text);

        }
    },

    textStyle: {
        get: function() {
            return this.field._text;
        },

        set: function(text) {

            this.field.setText(text);

        }
    },

    align: {
        get: function() {

            return this.field.align;
        },
        set: function(value) {
            this.field.align = value;
        }
    },

    width: {
        get: function() {
            return this._width;
        },
        set: function(value) {
            var width = this.getLocalBounds().width;
            if (width !== 0) {
                this.scale.x = value / width;
            } else {
                this.scale.x = 1;
            }
            this._width = value;
        }
    },

    textLeftToRight: {
        get: function() {

            return this.field.textLeftToRight;
        },
        set: function(value) {
            this.field.textLeftToRight = value;
        }
    },

    textTopToBottom: {
        get: function() {

            return this.field.textTopToBottom;
        },
        set: function(value) {
            this.field.textTopToBottom = value;
        }
    },

    alignHorizontalPriority: {
        get: function() {

            return this.field._alignHorizontalPriority;
        },
        set: function(value) {
            this.field._alignHorizontalPriority = value;
        }
    },

    /**
     * define if the align mode will be a normal one or a customized one
     *
     * @member {Boolean}
     * @default false
     * @memberof type.text.Input#
     */
    customAlign: {
        get: function() {
            return this.field._customAlign;
        },
        set: function(value) {
            this.field._customAlign = value;
        }
    }

});

Input.prototype.addEvents = function() {

    this.interactive = true;

    this.on('mousedown', this.focus)
        .on('touchstart', this.focus);
};

Input.prototype.animateCursor = function() {

    TweenMax.to(this.cursor, 0.5, {
        alpha: 0,
        yoyo: true,
        repeat: -1
    });

};

Input.prototype.setText = function(text, style) {
    this.field.setText(text, style);
};

Input.prototype.cursorSize = function(size) {
    this.cursor.setStyle({fontSize: size});
};


Input.prototype.focus = function() {

    this.emit("focus");

};

Input.prototype.hideCursor = function() {

    var self = this;
    this.interactive = false;
    TweenMax.killTweensOf(this.cursor);
    this.cursor.alpha = 0;

};

Input.prototype.addEvents = function() {
    this.interactive = true;

    this.on('mousedown', this.focus, this);
    this.on('touchstart', this.focus, this);

    this.on('mouseup', this.stopSelecting, this);
    this.on('touchend', this.stopSelecting, this);

    this.on('mousemove', this.select, this);
    this.on('touchmove', this.select, this);

    this.on('mouseupoutside', this.stopSelecting, this);
    this.on('touchendoutside', this.stopSelecting, this);
};

Input.prototype.removeEvents = function() {
    this.interactive = false;

    this.removeListener('mousedown');
    this.removeListener('touchstart');

    this.removeListener('mouseup');
    this.removeListener('touchend');

    this.removeListener('mousemove');
    this.removeListener('touchmove');

    this.removeListener('mouseupoutside');
    this.removeListener('touchendoutside');

};

Input.prototype.focus = function(e) {

    this.selectionStarted = true;
    this.emit("focus");

    this.initialChar = this.field.getCharAt(e.data.global.x, e.data.global.y);

};

Input.prototype.select = function(e) {

    if (this.selectionStarted) {

        this.finalChar = this.field.getCharAt(e.data.global.x, e.data.global.y);
        if (this.initialChar !== undefined && this.finalChar !== undefined && this.initialChar != this.finalChar) {
            this.drawSelection(this.field.getSelectionCoordinates(this.initialChar, this.finalChar));
        }
    }
};

Input.prototype.stopSelecting = function(e) {

    if (this.selectionStarted) {

        this.selectionStarted = false;

        this.finalChar = this.field.getCharAt(e.data.global.x, e.data.global.y);

        if (this.initialChar !== undefined && this.finalChar !== undefined && this.initialChar != this.finalChar) {
            this.drawSelection(this.field.getSelectionCoordinates(this.initialChar, this.finalChar));
        }

    }

    if (this.initialChar == this.finalChar) {
        this.selectionGraphics.clear();

        this.positionCursor(this.field.children[this.finalChar]);
    }
};

Input.prototype.drawSelection = function(coords) {
    this.selectionGraphics.clear();

    for (var i = 0; i < coords.length; i++) {
        this.selectionGraphics.beginFill(0x0000ff, 0.1);
        this.selectionGraphics.drawRect(coords[i].x, coords[i].y, coords[i].width, coords[i].height);
        this.selectionGraphics.endFill();
    }
};

Input.prototype.showCursor = function() {

    this.positionCursor(this.field.children[this.field.children.length - 1]);

    this.cursorDirection = 1;

    this.cursorAnimation();

};

Input.prototype.cursorAnimation = function() {

    var self = this;

    this.cursor.alpha = 1;

    this.showCursor = setInterval(function() {
        if (self.cursorDirection == 1) {
            self.cursor.alpha += 0.3;
        } else{
            self.cursor.alpha -= 0.3;
        }

        if (self.cursor.alpha >= 1) {
            self.cursorDirection = 0;
        }

        if (self.cursor.alpha <= 0) {
            self.cursorDirection = 1;
        }
    }, 130);

};

Input.prototype.openFeed = function(type) {
    var self = this;
    var color = "";
    var strokeThickness = 0;
    if (type == 'ok') {

        color = "#1ba218";
        strokeThickness = 3;

    }

    if (type == 'fail') {

        color = "#a21818";
        strokeThickness = 3;

    }

    this.field.setStroke(color, strokeThickness);

};

Input.prototype.closeFeed = function() {

    var style = {};

    style = {
        //font: "20.5px playtime",
        fill: "#3c3128",
        stroke: "#3c3128",
        fontFamily: "playtime",
        //fontSize: "20.5",
        strokeThickness: 3
    };

    this.setStyle(style);

};

Input.prototype.setWordStyle = function(word, style) {

    this.field.setWordStyle(word, style);

};

Input.prototype.setStyle = function(style) {

    this.field.setStyle(style);

};

Input.prototype.hideCursor = function(style) {

    clearInterval(this.showCursor);
    this.cursor.alpha = 0;
};

Input.prototype.positionCursor = function(character) {

    if (character === undefined) {
        character = null;
    }

    if (character === null) {
        if (this.field.align == "left" || this.field.align == "justify") {
            this.cursor.x = 2;
            this.cursor.y = 0;
        }
        if (this.field.align == "center") {
            this.cursor.x = this._width / 2;
            this.cursor.y = 0;
        }
        if (this.field.align == "right") {
            this.cursor.x = this.width - 3;
            this.cursor.y = 0;
        }

    }else{
        this.cursor.setStyle({fontSize: character._style.fontSize});
        this.cursor.x = character.x + character.width;
        this.cursor.y = character.y;
    }
};

});

define('core/text/index',['require','exports','module','./char/Char','./word/Word','./phrase/Phrase','./textfield/TextField','./char/KerningLibrary','./textfield/align/horizontalModule/HorizontalModule','./textfield/align/verticalModule/VerticalModule','./textfield/align/customModule/CustomModule','./textfield/align/customModule/TypeAlign','./input/Input'],function (require, exports, module) {var text = module.exports = {

    //base Char
    Char: require('./char/Char'),

    //conjuction of chars
    Word: require('./word/Word'),

    //conjuction of words
    Phrase: require('./phrase/Phrase'),

    //TextField
    TextField: require('./textfield/TextField'),

    //KerningLibrary
    KerningLibrary: require('./char/KerningLibrary'),

    //align horizontalModule
    HorizontalModule: require('./textfield/align/horizontalModule/HorizontalModule'),

    //align verticalModule
    VerticalModule: require('./textfield/align/verticalModule/VerticalModule'),

    //align verticalModule
    CustomModule: require('./textfield/align/customModule/CustomModule'),

    //align TypeAlign
    TypeAlign: require('./textfield/align/customModule/TypeAlign'),

    //Input for has cursos and selection(coming soon)
    Input: require('./input/Input')


};

});

define('core/index',['require','exports','module','./text/index'],function (require, exports, module) {var core = module.exports = {
    VERSION: '__VERSION__',
    text: require('./text/index')
};

});

define('Main',['require','exports','module','./core/index'],function (require, exports, module) {if(typeof(PIXI) == 'undefined' || PIXI === null || parseInt(PIXI.VERSION) < 4)
{
    throw 'Dependencie: pixi.js is required >= 4.0.0';
}
else{
    var core = require('./core/index');

    window.type = core;
}

});


require(["Main"]);
