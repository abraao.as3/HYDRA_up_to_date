var EventEmitter = require('EventEmitter');

function Scream() {

}

Scream.prototype.constructor = Scream;
module.exports = Scream;

Scream._map = {};
Scream._id = 0;
Scream._playingId = null;

Scream.Audio = function(url) {
    EventEmitter.call(this);
    var self = this;

    if (Scream.el === null || Scream.el === undefined) {
        Scream.el = document.createElement('audio');
        Scream.source = document.createElement('source');
        Scream.el.appendChild(Scream.source);
    }

    this.absolutePath = window.location.href.replace(/(index\.html)?\?[\w = ,]+/ig, '');
    this.src = url.replace(/\b\/{2}/ig, '/');

    var tempEl = document.createElement('audio');
    var tempSource = document.createElement('source');
    tempSource.src = this.absolutePath + this.src;
    tempEl.appendChild(tempSource);
    tempEl.onloadedmetadata = function(){
        self.duration = this.duration;
        tempSource.src = '';
        tempEl.removeChild(tempSource);
        tempSource = null;
        tempEl = null;

        self.emit('loadedmetadata');
    };
    tempEl.load();

    Scream._map[Scream._id] = this;
    this.id = Scream._id;
    Scream._id++;
};

Scream.Audio.prototype = Object.create(EventEmitter.prototype);
Scream.Audio.prototype.constructor = Scream.Audio;
module.export = Scream.Audio;

Scream.Audio.prototype._createAudio = function() {
    var self = this;

    Scream.source.src = this.absolutePath + this.src;
    Scream.el.load();
	Scream._playingId = this.id;
};

Scream.Audio.prototype.play = function() {
    var self = this;

    this._createAudio();

    Scream.el.onplay = function() {
        self.emit('play');
    };
    Scream.el.onended = function() {
        self.stop();
    };

    Scream.el.play();
};

Scream.Audio.prototype.stop = function() {
    if(Scream.el === undefined || Scream.el === null) return;
	if(Scream._playingId === this.id && Scream.el.currentTime > 0) {
		Scream.el.pause();
		Scream.el.currentTime = 0;
		Scream.source.src = '';

		this.emit('end');
	}
};

Scream.Audio.prototype.mute = function(){
    Scream.el.muted = true;
};

Scream.Audio.prototype.unmute = function(){
    Scream.el.muted = false;
};

Scream.Audio.prototype.volume = function(value){
    Scream.el.volume = value;
};
