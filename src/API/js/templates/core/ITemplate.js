/**
 * @module templates/core
 */
 var Const = require('../../core/Const');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');
var Button = require('../../display/buttons/Button');
var Interface = require('../../controller/InterfaceHandler');
var Popup = require('../../display/complex/Popup');
var TweenMax = require('TweenMax');
var PIXI = require('PIXI');

var TScroll = require('../templates/TScroll');

var Const = require('../../core/Const');

/**
 * @classdesc Interface para os templates
 * @memberof module:templates/core
 * @extends module:core.ObjectBase
 * @extends module:behavior.FadePoint
 * @constructor
 */
function ITemplate() {

    ObjectBase.call(this);

    this.isExercise = false;

    this._layersDisplacement = null;
    this._layersDisplay = null;
    this._config = null;
    this._SoundManager = null;

    this.isAssessment = false;

    this._interface = Interface.getInstance();

    this.addPlugin(new behavior.FadePoint());
    this.addPlugin(new behavior.Dragable());

    this._resetOnFail = false;

    this._scrolls = [];

}

ITemplate.prototype = Object.create(ObjectBase.prototype);
ITemplate.prototype.constructor = ITemplate;
module.exports = ITemplate;

Object.defineProperties(ITemplate.prototype, {

    /**
     * Sobrescreve o getter e setter do pixi enviando o x para o layerMoveReference
     * uma abreviação para position.x
     *
     * @member {number}
     * @memberof module:templates/core.ITemplate
     */
    x: {
        get: function() {
            return this.position.x;
        },
        set: function(value) {
            for (var i = 0; i < this._layersDisplacement.length; i++) {
                this._layersDisplacement[i].x = value;
            }
            this.transform.position.x = value;
        }
    },

    /**
     * Sobrescreve o getter e setter do pixi enviando o y para o layerMoveReference
     * uma abreviação para position.y
     *
     * @member {number}
     * @memberof module:templates/core.ITemplate
     */
    y: {
        get: function() {
            return this.position.y;
        },
        set: function(value) {
            for (var i = 0; i < this._layersDisplacement.length; i++) {
                this._layersDisplacement[i].y = value;
            }
            this.transform.position.y = value;
        }
    },

    /**
     * Retorna o objeto Layers do templates
     * @memberof module:templates/core.ITemplate
     * @type {module:display/core.Layers}
     * @readonly
     */
    layersDisplay: {
        get: function() {
            return this._layersDisplay;
        }
    },
    /**
     * Retorna um objeto abstrato que referencia as camadas de deslocamento
     * @memberof module:templates/core.ITemplate
     * @type {module:display/core.LayerMoveReference}
     * @readonly
     */
    layerDisplacement: {
        get: function() {
            return this._layersDisplacement;
        }
    },
    /**
     * Retorna o objeto com as configurações do template
     * @memberof module:templates/core.ITemplate
     * @type {Object}
     * @readonly
     */
    templateConfig: {
        get: function() {
            return this._config;
        }
    },
    /**
     * Retorna o objeto com os audios da tela
     * @memberof module:templates/core.ITemplate
     * @type {module:media.SoundManager}
     * @readonly
     */
    soundManager: {
        get: function() {
            return this._SoundManager;
        }
    }
});



/**
 * Método executado no momento em que o template é instanciado
 */

 var txt;
 var codeDebugScreen = "ddd"
 var cCodeDebugScreen = "";
 var txtTime = 1000;
 var txtTimeOut = null;

 var codeDebugMouse = "mmm";
 var cCodeDebugMouse = "";
 var mouseTime = 1000;
 var mouseTimeOut = null;
 // window.mousedown = null;
 var moveDown = function(e){console.log("X: ", Math.round(e.data.global.x), " Y ", Math.round(e.data.global.y));};
 window.addEventListener(
 	"keydown",
 	function(k){
 		if(k.key == "d"){//DEBUG SCREEN
            cCodeDebugScreen += k.key;
            txtTimeOut = setTimeout(function(){cCodeDebugScreen = "";}, txtTime);
            if(cCodeDebugScreen === codeDebugScreen){txt.alpha = txt.alpha == 0 ? 1 : 0; cCodeDebugScreen = ""; clearTimeout(txtTimeOut);}
         }

        //  if(k.key == "m"){//DEBUG MOUSE
        //       cCodeDebugMouse += k.key;
        //       mouseTimeOut = setTimeout(function(){codeDebugMouse = "";}, mouseTime);
        //       if(codeDebugMouse === codeDebugMouse){
        //           window.interactive = !window.interactive;
        //           window.mousedown = moveDown != null && moveDown != undefined ? null : moveDown;
         //
        //           codeDebugMouse = "";
        //           clearTimeout(mouseTimeOut);}
        //    }
 	},
 	false
 );

ITemplate.prototype.init = function() {
    var self = this;

    //Debug Mouse
    if(Const.APPConfig.debugMouse === true){
        this.interactive = true;
        this.mousedown = moveDown;
    }

    //Debug Screen
    txt = new PIXI.Text(this.config, {fontSize: 25, fill: 'red', stroke: 'white', strokeThickness: 2});
    txt.x = Const.BASE_WIDTH / 2 - txt.width / 2;
    txt.y = 20;
    this._layersDisplay.getLayer('sup').addChild(txt);
    txt.alpha = 0;

    this._nextPageMethod = this._config.nextPageMethod;

	if (!!this._config.interactions){

        this.addPlugin(new behavior.Interactions());

        this.prepareBehaviours();

    }

    if (this._config.toSupLayer !== undefined) {
        var sup = this.layersDisplay.getLayer('sup');

        var resolve = function(layer, type) {
            for (var k = 0; k < self._config.toSupLayer[type].length; k++) {
                var sel = self._config.toSupLayer[type][k];
                var objs = self.layersDisplay.getObjects(layer);

                if (type == "txt" || type == "images") {
                    for (var l = 0; l < objs.length; l++) {
                        if (sel == objs[l].index) {
                            var ob = objs[l];
                            var parent = ob.parent;
                            parent.removeChild(ob);
                            sup.addChild(ob);
                            break;
                        }
                    }
                } else {

                    var ob2 = objs[sel];
                    var parent2 = ob2.parent;
                    parent2.removeChild(ob2);
                    sup.addChild(ob2);
                    break;

                }
            }
        };

        if (this._config.toSupLayer.images !== undefined && this._config.toSupLayer.images.length > 0) {
            resolve("images", "images");
        }

        if (this._config.toSupLayer.txt !== undefined && this._config.toSupLayer.txt.length > 0) {
            resolve("text", "txt");
        }

        if (this._config.toSupLayer.buttons !== undefined && this._config.toSupLayer.buttons.length > 0) {
            resolve("interactions", "buttons");
        }

        if (this._config.toSupLayer.drag !== undefined && this._config.toSupLayer.drag.length > 0) {
            resolve("interactions", "drag");
        }
    }

	this._buttons = this.layersDisplay.getObjects('button');

	if (this._config.popup !== undefined && this._config.popup !== null) {
        for (var h = 0; h < this._config.popup.length; h++) {
            var p = new Popup();
            p.index = h;
            p.setBackground(this.layersDisplay.getObjects('background')[0],this._config.popup[h]);
            p.setConfig(this._config.popup[h], this.layersDisplay, this.soundManager);
            p.init();

            if (this._popups === undefined || this._popups === null) {
                this._popups = [];
            }

            this._popups.push(p);
            this.layersDisplay.addMap('popups', p);
        }
    }

    if (this._buttons !== undefined) {
        for (var i = 0; i < this._buttons.length; i++) {
            if (this._config.button[i].action !== undefined) {
                for (var j in this._config.button[i].action) {
                    this._buttons[i].isLock = false;
                    if (Button.ACTION_DEFAULT.indexOf(j) === -1) {
                        var ctx = this;
                        if (this._buttons[i].popupLinked !== undefined && this._buttons[i].popupLinked !== null && this._config.button[i].popupLinked !== false) {
                            ctx = this._popups[this._buttons[i].popupLinked];
                        }
                        this._buttons[i]._action.push({
                            fn: ctx[this._config.button[i].action[j]],
                            context: ctx,
                            params: this._config.button[i].params
                        });
                    } else {
                        var action = this._config.button[i].action[j];
                        this._buttons[i].soundManager = this.soundManager;
                        this._buttons[i].layersDisplay = this.layersDisplay;
                        this._buttons[i].defineAction(j, action);
                        this._buttons[i].on('openPopup', this._openPopup, this);
                        this._popupOpenMap = {};
                        this._popupOpen = [];
                    }
                }
            }
        }
    }

	var animacoes = this.layersDisplay.getLayer('animations').children;
	if(animacoes.length > 0) {
		animacoes.forEach(function(animacao) {
			animacao.setScreen(this);
			animacao.addControlEvents();
		}, this);
	}

    this._scrolls = this.layersDisplay.getMap('scrolls');//console.log("ITEMPLATE SCROLLS",this._scrolls);
    if(this._scrolls !== null && this._scrolls !== undefined){

        var config_scrolls = self._config.scrolls;

        for (var i = 0; i < this._scrolls.length; i++) {
            var container = config_scrolls[i].popup === null || config_scrolls[i].popup === undefined ? self : this._popups[config_scrolls[i].popup];
            this._scrolls[i].CreateMask(this._scrolls[i].scroll, self._config.scrolls[i], container);
        }

    }


	var statefullObjects = this._layersDisplay.getMap('statefullView');
	if(statefullObjects !== null ) {
		statefullObjects.forEach(function(statefullObject) {
			// console.log('statefullObject',statefullObject);
			statefullObject.loadStates();
		}, this);
	}

	if(this._config.imgTitulo !== undefined && this._config.imgTitulo !== null) {
		this._titulo = this.layersDisplay.getMapByID("images", this._config.imgTitulo);
	}
};

ITemplate.prototype.VerifyScroll = function(){

    var self = this;

    self.useScroll = self._config.scrolls !== undefined && self._config.scrolls !== null ? true : false;
    if(self.useScroll === true){

        var config_scrolls = self._config.scrolls;
        for (var i = 0; i < config_scrolls.length; i++) {

            var idx_imgs = config_scrolls[i].imgs; var idx_txts = config_scrolls[i].txts;

            var all_objs = [];

            //get all obj by id and add it in the "var all_objs = []"
            var imgs = this.ReturnBitmapsOfScrollConfig(idx_imgs);for (var j = 0; j < imgs.length; j++) {all_objs.push(imgs[j]);}
            var txts = this.ReturnTextsOfScrollConfig(idx_txts);for (j = 0; j < txts.length; j++) {all_objs.push(txts[j]);}

            //var scroll = new ObjectBase();//console.log("criei o scroll");

            var container = config_scrolls[i].popup === null || config_scrolls[i].popup === undefined || 1==1 ? self : this._popups[config_scrolls[i].popup];

            var c_id = this._scrolls.length;
            self._config.scrolls[i].area = new ObjectBase();

            //TENTATIVA FAZENDO DO JEITO CERTO!!!
            //add scrollable plugin into the current scroll
            // scroll.addPlugin(new behavior.Scrollable(self._config.scrolls[i]));//console.log("saindo do scrollable",this._scrolls[c_id]);
            // this._scrolls[c_id].CreateScrollContainer();
            // this._scrolls[c_id].AddObjectsIntoScroll(all_objs);


            //TENTATIVA PELO TSCROLL
            var scroll = new TScroll();//console.log("criei o scroll");return;
            this._scrolls.push(scroll.Create(self._config.scrolls[i], all_objs, container));
            //console.log(this._scrolls[i]);
        }
    }
};

ITemplate.prototype.ReturnBitmapsOfScrollConfig = function(ids){
    var bitmaps = [];
    for (var i = 0; i < ids.length; i++) {
        var b = this.layersDisplay.getMapByID('images', ids[i]);
        var p = b.parent;
        if(p){p.removeChild(b);}
        bitmaps.push(b);
    }
    return bitmaps;
};

ITemplate.prototype.ReturnTextsOfScrollConfig = function(ids){
    var type = [];
    for (var i = 0; i < ids.length; i++) {
        var t = this.layersDisplay.getMapByID('text', ids[i]);
        var p = t.parent;
        if(p){p.removeChild(t);}
        type.push(t);
    }
    return type;
};

/**
 * Método executado no momento em que a página é invocada
 */
ITemplate.prototype.start = function() {
    var self = this;
	this._showParticles();

    if (this._popups !== undefined && this._popups !== null) {
        for (var h = 0; h < this._popups.length; h++) {
            this._popups[h].start();
        }
    }

    for (var i = 0; i < this.layersDisplay.particleElements.length; i++) {
        window.particles.push(this.layersDisplay.particleElements[i]);
    }

    var bocas = this._layersDisplay.getObjects('mouths');
    for (i = 0; i < bocas.length; i++) {
        bocas[i].gotoAndStop(bocas[i].stationary);
    }
};

/**
 * Método executado no momento em que a página termina a abertura
 */
ITemplate.prototype.open = function() {
	var self = this;
	this._showParticles();

    if (this._config.calculator !== undefined && this._config.calculator === true) {
        this._interface.addChild(this._interface.calculator);
    }

    if (this._config.controller !== undefined) {

        if (this._config.controller.right !== undefined) {
            if (this._config.controller.right.type == "analog") {
                this._interface.addChild(this._interface.rightAnalogController);
            }

            if (this._config.controller.right.type == "digital") {
                this._interface.addChild(this._interface.rightDigitalController);
            }
        }

        if (this._config.controller.left !== undefined) {
            if (this._config.controller.left.type == "analog") {
                this._interface.addChild(this._interface.leftAnalogController);
            }

            if (this._config.controller.left.type == "digital") {
                this._interface.addChild(this._interface.leftDigitalController);
            }
        }
    }



	// this._showParticles();

    if (this._config.calculator !== undefined && this._config.calculator === true) {
        TweenMax.to(this._interface.calculator, 1, {
            y: "-=111",
            onComplete: function() {
                self._interface.calculator.addEvents();
            }
        });
    }
    if (!!self._config.clocks) {
        this.dealWithPoint = function() {

            this.cKey = parseInt(this.cKey);

            for (var sKey in self._clocks[this.cKey]._body.clockSequences) {
                if (this.point._sequenceId == self._clocks[this.cKey]._body.clockSequences[sKey].pointId) {
                    var timer = parseInt(self._clocks[this.cKey]._body.clockSequences[sKey].tempoAnim);
                    var horas = parseInt(self._clocks[this.cKey]._body.clockSequences[sKey].finalHPosition);
                    var minutos = parseInt(self._clocks[this.cKey]._body.clockSequences[sKey].finalMPosition);
                    self._clocks[this.cKey].resetClockAnimated(timer, horas, minutos);
                }

            }
        };
        self._clocks = this.layersDisplay.getMap('clocks');
        for (var cKey in self._clocks) {
            self._clocks[cKey].resetClock();

            if (!!self._clocks[cKey]._body.clockSequences) {

                for (var pKey in self._points) {
                    self._points[pKey]._sequenceId = parseInt(pKey);
                    self._points[pKey].once('pointStarted', this.dealWithPoint, {cKey:cKey, point:self._points[pKey]});
                }
            }
        }
    }

	var statefullObjects = this._layersDisplay.getMap('statefullView');
	if(statefullObjects !== null) {
		statefullObjects.forEach(function(statefullObject) {
			// console.log('statefullObject',statefullObject);
			statefullObject.addStateListeners();
		}, this);
	}

    this.once('sequenceComplete', this._initInteractions, this);
};
/**
 * Método executado no momento em que o stage é redimensionado
 */
ITemplate.prototype.resize = function() {};
/**
 * Método executado no momento em que a página é aberta `{reinicia todas as propriedades}`
 */
ITemplate.prototype.reset = function() {
	this.fadeStop();

	if (this._config.calculator !== undefined && this._config.calculator === true) {
        this._interface.removeChild(this._interface.calculator);
    }

    if (this._popupOpenMap !== undefined && this._popupOpenMap !== null) {
        for (var i in this._popupOpenMap) {
            delete this._popupOpenMap[i];
        }

        this._popupOpen.splice(0, this._popupOpen.length);
    }
};
/**
 * Método executado no momento em que a página é fechada
 */
ITemplate.prototype.close = function() {
	if (!!this._config.interactions){

        this._removeIntEvents();

    }

    if (this._config.calculator !== undefined && this._config.calculator === true) {
        this._interface.calculator.removeEvents();
    }

	this.removeListener('sequenceComplete', this._initInteractions);

    if (this._buttons !== undefined) {
        for (var i = 0; i < this._buttons.length; i++) {
            // this._buttons[i].removeListener('openPopup');
			this._blocked(true, true, [this._buttons[i]]);
        }
    }

	if(this._popups !== undefined && this._popups !== null && this._popups.length > 0) {
		for(var j = 0; j < this._popups.length; j++) {
			if(this._popups[j].parent !== undefined && this._popups[j].parent !== null) { // Caso o popup esteja aberto
				this._popups[j]._onClose({target:this._popups[j]._close}); // FECHA
			}
		}
	}

	var animacoes = this.layersDisplay.getLayer('animations').children;
	if(animacoes.length > 0) {
		animacoes.forEach(function(animacao) {
			animacao.cleanPlayQueue();
		}, this);
	}

	var statefullObjects = this._layersDisplay.getMap('statefullView');
	if(statefullObjects !== null ) {
		statefullObjects.forEach(function(statefullObject) {
			statefullObject.removeStateListeners();
			statefullObject.resetInitialState();
		}, this);
	}
};

/**
 * Método que recebe e armazena as configurações do template
 * @param config {Object} configuração do template
 */
ITemplate.prototype.setConfig = function(config) {
    var self = this;
    this._config = config;
    if (this._config.contour !== undefined && this._config.contour.length > 0) {
        for (var i = 0; i < this._config.contour.length; i++) {
            var contour = this._config.contour[i];
            var prop = "";

            if (contour.images !== undefined) {
                prop = "images";
            } else if (contour.button !== undefined) {
                prop = "button";
            }
            else if(contour.text !== undefined){

                prop = "text";
            }

            for (var j = 0; j < contour[prop].length; j++) {
                var __objs = this.layersDisplay.getMap(prop);
                for (var k = 0; k < __objs.length; k++) {
                    if (__objs[k].index == contour[prop][j]) {
                        var obj = __objs[k];
                        if(prop!= "text"){

                        obj.createContour(contour.thickness, parseInt(contour.color));
                    }else {
                        obj.contourTxt(contour.thickness, parseInt(contour.color));

                    }
                    }
                }
            }
        }
    }
};
/**
 * Método que recebe e armazena as Layers
 * @param config {module:display/core.Layers} layers do template
 */
ITemplate.prototype.setLayers = function(layers) {
    if (this._layersDisplacement === null && this._layersDisplay === null) {
        this._layersDisplacement = layers.layerDisplacement;
        this._layersDisplay = layers.layersDisplay;
        this.addChild(this._layersDisplay);
    }
};

/**
 * Método que recebe e armazena os Sons
 * @param sm {module:media.SoundManager} audios do template
 */
ITemplate.prototype.setSoundManager = function(sm) {
    this._SoundManager = sm;
};

/**
 * Método que adiciona eventos de arraste a tela para poder move-la
 */
ITemplate.prototype.addParallaxEvents = function() {

    this.addEvents();

};

ITemplate.prototype.removeParallaxEvents = function() {

    this.removeEvents();

};

ITemplate.prototype._showParticles = function() {
	var self = this;

	if(self.layersDisplay.particleElements !== undefined && self.layersDisplay.particleElements !== null && self.layersDisplay.particleElements.length > 0) {
		var particlesToDisplay = self.layersDisplay.particleElements.filter(function(particle) {
			if(window.particles.indexOf(particle) >= 0) {
				return false;
			}

			if(this._config.sequences !== undefined && this._config.sequences !== null && this._config.sequences.length > 0) {
				var sequenceParticles = [];
				for(var i = 0; i < this._config.sequences.length; i++) {
					var sequence = this._config.sequences[i];

					if(sequence.particles !== undefined && sequence.particles !== null && sequence.particles.length > 0) {
						sequenceParticles.concat(sequence.particles);
					}
				}

				if(sequenceParticles.length > 0) {
					for(var j = 0; j < sequenceParticles.length; j++) {
						var sequenceParticle = sequenceParticles[j];
						if(particle.index === sequenceParticle.numberObject) {
							return false;
						}
					}
				}
			}

			return true;
		}, this);


	    for (var i = 0; i < particlesToDisplay.length; i++) {
	        window.particles.push(particlesToDisplay[i]);
	    }
	}
};

ITemplate.prototype._initInteractions = function() {

    if (!!this._config.interactions){

        if (this._config.interactions[0].type !== "fadePoints") {
            this._addIntEvents();

        }

    }

    if (this._buttons !== undefined) {
        for (var i = 0; i < this._buttons.length; i++) {
			this._unblock([this._buttons[i]]);
            this._buttons[i].start();
        }
    }

	var statefullObjects = this._layersDisplay.getMap('statefullView');
	if(statefullObjects !== null ) {
		statefullObjects.forEach(function(statefullObject) {
			// console.log('statefullObject',statefullObject);
			statefullObject.addStatesInteraction();
		}, this);
	}
};

/** @function
* @description _blocked
* @private
*/
ITemplate.prototype._blocked = function(forced, stopAnimation, buttons) {
    var _stopAnimation = stopAnimation || false;
	var _forced = forced || false;
	var _buttons = buttons || this._buttons;

    for (var i = 0; i < _buttons.length; i++) {
		if(_forced || (_buttons[i]._config.action !== undefined && _buttons[i]._config.action !== null && _buttons[i]._config.action.popup !== undefined && _buttons[i]._config.action.popup !== null && !this._popups[_buttons[i]._config.action.popup]._keepOpenable)) {
			_buttons[i]._block = true;
		    if (_stopAnimation && _buttons[i].animating === true) {
		        _buttons[i].stopAnimation();
		    }
		}
    }
};

/** @function
* @description _unblock
* @private
*/
ITemplate.prototype._unblock = function(buttons, startAnimation) {
	var _buttons = buttons || this._buttons,
		_starAnimation = startAnimation || false;

    for (var i = 0; i < _buttons.length; i++) {
        if (_buttons[i].isLock === true) continue;
        _buttons[i]._block = false;
		if(_starAnimation && _buttons[i].animating === true) {
			_buttons[i].animate();
		}
    }
};

ITemplate.prototype._openPopup = function(e) {
    if (this._popups !== undefined) {
        var pop = this._popups[e.action];
        this.addChild(pop);

        pop.once('openStarted', this.popupOpenStarted, this);
        pop.once('openCompleted', this.popupOpenCompleted, this);
        pop.once('closeStarted', this.popupCloseStarted, this);
        pop.once('closeCompleted', this.popupCloseCompleted, this);

        pop.open();
    }
};

ITemplate.prototype.popupOpenStarted = function(e) {};

ITemplate.prototype.popupOpenCompleted = function(e) {

};

ITemplate.prototype.popupCloseStarted = function(e) {

};

ITemplate.prototype.popupCloseCompleted = function(e) {
    this.removeChild(e.target);

    var pop = e.target;
    if (this._popupOpenMap[pop.index] === undefined) {
        this._popupOpenMap[pop.index] = pop;
        this._popupOpen.push(pop);
    }

    if (this._popupOpen.length == this._popups.length) {
        this.popupsComplete();
    }
};

ITemplate.prototype.popupsComplete = function() {

};

ITemplate.prototype.checkRoadMethod = function(){
    // console.log(this._nextPageMethod);
    switch (this._nextPageMethod) {
        case "auto":
            this.emit("avancarTela");
            break;
        case "click":
            this.emit("allowNext");
            break;
        default:
            this.emit('parallax');
    }
};
