var EventEmitter = require('EventEmitter');
var templates = require('../templates/index');

/**
 * @classdesc Responsável pela criação dos templates
 * @memberof module:templates/core
 * @extends EventEmitter
 * @constructor
 */
function TemplateFactory(){
    EventEmitter.call(this);
}

TemplateFactory.prototype = Object.create(EventEmitter.prototype);
TemplateFactory.prototype.constructor = TemplateFactory;
module.exports = TemplateFactory;

/**
* Método retorna um novo template
* @memberof module:templates/core.TemplateFactory
* @param type {String} tipo do template que quer criar
* @return {module:templates/core.ITemplate}
*/
TemplateFactory.getTemplate = function(type){
    return new templates[type]();
};
