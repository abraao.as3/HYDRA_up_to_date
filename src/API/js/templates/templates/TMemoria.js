var TExercices = require('./TExercices');

/**
 * configuração do template:
 * @example
 *
 * Falta criar exemplo.
 *
 * @classdesc Classe que define o Template de Memória
 * @memberof module:templates/templates
 * @exports TMemoria
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */
function TMemoria() {
    TExercices.call(this);
    this.cardsFliped = [];
    this.contadorCertos = 0;
    this.acerteiTodas = 0;
}

TMemoria.prototype = Object.create(TExercices.prototype);
TMemoria.prototype.constructor = TMemoria;
module.exports = TMemoria;


/**
 * Método que inicia a classe
 */
TMemoria.prototype.init = function() {
    TExercices.prototype.init.call(this);
    var self = this;

    this._cards = this.layersDisplay.getObjects('card');

    if(this._config.cards.hiddenCard)
    {
        this._cards[0].children[1].alpha = 0;
        this._cards[0]._cardStatus = 1;
        this._cards[0]._hideCard = true;
        // this._cards[0].alpha = 0;
    }
    for (var i = 0; i < this._cards.length; i++)
    {
        this._cards[i].createContour(5,0xffffff);
        // this._cards[i].removeFundoCard();
    }

    this._start = false;
};

/**
 * Método que adiciona os eventos das cartas
 *
 * @private
 */
TMemoria.prototype.addCardEvents = function() {
    var self = this;

    for (var i = 0; i < this._cards.length; i++) {
        this._cards[i].id = i;
        this._cards[i].addEvents();
        this._cards[i].on('cardClicked', function()
        {
            if(this._cardStatus == 1)
            {
                self.cardsFliped.push(this);
                self.verify(this);
            }else{
                for (var i = 0; i < self.cardsFliped.length; i++) {
                    if(this.id === self.cardsFliped[i].id)
                    {
                        self.cardsFliped.splice(i,1);
                    }
                }
            }


        });
    }

};

/**
 * Método que verifica se acertou ou erro a combinação de cartas
 */
TMemoria.prototype.verify = function(carta) {
    var self = this;
    var status = this.verifyCards(carta);
    if (status === 1) {
        for (var i = 0; i < self.cardsFliped.length; i++) {
            self.cardsFliped[i].removeEvents();
            self.cardsFliped[i].openContour(0x1ba218);
        }

        this.contadorCertos = this.contadorCertos  + 1;
        self.cardsFliped = [];
    }

    if(status == -1)
    {
        self.blockCards(true);
        self.openFail();
        for (var k = 0; k < self.cardsFliped.length; k++) {
            self.cardsFliped[k].openContour(0xa21818);
        }


    }

    if (this.contadorCertos == this._config.cards.groups.length) {
        for (var k = 0; k < this._cards.length; k++) {
            this._cards[k].removeEvents();
        }
        self.openCorrect();
    }
};

/**
 * Método que faz a verificação e validação dos grupos das cartas
 */
TMemoria.prototype.verifyCards = function(carta) {
    var acertou = true;
    var acheiTodas = 0;

    for (var i = 0; i < this._config.cards.groups.length; i++) //passa de group em group
    {
        acertou = true;
        acheiTodas = 0;

        for (var j = 0; j < this.cardsFliped.length; j++) // array de cartas clicadas
        {
            var achei = false;
            for (var k = 0; k < this._config.cards.groups[i].length; k++) //verifica se as cartas clicadas pertencem ao group
            {
                if (this._config.cards.groups[i][k] == this.cardsFliped[j].id) {
                    achei = true;
                    continue;
                }
            }

            if (achei === false) {
                acheiTodas = -1;
            }

        }

        if (acheiTodas === 0) {

            if (this.cardsFliped.length == this._config.cards.groups[i].length) {
                acheiTodas = 1;

            }

            break;
        }
    }

    return acheiTodas;
    // if (acheiTodas) {
    //     return true;
    // } else {
    //     return false;
    // }

};

/**
 * Método que bloqueia o clique das cartas
 */
TMemoria.prototype.blockCards = function(status)
{
    var self = this;
    if(status)
    {
        for (var i = 0; i < this._cards.length; i++) {
            this._cards[i]._selectionEffect = false;
        }
    }else{
        for (var k = 0; k < this._cards.length; k++) {
            this._cards[k]._selectionEffect = true;
        }
    }
};

/**
 * Método que indica que o memória foi começou
 */
TMemoria.prototype.start = function() {
    TExercices.prototype.start.call(this);
    console.log("start");
};

TMemoria.prototype._initInteractions = function() {

    TExercices.prototype._initInteractions.call(this);
    var self = this;

	self._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {

            self.fadeRestart();
            self.once('sequenceComplete', function(){
            });
        });
    });

    self._interface.openRefresh();
    self.addCardEvents();
};


/**
 * Método que indica que o memória foi aberto
 */
TMemoria.prototype.open = function() {
    TExercices.prototype.open.call(this);
    var self = this;

    console.log("open");

    this._start = true;
};

/**
 * Método que abre o feed positivo
 */
TMemoria.prototype.openCorrect = function() {
    var self = this;
    if (self.correct) {
        self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }
};

/**
 * Método que abre o feed negativo
 */
TMemoria.prototype.openFail = function() {
    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();
            for (var i = 0; i < self.cardsFliped.length; i++) {
                self.cardsFliped[i].flipCard(0);
                self.cardsFliped[i].closeContour();
            }
            self.cardsFliped = [];
            self.blockCards(false);
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
            for (var i = 0; i < self.cardsFliped.length; i++) {
                self.cardsFliped[i].flipCard(0);
                self.cardsFliped[i].closeContour();
            }
            self.cardsFliped = [];
            self.blockCards(false);
        }, 2000);
    }
};

/**
 * Método que reinicia o Memória
 */
TMemoria.prototype.reset = function() {
    var self = this;
    console.log("reset");
    TExercices.prototype.reset.call(this);

    try {
        for (var k = 0; k < this._cards.length; k++) {
            this._cards[k].closeContour();
            this._cards[k].removeEvents();
            this._cards[k].removeListener('cardClicked');
            this._cards[k].resetCard();
        }
    } catch (e) {

    } finally {

    }

    self.cardsFliped = [];

    self.contadorCertos = 0;
};

/** @function
* @description close
*/
TMemoria.prototype.close = function() {


};
