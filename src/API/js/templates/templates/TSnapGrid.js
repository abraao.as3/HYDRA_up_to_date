var TExercices = require('./TExercices');
var behavior = require('../../behavior/index');
var SnapGrid = require('../../display/complex/SnapGrid');

/**
 * configuração do template:
 * @example
 *
 * Falta criar exemplo.
 *
 * @classdesc TSnapGrid Class
 * @memberof module:templates/templates
 * @exports TSnapGrid
 * @constructor
 */
function TSnapGrid() {
    TExercices.call(this);

}

TSnapGrid.prototype = Object.create(TExercices.prototype);
TSnapGrid.prototype.constructor = TSnapGrid;
module.exports = TSnapGrid;

/** @function
* @description init
*/
TSnapGrid.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this._sgConfig = this._config.snapGrid;

    var ib = this._layersDisplay.getObjects('images');
    for (var i = 0; i < ib.length; i++) {
        if (ib[i].index == this._sgConfig.imageBase) {
            this._imgBase = ib[i];
            break;
        }
    }

    this._snapGrid = new SnapGrid();
    this._snapGrid.pointSize = this._sgConfig.pointRadius;
    this.addChild(this._snapGrid);
    this._snapGrid.createGrid(6, 6, {
        x: this._imgBase.x - this._imgBase.width / 2,
        y: this._imgBase.y - this._imgBase.height / 2,
        w: this._imgBase.width,
        h: this._imgBase.height
    });

    this._snapGrid.setArtwork(this._sgConfig.artworkCoords);

    this._imgBase.visible = false;
};

/** @function
* @description _initInteractions
* @private
*/
TSnapGrid.prototype._initInteractions = function(){
    var self = this;
    TExercices.prototype._initInteractions.call(this);

    this._snapGrid.addEvents();
    this._snapGrid.on('correct', this._setOk, this);
    this._snapGrid.on('incorrect', this._setFail, this);

    this._interface.once('refreshOpened', function(){
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function(){
            self.fadeRestart();
            self._snapGrid.block();
            self._blocked();
            self.on('sequenceComplete', function(){
                self._snapGrid.unblock();
                self._unblock();
            });
        });
    });

    this._interface.openRefresh();
    this._unblock();
};

/** @function
* @description clear
*/
TSnapGrid.prototype.clear = function(){
    var self = this.context;
    self._snapGrid.clear();
};

/** @function
* @description verify
*/
TSnapGrid.prototype.verify = function(){
    var self = this.context;
    self._snapGrid.block();
    self._blocked();
    self._snapGrid._verifyGrid();
    self._interface.closeRefresh(true);
};

/** @function
* @description _setFail
* @private
*/
TSnapGrid.prototype._setFail = function(){
    var self = this;

    if(this.fail){
        this.fail.once('pointComplete', function(){
            self.emit('exercicioIncorreto');
            this._snapGrid.unblock();
            this._snapGrid.clear();
            this._unblock();
            this._interface.openRefresh();
        }, this);
        this.fail.timeline.play();
    }
    else{
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._snapGrid.unblock();
            self._snapGrid.clear();
            self._unblock();
            self._interface.openRefresh();
        }, 1000);
    }
};

/** @function
* @description _setOk
* @private
*/
TSnapGrid.prototype._setOk = function(){
    var self = this;

    this._interface._refreshButton.removeListener('clicked');
    this._interface.closeRefresh();
    this._blocked(true);

    if(this.correct){
        this.correct.once('pointComplete', function() {
            this.checkRoadMethod();
        }, this);
        this.correct.timeline.play();
    }
    else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }
};

/** @function
* @description _blocked
* @private
*/
TSnapGrid.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

/** @function
* @description _unblock
* @private
*/
TSnapGrid.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }
};

/** @function
* @description reset
*/
TSnapGrid.prototype.reset = function(){
    TExercices.prototype.reset.call(this);

    this._interface.closeRefresh();
    this._blocked(true);
    if (this.fail) this.fail.reset();
    if (this.correct) this.correct.reset();

    this._snapGrid.removeEvents();
    this._snapGrid.clear();

    this._snapGrid.removeListener('correct');
    this._snapGrid.removeListener('incorrect');
    this.removeListener('sequenceComplete');
};
