var ITemplate = require('../core/ITemplate');
var TweenMax = require('TweenMax');
var PIXI = require('PIXI');

/**
* configuração do template:
*
* @classdesc Classe que define um Template vazio para ser customizado
* @memberof module:templates/templates
* @exports TEmpty
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @constructor
*/
function TEmpty() {
    ITemplate.call(this);
}

TEmpty.prototype = Object.create(ITemplate.prototype);
TEmpty.prototype.constructor = TEmpty;
module.exports = TEmpty;

/** @function
* @description init
*/
TEmpty.prototype.init = function() {
};

/** @function
* @description open
*/
TEmpty.prototype.open = function() {

};

TEmpty.prototype.start = function() {

};

TEmpty.prototype.close = function() {

};

TEmpty.prototype.reset = function() {

};

TEmpty.prototype.loaded = function() {

};

TEmpty.prototype.resize = function() {

};
