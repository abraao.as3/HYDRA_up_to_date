var TExercices = require('./TExercices');

/**
 * configuração do template:
 * @example
 *
 {
     "path": "telas/binoculars/",
     "backgroundIndex": 1,                      // indice da tela de background
     "template": {
         "type": "binoculars",                  // tipo do template (ex.: ?select_module=binoculars)
         "config": {
             "toSupLayer":{
               "images":[6,7,8,9,10],           // Imagem(s) para mostrar quando os binóculos aparecem
               "txt":[]                         // Texto para mostrar quando os binóculos aparecem
             },
             "binoculars":{                     // Parâmetros de configuração binóculos
               "x":600,                         // Init x, o valor padrão é o sprite.json ...(ver NÃO FUNCIONANDO ver CreateMask)
               "y":1,                           // Init y, o valor padrão é o sprite.json ...(ver NÃO FUNCIONANDO ver CreateMask)
               "bg":3,                          // Apagão índice de imagem de fundo
               "mask":2,                        // Índice da imagem da máscara
               "color": "0xffffff",             // Máscara de cor de fundo
               "radius": 70,                    // Raio da máscara. Obs: um lado do círculo
               "angle": 55                      // Ângulo de intersecção entre os dois círculos
             },
             "text": {                         // textos da tela
                 "pt-BR": [
                 {"text": "Tente encontrar os animais com 4 patas."},
                 {"text": "Muito bem!"},
                 {"text": "Continue tentando..."}
               ]
             },
             "audio": {                         // audios da tela
                 "pt-BR": [
                   {"name": "laser",                // nome do audio
                   "id": 0},                        // id do audio
                   {"name": "laser","id": 1}
                 ]
             },
             "drags": [{
                     "img": [2],                // indice da img que representa o binoculo
                     "layerDisplacement": 1,
                     "sensor": {
                         "x": 97,               // ponto x de inicio do sensor de identificacao
                         "y": 54,               // ponto y de inicio do sensor de identificacao
                         "width": 20,           // largura do sensor de identificacao
                         "height": 20,          // altura do sensor de identificacao
                         "show": false          // exibe/oculta o sensor momentaneamente
                     }
             }],
             "sequences": [{                    // sequencia a ser mostrada na tela:
                 "id": 0,                           // indice da sequencia
                 "images": [4],                     // imagem(s) a ser(em) exibida(s)
                 "mouth": [0],                      // boca(s) a ser(em) exibida(s)
                 "txt": [0],                        // texto(s) a ser(em) exibido(s)
                 "audio": 0,                        // audio da sequencia
                 "fadeOut": true                    // forma de encerrar a sequencia: true = desaparece / false = mantem
             },{
                 "id": 1,                           // idem demais sequencias...
                 "images": [3],
                 "mouth": [],
                 "txt": [],
                 "audio": null,
                 "fadeOut": false
             }],
             "response": {                      // balao de feed da tela:
                 "correct": {                       // resposta positiva
                     "images": [5],                 // imagem(s) a ser(em) exibida(s)
                     "txt": [1],                    // texto(s) a ser(em) exibido(s)
                     "audio": 1,                    // audio do feed
                     "mouth": [0]                   // boca(s) a ser(em) exibida(s)
                 },
                 "fail": {                          // resposta negativa
                     "images": [11],                // imagem(s) a ser(em) exibida(s)
                     "txt": [2],                    // texto(s) a ser(em) exibido(s)
                     "audio": 2,                    // audio do feed
                     "mouth": []                    // boca(s) a ser(em) exibida(s)
                 }
             },
             "feedsO":{                             // Imagens de resposta positiva
               "imgs":[
                 {"id":6,                                       // indice da imagem(necessário)
                 "thickness":10,                                // Se necessário usar a espessura específica, o valor padrão é defaultThickness (NÃO é necessário)
                 "color":"0x77b505"},                           // se precisar usar a cor específica, o valor padrão é defaultColor (NÃO é necessário)
                 {"id":7, "thickness":10, "color":"0x77b505"}   // idem
               ],
               "timeToFeed":1500,                               // Tempo necessário para acertar a imagem para mostrar o feed
               "defaultThickness":50,                           // Espessura padrão para todas as imagens que não têm seu parâmetro específico de "thickness"
               "defaultColor":"0xffffff"                        // Cor padrão para todas as imagens que não têm seu parâmetro específico de "color"
             },
             "feedsI":{                                         // Imagens de resposta negativa, idem "feedsO"
               "imgs":[
                 {"id":8, "thickness":10, "color":"0xAA0000"},
                 {"id":9, "thickness":10, "color":"0xAA0000"},
                 {"id":10,"thickness":10, "color":"0xAA0000"}
               ],
               "timeToFeed":1500,                               // idem "feedsO"
               "defaultThickness":50,
               "defaultColor":"0xffffff"
             }
         }
     }
 }
 *
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TBinoculars
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */
function TBinoculars() {

    TExercices.call(this);

    this._drags = [];//drag of mask binoculars
    this._binocularMask = null;//image of mask of binoculars
    this._blackout = null;//image of black backgorund
    this.areaDrags = [];

    this.bg = null;//background

    this.feedsO = [];//positive feeds images bitmap
    this.timeToFeedO = 0;//time to show the negative, while the drag stay on the feedI

    this.feedsI = [];//negative feeds images bitmap
    this.timeToFeedI = 0;//time to show the negative, while the drag stay on the feedI


    this.qtdCorrects = 0;

}

TBinoculars.prototype = Object.create(TExercices.prototype);
TBinoculars.prototype.constructor = TBinoculars;
module.exports = TBinoculars;

/**
 * Metodo que inicializa a classe
 */
TBinoculars.prototype.init = function() {

    TExercices.prototype.init.call(this);

    this.configureDrags();
    var config = this._config;
    var images = this.layersDisplay.getMap('images');
    this.bg = this.layersDisplay._layers.background.children[0];
    this._blackout = this.layersDisplay.getMapByID('images', config.binoculars.bg);
    this._binocularMask = this.layersDisplay.getMapByID('images', config.binoculars.mask);

    this._mouths = [];
    for (var i = 0; i < this.layersDisplay._layers.mouths.children.length; i++) {
        this._mouths.push(this.layersDisplay._layers.mouths.children[i]);
    }
console.log(this._mouths);
    //this._mouths = this.layersDisplay//.getLayer('mouths', config.binoculars.bg);
    this.SetBinoculars(0);

    var img;
    var id;

    var thickness;
    var color;

    //set the values to array this.feedsO
    if(config.feedsO.timeToFeed){this.timeToFeedO = config.feedsO.timeToFeed;}
    for (i = 0; i < config.feedsO.imgs.length; i++) {//feedsO array

      img = GetImgByIndex(images, config.feedsO.imgs[i].id);

      if(img){//only images that exists
        id = this.feedsO.length;
        this.feedsO.push(img);

        thickness = GetValidValue(config.feedsO.defaultThickness, config.feedsO.imgs[id].thickness, false);
        color = GetValidValue(config.feedsO.defaultColor, config.feedsO.imgs[id].color, false);

        this.feedsO[id].createContour(thickness,parseInt(color));
        this.feedsO[id].hit = false;
        this.feedsO[id].timeToFeed = config.feedsO.timeToFeed;
      }
    }

      //set the values to array this.feedsI
      if(config.feedsI.timeToFeed){this.timeToFeedI = config.feedsI.timeToFeed;}
      for (i = 0; i < config.feedsI.imgs.length; i++) {//feedsI array

        img = GetImgByIndex(images, config.feedsI.imgs[i].id);

        if(img){//only images that exists
          id = this.feedsI.length;
          this.feedsI.push(img);

          thickness = GetValidValue(config.feedsI.defaultThickness, config.feedsI.imgs[id].thickness, false);
          color = GetValidValue(config.feedsI.defaultColor, config.feedsI.imgs[id].color, false);

          this.feedsI[id].createContour(thickness,parseInt(color));
          this.feedsI[id].hit = false;
          this.feedsI[id].timeToFeed = config.feedsI.timeToFeed;
        }
      }

};

/**
 * Metodo que executa a classe
 */
TBinoculars.prototype.start = function() {
    TExercices.prototype.start.call(this);
};

/**
 * Metodo que abre a classe
 */
TBinoculars.prototype.open = function() {
    TExercices.prototype.open.call(this);

    var self = this;
    var config = this._config;

};

/**
 * Metodo para o termino das sequencias
 * @private
 */
TBinoculars.prototype._initInteractions = function() {
  TExercices.prototype._initInteractions.call(this);
    var self = this;

    this._interface.once('refreshOpened', function() {

        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self.SetBinoculars(0);
            self.fadeRestart();
            self.removeDragEvents();
            self.once('sequenceComplete', function() {
                self.addDragEvents();
                self.SetBinoculars(1);
            });
        });
    });

    this._interface.openRefresh();
    this.addDragEvents();
    this.SetBinoculars(1);

};

/**
 * Metodo que adiciona eventos aos drags
 */
TBinoculars.prototype.addDragEvents = function() {for (var i = 0; i < this._drags.length; i++) {this._drags[i].addEvents();}};

/**
 * Metodo que remove eventos dos drags
 */
TBinoculars.prototype.removeDragEvents = function() {for (var i = 0; i < this._drags.length; i++) {this._drags[i].removeEvents();}};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
TBinoculars.prototype.configureDrags = function() {
    var self = this;
    var config = this._config;

    var images = this.layersDisplay.getObjects('images');
    var texts = this.layersDisplay.getObjects('text');

    this._drags = this.layersDisplay.getObjects('drag');
    var layerSup = this.layersDisplay.getLayer('sup');
    for (i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        d.id = i;
        d.setResetOnFail(false);
        d.on('positionUpdated', self.positionUpdated, self);

    }

};

/**
 * Metodo que verifica se ha colisao com feeds positivo ou negativo
 * @param e {Object} evento
 */
TBinoculars.prototype.positionUpdated = function(e) {

    var sensor = e.target.getSensor();
    var x = (e.target.x - (e.target.width/2))+sensor.x;
    var y = (e.target.y - (e.target.height/2))+sensor.y;
    var width = sensor.width;
    var height = sensor.height;

    var cFeed;
    var hit;

    //Check if has a contact with positive feed
    for (var i = 0; i < this.feedsO.length; i++) {
      cFeed = this.feedsO[i];

      hit = cFeed.hasContent(x, y, width, height) && cFeed.visible ? true : false;
      if (hit) {this.CorrectValidation(cFeed);}
      else{cFeed.timeToFeed = this.timeToFeedO;}
    }

    //Check if has a contact with negative feed
    for ( i = 0; i < this.feedsI.length; i++) {
      cFeed = this.feedsI[i];

      hit = cFeed.hasContent(x, y, width, height) && cFeed.visible ? true : false;
      if (hit) {this.WrongValidation(cFeed);continue;}
      else{cFeed.timeToFeed = this.timeToFeedI;}
    }
};

/**
 * Metodo caso haja colisao com feed positivo, se ultimo feed mostra resposta
 * @param e {Object} evento
 */
TBinoculars.prototype.CorrectValidation = function(e) {

  var self = this;

  e.timeToFeed -= 60;//60 fps

  if(!e.hit && e.timeToFeed <= 0){

      e.hit = true;
      e.timeToFeed = this.timeToFeedO;
      e.openContour();
      self.qtdCorrects += 1;
      if(self.qtdCorrects >= self.feedsO.length){//SHOW POSITIVE FEED

        self.resetDragPosition(this._drags[0], 300);
        self.correct.reset();
        self.correct.timeline.play();
        self.removeDragEvents();

        self.SetBinoculars(2);

        this._interface._refreshButton.visible = false;

        self.correct.once('pointComplete', function() {
            this._interface._refreshButton.visible = true;
            self.emit('parallax');
        }, self);

      }
    }

};

/**
* Metodo caso haja colisao com feed negativo, mostra resposta
* e retorna em estado inicial o binoculo
 * @param e {Object} evento
 */
TBinoculars.prototype.WrongValidation = function(e) {

  var self = this;

  e.timeToFeed -= 60;//60 fps

  if(!e.hit && e.timeToFeed <= 0){
    this.resetDragPosition(this._drags[0], 300);

    e.hit = true;
    e.timeToFeed = this.timeToFeedI;
    e.openContour();
    this._drags[0]._onUp();
    self.removeDragEvents();

    this._interface._refreshButton.visible = false;

    if (self.fail) {
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            this._interface._refreshButton.visible = true;
            this.emit('exercicioIncorreto');
            self.addDragEvents();
            this.fail.reset();
            e.closeContour();
            e.hit = false;
        }, self);
    } else {
      this.emit('exercicioIncorreto');
        setTimeout(function() {
            self.addDragEvents();
            this.fail.reset();
            e.closeContour();
            e.hit = false;
        }, 300);
    }

  }

};

/**
 * Metodo retorna ao ponto inicial do binoculo
 * @param e {Object} evento
 * @param time {Object} tempo
 */
TBinoculars.prototype.resetDragPosition = function(e, time) {

  setTimeout(function() {//reset the drag position

    TweenMax.killTweensOf(e);

    TweenMax.to(e.position, 1.5, {
        x: Math.round(e._initialX),
        y: Math.round(e._initialY),
        ease: Back.easeOut
    });
  }, time);

};

/**
 * Metodo para reinicializar tela
 */
TBinoculars.prototype.reset = function() {
  TExercices.prototype.reset.call(this);

this.ResetScreen();
    //uncolor

};

/**
 * Metodo para fechar tela
 */
TBinoculars.prototype.close = function() {};

/**
* Metodo para inicializar tela
 */
TBinoculars.prototype.ResetScreen = function() {

  this.qtdCorrects = 0;

  this.resetDragPosition(this._drags[0], 300);
  this.removeDragEvents();
  this._drags[0]._onUp();
  for (var i = 0; i < this.feedsO.length; i++) {this.feedsO[i].closeContour(); this.feedsO[i].hit = false;}
  for (i = 0; i < this.feedsI.length; i++) {this.feedsI[i].closeContour(); this.feedsI[i].hit = false;}
  this.SetBinoculars(0);
};

/**
 * Metodo para atribuir status atual da tela
 * @param mode [mode = 0] {number} Blackout desativar e sem imagens
 * @param mode [mode = 1] {number} Sem caráter, blackout habilitado e com imagens
 * @param mode [mode = 2] {number} Com caractere, blackout desativar e com imagens
 */
TBinoculars.prototype.SetBinoculars = function(mode){

  var i = 0;
  switch (mode) {
    case 0:

    this.SupLayer(this.bg, "background");
    for (i = 0; i < this.feedsO.length; i++) {this.SupLayer(this.feedsO[i],"images");this.feedsO[i].alpha = 0;}
    for (i = 0; i < this.feedsI.length; i++) {this.SupLayer(this.feedsI[i],"images");this.feedsI[i].alpha = 0;}

    this._drags.alpha = 0;
    this._blackout.alpha = 0;
    this._binocularMask.alpha = 0;

    this.CreateMask();

    this.SetMouthsVisible(true);

      break;
    case 1:

    this.SupLayer(this.bg, 'sup');

      for (i = 0; i < this.feedsO.length; i++) {this.SupLayer(this.feedsO[i],'sup'); this.feedsO[i].alpha = 1;}
      for (i = 0; i < this.feedsI.length; i++) {this.SupLayer(this.feedsI[i],'sup'); this.feedsI[i].alpha = 1;}

    this._drags.alpha = 0;
    this._blackout.alpha = 1;
    this._binocularMask.alpha = 1;

    this.SetMouthsVisible(false);

      break;
    case 2:

    this.SupLayer(this.bg,'background');
    for (i = 0; i < this.feedsO.length; i++) {this.SupLayer(this.feedsO[i],'sup'); this.feedsO[i].alpha = 1;}
    for (i = 0; i < this.feedsI.length; i++) {this.SupLayer(this.feedsI[i],'sup'); this.feedsI[i].alpha = 1;}

    this._drags.alpha = 0;
    this._blackout.alpha = 0;
    this._binocularMask.alpha = 0;
    this.layersDisplay._layers.sup.mask = null;

    this.SetMouthsVisible(true);

      break;
    default:

  }

};

/**
 * Metodo Configurar a máscara binocular
 */
TBinoculars.prototype.CreateMask = function(){

  var bConfig = this._config.binoculars;//binoculars config parameters

  //image of mask
   var CC = this._binocularMask;
// *** testes para a nova feature... pelo código conseguir mudar o x e y do binoculo!!!!!!!! ***
//   console.log(CC.parent.x, bConfig.x);
//   var difX = CC.parent.x; var difY = CC.parent.y;
//   CC.parent.x = GetValidValue(CC.parent.x, bConfig.x, false);difX -= CC.parent.x; CC.x -=  difX;
//   CC.parent.y = GetValidValue(CC.parent.y, bConfig.y, false);difY -= CC.parent.y; CC.y -=  difY;
//   CC.parent._initialX = CC.parent.x + difX;
//   CC.parent._initialY = CC.parent.y + difY;
//   console.log("NOVO: " + CC.parent.x + ", " + CC.parent.y);
//
   var ccX = CC.x + (CC.width/2);// +difX;
   var ccY = CC.y + (CC.height/2);// +difY;
// console.log(CC);
// console.log(ccX);
  //arc mask paremeters
  var color = bConfig.color;
  var radius = bConfig.radius;
  var angle = bConfig.angle;

  //mask sensor
  var sensor = this._drags[0].getSensor();console.log(sensor);
  var sW = sensor.width; sensor.x = ccX - (sW / 2);
  //sensor.x-=difX;
  var sH = sensor.height; sensor.y = ccY - (sH / 2);
  //sensor.y-=difY;
  //this._drags[0].drawSensor();

  if(!this._binocularMask._telaMask){
    this._binocularMask._telaMask = new PIXI.Graphics();
    this._binocularMask._telaMask.beginFill(0xFFFFFF);

    this._binocularMask._telaMask.arc(ccX-(Math.cos(angle*PIXI.DEG_TO_RAD)*radius), ccY, radius, angle*PIXI.DEG_TO_RAD, -angle*PIXI.DEG_TO_RAD, false);
    this._binocularMask._telaMask.arc(ccX+(Math.cos(angle*PIXI.DEG_TO_RAD)*radius), ccY, radius,(180+angle)*PIXI.DEG_TO_RAD,(180-angle)*PIXI.DEG_TO_RAD, false);

    this._binocularMask._telaMask.endFill();

    this._binocularMask.addChild(this._binocularMask._telaMask);

    var lastW = CC.width;
    CC.width = (radius * 4) - ( (1-Math.cos(angle * PIXI.DEG_TO_RAD)) * radius * 2);
    var difW = lastW - CC.width;
    CC.x += difW/2;

    var lastH = CC.height;
    CC.height = radius * 2;
    var difH = lastH - CC.height;
    CC.y += difH/2;

  }

  this.layersDisplay._layers.sup.mask = this._binocularMask._telaMask;console.log(this);
};

/**
 * Metodo mudar a layer da imagem
 * @param e {Object} evento
 * @param layer {Object} camada de apresentacao
 */
TBinoculars.prototype.SupLayer = function(e, layer){//console.log(e,layer);
  var sup = this.layersDisplay.getLayer(layer);
  var ob = e;
  var parent = ob.parent;
  parent.removeChild(ob);
  sup.addChild(ob);
};

/**
 * Metodo obter imagem pelo indice
 * @param images {Object}
 * @param idx {Object}
 */
function GetImgByIndex(images, idx){

  for (var i = 0; i < images.length; i++) {
    var c_img = images[i];
    if(c_img.index == idx){return c_img;}
  }

}

/**
 * Metodo exibe/inibe as bocas
 * @param v {boolean} tornar visivel/invisivel
 */
TBinoculars.prototype.SetMouthsVisible = function(v){
for (var i = 0; i < this._mouths.length; i++) {
    this._mouths[i].visible = v;
}
};

/**
 * Metodo para verificar e atribuir ou nao o valor default
 * @param default_value {number} Valor se não tiver um valor individual (c_value)
 * @param c_value {number} valor individual
 * @param blank {number} Se não tiver c_value, retornar "", sem teste default_value
 */
function GetValidValue(default_value, c_value, blank) {
    if ( !(IsValue(c_value)) ) {
        if ( !( IsValue(default_value) ) || blank) { return ""; }
        else { return default_value; }
    }
    return c_value;
}

/**
 * Metodo verificar se é um valor
 * @param e {string}
 */
function IsValue(e) {return e !== "" && typeof e !== "undefined" ? true : false;}
