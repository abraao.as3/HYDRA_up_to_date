var TExercices = require('./TExercices');

/**
 * configuração do template:
 * @example
 *
"config": {
     "onHitValidation":false,
     "text": {
         "pt-BR": [{
             "text": "GUA"
         }, {
             "text": "GUI"
         }, {
             "text": "GUE"
         }, {
             "text": "GUA"
         }, {
             "text": "GUE"
         }, {
             "text": "GUA"
         }, {
             "text": "GUE"
         }, {
             "text": "GUI"
         }, {
             "text": "GUI"
         }, {
             "text": "GUA"
         }, {
             "text": "Dessa vez, vamos pintar uma figura conforme a cor solicitada para cada sílaba."
         }, {
             "text": "Ficou lindo!"
         }]
     },
     "audio": {
         "pt-BR": [{
             "name": "7_1",
             "id": 0
         }, {
             "name": "7_2",
             "id": 1
         }, {
             "name": "7_2",
             "id": 2
         }, {
             "name": "7_2",
             "id": 3
         }, {
             "name": "7_2",
             "id": 4
         }, {
             "name": "7_2",
             "id": 5
         }, {
             "name": "7_2",
             "id": 6
         }]
     },
     "sequences": [{
         "id": 0,
         "img": 4,
         "mouth": [0],
         "txt": [8],
         "audio": 0,
         "fadeOut": false
     }],
     "drags": [{
             "img": [4],
             "color": "0xff0000",
             "layerDisplacement": 1,
             "sensor": {
                 "x": 100,
                 "y": 50,
                 "width": 10,
                 "height": 10,
                 "show": false
             }

         }, {
             "img": [5],
             "color": "0x7711cc",
             "layerDisplacement": 1,
             "sensor": {
                 "x": 10,
                 "y": 10,
                 "width": 10,
                 "height": 10,
                 "show": true
             }
         }, {
             "img": [6],
             "color": "0x00ff00",
             "layerDisplacement": 1,
             "sensor": {
                 "x": 10,
                 "y": 10,
                 "width": 10,
                 "height": 10,
                 "show": true
             }
         }

     ],
     "tints":[
         {
             "img":7,
             "correct": true,
             "correctColor": "0xff0000"
         },{
             "img":13,
             "correct": true,
             "correctColor": "0xff0000"
         },{
             "img":14,
             "correct": true,
             "correctColor": "0xff0000"
         },{
             "img":15,
             "correct": true,
             "correctColor": "0xff0000"
         },{
             "img":16,
             "correct": true,
             "correctColor": "0xff0000"
         },{
             "img":17,
             "correct": true,
             "correctColor": "0xff0000"
         },{
             "img":18,
             "correct": true,
             "correctColor": "0xff0000"
         }
     ],
     "response": {
         "fail": {
             "images": [9],
             "txt": [11],
             "audio": 1,
             "mouth": [0]
         },
         "correct": {
             "images": [9],
             "txt": [11],
             "audio": 2,
             "mouth": [0]
         }
     }
}
 *
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TBucket
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */

function TBucket() {
    TExercices.call(this);

    this._drags = [];
    this.areaDrags = [];
}

TBucket.prototype = Object.create(TExercices.prototype);
TBucket.prototype.constructor = TBucket;
module.exports = TBucket;


TBucket.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this.configureDrags();

};

TBucket.prototype.start = function() {
    TExercices.prototype.start.call(this);
};


TBucket.prototype.open = function() {
    TExercices.prototype.open.call(this);

    //this.layersDisplay.getObjects('images')[0].tint = '0xffff00';
};

TBucket.prototype._initInteractions = function() {
    var self = this;

    TExercices.prototype._initInteractions.call(this);

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._blocked();
            self.fadeRestart();
            self.removeDragEvents();
            self.on('sequenceComplete', function() {
                self._unblock();
                self.addDragEvents();
            });
        });
    });

    this._interface.openRefresh();

    this._unblock();
    this._block = false;

    this.addDragEvents();
};

/**
 * Metodo que adiciona eventos aos drags
 */
TBucket.prototype.addDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].addEvents();
    }
};

/**
 * Metodo que remove eventos dos drags
 */
TBucket.prototype.removeDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].removeEvents();
    }
};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
TBucket.prototype.configureDrags = function() {
    var self = this;
    var config = this._config;

    var images = this.layersDisplay.getObjects('images');
    var texts = this.layersDisplay.getObjects('text');

    this.tints = [];
    for (var i = 0; i < config.tints.length; i++) {

        if (config.tints[i].img !== undefined) {
            for (var j = 0; j < images.length; j++) {

                if (config.tints[i].img == images[j].index) {
                    images[j].correct = config.tints[i].correct;
                    images[j].correctColor = config.tints[i].correctColor;
                    this.tints.push(images[j]);
                }
            }
        }
        if (config.tints[i].text !== undefined) {
            for (var h = 0; h < texts.length; h++) {

                if (config.tints[i].text == texts[h].index) {
                    texts[h].correct = config.tints[i].correct;
                    texts[h].correctColor = config.tints[i].correctColor;
                    this.tints.push(texts[h]);


                }
            }
        }
    }

    this._drags = this.layersDisplay.getObjects('drag');
    var layerSup = this.layersDisplay.getLayer('sup');
    for (i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        var colisoes = [];

        d.id = i;
        d.tints = this.tints;
        d.color = config.drags[i].color;

        d.setResetOnFail(true);
        d.on('dragReleased', self.tint);
        d.on('tint', self.validation, self);
        d.parent.removeChild(d);
        layerSup.addChild(d);

    }
};




TBucket.prototype.tint = function(e) {

    var sensor = this.getSensor();

    var x = sensor.x + this.x - this.width / 2;
    var y = sensor.y + this.y - this.height / 2;
    var width = sensor.width;
    var height = sensor.height;

    for (var i = 0; i < this.tints.length; i++) {

        if (this.tints[i]._displayType != "type") {

            if (this.tints[i].hasContent(x, y, width, height)) {
                this.tints[i].tint = this.color;
                this.emit("tint");
            }

        } else {

            //compare if the sensor is inside the textbox
            if (x >= this.tints[i].x &&
                x + width <= this.tints[i].x + this.tints[i].width &&
                y >= this.tints[i].y &&
                y + height <= this.tints[i].y + this.tints[i].height) {
                this.tints[i].tint = this.color;
                this.emit("tint");
            }

        }
    }

};


TBucket.prototype.validation = function() {

    var self = this;
    var correto = true;

    self.removeDragEvents();

    if (this._config.onHitValidation) {

        for (var j = 0; j < self.tints.length; j++) {

            // se o tint for correto e a cor diferente da correta ou branco/neutro, ou se o campo for incorreto e tiver sido pintado da feed negativo
            if ((self.tints[j].tint !== self.tints[j].correctColor && self.tints[j].correct &&
                    (self.tints[j].tint != 16777215 || self.tints[j].tint != '0xffffff')) ||
                (!self.tints[j].correct && (self.tints[j].tint != 16777215 || self.tints[j].tint != '0xffffff'))) {

                //feed Negativo
                self.openFail();

            } else {

                //se não tiver nenhum campo sem pintar da feed positivo
                if (self.tints[j].tint == 16777215 || self.tints[j].tint == '0xffffff') {
                    correto = false;
                }

            }
        }

        if (correto) {
            self.openCorect();
        }

    } else {

        var validate = true;

        for (var i = 0; i < self.tints.length; i++) {

            if (self.tints[i].tint == 16777215 || self.tints[i].tint == '0xffffff') {
                validate = false;
            }
        }

        //se todos os tints estiverem pintados, validar
        if (validate) {

            for (i = 0; i < self.tints.length; i++) {

                if ((self.tints[i].tint !== self.tints[i].correctColor && self.tints[i].correct) ||
                    (!self.tints[i].correct && self.tints[i].tint != 16777215 && self.tints[i].tint != '0xffffff')
                ) {
                    correto = false;
                }
            }
            if (correto) {
                self.openCorect();
            } else {
                self.openFail();
            }

        } else {
            self.addDragEvents();
        }
    }

};


TBucket.prototype.openCorect = function() {

    var self = this;

    if (self.correct) {
        self.correct.reset();
        self.correct.timeline.play();
        self._blocked();
        self.correct.once('pointComplete', function() {
            for (var l = 0; l < self._drags.length; l++) {
                self._drags[l]._parallaxX = self._drags[l].x;
                self._drags[l]._parallaxY = self._drags[l].y;
            }
            self._blocked();
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            for (var l = 0; l < self._drags.length; l++) {
                self._drags[l]._parallaxX = self._drags[l].x;
                self._drags[l]._parallaxY = self._drags[l].y;
            }
            self.checkRoadMethod();
        }, 1000);
    }

};

TBucket.prototype.openFail = function() {

    var self = this;

    self.uncolorWrong();

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self._blocked();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            self._unblock();
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();
            self.addDragEvents();
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._unblock();
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
            self.addDragEvents();
        }, 1000);
    }

};

TBucket.prototype.uncolorWrong = function() {
    var self = this;
    for (var j = 0; j < self.tints.length; j++) {
        if ((self.tints[j].tint !== self.tints[j].correctColor && self.tints[j].correct && (self.tints[j].tint != 16777215 || self.tints[j].tint != '0xffffff')) ||
            (!self.tints[j].correct && (self.tints[j].tint != 16777215 || self.tints[j].tint != '0xffffff'))) {
            self.tints[j].tint = '0xffffff';
        }
    }



};

TBucket.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }

    this._interface.openRefresh();
};

TBucket.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

TBucket.prototype.reset = function() {

    TExercices.prototype.reset.call(this);

    var self = this;

    for (var j = 0; j < self.tints.length; j++) {

        self.tints[j].tint = '0xffffff';

    }

    self._blocked(true);

};


TBucket.prototype.close = function() {

};
