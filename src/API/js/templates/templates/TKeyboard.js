var ObjectBase = require('../../core/ObjectBase');
var ITemplate = require('../core/ITemplate');
var $ = require('jquery');

/**
* @classdesc TKeyboard Class
* @memberof module:templates/templates
* @exports TKeyboard
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @constructor
*/
function TKeyboard() {
    ObjectBase.call(this);

    this._inputs = [];
    this._focussedInput = null;
    this._keyboard = this._interface.keyboard;
}

TKeyboard.prototype = Object.create(ObjectBase.prototype);
TKeyboard.prototype.constructor = TKeyboard;
module.exports = TKeyboard;

/** @function
* @description init
*/
TKeyboard.prototype.init = function() {
    var self = this;

    var fields = this.layersDisplay.getObjects("interactions").filter(function(elem) {
        return elem._displayType == "typeInput";
    });


    fields.forEach(function(elem) {
        var input = {
            component: elem,
            text: ""
        };
        self._inputs.push(input);
    });

};

/** @function
* @description open
*/
TKeyboard.prototype.open = function() {
    this.addChild(this._keyboard);
};

/** @function
* @description _initInteractions
* @private
*/
TKeyboard.prototype._initInteractions = function() {
    var self = this;

    this._interface.once('refreshOpened', function(f) {

        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function(e) { // funcionalidade do botão de "replay" do enunciado
            self._interface._refreshButton.removeEvents();

            self.fadeRestart();
            self.removeInputEvents();
            self.removeKeyboardEvents();
            self._focussedInput.component.hideCursor();
            self.once('sequenceComplete', function() {
                if (self._corrects !== undefined) {
                    var inputs = self._inputs.filter(function(input) {
                        return self._corrects.indexOf(input) < 0; // não existe no array de acertos
                    });
                    self.addInputEvents(inputs);
                } else {
                    self.addInputEvents();
                }

                self.addKeyboardEvents();
                self._interface._refreshButton.addEvents();

                self._focussedInput.component.showCursor();
            });
        });
    });

    this._interface.openRefresh();

    self.addInputEvents();

    self._focussedInput = self._inputs[0];
    self._focussedInput.component.showCursor();

    if (this._config.softKeyboard) {
        this._keyboard.showWithAnimation();

        setTimeout(function() { // aguarda o fim da animação do teclado para adicionar os listeners
            self.addKeyboardEvents();
        }, (2.3 * 1000));
    } else {
        self.addKeyboardEvents();
        $(window).on('keydown', function(e) {
            TKeyboard.prototype._hardKeyboard.call(self, e);
        });
    }
};

/** @function
* @description _focus
* @private
*/
TKeyboard.prototype._focus = function(e) {
    var self = this;
    var target = e.target;

    if (target.index != self._focussedInput.component.index) {
        var ipt = self._inputs.find(function(inpt) {
            return inpt.component.index == target.index;
        }, this);

        self._focussedInput.component.hideCursor();
        self._focussedInput = ipt;
        self._focussedInput.component.showCursor();
    }
};

/** @function
* @description _keyPressed
* @private
*/
TKeyboard.prototype._keyPressed = function(e) {
    var self = this;
    var textValue = self._focussedInput.text;
    var textMaxLength = null;

    if (e.value.match(/backspace/i) !== null) {
        textValue = textValue.slice(0, self._focussedInput.text.length - 1);
    } else if ((self._focussedInput.component.config.maxLength !== undefined && textValue.length < self._focussedInput.component.config.maxLength) || (self._focussedInput.component.config.correct !== undefined && textValue.length < self._focussedInput.component.config.correct.length)) {
        textValue += e.value;
    }

    if (self._focussedInput.text != textValue) {
        self._focussedInput.text = textValue;
        self._focussedInput.component.setText(textValue);
    }
};

/** @function
* @description addInputEvents
*/
TKeyboard.prototype.addInputEvents = function(arrInputs) {
    var self = this;

    var inputs = arrInputs || self._inputs;

    inputs.forEach(function(input) {
        input.component.addEvents();
        input.component.on("focus", self._focus, self);
    });
};

/** @function
* @description removeInputEvents
*/
TKeyboard.prototype.removeInputEvents = function(arrInputs) {
    var self = this;

    var inputs = arrInputs || self._inputs;

    inputs.forEach(function(input) {
        input.component.removeEvents();
        input.component.field.removeListener('focus', self._focus);
    });
};

/** @function
* @description addKeyboardEvents
*/
TKeyboard.prototype.addKeyboardEvents = function() {
    var self = this;

    this._keyboard.on('keyPressed', self._keyPressed, self);
};

/** @function
* @description removeKeyboardEvents
*/
TKeyboard.prototype.removeKeyboardEvents = function() {
    var self = this;

    self._keyboard.removeListener('keyPressed', self._keyPressed);
};

TKeyboard.prototype._hardKeyboard = function(e) {
	console.log(e.key.match(/arrow/i));
    if (
		e.key.match(/arrow/i) === null &&
		e.key.match(/control/i) === null &&
		e.key.match(/shift/i) === null &&
		e.key.match(/dead/i) === null &&
		e.key.match(/alt/i) === null
	) {
		var value = e.key;
		if(value.match(/enter/i) !== null){
			value = "\n"
		}
        this._keyboard.emit('keyPressed', {value: value});
    }
}

/** @function
* @description close
*/
TKeyboard.prototype.close = function() {
    ITemplate.prototype.close.call(this);

    var self = this;
    self._focussedInput.component.hideCursor();
    this._keyboard.hide();
    self.removeInputEvents();
    self.removeKeyboardEvents();
    this._interface._refreshButton.removeAllListeners("clicked");
    this._interface.refreshButton.removeEvents();
	$(window).off('keydown');

};

/** @function
* @description reset
*/
TKeyboard.prototype.reset = function() {

    this._inputs.forEach(function(input) {
        input.text = "";
        input.component.setText(input.text);
    });

};
