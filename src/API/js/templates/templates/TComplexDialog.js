var ITemplate = require('../core/ITemplate');
var behavior = require('../../behavior/index');

/**
 * @classdesc Classe que define o Template de dialogo complexo
 * @memberof module:templates/templates
 * @exports TComplexDialog
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @see module:animation.Point
 * @constructor
 */

function TComplexDialog() {
    ITemplate.call(this);

    this._end = false;
    this._countAudioExtra = 0;
}

TComplexDialog.prototype = Object.create(ITemplate.prototype);
TComplexDialog.prototype.constructor = TComplexDialog;
module.exports = TComplexDialog;


TComplexDialog.prototype.init = function() {
    ITemplate.prototype.init.call(this);

    //this.setSequencies(this._config.sequences, this._SoundManager);

    var self = this;



};

TComplexDialog.prototype.start = function() {
    ITemplate.prototype.start.call(this);

    var self = this;

};

TComplexDialog.prototype._firstFade = function() {

    var self = this;

};

TComplexDialog.prototype.open = function() {

    ITemplate.prototype.open.call(this);

    var self = this;
    self._initInteractions();

};

TComplexDialog.prototype.reset = function() {
    ITemplate.prototype.reset.call(this);

    this._fadeAtual = 0;
    //resetando os pontos
    for (var i = 0; i < this._pointController.length - 1; i++) {
        var fp = this._pointController[i];
        fp.fadeStop();
    }

    //resetando os botões
    for (i = 0; i < this._interactionButtons.length; i++) {
        var b = this._interactionButtons[i];
        // b.removeListener('mousedown');
        b._blocked = false;
        // b._block = false;
    }

    this._end = false;
};

TComplexDialog.prototype._initInteractions = function() {
    ITemplate.prototype._initInteractions.call(this);

    this._autorizeClick = false;

    var self = this;

    //Como os pontos sao gerenciados manualmente no dialogo complexo preciso carregar os points um tempo depois do open, ja que nao existe uma sequencia principal pra triggerar o sequenceComplete do _initInteractions
    setTimeout(function() {
        for (var i = 0; i < self._pointController.length; i++) {
            if (self._pointController[i].id === 0) {

                self._fadeAtual = 0;

                if (self._config.pointHistory) {
                    self._pointTimeline = [];
                }
                var tmpEvents = self._events;

                self.addPlugin(self._pointController[i]);

                self._events = tmpEvents;

                for (var l = 0; l < self._config.sequences.length; l++) {

                    if (self._config.sequences[l].idSeq === 0) {

                        for (var m = 0; m < self._config.sequences[l].order.length; m++) {

                            var idPoint = self._config.sequences[l].order[m];

                            self._points[idPoint].play();

                            if (m == (self._config.sequences[l].order.length - 1)) {

                                if (!self._points[idPoint].listeners('pointStarted', true)) {
                                    self._points[idPoint].once('pointStarted', self.onPointStarted, {
                                        self: self,
                                        id: idPoint
                                    });
                                }
                            }
                        }
                    }
                }

            }
        }

        self._autorizeClick = true;

        if (self._config.showPointOnInitById !== undefined) {
            if (self._interactionButtons !== undefined && self._interactionButtons !== null) {
                self._waitForButtons();
            }
            else{
                // self.once('buttonsConfigured', self._waitForButtons, {ctx: self});
                self._enterframe = setInterval(function(){
                    console.log("enterframe");
                    if(self.buttonsConfigured){
                        clearInterval(self._enterframe);
                        self._waitForButtons();
                    }
                }, 100);
            }
        }

    }, 100);
};

TComplexDialog.prototype._waitForButtons = function(e){
    var bt = this._interactionButtons[this._config.showPointOnInitById];
    bt.emit('mousedown', {target: bt});
};

TComplexDialog.prototype.onPointStarted = function(e) {
    var self = this.self;
    idPoint = this.id;

    self._currentPointInteractions(self._config.points[idPoint]);
};

TComplexDialog.prototype._currentPointInteractions = function(point) {

    var self = this;

    for (var i = 0; i < point.interactions.length; i++) {

        if (point.interactions[i].type === "clickable") {

            self._preparePointActions(point.interactions[i]);

        }
    }

    this.buttonsConfigured = true;
};

TComplexDialog.prototype._verifyProgress = function() {

    var self = this;
    var passouTeste = true;

    var tmpInts = self.layersDisplay.getLayer('interactions');

    for (var i = 0; i < tmpInts.children.length; i++) {

        if (!tmpInts.children[i]._clicked) {
            passouTeste = false;
        }

    }

    if (passouTeste) {
        this.checkRoadMethod();
    }

};

TComplexDialog.prototype._preparePointActions = function(int) {

    var self = this;

    //por enquanto eu so pego a primeira imagem dos botoes para ativar os eventos
    var imgId = int.images[0];
    var button = self.layersDisplay.getMapByID('images', imgId);

    if (this._interactionButtons === undefined || this._interactionButtons === null) {
        this._interactionButtons = [];
    }

    this._interactionButtons.push(button);

    if (!button._block) {
        // button.addEvents();
        button.interactive = true;
        button.on('mousedown', this._handleIntMouse, {
            ctx: this
        });
        button.on('touchstart', this._handleIntMouse, {
            ctx: this
        });

        button.once('shutInteractions', this._destroyEvents, {
            ctx: this,
            bitmap: int
        });
    }

};
