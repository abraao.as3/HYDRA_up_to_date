var TExercices = require('./TExercices');
var behavior = require('../../behavior/index');
var TweenMax = require('TweenMax');

/**
 * @classdesc THover Class
 * @memberof module:templates/templates
 * @exports THover
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */
function THover() {
    TExercices.call(this);

    this._drags = [];
    this.areaDrags = [];

}

THover.prototype = Object.create(TExercices.prototype);
THover.prototype.constructor = THover;
module.exports = THover;

/** @function
* @description init
*/
THover.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this.configureHover();

};

/** @function
* @description start
*/
THover.prototype.start = function() {
    TExercices.prototype.start.call(this);
};

/** @function
* @description open
*/
THover.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

/** @function
* @description _initInteractions
* @private
*/
THover.prototype._initInteractions = function() {
    var self = this;

    TExercices.prototype._initInteractions.call(this);

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self.fadeRestart();
            self.removeHoverEvents();
            self.on('sequenceComplete', function() {
                self.addHoverEvents();
            });
        });
    });

    this._interface.openRefresh();

    this.addHoverEvents();
};

/**
 * Metodo que adiciona eventos aos hovers
 */
THover.prototype.addHoverEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].addEvents();
    }
};

/**
 * Metodo que remove eventos dos hovers
 */
THover.prototype.removeHoverEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].removeEvents();
    }
};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
THover.prototype.configureHover = function() {

    var self = this;

    //var layer = this.layersDisplay.getObjects('text');

    var config = this._config;

    this._drags = this.layersDisplay.getObjects('drag');
    this.areaDrags = this.layersDisplay.getObjects('areaDrag');

    this._obstacles = [];

    for (var i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        var colisoes = [];

        d.id = i;
        d._countCorrects = 0;
        d._complete = false;

        d._minY = config.drags[i].minY || -1;
        d._maxY = config.drags[i].maxY || -1;
        d._minX = config.drags[i].minX || -1;
        d._maxX = config.drags[i].maxX || -1;
        d.addPlugin(new behavior.Sensor(config.drags[i].sensorPos.x,
                                         config.drags[i].sensorPos.x,
                                         config.drags[i].sensorPos.width,
                                         config.drags[i].sensorPos.height));

        if (config.drags[i].showSensor) {
            d.drawSensor();
        }

        if (config.drags[i].receptors !== "all") {

            for (var j = 0; j < config.drags[i].receptors.length; j++) {
                colisoes.push(this.areaDrags[config.drags[i].receptors[j]]);
            }

            d.setReceptors(colisoes);

        } else {

            d.setReceptors(this.areaDrags);

        }

        d.setUsePoint(config.drags[i].usePoint);
        d.setIsAdjust(config.drags[i].adjust);
        d.setResetOnFail(config.drags[i].resetOnFail);
        d.setIsHover(config.drags[i].isHover);

        d.removeListener('correct');

        d.on('colision', self.hoverColisions, self);
        d.on('positionUpdated', self.updateSensor, self);
        d.on('apagouTudo', self.verify, {hover:d, context:self});
    }

};

/**
 * Metodo de controle de drags em colisao com o Hover
 */
THover.prototype.hoverColisions = function(elements) {

    var drag = elements.drag;
    var object = elements.object;
    var self = this;
    var objIdx = 0;

    var contato = object.hasContent(
        drag._sensor.x,
        drag._sensor.y,
        drag._sensor.width,
        drag._sensor.height
    );

    if (contato) {


            if (object.alpha > 0) {
                object.alpha -= 0.03;
                if (object.alpha <= 0.10) {

                    objIdx = drag._obstacles.indexOf(object);
                    if (objIdx > -1) {

                        var scale = object.scale;
                        var position = object.position;
                        var xPos = object.x - object.width/2;
                        var yPos = object.y - object.height/2;

                        TweenMax.to(object, 3,{alpha:0 });
                        TweenMax.to(position, 0.5,{x:xPos , y: yPos});
                        TweenMax.to(scale, 0.5,{ x:3, y:3 });

                        drag._obstacles.splice(objIdx, 1);

                        if (drag._obstacles.length <= 0) {
                            drag.emit('apagouTudo');
                        }
                    }

                }
             }
    }


};

/**
 * Atualiza as posicoes do sensor para poder ser utilizado na checagem de colisao
 */
THover.prototype.updateSensor = function(elements) {

    var sensor = elements.target._sensor;
    var x = elements.xDif;
    var y = elements.yDif;

    sensor.x = x;
    sensor.y = y;

};

/**
 * Faz o controle para ver se todos os areas drag estão preenchidos
 */
THover.prototype.stopOnColision = function(elements) {

    var drag = elements.drag;
    var object = elements.object;

    drag._moveEnabled = false;
    drag.removeEvents();

    this.openFail();
};


/**
 * Valida todas as areas drag e toma a decisão de avancar a tela ou nao
 * @fires parallax Evento que libera a tela para avancar
 * @fires avancarTela Evento que avanca automaticamente a tela
 */
THover.prototype.validateHover = function() {

    var self = null;
    if (this.context !== undefined) self = this.context;
    else self = this;

    var advance = true;
    var drags = this._drags;

    for (var i = 0; i < drags.length; i++) {
        if (!drags[i]._complete) {
            advance = false;
        }

    }

    if (advance) self.openCorect();

};

/** @function
* @description openCorect
*/
THover.prototype.openCorect = function() {

    var self = this;

    self.removeHoverEvents();
    for (var i = 0; i < self._drags.length; i++) {
        self._drags[i].resetPosition();
        self._drags[i].emit('dragReleased');
    }

    if (self.correct) {
        self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {

            for (var l = 0; l < self._drags.length; l++) {
                self._drags[l]._parallaxX = self._drags[l].x;
                self._drags[l]._parallaxY = self._drags[l].y;
            }

            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

/** @function
* @description openFail
*/
THover.prototype.openFail = function() {

    var self = this;
    self.removeEvents();

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();
            self.addHoverEvents();
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
            self.addHoverEvents();
        }, 1000);
    }

};

/** @function
* @description reset
*/
THover.prototype.reset = function() {
    var self = this;

    TExercices.prototype.reset.call(this);

    for (var l = 0; l < self._drags.length; l++) {

        for (var i = 0; i < self._config.drags[l].obstacles.img.length; i++) {

            var tmpImg = self._config.drags[l].obstacles.img[i];


            tmpImg = self.layersDisplay.getMapByID('images', tmpImg);

            if (tmpImg.alpha <= 0) {

                tmpImg.alpha = 1;
                tmpImg.visible = true;
                tmpImg.scale.set(1,1);
                tmpImg.x = tmpImg.x + tmpImg.width/2;
                tmpImg.y = tmpImg.y + tmpImg.height/2;
                self._drags[l]._obstacles.push(tmpImg);
            }


        }
        self._drags[l]._complete = false;
        self._drags[l]._parallaxX = self._drags[l]._initialX;
        self._drags[l]._parallaxY = self._drags[l]._initialY;
    }

};

/** @function
* @description verify
*/
THover.prototype.verify = function() {

    var self = null;
    if (this.context !== undefined) self = this.context;
    else self = this;

    this.hover._complete = true;

    self.validateHover();

};

/** @function
* @description close
*/
THover.prototype.close = function() {

    TExercices.prototype.close.call(this);

    var self = this;

    for (var l = 0; l < self._drags.length; l++) {

        self._drags[l].resetPosition();
        self._drags[l]._moveEnabled = false;
    }

};
