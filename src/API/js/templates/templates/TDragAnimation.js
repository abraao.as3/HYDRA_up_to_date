var TExercices = require('./TExercices');
var TweenMax = require('TweenMax');



function TDragAnimation() {
    TExercices.call(this);

    this._drags = [];
    this.areaDrags = [];
    this.contador = 0;
}

TDragAnimation.prototype = Object.create(TExercices.prototype);
TDragAnimation.prototype.constructor = TDragAnimation;
module.exports = TDragAnimation;

TDragAnimation.prototype.init = function() {
    TExercices.prototype.init.call(this);
    this.telaMovies = this.layersDisplay.getObjects('movieClip');

    var self = this;


    var outAfterEx = this._config.outAfterEx;

    var selfImages = this.layersDisplay.getMap('images');
    var selfText = this.layersDisplay.getMap('text');
    this._itensForClose = [];

    for(var za = 0; za < outAfterEx.images.length; za++)
    {
        for(var ya = 0;ya < selfImages.length; ya++)
        {
            if(outAfterEx.images[za] == selfImages[ya].index)
            {
                this._itensForClose.push(selfImages[ya]);
                break;
            }
        }

    }

    for(var wa = 0; wa < outAfterEx.text.length; wa++)
    {
        for(var ta = 0;ta < selfText.length; ta++)
        {
            if(outAfterEx.text[wa] == selfText[ta].index)
            {
                this._itensForClose.push(selfText[ta]);
                break;
            }
        }

    }
    for(var a = 0;a < this.telaMovies.length; a++){

    this.telaMovies[a].visible = false;


    }

    this.configureDrags();



};

TDragAnimation.prototype.start = function() {
    TExercices.prototype.start.call(this);
    this.fadeStart();
};


TDragAnimation.prototype.open = function() {
    TExercices.prototype.open.call(this);

    var self = this;
    this.fadeOpen();
};

TDragAnimation.prototype._initInteractions = function() {
    var self = this;

    TExercices.prototype._initInteractions.call(this);


    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._interface.closeRefresh();
            self.fadeRestart();
            self.telaMovies[0].gotoAndStop(0);
            self.removeDragEvents();
            self.on('sequenceComplete', function() {
                self.addDragEvents();
                self._interface.openRefresh();
                    //  this.telaMovies[0].play();

            });
        });
    });

    this._interface.openRefresh();



//    this.playMovie();

    this.addDragEvents();
};

/**
 * Metodo que adiciona eventos aos drags
 */

TDragAnimation.prototype.playMovie = function(){

     var self = this;



     this.telaMovies[this.contador].onComplete = function(){

         self.contador++;

         if (self.telaMovies.length <= self.contador) {
             self.onMovieComplete(self);
         }else{
             self.playMovie();
         }

     };
     this.telaMovies[this.contador].play();
 };

TDragAnimation.prototype.onMovieComplete = function(self){

     self._interface.closeRefresh();
      self.checkRoadMethod();
      //self.emit("parallax");

 };

TDragAnimation.prototype.addDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].addEvents();
    }
};

/**
 * Metodo que remove eventos dos drags
 */
TDragAnimation.prototype.removeDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].removeEvents();
    }
};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
TDragAnimation.prototype.configureDrags = function() {

    var self = this;

    //var layer = this.layersDisplay.getObjects('text');

    var config = this._config;

    this._drags = this.layersDisplay.getObjects('drag');
    this.areaDrags = this.layersDisplay.getObjects('areaDrag');

    this._obstacles = [];

    for (var i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        var colisoes = [];

        d.id = i;

        if (config.drags[i].receptors !== "all") {


            for (var j = 0; j < config.drags[i].receptors.length; j++) {
                colisoes.push(this.areaDrags[config.drags[i].receptors[j]]);
            }

            d.setReceptors(colisoes);

        } else {

            d.setReceptors(this.areaDrags);

        }


        d.setUsePoint(config.drags[i].usePoint);
        d.setIsAdjust(config.drags[i].adjust);
        d.setResetOnFail(config.drags[i].resetOnFail);

        d.removeListener('correct');
        d.on('correct', self.dragHit, self);
        if (config.failOnColision) d.on('colision', self.stopOnColision, self);
    }


    for (i = 0; i < config.areaDrags.length; i++) {

        var a = this.areaDrags[i];

        a.index = i;
        a.alpha = config.areaDrags[i].alpha;

        if (config.areaDrags[i].maxChildren !== undefined) {
            a.maxChildren = config.areaDrags[i].maxChildren;
        } else {
            a.maxChildren = this._drags.length;
        }

        a.disableDragOnHit = config.areaDrags[i].disableDragOnHit;
        a.corrects = config.areaDrags[i].corrects;
        a.alignMode = config.areaDrags[i].alignMode;
        a.vspace = config.areaDrags[i].vspace;
        a.hspace = config.areaDrags[i].hspace;
        //a._lockArea = false;

        if (config.areaDrags[i].alignMode === 'person') {
            a._points = config.areaDrags[i].points;
        }


        if (a.alignMode == "grid") {
            a.createGrid(config.areaDrags[i].createGrid[0], config.areaDrags[i].createGrid[1]);
        }
    }


};



/**
 * Faz o controle para ver se todos os areas drag estão preenchidos
 */
TDragAnimation.prototype.stopOnColision = function(elements) {

    var drag = elements.drag;
    var object = elements.object;

    drag._moveEnabled = false;
    drag.removeEvents();

    this.openFail();
};

/**
 * Faz o controle para ver se todos os areas drag estão preenchidos
 */
TDragAnimation.prototype.dragHit = function() {


    var validateDrags = true;

    for (var i = 0; i < this.areaDrags.length; i++) {
        if (this.areaDrags[i]._containerDrags.length !== this.areaDrags[i].maxChildren) {
            validateDrags = false;
        }
    }

    if (validateDrags) {
        this.removeDragEvents();
        this._interface._refreshButton.removeEvents();
        this._interface.closeRefresh();
        this.validateDrags();
    }

};

/**
 * Valida todas as areas drag e toma a decisão de avancar a tela ou nao
 * @fires parallax Evento que libera a tela para avancar
 * @fires avancarTela Evento que avanca automaticamente a tela
 */
TDragAnimation.prototype.validateDrags = function() {

    var self = null;

    if (this.context !== undefined) self = this.context;
    else self = this;

    var advance = true;


    if (self._config.resetAllOnFail !== undefined && self._config.resetAllOnFail === true) {
        for (var i = 0; i < this.areaDrags.length; i++) {

            if (!self.areaDrags[i].checkCorrect()) {
                advance = false;
                self.areaDrags[i].clear();
            }
        }
    } else {
        for (var j = 0; j < self.areaDrags.length; j++) {

            if (!self.areaDrags[j].checkCorrect()) {
                advance = false;
                self.areaDrags[j].clearIncorrect();
                self.areaDrags[j]._align();
            }
        }
    }


    if (advance === true) self.openCorect();
    else self.openFail();

};


TDragAnimation.prototype.openCorect = function() {

    var self = this;

    if (self.correct) {
        for(var a = 0;a < this.telaMovies.length; a++){

        this.telaMovies[a].visible = true;
        this._drags[a].visible = false;
    }

        self.correct.reset();
        self.playMovie();
        self.telaMovies[0].play();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {

            for (var l = 0; l < self._drags.length; l++) {
                self._drags[l]._parallaxX = self._drags[l].x;
                self._drags[l]._parallaxY = self._drags[l].y;
            }
            this.checkRoadMethod();
            //self.emit("parallax");
        }, self);
    } else {
        for(var c = 0;c < this.telaMovies.length; c++){

        this.telaMovies[c].visible = true;

    }
    for (var d = 0; d < this._drags.length; d++){
        this._drags[d].visible = false;
    }
        TweenMax.to(self._itensForClose, 0.2, {alpha:0});
        this.playMovie();
        this.telaMovies[0].play();

        // setTimeout(function() { self.emit("parallax");}, 1000);


    }
};

TDragAnimation.prototype.openFail = function() {

    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();
            self.addDragEvents();
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
            self.addDragEvents();
        }, 1000);
    }

};

TDragAnimation.prototype.reset = function() {

    TExercices.prototype.reset.call(this);

    var self = this;
    this.contador = 0;

    for(var a = 0;a < this.telaMovies.length; a++){

        this.telaMovies[a].visible = false;
         self.telaMovies[a].gotoAndStop(0);
    }
    //TweenMax.to(self.telaMovies[0], 0, {alpha: 0});
    TweenMax.to(self._itensForClose, 0.2, {alpha:1});
    for(var b = 0; b< this._drags.length; b++){
        this._drags[b].visible = true;
    }

    for (var i = 0; i < self.areaDrags.length; i++) {

        self.areaDrags[i].clear();

    }

    for (var l = 0; l < self._drags.length; l++) {
        self._drags[l]._parallaxX = self._drags[l]._initialX;
        self._drags[l]._parallaxY = self._drags[l]._initialY;
    }

};

TDragAnimation.prototype.verify = function() {

    var self = null;

    if (this.context !== undefined) self = this.context;
    else self = this;


    self.removeDragEvents();

    self.validateDrags();

};


TDragAnimation.prototype.close = function() {

    TExercices.prototype.close.call(this);

};
