var behavior = require('../../behavior/index');
var ObjectBase = require('../../core/ObjectBase');
var Bitmap = require('../../display/core/Bitmap');

function TScroll() {

}

TScroll.prototype = Object.create(ObjectBase.prototype);
TScroll.prototype.constructor = TScroll;
module.exports = TScroll;

TScroll.prototype.Create = function(obj_scroll, all_objs, parentContainer){

    this.scroll = this.CreateScrollContainer(obj_scroll);//console.log("dentro do scroll");return;
    this.AddObjectsIntoScroll(this.scroll, all_objs, obj_scroll, parentContainer);console.log(this.scroll);
    return this;
};

/////// FUNCTIONS OF SCROLLABLE
///////
///////
TScroll.prototype.CreateScrollContainer = function(obj_scroll){
    //create a scroll area container
    var s = new ObjectBase();

    s.autoAdjust = true;

    s.sW = obj_scroll.width + obj_scroll.marginX;//width of scroll screen
    s.sH = obj_scroll.height + obj_scroll.marginY;//height of scroll screen

    //X
    s.initX = 99999999;//init x of first object of scroll
    s.endX = -99999999;//end x of last object of scroll
    s.porcX = 0;//current porcentage of horizontal scroll view(0 <-> 1)
    s.pixelX = 0;//current pixel of scroll view (initX <-> endX)
    s.marginX = 5;//margem of right and left sides
    s.scrollHorizontal = false;//has horizontal scroll bar
    s.difX = 0;//diference between init and end of X

    //Y
    s.initY = 99999999;//init y of first object of scroll
    s.endY = -99999999;//end y of last object of scroll
    s.porcY = 0;//current porcentage of vertical scroll view(0 <-> 1)
    s.pixelY = 0;//current pixel of scroll view (initY <-> endY)
    s.marginY = 5;//margem of top and bottom sides
    s.scrollVertical = false;//has vertical scroll bar
    s.difY = 0;//diference between init and end of Y

    return s;

};

TScroll.prototype.AddObjectsIntoScroll = function(s,objs, obj_scroll, parentContainer){//the objs are bitmap or type(texts)

    var objContainer = new ObjectBase();
    for (var i = 0; i < objs.length; i++) {var o = objs[i]; objContainer.addChild(o);}//add all objs

    s.addChild(objContainer);

    var changeLimitHorizontal = false;
    var changeLimitVertical = false;

    for (var i = 0; i < objs.length; i++) {//verify if need to change a limits

        var obj_pos = {};
        var obj_size = {};
// console.log(objs[i]);
        if(objs[i]._displayType === "type"){
            obj_size = objs[i].field;
            // console.log("field._W ", objs[i].field._width, " field._H ", objs[i].field._height);//tamanho q vem do psd
            // console.log("field.W ", objs[i].field._width, " field.H ", objs[i].field.height);//tamanho real (caso tenha estourado)
            // console.log("W ", objs[i].width, " H ", objs[i].height);//tamanho real (caso tenha estourado)
            obj_pos.x = objs[i].x + objs[i].width/2;
            obj_pos.y = objs[i].y + objs[i].height/2;
        }
        else{
            obj_pos = objs[i];
            obj_size = objs[i];
        }

        objs[i]._isScrollable = true;
        objs[i]._scroll = obj_scroll;

        // objs[i].height = obj_scroll.height;

        //x limits
        if(obj_pos.x - obj_size.width/2 < s.initX || obj_pos.x + obj_size.width/2 > s.endX){
            if(obj_pos.x - obj_size.width/2 < s.initX){s.initX = obj_pos.x - obj_size.width/2;}
            if(obj_pos.x + obj_size.width/2 > s.endX){s.endX = obj_pos.x + obj_size.width/2;}
            changeLimitHorizontal = true;
        }

        //y limits
        if(obj_pos.y - obj_size.height/2 < s.initY || obj_pos.y + obj_size.height/2 > s.endY){
            if(obj_pos.y - obj_size.height/2 < s.initY){s.initY = obj_pos.y - obj_size.height/2;}
            if(obj_pos.y + obj_size.height/2 > s.endY){s.endY = obj_pos.y + obj_size.height/2;}
            changeLimitVertical = true;
        }

    }

    s.initX -= obj_scroll.marginX;
    s.endX += obj_scroll.marginX;

    s.initY -= obj_scroll.marginY;
    s.endY += obj_scroll.marginY;

    // console.log("-------------------------");
    //console.log("X " , s.initX, s.endX, " Y ", s.initY, s.endY );

    s.difX = s.endX - s.initX/* + obj_scroll.marginX*/;
    s.difY = s.endY - s.initY/* + obj_scroll.marginY*/;

    var Hconfig = {
        //back layer of scroll
        scroll_barWidth : s.sW - 15,
        scroll_barHeight : 15,
        scroll_barColor : "0xBBBBBB",
        scroll_barAlpha : 1,
        scroll_barRadius : 15,
        //scroll point of current porcentage
        point_scrollWidth : 15,
        point_scrollHeight : 15,
        point_scrollColor : "0xFFFFFF",
        point_scrollAlpha : 0.5,
        point_scrollRadius : 15,
        //scroll of viewed bar
        viewed_scrollWidth : 0,
        viewed_scrollHeight : 0,
        viewed_scrollColor : 0,
        viewed_scrollAlpha : 0
    }
    var Vconfig = {
        //back layer of scroll
        scroll_barWidth : 15,
        scroll_barHeight : s.sH,
        scroll_barColor : "0xBBBBBB",
        scroll_barAlpha : 1,
        scroll_barRadius : 15,
        //scroll point of current porcentage
        point_scrollWidth : 15,
        point_scrollHeight : 15,
        point_scrollColor : "0xFFFFFF",
        point_scrollAlpha : 0.5,
        point_scrollRadius : 15,
        //scroll of viewed bar
        viewed_scrollWidth : 0,
        viewed_scrollHeight : 0,
        viewed_scrollColor : 0,
        viewed_scrollAlpha : 0
    }

    var NeedCreateHorizontalBar = this.NeedCreateHorizontalBar(s);
    var NeedCreateVerticalBar = this.NeedCreateVerticalBar(s);
    var autoAdjust = obj_scroll.autoAdjust;

    if(autoAdjust === true){
        if(NeedCreateVerticalBar === false){s.sH = s.difY + obj_scroll.marginY;}
        if(NeedCreateHorizontalBar === false){s.sW = s.difX;}
    }

    if(changeLimitHorizontal && NeedCreateHorizontalBar === true){
        Hconfig.scroll_barWidth += changeLimitVertical === true ? Vconfig.scroll_barWidth : 0;
        this.AdjustHorizontalLimit(s,Hconfig);
    }else{s.scrollHorizontal = false;}

    if(changeLimitVertical && NeedCreateVerticalBar === true){
        Vconfig.scroll_barHeight += changeLimitHorizontal === true ? Hconfig.scroll_barHeight : 0;
        this.AdjustVerticalLimit(s,Vconfig);
    }else{s.scrollVertical = false;}

    obj_scroll.x = s.x;
    obj_scroll.y = s.y;
    obj_scroll.width = s.width;
    obj_scroll.height = s.height;

    //this.CreateMask(s, obj_scroll, parentContainer);

};

TScroll.prototype.NeedCreateHorizontalBar = function(s){return s.difX > s.sW && s.scrollHorizontal === false? true : false;};
TScroll.prototype.NeedCreateVerticalBar = function(s){return s.difY > s.sH && s.scrollVertical === false ? true : false;};

TScroll.prototype.AdjustHorizontalLimit = function(s, config){
    s.scrollHorizontal = true; this.CreateHorizontalScrollBar(s, config, "bottom");
};

TScroll.prototype.AdjustVerticalLimit = function(s,config){
    s.scrollVertical = true; this.CreateVerticalScrollBar(s, config, "right");
};

TScroll.prototype.CreateMask = function(v_scroll, v_obj_scroll, parentContainer){
    var s = v_scroll;

    //cria um retangulo de referencia
    var rect = v_obj_scroll.rect;

    var scrollMask = new PIXI.Graphics();
    parentContainer.addChild(scrollMask);

    //set rect values
    var r = new PIXI.Graphics();
    r.lineStyle(rect.lineSize, rect.lineColor, rect.lineAlpha);
    r.beginFill(0xFFFFFF, 0);
    r.drawRoundedRect(0,0,s.sW, s.sH,rect.roundedAngle);
    r.endFill();
    r.position.set(s.initX,s.initY);

    s.addChild(r);

    parentContainer.addChild(s);
    s.mask = scrollMask;

    scrollMask.clear();
    scrollMask.beginFill(0x00ff00,0);
    scrollMask.drawRoundedRect(0, 0,s.sW, s.sH,rect. roundedAngle);
    scrollMask.endFill();
    scrollMask.position.set(s.initX, s.initY);

};

/////// FUNCTIONS OF SCROOL
///////
///////

/*
orientation: (string)
-vertical: "left", "right"(default)
-horizontal: "top", "bottom"(default)
*/

/*
config: (object)

//use the default configuration of scroll values
-default (bool)

//back layer of scroll
-scroll_barWidth
-scroll_barHeight
-scroll_barColor
-scroll_barAlpha

//scroll point of current porcentage
-point_scrollWidth
-point_scrollHeight
-point_scrollColor
-point_scrollAlpha

//scroll of viewed bar
-viewed_scrollWidth
-viewed_scrollHeight
-viewed_scrollColor
-viewed_scrollAlpha
*/
TScroll.prototype.CreateHorizontalScrollBar = function(fatherContainer, config, orientation){
    var self = this;

    //ainda sem funcionalidade
    var validate = orientation == "top" ? true : false;
    orientation = validate ? orientation : "bottom";

    //create scroll bar
    var sb = new PIXI.Graphics();
    sb.beginFill(0xBBBBBB, 1);
    sb.drawRoundedRect(0,0,config.scroll_barWidth , config.scroll_barHeight, config.scroll_barRadius);
    sb.endFill();

    var scroll_bar = new Bitmap(sb.generateCanvasTexture());
    scroll_bar.position.set(fatherContainer.initX,fatherContainer.initY + fatherContainer.sH);
    scroll_bar.addPlugin(new behavior.Clickable());
    scroll_bar._selectionEffect = false;
    scroll_bar.addEvents();

    //create scroll point
    var sp = new PIXI.Graphics();
    sp.beginFill(0xFFFFFF, 0.5);
    sp.drawRoundedRect(0,0,config.point_scrollWidth, config.point_scrollHeight,config.point_barRadius);
    sp.endFill();

    var scroll_point = new Bitmap(sp.generateCanvasTexture());
    scroll_point.position.set(fatherContainer.initX,fatherContainer.initY + fatherContainer.sH);
    scroll_point.addPlugin(new behavior.Dragable());
    scroll_point.addEvents();
    scroll_point._lockY = true;
    scroll_point.minX = fatherContainer.initX;
    scroll_point.maxX = fatherContainer.initX + config.scroll_barWidth - scroll_point.width;
    scroll_point.difX = scroll_point.maxX - scroll_point.minX;

    //adjust main scroll container endY and height
    fatherContainer.endY += config.scroll_barHeight;
    fatherContainer.sH += config.scroll_barHeight;
    fatherContainer.addChild(scroll_bar,scroll_point);

    //add scroll_point drag functions
    scroll_point.on("dragStart", self.HorizontalPointStart, this);
    scroll_point.on("dragReleased", self.HorizontalPointReleased, this);
    scroll_point.on("positionUpdated", self.HorizontalPointUpdate, {c_scroll:fatherContainer,sp:scroll_point,self:self});

};

TScroll.prototype.CreateVerticalScrollBar = function(fatherContainer, config, orientation){
    var self = this;

    //ainda sem funcionalidade
    var validate = orientation == "left" ? true : false;
    orientation = validate ? orientation : "right";

    var sb = new PIXI.Graphics();
    sb.beginFill(0xBBBBBB, 1);
    sb.drawRoundedRect(0,0,config.scroll_barWidth,config.scroll_barHeight - config.scroll_barWidth,config.scroll_barRadius);
    sb.endFill();

    var scroll_bar = new Bitmap(sb.generateCanvasTexture());
    scroll_bar.position.set(fatherContainer.initX + fatherContainer.sW,fatherContainer.initY);
    scroll_bar.addPlugin(new behavior.Clickable());
    scroll_bar._selectionEffect = false;
    scroll_bar.addEvents();

    //create scroll point
    var sp = new PIXI.Graphics();
    sp.beginFill(0xFFFFFF, 0.5);
    sp.drawRoundedRect(0,0,config.point_scrollWidth, config.point_scrollHeight,config.point_barRadius);
    sp.endFill();

    var scroll_point = new Bitmap(sp.generateCanvasTexture());
    scroll_point.position.set(fatherContainer.initX + fatherContainer.sW, fatherContainer.initY);
    scroll_point.addPlugin(new behavior.Dragable());
    scroll_point.addEvents();
    scroll_point._lockX = true;
    scroll_point.minY = fatherContainer.initY;
    scroll_point.maxY = fatherContainer.initY + config.scroll_barHeight - scroll_point.height*2;
    scroll_point.difY = scroll_point.maxY - scroll_point.minY;

    //readjust father container values
    fatherContainer.endX += config.scroll_barWidth;
    fatherContainer.sW += config.scroll_barWidth;
    fatherContainer.addChild(scroll_bar,scroll_point);

    //add scroll_point drag functions
    scroll_point.on("dragStart", self.VerticalPointStart, this);
    scroll_point.on("dragReleased", self.VerticalPointReleased, this);
    scroll_point.on("positionUpdated", self.VerticalPointUpdate, {c_scroll:fatherContainer,sp:scroll_point,self:self});

};

TScroll.prototype.HorizontalPointUpdate = function(e){
        var newX = this.sp.x;
        if(this.sp.x <= this.sp.minX){newX = this.sp.minX;}
        if(this.sp.x >= this.sp.maxX){newX = this.sp.maxX;}
        var porc = (newX - this.sp.minX) / this.sp.difX;
        this.sp.x = newX;
        this.self.SetPorcXScroll(this.c_scroll, porc);
};

TScroll.prototype.VerticalPointUpdate = function(e){
        var newY = this.sp.y;
        if(this.sp.y <= this.sp.minY){newY = this.sp.minY;}
        if(this.sp.y >= this.sp.maxY){newY = this.sp.maxY;}
        var porc = (newY - this.sp.minY) / this.sp.difY;
        this.sp.y = newY;
        this.self.SetPorcYScroll(this.c_scroll, porc);
};

TScroll.prototype.SetPorcXScroll = function(scroll, x){//change the horizontal scroll with the porcentage
    var difX = scroll.endX - scroll.initX - scroll.sW;//pixel diference
    var difPorc = scroll.porcX - x;//pixel porcentage difference

    scroll.porcX = x;//console.log(scroll.porcX,"%");
    var aux = (difX * difPorc)/* + scroll.marginX*/;

    scroll.children[0].x += aux;
};

TScroll.prototype.SetPorcYScroll = function(scroll, y){//change the horizontal scroll with the porcentage
    var difY = scroll.endY - scroll.initY - scroll.sH;//pixel diference
    var difPorc = scroll.porcY - y;//pixel porcentage difference

    scroll.porcY = y;//console.log(scroll.porcY,"%");
    var aux = (difY * difPorc)/* + scroll.marginY*/;

    scroll.children[0].y += aux;
};
