var TExercices = require('./TExercices');
var TKeyboard = require('./TKeyboard');

/**
 * configuração do template:
 * <br/>`Veja as opções completas em {@link module:animation.Point}`
 * @classdesc Classe que define o Template de dialogo simples
 * @memberof module:templates/templates
 * @exports TCrossword
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @see module:animation.Point
 * @constructor
 */

function TCrossword() {
    // ObjectBase.call(this);
    TExercices.call(this);
    TKeyboard.call(this);

    var contador = 0;
    this.copyInput = [];

}

TCrossword.prototype = Object.create(TExercices.prototype);
TCrossword.prototype.constructor = TCrossword;
module.exports = TCrossword;


TCrossword.prototype.init = function() {
    TExercices.prototype.init.call(this);
    TKeyboard.prototype.init.call(this);
    var text = this.layersDisplay.getMap('text'); // fiz apartir daqui com Abraão
    var inputs = this.layersDisplay.getMap('inputs');
    var _g = this._config.group;

    this._palavra = {};
    this._organize = {};
    this._corrects = [];

    for(var i = 0; i < _g.length; i++){
        for(var j = 0; j < text.length; j++){
            if(_g[i].textField == text[j].index){
                var t = text[j];
                var valor = t.field._text;
                this._palavra[valor] = t;
                this._organize[valor] = 0;

                var _inp = _g[i].inputs;
                for(var k = 0; k < _inp.length; k++){
                    for(var l = 0; l < inputs.length; l++){
                        if(_inp[k] == inputs[l].index){
                            if(inputs[l].palavra === undefined){
                                inputs[l].palavra = valor;
                            }
                            else{
                                inputs[l].palavra += '|' + valor;
                            }
                            break;
                        }
                    }
                }

                break; // parei aqui com o Abraão
            }
        }
    }

    this._corrects = [];
};

TCrossword.prototype.comparar = function(nome) { // com Abraão
    if(this._organize[nome] == nome.length){
        this._palavra[nome].field.setStyle({"strokeThickness": 3, "stroke": "#aaef42"});
    }
};

TCrossword.prototype.start = function() {
    TExercices.prototype.start.call(this);
};

TCrossword.prototype.open = function() {

    TExercices.prototype.open.call(this);
    TKeyboard.prototype.open.call(this);

    this.contador = 0;

    this.copyInput = this._input;

};

TCrossword.prototype._initInteractions = function(){
   TExercices.prototype._initInteractions.call(this);
   TKeyboard.prototype._initInteractions.call(this);

};

TCrossword.prototype._focus = function(e) {
	TKeyboard.prototype._focus.call(this, e);
};

TCrossword.prototype._keyPressed = function(e){
    TKeyboard.prototype._keyPressed.call(this, e);

    var self = this;

    if (self._focussedInput.text.length >= self._focussedInput.component.config.correct.length) {
       self.verify();
   }
};

TCrossword.prototype.verify = function() {
	var self = this;

	var inputsToVerify = self._inputs.filter(function(input) {
		return self._corrects.indexOf(input) < 0; //não existe no array de acertos
	});

	self.removeKeyboardEvents();
	self.removeInputEvents(inputsToVerify);
	self._focussedInput.component.hideCursor();

	if (self._focussedInput.text == self._focussedInput.component.config.correct) {
        self._focussedInput.component.setStyle({"strokeThickness": 3, "stroke": "#aaef42"});
		self._corrects.push(self._focussedInput);
		inputsToVerify.splice(inputsToVerify.indexOf(self._focussedInput),1);
        var palavra = self._focussedInput.component.palavra;
        if(palavra.indexOf('|') != -1){
            var all = palavra.split('|');
            for(var i = 0; i < all.length; i++){
                self._organize[all[i]]++;
                self.comparar(all[i]);
            }
        } else {
            self._organize[palavra]++;
            self.comparar(palavra);
        }
		if(inputsToVerify.length === 0) {
			self.openCorrect();
		} else {
			self.addKeyboardEvents();
			self.addInputEvents(inputsToVerify);

			self._focussedInput = inputsToVerify[0];

			self._focussedInput.component.showCursor();
		}
	} else {
		self.openFail();

		self.once('feedComplete', function() {
			self.addKeyboardEvents();
			self.addInputEvents(inputsToVerify);
		});
	}
};

// TCrossword.prototype.openCorect = function() {
//
//     var self = this;
//
//     if (self.correct) {
//         self.correct.timeline.play();
//         self.correct.once('pointComplete', function() {
//             self.emit("parallax");
//         }, self);
//     } else {
//         setTimeout(function() {
//             self.emit("parallax");
//         }, 1000);
//     }
//
// };

TCrossword.prototype.openCorrect = function() {

    var self = this;

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};


TCrossword.prototype.openFail = function() {

    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();

            self._focussedInput.text = "";
            self._focussedInput.component.setText(self._focussedInput.text);
            self.addKeyboardEvents();
            self.addInputEvents();
            self._focussedInput.component.showCursor();
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();

            self.addKeyboardEvents();
            self.addInputEvents();
        }, 1000);
    }

};
TCrossword.prototype.addInputEvents = function(arrInputs){
   TKeyboard.prototype.addInputEvents.call(this, arrInputs);
};

TCrossword.prototype.removeInputEvents = function(arrInputs) {
	TKeyboard.prototype.removeInputEvents.call(this, arrInputs);
};

TCrossword.prototype.addKeyboardEvents = function(){
    TKeyboard.prototype.addKeyboardEvents.call(this);
};

TCrossword.prototype.removeKeyboardEvents = function() {
	TKeyboard.prototype.removeKeyboardEvents.call(this);
};

TCrossword.prototype.close = function() {

    TKeyboard.prototype.close.call(this);

};

TCrossword.prototype.reset = function() {
    TKeyboard.prototype.reset.call(this);

        this._corrects = [];

        this.contador = 0;

        for (var x in this._organize) {
            this._organize[x] = 0;
        }

        for (var palavra in this._palavra) { // fiz com Amauri
            this._palavra[palavra].field.setStyle({"strokeThickness": 0, "stroke": "#aaef42"});

    }

    //this._input = this.copyInput;


};
