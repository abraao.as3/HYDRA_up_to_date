var TExercices = require('./TExercices');
var DataBoard = require('../../ui/d3/DataBoard');
var ObjectBase = require('../../core/ObjectBase');
var Const = require('../../core/Const');
var PIXIConfig = require('../../core/PIXIConfig');
var TKeyboard = require('./TKeyboard');

function TLineNumerical(){

    TExercices.call(this);
    TKeyboard.call(this);

    this._back = $(document.createElement('div'));
    this._back.attr('id','backFundo');
    this._back.css({
        position: 'absolute',
        overflow: 'hidden',
        zIndex: -100
    });
    this._imageBackground = null;
    this._canvasView = $(PIXIConfig.renderer.view);
    this._factorScale = 1;

    this._inputs = [];
	this._focussedInput = null;
    this._keyboard = this._interface.keyboard;

}

TLineNumerical.prototype = Object.create(TExercices.prototype);
TLineNumerical.prototype.constructor = TLineNumerical;
module.exports = TLineNumerical;

TLineNumerical.prototype.init = function(){
    TExercices.prototype.init.call(this);
	TKeyboard.prototype.init.call(this);

    this._cwConfig = this._config.dataBoard;
    var ib = this._layersDisplay.getObjects('images');
    for (var i = 0; i < ib.length; i++) {
        if (ib[i].index == this._cwConfig.imageBase) {
            this._imgBase = ib[i];

            break;
        }
    };

    this._dataBoard = new DataBoard();
    var ticks = this._config.dataBoard.ticks;
    var inLine = this._config.dataBoard.inLine;
    var otLine = this._config.dataBoard.otLine;
    var posUser = this._config.dataBoard.posUser;
    var strokeTickness = this._config.dataBoard.strokeTickness;
    var fontSize = this._config.dataBoard.fontSize;
    var fontColor = this._config.dataBoard.fontColor;
    var lineColor = this._config.dataBoard.lineColor;
    var ballRadius = this._config.dataBoard.ballRadius;
    var colorBall = this._config.dataBoard.colorBall;

    this._dataBoard.init(colorBall, ballRadius, inLine,otLine, posUser,ticks,this._imgBase, strokeTickness,fontSize,fontColor,lineColor);
    var layerBack = this.layersDisplay.getLayer('background');
    this._imageBackground = $(PIXIConfig.renderer.extract.image(layerBack));
    layerBack.visible = false;
    this._dataBoard._visualise[0].hidden = true;
    this._imageBackground.css({
        width: '100%',
        height: '100%',
        position: 'relative'
    });
    this._back.append(this._imageBackground);
    $('body').first().prepend(this._back);

    this._back.css('display', 'none');

    this._corrects = [];
    this._imgBase.visible = false;

};

TLineNumerical.prototype.start = function () {
    var self = this;
    this._dataBoard.start();
    this._back.css('display', 'block');

    this._dataBoard.domResize(this.factorScale, this.canvasPos);
};

TLineNumerical.prototype.open = function(){
    TExercices.prototype.open.call(this);
    TKeyboard.prototype.open.call(this);
    // this._dataBoard._visualise[0].hidden = false;


};

TLineNumerical.prototype._initInteractions = function(){
        TExercices.prototype._initInteractions.call(this);
        TKeyboard.prototype._initInteractions.call(this);
        this._dataBoard._visualise[0].hidden = false;
};

TLineNumerical.prototype._focus = function(e) {
	TKeyboard.prototype._focus.call(this, e);
};

TLineNumerical.prototype._keyPressed = function(e) {
	TKeyboard.prototype._keyPressed.call(this, e);

	var self = this;

	if(self._focussedInput.text.length >= self._focussedInput.component.config.correct.length) {
		self.verify();
	}
};

TLineNumerical.prototype.addInputEvents = function(arrInputs) {
	TKeyboard.prototype.addInputEvents.call(this, arrInputs);
};

TLineNumerical.prototype.removeInputEvents = function(arrInputs) {
	TKeyboard.prototype.removeInputEvents.call(this, arrInputs);
};

TLineNumerical.prototype.addKeyboardEvents = function() {
	TKeyboard.prototype.addKeyboardEvents.call(this);
};

TLineNumerical.prototype.removeKeyboardEvents = function() {
	TKeyboard.prototype.removeKeyboardEvents.call(this);
};

TLineNumerical.prototype.openCorrect = function() {

    var self = this;

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            self.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

TLineNumerical.prototype.openFail = function() {

    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
             self._focussedInput.text = "";
             self._focussedInput.component.setText(self._focussedInput.text);
            // this._interface.openRefresh();
            // this._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            // self._interface.openRefresh();
            // self._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, 1000);
    }

};

TLineNumerical.prototype.verify = function() {
	var self = this;

	var inputsToVerify = self._inputs.filter(function(input) {
		return self._corrects.indexOf(input) < 0; //não existe no array de acertos
	});

	self.removeKeyboardEvents();
	self.removeInputEvents(inputsToVerify);
	self._focussedInput.component.hideCursor();

	if (self._focussedInput.text == self._focussedInput.component.config.correct) {
		self._corrects.push(self._focussedInput);
		inputsToVerify.splice(inputsToVerify.indexOf(self._focussedInput),1);

		if(inputsToVerify.length === 0) {
			self.openCorrect();
		} else {
			self.addKeyboardEvents();
			self.addInputEvents(inputsToVerify);

			self._focussedInput = inputsToVerify[0];

			self._focussedInput.component.showCursor();
		}
	} else {
		self.openFail();

		self.once('feedComplete', function() {
			self.addKeyboardEvents();
			self.addInputEvents(inputsToVerify);
			self._focussedInput.component.showCursor();
		});
	}
};

TLineNumerical.prototype.close = function() {
    TKeyboard.prototype.close.call(this);
};

TLineNumerical.prototype.reset = function() {
	TKeyboard.prototype.reset.call(this);
    this._corrects = [];
};

TLineNumerical.prototype.resize = function(factorScale, canvasPos){

    this.factorScale = factorScale;
    this.canvasPos = canvasPos;

    this._back.width(this._canvasView.width());
    this._back.height(this._canvasView.height());
    this._back.offset({
        left: this._canvasView.offset().left,
        top: this._canvasView.offset().top
    });
    this._dataBoard.domResize(factorScale, canvasPos);
};
