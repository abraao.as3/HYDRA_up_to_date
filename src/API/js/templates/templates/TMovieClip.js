var ITemplate = require('../core/ITemplate');

/**
* configuração do template:
* <br/>`Veja as opções completas em {@link module:animation.Point}`
* @example
*
* Falta criar exemplo.
*
* @classdesc TMovieClip Class
* @memberof module:templates/templates
* @exports TMovieClip
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @see module:animation.Point
* @constructor
*/
function TMovieClip() {
    ITemplate.call(this);

    this.contador = 0;
}

TMovieClip.prototype = Object.create(ITemplate.prototype);
TMovieClip.prototype.constructor = TMovieClip;
module.exports = TMovieClip;

/** @function
* @description init
*/
TMovieClip.prototype.init = function() {
	this.setSequencies(this._config.sequences, this._SoundManager);

    ITemplate.prototype.init.call(this);
};

/** @function
* @description start
*/
TMovieClip.prototype.start = function(){
    this.fadeStart();
};

/** @function
* @description open
*/
TMovieClip.prototype.open = function() {
    var self = this;

    this.once('sequenceComplete', this._initInteractions, this);
    this.fadeOpen();
};

/** @function
* @description _initInteractions
* @private
*/
TMovieClip.prototype._initInteractions = function() {
    var self = this;

    ITemplate.prototype._initInteractions.call(this);

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._interface.closeRefresh();
            self.fadeRestart();
            self.movieClip[0].gotoAndStop(0);
            self.on('sequenceComplete', function() {
                self._interface.openRefresh();
                this.movieClip[0].play();
            });
        });
    });

    this._interface.openRefresh();

    this.movieClip = this.layersDisplay.getObjects('movieClip');

    this.playMovie();

};

/** @function
* @description playMovie
*/
TMovieClip.prototype.playMovie = function(){

    var self = this;

    this.movieClip[this.contador].onComplete = function(){
        self.contador++;

        if (self.movieClip.length <= self.contador) {
            self.onMovieComplete(self);
        }else{
            self.playMovie();
        }

    };
    this.movieClip[this.contador].play();
};

/** @function
* @description onMovieComplete
*/
TMovieClip.prototype.onMovieComplete = function(self){

    self._interface.closeRefresh();
    self.checkRoadMethod();
};

/** @function
* @description reset
*/
TMovieClip.prototype.reset = function() {
    var self = this;

     this.contador = 0;
    for(var a = 0;a < this.movieClip.length; a++){

         self.movieClip[a].gotoAndStop(0);
    }
    this.once('sequenceComplete', this._initInteractions, this);
    this.fadeOpen();
};
