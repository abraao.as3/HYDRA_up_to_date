var ITemplate = require('../core/ITemplate');
var behavior = require('../../behavior/index');
var ObjectBase = require('../../core/ObjectBase');
var Scroll = require('../../display/simple/Scroll');
var Bitmap = require('../../display/core/Bitmap');

function TScrollTeste() {
    ITemplate.call(this);

    this._scrolls = [];
    /*
        scroll: object
        - _objs: container that contains all objs, texts, images, etc, inside de scroll
        - _hBar: container that contains horizontal bar of scroll
        - _vBar: container that contains vertical bar of scroll
     */
}

TScrollTeste.prototype = Object.create(ITemplate.prototype);
TScrollTeste.prototype.constructor = TScrollTeste;
module.exports = TScrollTeste;

/** @function
* @description init
*/
TScrollTeste.prototype.init = function() {
    ITemplate.prototype.init.call(this);

    var self = this;

    var config_scrolls = this._config.scrolls;//only the config of scroll

    for (var i = 0; i < config_scrolls.length; i++) {
        this.NewScroll(config_scrolls[i]);
        console.log("TScrollTeste ", i , " ",this._scrolls[i]);
    }

    // this._scrolls[0]._objs.x -= 100;
    // this._scrolls[0].y += 100;
    // this._scrolls[0]._objs.alpha = .5;
    // console.log("acabei o init do TScrool");
    // console.log("todos", this._scrolls);
};

TScrollTeste.prototype.NewScroll = function(obj_scroll){
    var idx_imgs = obj_scroll.imgs; var idx_txts = obj_scroll.txts;

    var all_objs = [];

    //get all obj by id and add it in the "var all_objs = []"
    var imgs = this.ReturnBitmapsOfScrollConfig(idx_imgs);for (var i = 0; i < imgs.length; i++) {all_objs.push(imgs[i]);}
    var txts = this.ReturnTextsOfScrollConfig(idx_txts);for (i = 0; i < txts.length; i++) {all_objs.push(txts[i]);}

    var c_id = this._scrolls.length;



    //add scrollable plugin into the current scroll
    // this._scrolls[c_id].addPlugin(new behavior.Scrollable());//console.log("saindo do scrollable",this._scrolls[c_id]);
    // this._scrolls[c_id].CreateScrollContainer();
    // this._scrolls[c_id].AddObjectsIntoScroll(all_objs);

    this._scrolls.push(this.CreateScrollContainer("this._scrolls[c_id]", obj_scroll));
    this.AddObjectsIntoScroll(this._scrolls[c_id], all_objs, obj_scroll);
};

TScrollTeste.prototype.ReturnBitmapsOfScrollConfig = function(ids){
    var bitmaps = [];
    for (var i = 0; i < ids.length; i++) {bitmaps.push(this.layersDisplay.getMapByID('images', ids[i]));}
    return bitmaps;
};

TScrollTeste.prototype.ReturnTextsOfScrollConfig = function(ids){
    var type = [];
    for (var i = 0; i < ids.length; i++) {type.push(this.layersDisplay.getMapByID('text', ids[i]));}
    return type;
};

TScrollTeste.prototype.start = function() {ITemplate.prototype.start.call(this);};

TScrollTeste.prototype.open = function() {ITemplate.prototype.open.call(this);};

TScrollTeste.prototype.close = function(){ITemplate.prototype.close.call(this);};

TScrollTeste.prototype.reset = function() {ITemplate.prototype.reset.call(this);};

/////// FUNCTIONS OF SCROLLABLE
///////
///////
TScrollTeste.prototype.CreateScrollContainer = function(si, obj_scroll){
    //create a scroll area container
    var s = new ObjectBase();

    s.autoAdjust = true;

    s.sW = obj_scroll.width;//width of scroll screen
    s.sH = obj_scroll.height;//height of scroll screen

    //X
    s.initX = 99999999;//init x of first object of scroll
    s.endX = -99999999;//end x of last object of scroll
    s.porcX = 0;//current porcentage of horizontal scroll view(0 <-> 1)
    s.pixelX = 0;//current pixel of scroll view (initX <-> endX)
    s.marginX = 5;//margem of right and left sides
    s.scrollHorizontal = false;//has horizontal scroll bar
    s.difX = 0;//diference between init and end of X

    //Y
    s.initY = 99999999;//init y of first object of scroll
    s.endY = -99999999;//end y of last object of scroll
    s.porcY = 0;//current porcentage of vertical scroll view(0 <-> 1)
    s.pixelY = 0;//current pixel of scroll view (initY <-> endY)
    s.marginY = 5;//margem of top and bottom sides
    s.scrollVertical = false;//has vertical scroll bar
    s.difY = 0;//diference between init and end of Y

    s._objs = new ObjectBase();//Create a container for the all objs
    s._hBar = new ObjectBase();//Horizontal scroll bar
    s._vBar = new ObjectBase();//Vertical scroll bar

    return s;

};

TScrollTeste.prototype.AddObjectsIntoScroll = function(s,objs, obj_scroll){//the objs are bitmap or type(texts)

    for (var i = 0; i < objs.length; i++) {s._objs.children.push(objs[i]);}//add all objs
    // s._objs.all_objs = [];
    // for (var i = 0; i < objs.length; i++) {s._objs.all_objs.push(objs[i]);}

    var changeLimitHorizontal = false;
    var changeLimitVertical = false;

    for (var i = 0; i < objs.length; i++) {//verify if need to change a limits

        //x limits
        if(objs[i].x - objs[i].width/2 < s.initX || objs[i].x + objs[i].width/2 > s.endX){
            if(objs[i].x - objs[i].width/2 < s.initX){s.initX = objs[i].x - objs[i].width/2;}
            if(objs[i].x + objs[i].width/2 > s.endX){s.endX = objs[i].x + objs[i].width/2;}
            changeLimitHorizontal = true;
        }

        //y limits
        if(objs[i].y - objs[i].height/2 < s.initY || objs[i].y + objs[i].height/2 > s.endY){
            if(objs[i].y - objs[i].height/2 < s.initY){s.initY = objs[i].y - objs[i].height/2;}
            if(objs[i].y + objs[i].height/2 > s.endY){s.endY = objs[i].y + objs[i].height/2;}
            changeLimitVertical = true;
        }

        objs[i].initX = objs[i].x - s.initX;
        objs[i].initY = objs[i].y - s.initY;

    }

    // console.log("s.sW",s.sW);
    s.difX = s.endX - s.initX;
    s.difY = s.endY - s.initY;//console.log("s.endY",s.difY);

    // var hasHorizontalBar = false;
    // var hasVerticalBar = false;
    var Hconfig = {
        //back layer of scroll
        scroll_barWidth : s.sW - 15,
        scroll_barHeight : 15,
        scroll_barColor : "0xBBBBBB",
        scroll_barAlpha : 1,
        //scroll point of current porcentage
        point_scrollWidth : 15,
        point_scrollHeight : 15,
        point_scrollColor : "0xFFFFFF",
        point_scrollAlpha : 0.5,
        //scroll of viewed bar
        viewed_scrollWidth : 0,
        viewed_scrollHeight : 0,
        viewed_scrollColor : 0,
        viewed_scrollAlpha : 0
    }
    var Vconfig = {
        //back layer of scroll
        scroll_barWidth : 15,
        scroll_barHeight : s.sH,
        scroll_barColor : "0xBBBBBB",
        scroll_barAlpha : 1,
        //scroll point of current porcentage
        point_scrollWidth : 15,
        point_scrollHeight : 15,
        point_scrollColor : "0xFFFFFF",
        point_scrollAlpha : 0.5,
        //scroll of viewed bar
        viewed_scrollWidth : 0,
        viewed_scrollHeight : 0,
        viewed_scrollColor : 0,
        viewed_scrollAlpha : 0
    }

    if(changeLimitHorizontal && this.NeedCreateHorizontalBar(s) === true){
        Hconfig.scroll_barWidth += changeLimitVertical === true ? Vconfig.scroll_barWidth : 0;
        this.AdjustHorizontalLimit(s,Hconfig);
    }
    else{
        s.scrollHorizontal = false;
        // if(s.autoAdjust === true){
        //     s.sW = s.difX + (s.marginX * 2);
        //     s.endX = s.initX + s.sW;
        // }
    }

    if(changeLimitVertical && this.NeedCreateVerticalBar(s) === true){
        Vconfig.scroll_barHeight += changeLimitHorizontal === true ? Hconfig.scroll_barHeight : 0;
        this.AdjustVerticalLimit(s,Vconfig);
    }
    else{
        s.scrollVertical = false;
        // if(s.autoAdjust === true){
        //     s.sH = s.difY + (s.marginY * 2);
        //     s.endY = s.initY + s.sH;
        // }
    }

    this.CreateMask(s, s._objs.chidren, obj_scroll);

};

TScrollTeste.prototype.NeedCreateHorizontalBar = function(s){return s.difX > s.sW && s.scrollHorizontal === false? true : false;};
TScrollTeste.prototype.NeedCreateVerticalBar = function(s){return s.difY > s.sH && s.scrollVertical === false ? true : false;};

TScrollTeste.prototype.AdjustHorizontalLimit = function(s, config){
    s.scrollHorizontal = true;
    this.CreateHorizontalScrollBar(s, config, "bottom");//CRIA O SCROLL BAR HORIZONTAL!!!
};

TScrollTeste.prototype.AdjustVerticalLimit = function(s,config){
    s.scrollVertical = true;
    this.CreateVerticalScrollBar(s, config, "right");//CRIA O SCROLL BAR VERTICAL!!!
};

TScrollTeste.prototype.CreateMask = function(v_scroll, v_objs, v_obj_scroll){
// console.log("olha eu aki", this);
    var s = v_scroll;
    var obj = v_objs;

    //cria um retangulo de referencia
    var rect = v_obj_scroll.rect;

    var scrollMask = new PIXI.Graphics();
    this.addChild(scrollMask);

    //set rect values
    var r = new PIXI.Graphics();
    r.lineStyle(rect.lineSize, rect.lineColor, rect.lineAlpha);
    r.beginFill(0xFFFFFF, 0);
    r.drawRoundedRect(0,0,s.sW, s.sH,rect.roundedAngle);
    r.endFill();
    r.position.set(s.initX,s.initY);

    s.addChild(r);
// console.log("scroll antes ", s);
// console.log("mascara W e H ", scrollMask.width, scrollMask.height);
// s._objs._mask = scrollMask;//console.log("_scroll",s);
    // s._objs.mask = scrollMask;//console.log("_scroll",s);
    // for (var i = 0; i < s._objs.children.length; i++) {
    //     scrollMask.addChild(s._objs.children[i]);
    // }
    // obj.mask = scrollMask;
    this.addChild(s);
    s.mask = scrollMask;//console.log("_scroll",s);
// console.log("scroll depois ", s);
// console.log("scroll_mask ", s.mask);
scrollMask.clear();
scrollMask.beginFill(0x00ff00);
scrollMask.drawRect(0, 0,s.sW, s.sH);
scrollMask.endFill();
scrollMask.position.set(s.initX, s.initY);


    //
};

/////// FUNCTIONS OF SCROOL
///////
///////

/*
orientation: (string)
-vertical: "left", "right"(default)
-horizontal: "top", "bottom"(default)
*/

/*
config: (object)

//use the default configuration of scroll values
-default (bool)

//back layer of scroll
-scroll_barWidth
-scroll_barHeight
-scroll_barColor
-scroll_barAlpha

//scroll point of current porcentage
-point_scrollWidth
-point_scrollHeight
-point_scrollColor
-point_scrollAlpha

//scroll of viewed bar
-viewed_scrollWidth
-viewed_scrollHeight
-viewed_scrollColor
-viewed_scrollAlpha
*/
TScrollTeste.prototype.CreateHorizontalScrollBar = function(fatherContainer, config, orientation){
    var self = this;

    //ainda sem funcionalidade
    var validate = orientation == "top" ? true : false;
    orientation = validate ? orientation : "bottom";

    var sb = new PIXI.Graphics();
    sb.beginFill(0xBBBBBB, 1);
    sb.drawRoundedRect(0,0,config.scroll_barWidth , config.scroll_barHeight,1);
    sb.endFill();

    var scroll_bar = new Bitmap(sb.generateCanvasTexture());
    scroll_bar.position.set(fatherContainer.initX,fatherContainer.initY + fatherContainer.sH);
    scroll_bar.addPlugin(new behavior.Clickable());
    scroll_bar.addEvents();

    //create scroll point
    var sp = new PIXI.Graphics();
    sp.beginFill(0xFFFFFF, 0.5);
    sp.drawRoundedRect(0,0,config.point_scrollWidth, config.point_scrollHeight,1);
    sp.endFill();

    var scroll_point = new Bitmap(sp.generateCanvasTexture());
    scroll_point.position.set(fatherContainer.initX,fatherContainer.initY + fatherContainer.sH);
    scroll_point.addPlugin(new behavior.Dragable());
    scroll_point.addEvents();
    scroll_point._lockY = true;
    scroll_point.minX = fatherContainer.initX;
    scroll_point.maxX = fatherContainer.initX + config.scroll_barWidth - scroll_point.width;
    scroll_point.difX = scroll_point.maxX - scroll_point.minX;

    //adjust main scroll container endY and height
    fatherContainer.endY += config.scroll_barHeight;
    fatherContainer.sH += config.scroll_barHeight;
    fatherContainer.addChild(scroll_bar,scroll_point);

    //add scroll_point drag functions
    scroll_point.on("dragStart", self.HorizontalPointStart, this);
    scroll_point.on("dragReleased", self.HorizontalPointReleased, this);
    scroll_point.on("positionUpdated", self.HorizontalPointUpdate, {c_scroll:fatherContainer,sp:scroll_point,self:self});

};

TScrollTeste.prototype.CreateVerticalScrollBar = function(fatherContainer, config, orientation){

    var self = this;

    //ainda sem funcionalidade
    var validate = orientation == "left" ? true : false;
    orientation = validate ? orientation : "right";

    var sb = new PIXI.Graphics();
    sb.beginFill(0xBBBBBB, 1);
    sb.drawRoundedRect(0,0,config.scroll_barWidth,config.scroll_barHeight - config.scroll_barWidth,1);
    sb.endFill();

    var scroll_bar = new Bitmap(sb.generateCanvasTexture());
    scroll_bar.position.set(fatherContainer.initX + fatherContainer.sW,fatherContainer.initY);
    scroll_bar.addPlugin(new behavior.Clickable());
    scroll_bar.addEvents();

    //create scroll point
    var sp = new PIXI.Graphics();
    sp.beginFill(0xFFFFFF, 0.5);
    sp.drawRoundedRect(0,0,config.point_scrollWidth, config.point_scrollHeight,1);
    sp.endFill();

    var scroll_point = new Bitmap(sp.generateCanvasTexture());
    scroll_point.position.set(fatherContainer.initX + fatherContainer.sW, fatherContainer.initY);
    scroll_point.addPlugin(new behavior.Dragable());
    scroll_point.addEvents();
    scroll_point._lockX = true;
    scroll_point.minY = fatherContainer.initY;
    scroll_point.maxY = fatherContainer.initY + config.scroll_barHeight - scroll_point.height*2;
    scroll_point.difY = scroll_point.maxY - scroll_point.minY;

    //readjust father container values
    fatherContainer.endX += config.scroll_barWidth;
    fatherContainer.sW += config.scroll_barWidth;
    fatherContainer.addChild(scroll_bar,scroll_point);

    //add scroll_point drag functions
    scroll_point.on("dragStart", self.VerticalPointStart, this);
    scroll_point.on("dragReleased", self.VerticalPointReleased, this);
    scroll_point.on("positionUpdated", self.VerticalPointUpdate, {c_scroll:fatherContainer,sp:scroll_point,self:self});

};

TScrollTeste.prototype.HorizontalPointUpdate = function(e){
        var newX = this.sp.x;
        if(this.sp.x <= this.sp.minX){newX = this.sp.minX;}
        if(this.sp.x >= this.sp.maxX){newX = this.sp.maxX;}
        var porc = (newX - this.sp.minX) / this.sp.difX;
        this.sp.x = newX;
        this.self.SetPorcXScroll(this.c_scroll, porc);
};

TScrollTeste.prototype.VerticalPointUpdate = function(e){
        var newY = this.sp.y;
        if(this.sp.y <= this.sp.minY){newY = this.sp.minY;}
        if(this.sp.y >= this.sp.maxY){newY = this.sp.maxY;}
        var porc = (newY - this.sp.minY) / this.sp.difY;
        this.sp.y = newY;
        this.self.SetPorcYScroll(this.c_scroll, porc);
};

TScrollTeste.prototype.SetPorcXScroll = function(scroll, x){//change the horizontal scroll with the porcentage
    var difX = scroll.endX - scroll.initX - scroll.sW;//pixel diference

    scroll.porcX = x;console.log(scroll.porcX,"%");
    var aux = scroll.initX + (difX * -x) + scroll.marginX;

    // scroll._objs.children[0].x = scroll.initX + aux;
    scroll._objs.x = scroll.initX + aux;
};

TScrollTeste.prototype.SetPorcYScroll = function(scroll, y){//change the horizontal scroll with the porcentage
    var difY = scroll.endY - scroll.initY - scroll.sH;//pixel diference

    scroll.porcY = y;console.log(scroll.porcY,"%");
    var aux = scroll.initY + (difY * -y) + scroll.marginY;

    scroll._objs.y = scroll.initY + aux;
};
