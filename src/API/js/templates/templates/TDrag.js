var TExercices = require('./TExercices');

/**
 * configuração do template:
 * @example
 *
 "config": {
     "resetAllOnFail": false,
     "failOnColision": false,
     "text": {
         "pt-BR": [{
             "text": "<a>PARAGÜERO</a>",
             "style": {
                 "a": {
                     "fontSize": 13
                 }
             }
         }, {
             "text": "<a>PIRAGÜISTA</a>",
             "style": {
                 "a": {
                     "fontSize": 15
                 }
             }
         }, {
             "text": "<a>PINGÜINO</a>",
             "style": {
                 "a": {
                     "fontSize": 15
                 }
             }
         }, {
             "text": "<a>CIGÜEÑA</a>",
             "style": {
                 "a": {
                     "fontSize": 15
                 }
             }
         }, {
             "text": "<a>UNGÜENTO</a>",
             "style": {
                 "a": {
                     "fontSize": 15
                 }
             }
         }, {
             "text": "GÜE"
         }, {
             "text": "GÜI"
         }, {
             "text": "Ahora fíjate bien cómo se escriben y relaciona las palabras con las sílabas que les correspondan."
         }, {
             "text": "Fantástico"
         }, {
             "text": "Vuelve a intentarlo."
         }]
     },
     "audio": {
         "pt-BR": [{
             "name": "7_1",
             "id": 0
         }, {
             "name": "7_2",
             "id": 1
         }, {
             "name": "7_2",
             "id": 2
         }]
     },
     "button": [{
         "img": [6],
         "animate": true,
         "action":{
             "audio": {
                 "id": 4,
                 "mouth": [0]
             }
         }
     },{
         "img": [7],
         "animate": true,
         "action":{
             "audio": {
                 "id": 5,
                 "mouth": [0]
             }
         }
     },{
         "img": [8],
         "animate": true,
         "action":{
             "audio": {
                 "id": 6,
                 "mouth": [0]
             }
         }
     },{
         "img": [9],
         "animate": true,
         "action":{
             "audio": {
                 "id": 3,
                 "mouth": [0]
             }
         }
     }],
     "sequences": [{
         "id": 0,
         "img": 4,
         "mouth": [0],
         "txt": [7],
         "audio": 0,
         "fadeOut": false
     }],
     "areaDrags": [{
         "img": [14],
         "layerDisplacement": 1,
         "disableDragOnHit": true,
         "id": 0,
         "alpha": 0,
         "corrects": [0, 3, 4],
         "maxChildren": 3,
         "alignMode": "horizontal",
         "points": [{
             "x": 250,
             "y": 180
         }, {
             "x": 40,
             "y": 40
         }],
         "createGrid":[2,2],
         "vspace": 10,
         "hspace": 10
     }, {
         "img": [15],
         "layerDisplacement": 1,
         "disableDragOnHit": true,
         "id": 0,
         "alpha": 0.5,
         "corrects": [1, 2],
         "maxChildren": 2,
         "alignMode": "vertical",
         "points": [{
             "x": 250,
             "y": 180
         }, {
             "x": 40,
             "y": 40
         }],
         "createGrid":[2,2],
         "vspace": 10,
         "hspace": 10
     }],
     "drags": [{
             "text": [0],
             "img": [0],
             "textAlignLeft": true,
             "layerDisplacement": 1,
             "receptors": [0, 1],
             "adjust": false,
             "usePoint": true,
             "defineArea": 40,
             "resetOnFail": true,
             "obstacles": {
                 "img": [3],
                 "show": false
             }
         }, {
             "text": [1],
             "img": [0],
             "textAlignLeft": true,
             "layerDisplacement": 1,
             "receptors": [0, 1],
             "usePoint": true,
             "adjust": false,
             "resetOnFail": true
         }, {
             "text": [2],
             "img": [0],
             "textAlignLeft": true,
             "layerDisplacement": 1,
             "receptors": [0, 1],
             "usePoint": true,
             "adjust": false,
             "resetOnFail": true
         }, {
             "text": [3],
             "img": [0],
             "textAlignLeft": true,
             "layerDisplacement": 1,
             "receptors": [0, 1],
             "usePoint": true,
             "adjust": false,
             "resetOnFail": true
         }, {
             "text": [4],
             "img": [0],
             "layerDisplacement": 1,
             "receptors": [0, 1],
             "adjust": false,
             "usePoint": true,
             "defineArea": 40,
             "resetOnFail": true
         }

     ],
     "response": {
         "fail": {
             "images": [6],
             "txt": [9],
             "audio": 2,
             "mouth": [0]
         },
         "correct": {
             "images": [5],
             "txt": [8],
             "audio": 1,
             "mouth": [0]
         }
     }
 }
 *
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TDrag
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */
function TDrag() {
    TExercices.call(this);

    this._drags = [];
    this.areaDrags = [];
}

TDrag.prototype = Object.create(TExercices.prototype);
TDrag.prototype.constructor = TDrag;
module.exports = TDrag;

/** @function
 * @description init
 */
TDrag.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this.configureDrags();

};

/** @function
 * @description start
 */
TDrag.prototype.start = function() {
    TExercices.prototype.start.call(this);
};

/** @function
 * @description open
 */
TDrag.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

/** @function
 * @description _initInteractions
 * @private
 */
TDrag.prototype._initInteractions = function() {
    var self = this;

    TExercices.prototype._initInteractions.call(this);

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._blocked();
            self.fadeRestart();
            self.removeDragEvents();
            self.on('sequenceComplete', function() {
                self._unblock();
                self.addDragEvents();
            });
        });
    });

    this._interface.openRefresh();

    this._unblock();
    this._block = false;

    this.addDragEvents();
};

/**
 * Metodo que adiciona eventos aos drags
 */
TDrag.prototype.addDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].addEvents();
    }
};

/**
 * Metodo que remove eventos dos drags
 */
TDrag.prototype.removeDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].removeEvents();
    }
};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
TDrag.prototype.configureDrags = function() {

    var self = this;

    //var layer = this.layersDisplay.getObjects('text');

    var config = this._config;

    this._drags = this.layersDisplay.getObjects('drag');
    this.areaDrags = this.layersDisplay.getObjects('areaDrag');

    this._obstacles = [];

    for (var i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        var colisoes = [];

        d.id = i;

        if (config.drags[i].receptors !== "all") {


            for (var j = 0; j < config.drags[i].receptors.length; j++) {
                colisoes.push(this.areaDrags[config.drags[i].receptors[j]]);
            }

            d.setReceptors(colisoes);

        } else {

            d.setReceptors(this.areaDrags);

        }


        d.setUsePoint(config.drags[i].usePoint);
        d.setIsAdjust(config.drags[i].adjust);
        d.setResetOnFail(config.drags[i].resetOnFail);

        d.removeListener('correct');
        d.on('correct', self.dragHit, self);
        if (config.failOnColision) d.on('colision', self.stopOnColision, self);
    }


    for (i = 0; i < config.areaDrags.length; i++) {

        var a = this.areaDrags[i];

        a.index = i;
        a.alpha = config.areaDrags[i].alpha;

        if (config.areaDrags[i].maxChildren !== undefined) {
            a.maxChildren = config.areaDrags[i].maxChildren;
        } else {
            a.maxChildren = this._drags.length;
        }

        a.disableDragOnHit = config.areaDrags[i].disableDragOnHit;
        a.corrects = config.areaDrags[i].corrects;
        a.alignMode = config.areaDrags[i].alignMode;
        a.vspace = config.areaDrags[i].vspace;
        a.hspace = config.areaDrags[i].hspace;
        //a._lockArea = false;

        if (config.areaDrags[i].alignMode === 'person') {
            a._points = config.areaDrags[i].points;
        }


        if (a.alignMode == "grid") {
            a.createGrid(config.areaDrags[i].createGrid[0], config.areaDrags[i].createGrid[1]);
        }
    }


};

/**
 * Faz o controle para ver se todos os areas drag estão preenchidos
 */
TDrag.prototype.stopOnColision = function(elements) {

    var drag = elements.drag;
    var object = elements.object;

    drag._moveEnabled = false;
    drag.removeEvents();

    this.openFail();
};

/**
 * Faz o controle para ver se todos os areas drag estão preenchidos
 */
TDrag.prototype.dragHit = function() {


    var validateDrags = true;

    for (var i = 0; i < this.areaDrags.length; i++) {
        if (this.areaDrags[i]._containerDrags.length !== this.areaDrags[i].maxChildren) {
            validateDrags = false;
        }
    }

    if (validateDrags) {
        this.removeDragEvents();
        if (this._interface.refreshButton !== undefined && this._interface.refreshButton !== null) {
            this._interface._refreshButton.removeEvents();
            this._interface.closeRefresh();
        }
        this.validateDrags();
    }

};

/**
 * Valida todas as areas drag e toma a decisão de avancar a tela ou nao
 * @fires parallax Evento que libera a tela para avancar
 * @fires avancarTela Evento que avanca automaticamente a tela
 */
TDrag.prototype.validateDrags = function() {

    var self = null;
    if (this.context !== undefined) self = this.context;
    else self = this;

    var advance = true;


    if (self._config.resetAllOnFail !== undefined && self._config.resetAllOnFail === true) {
        for (var i = 0; i < this.areaDrags.length; i++) {

            if (!self.areaDrags[i].checkCorrect()) {
                advance = false;
                self.areaDrags[i].clear();
            }
        }
    } else {
        for (var j = 0; j < self.areaDrags.length; j++) {

            if (!self.areaDrags[j].checkCorrect()) {
                advance = false;
                self.areaDrags[j].clearIncorrect();
                self.areaDrags[j]._align();
            }
        }
    }

    if (advance === true) self.openCorect();
    else self.openFail();

};

/** @function
 * @description openCorect
 */
TDrag.prototype.openCorect = function() {

    var self = this;

    if (self.correct) {
        self.correct.reset();
        self.correct.timeline.play();
        self._blocked();
        self.correct.once('pointComplete', function() {
            self._blocked();

            for (var l = 0; l < self._drags.length; l++) {
                self._drags[l]._parallaxX = self._drags[l].x;
                self._drags[l]._parallaxY = self._drags[l].y;
            }
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

/** @function
 * @description openFail
 */
TDrag.prototype.openFail = function() {

    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self._blocked();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._unblock();
            if (self._interface.refreshButton !== undefined && self._interface.refreshButton !== null) {
                self._interface.openRefresh();
                self._interface._refreshButton.addEvents();
            }
            self.addDragEvents();
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._unblock();
            if (self._interface.refreshButton !== undefined && self._interface.refreshButton !== null) {
                self._interface.openRefresh();
                self._interface._refreshButton.addEvents();
            }
            self.addDragEvents();
        }, 1000);
    }

};

TDrag.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }

    this._interface.openRefresh();
};

TDrag.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

TDrag.prototype.reset = function() {
    var self = this;

    TExercices.prototype.reset.call(this);

    self._blocked(true);

    for (var i = 0; i < self.areaDrags.length; i++) {

        self.areaDrags[i].clear();

    }

    for (var l = 0; l < self._drags.length; l++) {
        self._drags[l]._parallaxX = self._drags[l]._initialX;
        self._drags[l]._parallaxY = self._drags[l]._initialY;
    }

};

/** @function
 * @description verify
 */
TDrag.prototype.verify = function() {

    var self = null;
    if (this.context !== undefined) self = this.context;
    else self = this;


    self.removeDragEvents();

    self.validateDrags();

};

/** @function
 * @description cloase
 */
TDrag.prototype.close = function() {

    TExercices.prototype.close.call(this);

};
