var TExercices = require('./TExercices');

/**
 * configuração do template:
 * @example
 *
 {
     "path": "telas/bexiga/",
     "backgroundIndex": 3,                                  // indice da tela de background
     "template": {
         "type": "ballon",                                  // tipo do template (ex.: ?select_module=bexiga)
         "config": {
             "onHitValidation":false,                       // ver ???
             "text": {
                 "pt-BR": [
                 {
                     "id": 0,                               // id do texto
                     "text": "<a>A</a>",                    // textos da tela
                     "style":{                                  // estilo para o texto
                       "a":{
                         "fontSize": "36px",                    // tam. do fonte
                         "strokeThickness": 4,                  // negrito
                         "stroke": "#386162",                   // contorno, destaque
                         "fill": "white"                        // cor
                       }
                     },
                     "person": "nome do personagem",            // identificacao do personagem p/ roteiro
                     "audioID": 0                               // id do audio p/ roteiro
                 },
                 {
                   "observation": "Observação da tela."         // observacao do roteiro
                 }]
             },
             "audio": {                                         // audios da tela
                 "pt-BR": [{
                     "name": "7_1",                                 // nome do audio
                     "id": 0                                        // id do audio
                 },
               ]
             },
             "button": [{                                       // botao de audio
                 "img": [12],                                       // indice(s) da(s) imagen(s) do(s) botao(oes)
                 "animate": true,                                   // liga/desliga animacao do(s) botao(oes)
                 "action":{
                     "audio": {                                 // p/ botao de audio:
                         "id": 0,                                   // id do audio
                         "mouth": [0]                               // id da boca
                     }
                 }
             }
           ],
             "sequences": [{                    // sequencia a ser mostrada na tela:
                 "id": 0,                           // indice da sequencia
                 "images": [2],                     // imagem(s) a ser(em) exibida(s)
                 "mouth": [0],                      // boca(s) a ser(em) exibida(s)
                 "txt": [6],                        // texto(s) a ser(em) exibido(s)
                 "audio": 6,                        // audio da sequencia
                 "fadeOut": true                    // forma de encerrar a sequencia: true = desaparece / false = mantem
             },{
                 "id": 1,                           // idem demais sequencias...
                 "images": [3],
                 "mouth": [],
                 "txt": [],
                 "audio": null,
                 "fadeOut": false
             }],
             "drags": [{
                     "img": [18],                   // indice da img que representa o binoculo
                     "sensor": {
                         "x": 0,                    // ponto x de inicio do sensor de identificacao
                         "y": 50,                   // ponto y de inicio do sensor de identificacao
                         "width": 10,               // largura do sensor de identificacao
                         "height":10,               // altura do sensor de identificacao
                         "show": false              // exibe/oculta o sensor momentaneamente
                        }
             }],
             "ballons":[
                 {
                     "img":[6],                                 // imagem da bexiga
                     "text": [0],                               // texto da bexiga
                     "correct": true                            // status da bexiga positiva/negativa
                 },
                 {
                     "img":[7],                                 // idem para demais bexigas...
                     "text": [1],
                     "correct": false
                 }
             ],
             "response": {                      // balao de feed da tela:
                 "correct": {                       // resposta positiva
                     "images": [5],                 // imagem(s) a ser(em) exibida(s)
                     "txt": [9],                    // texto(s) a ser(em) exibido(s)
                     "audio": 9,                    // audio do feed
                     "mouth": [0]                   // boca(s) a ser(em) exibida(s)
                 },
                 "fail": {                          // resposta negativa
                     "images": [4],                 // imagem(s) a ser(em) exibida(s)
                     "txt": [8],                    // texto(s) a ser(em) exibido(s)
                     "audio": 8,                    // audio do feed
                     "mouth": [0]                   // boca(s) a ser(em) exibida(s)
                 }
             }
         }
     }
 }
 *
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TBallon
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */
function TBallon() {
    TExercices.call(this);

    this._drags = [];
    this.areaDrags = [];
    this.total_correct = 0;
    this.cont_correct = 0;

}

TBallon.prototype = Object.create(TExercices.prototype);
TBallon.prototype.constructor = TBallon;
module.exports = TBallon;

/**
 * Metodo que inicializa a classe
 */
TBallon.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this.ballons = this.layersDisplay.getObjects("ballon");

    for (var i = 0; i < this.ballons.length; i++) {
      for (var j = 0; j < this.ballons[i].children.length; j++) {
        var ballon = this.ballons[i].children[j];
        if(ballon._displayType == "bitmap"){
          ballon.tint = "0x00FFFF";
        }

      }
    }
    // this.ballons[0].children[0].tint = "0x77b505";

    for (var k = 0; k < this.ballons.length; k++) {
      if (this.ballons[k].config.correct){
        this.total_correct += 1;
      }
    }

    this.configureDrags();

};

/**
 * Metodo que executa a classe
 */
TBallon.prototype.start = function() {
    TExercices.prototype.start.call(this);
};

/**
 * Metodo que abre a classe
 */
TBallon.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

/**
 * Metodo para o termino das sequencias
 * @private
 */
TBallon.prototype._initInteractions = function() {
    var self = this;

    TExercices.prototype._initInteractions.call(this);

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._blocked();
            self.fadeRestart();
            self.removeDragEvents();
            self.on('sequenceComplete', function() {
                self._unblock();
                self.addDragEvents();
            });
        });
    });

    this._interface.openRefresh();

    this._unblock();
    this._block = false;

    this.addDragEvents();
};

/**
 * Metodo que adiciona eventos aos drags
 */
TBallon.prototype.addDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].addEvents();
    }
};

/**
 * Metodo que remove eventos dos drags
 */
TBallon.prototype.removeDragEvents = function() {

    for (var i = 0; i < this._drags.length; i++) {
        this._drags[i].removeEvents();
    }
};

/**
 * Configura os caracteristicas do drags baseadas no config.json
 */
TBallon.prototype.configureDrags = function() {
    var self = this;
    var config = this._config;

    this.ballons = this.layersDisplay.getObjects("ballon");
    this._drags = this.layersDisplay.getObjects('drag');
    var layerSup = this.layersDisplay.getLayer('sup');
    for (i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        var colisoes = [];

        d.id = i;

        d.setResetOnFail(true);
        d.on('positionUpdated', self.positionUpdated, self);
        d.parent.removeChild(d);
        layerSup.addChild(d);

    }
};

/**
 * Metodo que verifica se ha colisao com feeds positivo ou negativo
 * @param e {Object} evento
 */
TBallon.prototype.positionUpdated = function(e) {

    var sensor = e.target.getSensor();

    var x = (e.target.x - (e.target.width/2))+sensor.x;
    var y = (e.target.y - (e.target.height/2))+sensor.y;
    var width = sensor.width;
    var height = sensor.height;

    for (var i = 0; i < this.ballons.length; i++) {

      if (this.ballons[i].visible !== false && this.ballons[i].hasContent(x, y, width, height)) {
            this.validation(this.ballons[i]);
        }
    }
};

/**
 * Metodo que valida exercicio (feed)
 * @param e {Object} evento
 */
TBallon.prototype.validation = function(e) {

    var self = this;
    var agulha = this.layersDisplay.getLayer('sup').getChildAt(0);

    if (e.config.correct){
      this.cont_correct += 1;
      e.visible = false;
      self._interface.blowBallon(e.x,e.y);
      if (this.correct) {
        if (this.cont_correct == this.total_correct){
          this.correct.reset();
          this.correct.timeline.play();
          self._blocked();
          this.removeDragEvents();
          agulha._onUp();
          this.correct.once('pointComplete', function() {
              self._blocked();
              this.checkRoadMethod();
          }, this);
        }
      } else {
          setTimeout(function() {
              self.checkRoadMethod();
          }, 1000);
      }
    }

    if (e.config.correct === false){
      this.removeDragEvents();
      agulha._onUp();
      e.visible = false;
      self._interface.blowBallon(e.x,e.y);
      if (this.fail) {
          this.fail.reset();
          this.fail.timeline.play();
          self._blocked();
          this.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._unblock();
            e.visible = true;
            e.resetBallon(this);
          }, this);
      } else {
          this.emit('exercicioIncorreto');
          setTimeout(function() {
              this._unblock();
              this.addDragEvents();
          }, 1000);
      }
    }
};

/**
 * Metodo que desbloqueia os botoes
 * @private
 */
TBallon.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }

    this._interface.openRefresh();
};

/**
 * Metodo que bloqueia animacao
 * @param all {boolean}
 * @private
 */
TBallon.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

/**
 * Metodo que reinicia a tela
 * @param e {Object} evento
 */
TBallon.prototype.reset = function(e) {
    TExercices.prototype.reset.call(this);

    var self = this;

    for (var i = 0; i < this.ballons.length; i++) {
      this.ballons[i].visible = true;
    }

    this.cont_correct = 0;

    self._blocked(true);

};

/**
 * Metodo que fecha tela
 */
TBallon.prototype.close = function() {

  this.removeDragEvents();

};
