var TExercices = require('./TExercices');

/*
 * configuração do template:
 * @example
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TGenius
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */

function TGenius() {

/* config.json documentation is in the end of this script */

    TExercices.call(this);

    this._btns = [];//all buttons

    this.initParameters();

}

TGenius.prototype = Object.create(TExercices.prototype);
TGenius.prototype.constructor = TGenius;
module.exports = TGenius;

TGenius.prototype.initParameters = function(){
  this._seq = [];//index of buttons in the seauence
  this._currentSeq = 0;//current index of sequence
  this.score = 0;//game score
  this.level = 1;//game level

  this.timeSeq = 1;//time between show the current and the next sequence button
};

TGenius.prototype.init = function() {

    var self = this;

    TExercices.prototype.init.call(this);

    var config = this._config;

    var interactions = this.layersDisplay.getMap('interactions');
    this._TxtFeedPos = this.layersDisplay.getMap('text')[1];

    for (var i = 0; i < interactions.length; i++) {

      if(interactions[i]._config.geniuBtn === true){
        var id = this._btns.length;
        this._btns.push(this.layersDisplay.getMapByID('interactions', i));
        var cBtn = this._btns[id];
        cBtn.initW = cBtn.width;
        cBtn.initH = cBtn.height;

        var color = GetValidValue(config.genius.defaultColor, cBtn._config.color, false);
        var stroke = GetValidValue(config.genius.defaultStroke, cBtn._config.stroke, false);
        cBtn.createContour(stroke,parseInt(color));

        cBtn._id = id;
        cBtn.on("clicked", self.onClicked, self);
      }

    }

    self.on('VerifyButton', self.VerifyButton, { ctx : self});

    self.InitSeq();

};

TGenius.prototype.onClicked = function(e){this.emit('VerifyButton', {data : e.target });};

TGenius.prototype.InitShowSequence = function() {
  this.SetBtnsBlock(true);//console.log(this.level);
  var dif_level = this._config.genius.seqQttEnd - this._config.genius.seqQttInit;
  var rel_level = (dif_level - (this.level - 1)) / dif_level;
  var rel_time = Math.abs(this._config.genius.endTime - this._config.genius.initTime);
  var time = this._config.genius.endTime + (rel_level * rel_time);
  this.ShowSequence(0, time);
};

TGenius.prototype.ShowSequence = function(idx, time) {

  var self = this;

  var c_btn = idx < self._seq.length ? self._btns[self._seq[idx]] : null;
  var l_btn = idx - 1 >= 0 ? self._btns[self._seq[idx - 1]] : null;

  if(l_btn !== null){
    l_btn.closeContour();
    TweenMax.to(l_btn, 0, {
      alpha: 1,
      height: l_btn.initH,
      width: l_btn.initW,
    });
  }

  if(c_btn === null){self.SetBtnsBlock(false); return;}
  self.PlayButtonSound(c_btn._id);
  c_btn.openContour();
  TweenMax.to(c_btn, time, {
      alpha: 0.2,
      height: c_btn.initH * 1.1,
      width: c_btn.initW * 1.1,
      onComplete: function(){
          self.ShowSequence(idx + 1, time);
      }
  });

};

TGenius.prototype.SetBtnsBlock = function(b) {//b {Boolean} true -> disable all genius btns / false -> eneable
  var self = this;

  if (b === true) {for (var i = 0; i < self._btns.length; i++) {self._btns[i].removeEvents();}}
  else {for (var j = 0; j < self._btns.length; j++) {self._btns[j].addEvents();}}

};

TGenius.prototype.VerifyButton = function(e) {

    var self = this;
    var ctx = self.ctx;

    ctx.PlayButtonSound(e.data._id);
    if(e.data._id == ctx._seq[ctx._curretSeq]){//verify if is the correct

      if(ctx._curretSeq + 1 >= ctx._seq.length){//is the last of the sequence?

        if(ctx._seq.length < ctx._config.genius.seqQttEnd){//not is the limit seq quantity
          ctx.level += 1;
          ctx._curretSeq = 0;//reset current index of sequence
          self.ctx.RaffleSeq();//raffle the next index of sequence

          if (ctx._config.response.correct){//verify if exists correct response feed
            /* TENTATIVA DE MUDANÇA DE TEXTO DO FEED!!!
            // console.log(ctx._config.text["pt-BR"][1]);
            // ctx._config.text["pt-BR"][1].text = "Vamos para o level " + ctx.level;
            // console.log(ctx._TxtFeedPos);
            // var txt = "Vamos para o level " + ctx.level;
            // ctx._TxtFeedPos.setText(txt, ctx._TxtFeedPos.children[0]._custonStyle);
            */
            // ctx.correct.reset();
            // ctx.correct.timeline.play();
            // ctx.correct.once('pointComplete', function() {
            //   self.ctx.InitShowSequence(0);//init to show a sequence again
            // }, ctx);

            self.ctx.InitShowSequence(0);//init to show a sequence again

          }



        }
        else{

          ctx.SetBtnsBlock(true);

          if (ctx._config.response.correct){//verify if exists correct response feed

            ctx.correct.reset();
            ctx.correct.timeline.play();
            ctx._interface._refreshButton.visible = false;
            ctx.correct.once('pointComplete', function() {
                ctx._interface._refreshButton.visible = true;
                ctx.emit('parallax');
            }, ctx);
          }

        }

      }
      else {//continue in the next sequence
        ctx._curretSeq+=1;
      }

    }
    else{//if is the incorret

      ctx.SetBtnsBlock(true);

      if (ctx._config.response.fail){//verify if exists fail response feed

        ctx.fail.reset();
        ctx.fail.timeline.play();
        ctx.fail.once('pointComplete', function() {
            ctx._interface._refreshButton.visible = true;
            ctx.emit('exercicioIncorreto');
            ctx.initParameters();
            ctx.InitSeq();
            ctx.InitShowSequence();
        }, ctx);

      }

    }

};

TGenius.prototype.InitSeq = function() {//init sequence of genius
  var self = this;
  var qtt = self._config.genius.seqQttInit;
  self._seq = [];
  self._curretSeq = 0;
  for (var i = 0; i < qtt; i++) {self.RaffleSeq();}
};

TGenius.prototype.RaffleSeq = function() {//raffle a new index button to the sequence
  var self = this;
  self._seq.push( Math.floor((Math.random() * self._btns.length) ));
};

TGenius.prototype.PlayButtonSound = function(id_btn) {//raffle a new index button to the sequence
  var self = this;
  var audioIndex = self._btns[id_btn]._config.audio;
  self.soundManager.playSound(audioIndex);
};

TGenius.prototype.start = function() {
    TExercices.prototype.start.call(this);
};


TGenius.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

TGenius.prototype._initInteractions = function() {
  TExercices.prototype._initInteractions.call(this);
    var self = this;

    this._interface.once('refreshOpened', function() {

        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {

            this.initParameters();
            self.fadeRestart();

            self.once('sequenceComplete', function() {

            });
        });
    });

    for (var i = 0; i < this._btns.length; i++) {
      this._btns[i].initW = this._btns[i].width;
      this._btns[i].initH = this._btns[i].height;
    }

    this.InitShowSequence();
    this._interface.openRefresh();

};

TGenius.prototype.reset = function() {
  TExercices.prototype.reset.call(this);

  this.initParameters();
  this.InitSeq();
};

TGenius.prototype.close = function() {};

function GetValidValue(default_value, c_value, blank) {
//default_value = value if dont have a individual value (c_value)
//c_value       = individual value
//blank         = if dont have c_value, return "", without test default_value
    if ( !(IsValue(c_value)) ) {
        if ( !( IsValue(default_value) ) || blank) { return ""; }
        else { return default_value; }
    }
    return c_value;
}

function IsValue(e) {return e !== "" && typeof e !== "undefined" ? true : false;}

/***

config.json EXAMPLES

{
    "path": "telas/genius/",
    "backgroundIndex": 3,
    "template": {
        "type": "genius",
        "config": {
            "genius":{//genius parameters
              "seqQttInit":3,                //init raffle quantity of sequence
              "seqQttEnd":8,                 //end quantity of raffles of sequence
              "initTime": 1,                 //init time between the current and the next sequence (first sequence)
              "endTime": 0.5,                //end time between the current and the next sequence (last sequence)
              "defaultColor":"0x00000",     //default countor color
              "defaultStroke":"10"          //default countor stroke
            },
            "text": {
                "pt-BR": [
                {"text": "Tente lembrar a sequência..."},
                {"text": "Parabéns você conseguiu!"},
                {"text": "Que pena... Você esqueceu."}
              ]
            },
            "audio": {
                "pt-BR": [
                  {"name": "laser","id": 0},
                  {"name": "laser","id": 1},
                  {"name": "laser","id": 2},
                  {"name": "laser","id": 2}
                ]
            },
            "button": [
              {
                  "img": [1],
                  "animate": true
              },
              {
                  "img": [2,3],//obs: 2: button image index / 3: animal image index
                  "btnImg":2,//button image index
                  "audio" : 0,
                  "action":{"response" : "VerifyButton"},
                  "geniuBtn": true,//if is or not a genius button
                  "color":"0xd6e5dd",//custom countor color
                  "stroke":"20"//custom countor stroke
              },
              {
                  "img": [4,5],//obs: 4: button image index / 5: animal image index
                  "btnImg":4,//button image index
                  "audio" : 0,
                  "action":{"response" : "VerifyButton"},
                  "geniuBtn": true,//if is or not a genius button
                  "color":"0xc5e0e6",//custom countor color
                  "stroke":"20"//custom countor stroke
              },
              {
                  "img": [6,7],//obs: 6: button image index / 7: animal image index
                  "btnImg":6,//button image index
                  "audio" : 0,
                  "action":{"response" : "VerifyButton"},
                  "geniuBtn": true,//if is or not a genius button
                  "color":"0xe5c1c5",//custom countor color
                  "stroke":"20"//custom countor stroke
              },
              {
                  "img": [8,9],//obs: 8: button image index / 9: animal image index
                  "btnImg":8,//button image index
                  "audio" : 0,
                  "action":{"response" : "VerifyButton"},
                  "geniuBtn": true,//if is or not a genius button
                  "color":"0xf8f2b0",//custom countor color
                  "stroke":"20"
              }
            ],
            "sequences": [{
                "id": 0,
                "images": [10],
                "mouth": [0],
                "txt": [0],
                "audio": 0,
                "fadeOut": false
            }],
            "response": {
                "correct": {
                    "images": [11],
                    "txt": [1],
                    "audio": 1,
                    "mouth": [0]
                },
                "fail": {
                    "images": [12],
                    "txt": [2],
                    "audio": 1,
                    "mouth": [0]
                }
            }
        }
    }
}


***/
