var TExercices = require('./TExercices');

/**
 * configuração do template:
 * @example
 *
 "config": {
 }
 *
 * @classdesc Classe que define o Template de bussula
 * @memberof module:templates/templates
 * @exports TCompass
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */

function TCompass() {
    TExercices.call(this);
}

TCompass.prototype = Object.create(TExercices.prototype);
TCompass.prototype.constructor = TCompass;
module.exports = TCompass;

TCompass.prototype.init = function() {
    TExercices.prototype.init.call(this);

	this._compassess = this.layersDisplay.getObjects('compass');

	this._resetCompasses();
};

TCompass.prototype.start = function() {
    TExercices.prototype.start.call(this);
};

TCompass.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

TCompass.prototype._initInteractions = function() {
    var self = this;

    TExercices.prototype._initInteractions.call(this);

	this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self.fadeRestart();
            self.removeCompassEvents();
            self.on('sequenceComplete', function() {
				var compasses = self._compassess.filter(function(compass) {
					return self._corrects.indexOf(compass) < 0; //não existe no array de acertos
				});
                self.addCompassEvents(compasses);
            });
        });
    });

    this._interface.openRefresh();

	this.addCompassEvents();
};

TCompass.prototype._resetCompasses = function() {
	this._compassess.forEach(function(compass) {
		if(compass.config.totalPositions !== undefined && compass.config.totalPositions !== null) {
			compass.setTotalPositions(compass.config.totalPositions);
		}
		this._setRotation(compass);
	}, this);

	this._corrects = [];
};

TCompass.prototype._setRotation = function(compass, hour) {
	var h = hour || compass.config.initialPosition || 0;
	var angle = h > 11 || h < 0 ? 0 : h*30;
	compass.setRotation(angle);
};

TCompass.prototype._rotationStarted = function(e) {
	var self = this;

	var compasses = self._compassess.filter(function(compass) {
		return compass !== e.target; //não é o target do evento;
	});

	self.removeCompassEvents(compasses);
};

TCompass.prototype._rotationEnded = function(e) {
	var self = this;
	var compasses = null;

	if((e.target.config.correctPosition*30) == e.angle) {
		self.removeCompassEvents([e.target]);
		self._corrects.push(e.target);

		compasses = self._compassess.filter(function(compass) {
			return self._corrects.indexOf(compass) < 0; //não existe no array de acertos
		});

		if(self._compassess.length == self._corrects.length) {
			self.openCorrect();
		} else {
			self.addCompassEvents(compasses);
		}
	} else {
		compasses = self._compassess.filter(function(compass) {
			return self._corrects.indexOf(compass) < 0; //não existe no array de acertos
		});

		self.removeCompassEvents(compasses);
		self.openFail();

		self.once('feedComplete', function() {
			self._setRotation(e.target);

			self.addCompassEvents(compasses);
		});
	}
};

/**
 * Metodo que adiciona eventos aos "compassess"
 */
TCompass.prototype.addCompassEvents = function(arrCompass) {
	var compasses = arrCompass || this._compassess;
    for (var i = 0; i < compasses.length; i++) {
		compasses[i].on('rotationStarted', this._rotationStarted, this);
		compasses[i].on('rotationEnded', this._rotationEnded, this);
        compasses[i].addEvents();
    }
};

TCompass.prototype.removeCompassEvents = function(arrCompass) {
	var compasses = arrCompass || this._compassess;
	compasses.forEach(function(compass) {
		compass.removeEvents();
		compass.removeListener('rotationEnded', this._rotationEnded);
	}, this);
};

TCompass.prototype.openCorrect = function() {

    var self = this;

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

TCompass.prototype.openFail = function() {

    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, 1000);
    }

};

TCompass.prototype.verify = function() {};

TCompass.prototype.reset = function() {
    TExercices.prototype.reset.call(this);
	this._resetCompasses();
};

TCompass.prototype.close = function() {
    TExercices.prototype.close.call(this);
	this.removeCompassEvents();
};
