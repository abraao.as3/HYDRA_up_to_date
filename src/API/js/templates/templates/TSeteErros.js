var TExercices = require('./TExercices');
var Clock = require('../../display/complex/Clock');

/**
 * configuração do template:
 * @example
 *
 * Falta criar exemplo.
 *
 * @classdesc TSeteErros Class
 * @memberof module:templates/templates
 * @exports TSeteErros
 * @extends module:templates/core.ITemplate
 * @constructor
 */
function TSeteErros() {
    TExercices.call(this);
}

TSeteErros.prototype = Object.create(TExercices.prototype);
TSeteErros.prototype.constructor = TSeteErros;
module.exports = TSeteErros;

TSeteErros.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this._erros = this.layersDisplay.getMap('seteErros');
    this._errosCompletos = 0;

};

/** @function
* @description start
*/
TSeteErros.prototype.start = function() {
    TExercices.prototype.start.call(this);
    this._errosCompletos = 0;

    this._errosCompletos = 0;

    for (var i = 0; i < this._erros.length; i++) {
        this._erros[i].initSeteErros();
    }

};

/** @function
* @description open
*/
TSeteErros.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

/** @function
* @description _initInteractions
* @private
*/
TSeteErros.prototype._initInteractions = function() {
    TExercices.prototype._initInteractions.call(this);
    var self = this;

    self.onSeteErrosEvents();

	self._interface.once('refreshOpened', function() {

        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {

            this.visible = false;
            self.fadeRestart();
            self.offSeteErrosEvents();

            self.once('sequenceComplete', function(){

                self._interface._refreshButton.visible = true;
                self.onSeteErrosEvents();

                self._removeIntEvents();
                self._addIntEvents();
            });
        });
    });

    self._interface.openRefresh();

};

/** @function
* @description onSeteErrosEvents
*/
TSeteErros.prototype.onSeteErrosEvents = function() {
    var self = this;

    for (var i = 0; i < self._erros.length; i++) {
        self._erros[i].addSeteErrosEvents();
        self._erros[i].on("exercicioCompleto", self.verify, { ctx : self._erros[i], tela : self});

    }


};

/** @function
* @description offSeteErrosEvents
*/
TSeteErros.prototype.offSeteErrosEvents = function() {
    var self = this;

    for (var i = 0; i < self._erros.length; i++) {
        self._erros[i].removeSeteErrosEvents();
        self._erros[i].off("exercicioCompleto");
    }

    self._removeIntEvents();

};

/** @function
* @description openCorrect
*/
TSeteErros.prototype.openCorrect = function() {

    var self = this;
    self.offSeteErrosEvents();
    self._interface._refreshButton.visible = false;

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

/** @function
* @description openFail
*/
TSeteErros.prototype.openFail = function() {

    var self = this;
    self.offSeteErrosEvents();
    self._interface._refreshButton.visible = false;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {

            self._interface._refreshButton.visible = true;
            self.onSeteErrosEvents();

            self._removeIntEvents();
            self._addIntEvents();

            // this._interface.openRefresh();''
            // this._interface._refreshButton.addEvents();

            self.emit('exercicioIncorreto');


        }, self);
    } else {
        setTimeout(function() {

            self._interface._refreshButton.visible = true;

            self.onSeteErrosEvents();

            self._removeIntEvents();
            self._addIntEvents();

            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
            self.emit('exercicioIncorreto');
        }, 1000);
    }

};

/** @function
* @description verify
*/
TSeteErros.prototype.verify = function() {

    var self = this;

    self.tela._errosCompletos++;
    if (self.tela._errosCompletos == self.tela._erros.length) {
        self.tela.openCorrect();
    }
    // Object.keys(self._erros).forEach(function(key) {
    //     self._erros[key].verifySeteErros();
    // });
};

/** @function
* @description reset
*/
TSeteErros.prototype.reset = function() {
    TExercices.prototype.reset.call(this);


};

/** @function
* @description close
*/
TSeteErros.prototype.close = function() {
    TExercices.prototype.close.call(this);
};
