var ITemplate = require('../core/ITemplate');
// var behavior = require('../../behavior/index');
var TKeyboard = require('./TKeyboard');

/**
* configuração do template:
* <br/>`Veja as opções completas em {@link module:animation.Point}`
* @example
*
* Falta criar exemplo
* @classdesc TWord Class
* @memberof module:templates/templates
* @exports TWord
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @see module:animation.Point
* @constructor
*/
function TWord() {
    ITemplate.call(this);
	TKeyboard.call(this);
}

TWord.prototype = Object.create(ITemplate.prototype);
TWord.prototype.constructor = TWord;
module.exports = TWord;

/** @function
* @description init
*/
TWord.prototype.init = function() {
    ITemplate.prototype.init.call(this);
	TKeyboard.prototype.init.call(this);

    this.fadeStart();
};

/** @function
* @description start
*/
TWord.prototype.start = function() {
    ITemplate.prototype.start.call(this);
};

/** @function
* @description open
*/
TWord.prototype.open = function() {
	ITemplate.prototype.open.call(this);
    TKeyboard.prototype.open.call(this);

    this.fadeOpen();
};

/** @function
* @description _initInteractions
* @private
*/
TWord.prototype._initInteractions = function() {
    ITemplate.prototype._initInteractions.call(this);
	TKeyboard.prototype._initInteractions.call(this);
};

/** @function
* @description _focus
* @private
*/
TWord.prototype._focus = function(e) {
	TKeyboard.prototype._focus.call(this, e);
};

/** @function
* @description _keyPressed
* @private
*/
TWord.prototype._keyPressed = function(e) {
	var self = this;

	TKeyboard.prototype._keyPressed.call(this, e);

	if (e.value !== "backspace") {
		self.checkRoadMethod();
	}
};

/** @function
* @description addInputEvents
*/
TWord.prototype.addInputEvents = function(arrInputs) {
	TKeyboard.prototype.addInputEvents.call(this);
};

/** @function
* @description removeInputEvents
*/
TWord.prototype.removeInputEvents = function(arrInputs) {
	TKeyboard.prototype.removeInputEvents.call(this);
};

/** @function
* @description addKeyboardEvents
*/
TWord.prototype.addKeyboardEvents = function() {
	TKeyboard.prototype.addKeyboardEvents.call(this);
};

/** @function
* @description removeKeyboardEvents
*/
TWord.prototype.removeKeyboardEvents = function() {
	TKeyboard.prototype.removeKeyboardEvents.call(this);
};

/** @function
* @description close
*/
TWord.prototype.close = function() {
    TKeyboard.prototype.close.call(this);
};

/** @function
* @description reset
*/
TWord.prototype.reset = function() {
    TKeyboard.prototype.reset.call(this);
};
