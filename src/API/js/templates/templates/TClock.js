var TExercices = require('./TExercices');
var Clock = require('../../display/complex/Clock');

/**
 * configuração do template:
 * @example
 *
 "clocks": [{
     "id": 0, // Id do Clock
     "perpetual": false, //Se o relogio for utilizado em telas nao de exercicio ( relogio de parede )
     "img": [7], // Img do corpo do relogio
     "pointerH": [8], // Img do ponteiro das Horas
     "pointerM": [9], // Img do ponteiro dos Minutos
     "correctPositionH": 4, // Se o Relogio for perpetual, estes quatro parametros nao se aplicam ( podem ser ignorados )
     "initialPositionH": 0, // Se o relogio for usado em animacoes, pode-se usar os initialPosition
     "correctPositionM": 3, //
     "initialPositionM": 2,  //
     "clockSequences" : [{
        "pointId" : 0,
        "tempoAnim" : 3,
        "finalHPosition" : 5,
        "finalMPosition" : 6
    },
    {
       "pointId" : 1,
       "tempoAnim" : 5,
       "finalHPosition" : 11,
       "finalMPosition" : 6
    }]
 }]
 *
 * @classdesc Classe que define o Clock e exercicios de relogio
 * @memberof module:templates/templates
 * @exports TClock
 * @extends module:templates/core.ITemplate
 * @constructor
 */

function TClock() {
    TExercices.call(this);
}

TClock.prototype = Object.create(TExercices.prototype);
TClock.prototype.constructor = TClock;
module.exports = TClock;

TClock.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this._clocks = this.layersDisplay.getObjects('clock');

};

TClock.prototype.start = function() {
    TExercices.prototype.start.call(this);

};

TClock.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

TClock.prototype._initInteractions = function() {
    TExercices.prototype._initInteractions.call(this);
    var self = this;

	self._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {

            self.fadeRestart();
            self.offClockEvents();
            self.once('sequenceComplete', function(){
                self.onClockEvents();
            });
        });
    });

    self._interface.openRefresh();
    self.onClockEvents();

};

TClock.prototype.onClockEvents = function() {
    var self = this;

    Object.keys(self._clocks).forEach(function(key) {
        if (!self._clocks[key].perpetual) {
            self._clocks[key].addClockEvents();
            self._clocks[key].on('correct', self.openCorrect, self);
            self._clocks[key].on('incorrect', self.openFail, self);
        }
    });

};

TClock.prototype.offClockEvents = function() {
    var self = this;

    Object.keys(self._clocks).forEach(function(key) {
        if (!self._clocks[key].perpetual) {
            self._clocks[key].removeClockEvents();
            self._clocks[key].off('correct');
            self._clocks[key].off('incorrect');
        }
    });

};

TClock.prototype.openCorrect = function() {

    var self = this;
    self.offClockEvents();

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            this.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

TClock.prototype.openFail = function() {

    var self = this;
    self.offClockEvents();

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();

            Object.keys(self._clocks).forEach(function(key) {
                self._clocks[key].resetClock();
            });
            self.onClockEvents();

        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();

            Object.keys(self._clocks).forEach(function(key) {
                self._clocks[key].resetClock();
            });
            self.onClockEvents();

        }, 1000);
    }

};

TClock.prototype.verify = function() {

    var self = this.context;

    Object.keys(self._clocks).forEach(function(key) {
        self._clocks[key].verifyClock();
    });

};

TClock.prototype.reset = function() {
    TExercices.prototype.reset.call(this);

    var self = this;

    Object.keys(self._clocks).forEach(function(key) {
        self._clocks[key].resetClock();
    });
    //self.onClockEvents();

    // Object.keys(self._clocks).forEach(function(key) {
    //     if (!self._clocks[key].perpetual) {
    //         self._clocks[key].setRotation(0);
    //     }
    // });

};

TClock.prototype.close = function() {
    TExercices.prototype.close.call(this);
};
