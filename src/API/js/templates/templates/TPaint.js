var TDialog = require('./TDialog');
var PIXIConfig = require('../../core/PIXIConfig');
var Const = require('../../core/Const');
var $ = require('jquery');

/**
* configuração do template:
* @example
*
* Falta criar exemplo.
*
 * @classdesc TPaint Class
 * @memberof module:templates/templates
 * @exports TPaint
 * @constructor
 */
function TPaint(){
    TDialog.call(this);

    this._paint = this._interface.paint;

    this._backgroundDOM = $(document.createElement('div'));
    this._backgroundDOM.css({
        position: 'absolute',
        overflow: 'hidden',
        zIndex: -100
    });
    this._imageBackground = null;
    this._canvasView = $(PIXIConfig.renderer.view);

    this._factorScale = 1;
}

TPaint.prototype = Object.create(TDialog.prototype);
TPaint.prototype.constructor = TPaint;
module.exports = TPaint;

/** @function
* @description init
*/
TPaint.prototype.init = function(){
    TDialog.prototype.init.call(this);

    this._paint.init();
    this.addChild(this._paint);

    var layerBack = this.layersDisplay.getLayer('background');
    this._imageBackground = $(PIXIConfig.renderer.extract.image(layerBack));
    layerBack.visible = false;

    this._imageBackground.css({
        width: '100%',
        height: '100%',
        position: 'relative'
    });

    this._backgroundDOM.append(this._imageBackground);
    $('body').first().prepend(this._backgroundDOM);

    this._backgroundDOM.css('display', 'none');
};

/** @function
* @description start
*/
TPaint.prototype.start = function(){
    TDialog.prototype.start.call(this);
    var self = this;

    this._paint.start();
    this._paint.closeToolbar();

    this._initEnterFrame();

    this._backgroundDOM.css('display', 'block');
    this._domResize();
};

TPaint.prototype.open = function(){
    TDialog.prototype.open.call(this);

    this._endEnterFrame();
};

TPaint.prototype._initInteractions = function(){
    this._paint.openToolbar(true);
    this._paint.once('drawinboard', this._allowButtonNext, this);
};

/** @function
* @description resize
* @param factorScale {number} factorScale
* @param canvasPos {number} canvasPos
*/
TPaint.prototype.resize = function(factorScale, canvasPos){
    this._factorScale = factorScale;

    this._paint.resize(factorScale, canvasPos);


    this._domResize();
};

TPaint.prototype._domResize = function(){
    this._backgroundDOM.width(this._canvasView.width());
    this._backgroundDOM.height(this._canvasView.height());
    this._backgroundDOM.offset({
        left: this._canvasView.offset().left,
        top: this._canvasView.offset().top
    });
};

TPaint.prototype._domPosition = function(){
    var result = this.x * this._factorScale;
    this._imageBackground.css('left', result + 'px');
    this._paint.moveBoard(result);
};

TPaint.prototype._finalizePage = function(){

};

TPaint.prototype._initEnterFrame = function(){
    var self = this;
    this._enterframe = setInterval(function(){
        self._domPosition();
    }, 1000 / 60);
};

TPaint.prototype._endEnterFrame = function(){
    clearInterval(this._enterframe);
    this._domPosition();
};

TPaint.prototype._allowButtonNext = function(){
    this.emit('allowNext');
};

TPaint.prototype.close = function(){
    TDialog.prototype.close.call(this);

    this._paint._saveForm();
    this._paint.closeToolbar();
    this._paint.removeAllDrawEvents();

    this._initEnterFrame();
};

TPaint.prototype.reset = function(){
    TDialog.prototype.reset.call(this);

    this._backgroundDOM.css('display', 'none');

    this._paint.reset();

    this._endEnterFrame();
};
