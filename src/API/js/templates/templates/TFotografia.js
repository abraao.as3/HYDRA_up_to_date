var TExercices = require('./TExercices');
var Layers = require('../../display/core/Layers');
var Bitmap = require('../../display/core/Bitmap');
var Interface = require('../../controller/InterfaceHandler');
var PIXIConfig = require('../../core/PIXIConfig');
var Sillhuette = require('../../utils/Sillhuette');
/*
 * configuração do template:
 * @example
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TFotografia
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */

function TFotografia() {

/* config.json documentation is in the end of this script */

    TExercices.call(this);

    this._drags = [];//drag of mask

    this._blackout = null;//image of black backgorund

    this.areaDrags = [];

    this.bg = null;//background

    this.galPosY = null; // incio da posição y das fotos

    this.speedZ = null; // velocidade do zoom out

    this.timeViz = null; // tempo de parada da foto para o aluno visualizar antes da foto ir para a galeria

    this.feedsO = []; //array de imagens do feed positivo

    this.feedsI = []; //array de imagens do feed negativo

    this.qtdCorrects = 0;

    this.spaceY = null; // espaço entre as fotos na galeria

    this._interfacefoto = null;

    this.texture = null;

    this._foto = null;

    this.cont=0;

    this.paperPhoto = [];
}

TFotografia.prototype = Object.create(TExercices.prototype);
TFotografia.prototype.constructor = TFotografia;
module.exports = TFotografia;

TFotografia.prototype.init = function() {

    TExercices.prototype.init.call(this);

    this.configureDrags();
    var config = this._config;
    var images = this.layersDisplay.getMap('images');



    //define os valores para o array this.feedsO
    for (var i = 0; i < config.feedsO.imgs.length; i++) {//feedsO array

      var img = GetImgByIndex(images, config.feedsO.imgs[i].id);

      if(img){//ferifica se imagens existe
        var id = this.feedsO.length;
        this.feedsO.push(img);

        var thickness = GetValidValue(config.feedsO.defaultThickness, config.feedsO.imgs[id].thickness, false);
        var color = GetValidValue(config.feedsO.defaultColor, config.feedsO.imgs[id].color, false);

        this.feedsO[id].createContour(thickness,parseInt(color));
        this.feedsO[id].hit = false;
      }
    }

      //define os valores para o array this.feedsI
      for (var j = 0; j < config.feedsI.imgs.length; j++) {//feedsI array
        var img1 = GetImgByIndex(images, config.feedsI.imgs[j].id);
        if(img1){//only images that exists
          var idi = this.feedsI.length;
          this.feedsI.push(img1);

          var thicknessi = GetValidValue(config.feedsI.defaultThickness, config.feedsI.imgs[idi].thickness, false);
          var colori = GetValidValue(config.feedsI.defaultColor, config.feedsI.imgs[idi].color, false);

          this.feedsI[idi].createContour(thicknessi,parseInt(colori));
          this.feedsI[idi].hit = false;


        }
      }

      //valores padrão, caso não seja informado no config.json
      this.galPosY = 30;
      this.speedZ = 0.2;
      this.timeVis = 1;
      this.spaceY = 0 ;
      //armazena as configurações de config.json
      if( config.GaleryInitPosY ){this.galPosY = config.GaleryInitPosY;}
      if( config.speedZoomOut ){this.speedZ = config.speedZoomOut;}
      if( config.timeVisualization ){this.timeVis = config.timeVisualization;}
      if( config.spacePhoto ){this.spaceY = config.spacePhoto;}
      this.feedIimg =  GetImgByIndex(images, config.feedsI.feedImg);
      this.feedOimg =  GetImgByIndex(images, config.feedsO.feedImg);
      this.luzImg =  GetImgByIndex(images, config.feedsO.imgLuz);


      this.feedOimg.visible = false;
      this.feedIimg.visible = false;
      this.luzImg.visible = false;

};

TFotografia.prototype.start = function() {
    TExercices.prototype.start.call(this);
};


TFotografia.prototype.open = function() {
    TExercices.prototype.open.call(this);

    var self = this;
    var config = this._config;

};

TFotografia.prototype._initInteractions = function() {
  TExercices.prototype._initInteractions.call(this);
    var self = this;

    this._interface.once('refreshOpened', function() {

        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self.fadeRestart();
            self.removeDragEvents();
            self.once('sequenceComplete', function() {
                self.addDragEvents();
            });
        });
    });

    this._interface.openRefresh();
    this.addDragEvents();
};

/**
 * Metodo que adiciona eventos aos drags
 */
TFotografia.prototype.addDragEvents = function() {for (var i = 0; i < this._drags.length; i++) {this._drags[i].addEvents();}};

/**
 * Metodo que remove eventos dos drags
 */
TFotografia.prototype.removeDragEvents = function() {for (var i = 0; i < this._drags.length; i++) {this._drags[i].removeEvents();}};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
TFotografia.prototype.configureDrags = function() {
    var self = this;
    var config = this._config;

    var images = this.layersDisplay.getObjects('images');
    var texts = this.layersDisplay.getObjects('text');

    this._drags = this.layersDisplay.getObjects('drag');
    var layerSup = this.layersDisplay.getLayer('sup');
    for (i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        d.id = i;
        d.setResetOnFail(true);
        d.on('dragReleased', self.dragReleased, self);

    }

};

TFotografia.prototype.dragReleased = function(e) {

    var sensor = e.target._sensor;

    var x = (e.target.x - (e.target.width/2))+sensor.x;
    var y = (e.target.y - (e.target.height/2))+sensor.y;
    var width = sensor.width;
    var height = sensor.height;

    //verifica sobreposição com os feed positivos
    for (var i = 0; i < this.feedsO.length; i++) {
      var cFeed = this.feedsO[i];
      var hit = cFeed.hasContent(x - cFeed.parent.x, y - cFeed.parent.y, width, height)? true : false;
      if (hit) {this.CorrectValidation(cFeed);}
    }

    //verifica sobreposição com os feed negativos
    for (var k = 0; k < this.feedsI.length; k++) {
      var cFeedi = this.feedsI[k];
      var hiti = cFeedi.hasContent(x, y, width, height) && cFeedi.visible ? true : false;
      if (hiti) {this.WrongValidation(cFeedi);continue;}
    }
};

TFotografia.prototype.CorrectValidation = function(e) {//MEPG
  var self = this;

  if(!e.hit){

      e.hit = true;

      e.openContour();

      self.qtdCorrects += 1;
      this.feedOimg.x =  e.x + 30;
      this.feedOimg.y =  e.y - 20;
      this.feedOimg.visible = true;
      this.luzImg.visible = true;
      var yf = this.galPosY;
      var f = this.feedOimg;
      var l = this.luzImg;
      TweenMax.to(this.luzImg, 1, {alpha: 0});
      TweenMax.to(this.feedOimg, 1, {alpha: 0});
      setTimeout(function(){
          self.polaroide(e,yf);// chama as fotos
      },500);
      setTimeout(function(){
          f.visible = false;
          f.alpha = 1;
          l.visible = false;
          l.alpha = 1;

      },1100);

      if(self.qtdCorrects >= self.feedsO.length){//Feed positivo
        self.resetDragPosition(this._drags[0], 300);
        self.correct.reset();
        self.correct.timeline.play();
        self.removeDragEvents();

        this._interface._refreshButton.visible = false;

        self.correct.once('pointComplete', function() {
            this._interface._refreshButton.visible = true;
            this.checkRoadMethod();
        }, self);

      }
      if(this.spaceY>10){this.spaceY=10;}
      if(this.spaceY < -40){this.spaceY= -40;}
      this.galPosY += 100 - 10 + this.spaceY;
    }

};

TFotografia.prototype.polaroide = function(e,yf){//MPG

    var width = 300;
    var height = 310;
    var rect = new PIXI.Graphics();
    rect.lineStyle(20,0xffffff);
    rect.beginFill(0xDDDDDD);
    rect.drawRect(0, 0, width, height);
    rect.endFill();
    rect.x = 340;
    rect.y = 90;

    this.paperPhoto[this.cont] = new PIXI.Container();//crio o papel fotografico
    this.paperPhoto[this.cont].addChild(rect);

    var texture = e._sprite._texture.clone();
    var img = new Bitmap(texture);
    img.width = img.width +25;
    img.height = img.height + 12;
    img.x = rect.x + rect.width / 2 - img.width/2 - rect.lineWidth / 2 ;
    img.y = rect.y + rect.height / 2 - img.height/2;
    this.paperPhoto[this.cont].addChild(img);
    c = this.addChild(this.paperPhoto[this.cont]);

    TweenMax.to(c,this.speedZ, {width:200, height:210, x:200, y:90});//menos zoom
    setTimeout(function(){
        TweenMax.to(c, 0.3, {x:780, y: yf});
        TweenMax.to(c, 0.2, {width:90, height:90});//menos zoom

    },this.timeVis*1000);

    console.log("->"+this.cont);
    this.cont++;

};

TFotografia.prototype.WrongValidation = function(e) {

  var self = this;

  if(!e.hit){
    this.resetDragPosition(this._drags[0], 300);
    this.feedIimg.visible = true;
    this.feedIimg.x =  e.x;
    this.feedIimg.y =  e.y;
    var f = this.feedIimg;
    TweenMax.to(this.feedIimg, 2, {alpha: 0});
    setTimeout(function(){
        f.visible = false;
        f.alpha = 1;
    },2000);

    e.hit = true;
    e.openContour();
    this._drags[0]._onUp();
    self.removeDragEvents();

    this._interface._refreshButton.visible = false;

    if (self.fail) {
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            this._interface._refreshButton.visible = true;
            this.emit('exercicioIncorreto');
            self.addDragEvents();
            this.fail.reset();
            e.closeContour();
            e.hit = false;
            this.feedIimg.visible = false;
        }, self);
    } else {
      this.emit('exercicioIncorreto');
        setTimeout(function() {
            self.addDragEvents();
            this.fail.reset();
            e.closeContour();
            e.hit = false;
            this.feedIimg.visible = false;

        }, 300);
    }

  }

};

TFotografia.prototype.resetDragPosition = function(e, time) {

  setTimeout(function() {//reset the drag position

    TweenMax.killTweensOf(e);

  }, time);

};

TFotografia.prototype.reset = function() {
  TExercices.prototype.reset.call(this);

this.ResetScreen();
    //uncolor

};


TFotografia.prototype.close = function() {};

TFotografia.prototype.ResetScreen = function() {//Reset da tela. MPG

  this.qtdCorrects = 0;
  this.galPosY = 38;
  this.resetDragPosition(this._drags[0], 300);
  this.removeDragEvents();
  this._drags[0]._onUp();

  //Destroi as fotos
  for (var i = 0; i < this.paperPhoto.length; i++) {
      this.paperPhoto[i].destroy();
      this.removeChild(this.paperPhoto[i]);
  }

  //Removo os contornos do feed positivo
  for (var j = 0; j < this.feedsO.length; j++) {
      this.feedsO[j].closeContour();
      this.feedsO[j].hit = false;
  }

  //Removo os contornos do feed negativo
  for (var k = 0; k < this.feedsI.length; k++) {
      this.feedsI[k].closeContour();
      this.feedsI[k].hit = false;
  }

  this.cont=0;//reseto o contador do array das fotos
};





function GetImgByIndex(images, idx){

  for (var i = 0; i < images.length; i++) {
    var c_img = images[i];
    if(c_img.index == idx){return c_img;}
  }

}

//TFotografia.prototype.GetValidValue = function (default_value, c_value, blank) {
function GetValidValue(default_value, c_value, blank) {
//default_value = value if dont have a individual value (c_value)
//c_value       = individual value
//blank         = if dont have c_value, return "", without test default_value
    if ( !(IsValue(c_value)) ) {
        if ( !( IsValue(default_value) ) || blank) { return ""; }
        else { return default_value; }
    }
    return c_value;
}

// TFotografia.prototype.IsValue = function (e) {return e != "" && typeof e !== "undefined" ? true : false;}
function IsValue(e) {return e !== "" && typeof e !== "undefined" ? true : false;}

/**** EXEMPLO FOTOGRAFIA CONFIG.json Pinheiro

{
    "path": "telas/fotografia/",
    "backgroundIndex": 1,
    "template": {
        "type": "fotografia",
        "config": {
            "GaleryInitPosY":40,   //posição vertical da galeria de fotos
            "speedZoomOut":0.2,    //Velocidade do menos-zoom quando a foto aparece ex: 0.2 ou 1...2...3 segundos
            "timeVisualization":1.5,  //Tempo de parada da foto para o aluno visualizar antes da foto ir para a galeria
            "spacePhoto":0,  //Espaço vertical entre as fotos. Aceita inteiros entre -40 e 10

            "text": {
                "pt-BR": [{
                    "text": "e a sua vez de fotografar"
                }, {
                    "text": "Parabéns!"
                }, {
                    "text": "Tente novamente!"
                }]
            },
            "audio": {
                "pt-BR": [{
                    "name": "7_1",
                    "id": 0
                }, {
                    "name": "7_2",
                    "id": 1
                }, {
                    "name": "7_2",
                    "id": 2
                }, {
                    "name": "7_2",
                    "id": 3
                }, {
                    "name": "7_2",
                    "id": 4
                }, {
                    "name": "7_2",
                    "id": 5
                }, {
                    "name": "7_2",
                    "id": 6
                }]
            },
            "sequences": [
                {
                    "id":0,
                    "img": 11,
                    "mouth":[0],
                    "txt": [0],
                    "audio": 0,
                    "fadeOut": false
                }
            ],

            "drags": [
                {
                    "img": [12],
                    "layerDisplacement": 1,
                    "sensor": {
                        "x": 50,
                        "y": 50,
                        "width": 20,
                        "height": 20,
                        "show": false
                    }
                }
            ],
            "response":{
                "fail": {
                    "images": [13],
                    "txt": [2],
                    "audio": 2,
                    "mouth": [0]
                },
                "correct": {
                    "images": [14],
                    "txt": [1],
                    "audio": 1,
                    "mouth": [0]
                }
            },
            "feedsO":{  //Imagens do feed positivo
              "imgs":[
                {"id":3, "thickness":10, "color":"0x77b505"},
                {"id":4, "thickness":10, "color":"0x77b505"},
                {"id":5, "thickness":10, "color":"0x77b505"}

              ],
              "feedImg":10,              //Imagem que aparece ao "fotografar" ex: Imagem com o brilho concentrado do flash
              "imgLuz":15,               //imagem de clarão do flash no ambiente, pisca um clarão na tela ao fotografar, null não mostra o clarão
              "defaultThickness":50,     // Expessura do contorno
              "defaultColor":"0xffffff"  //Cor do contorno
            },
            "feedsI":{//Imagens do feed negativo
              "imgs":[
                {"id":6, "thickness":10, "color":"0xAA0000"},
                {"id":7, "thickness":10, "color":"0xAA0000"},
                {"id":8,"thickness":10, "color":"0xAA0000"}
              ],
              "feedImg":9,                //Imagem que aparece ao tentar "fotografar" e errar ex: Imagem das linhas de enquadramento com um x Vermelho indicando erro
              "defaultThickness":50,      // Expessura do contorno
              "defaultColor":"0xffffff"   //Cor do contorno, geralmente vermelho
          }
        }
    }
}


*****/
