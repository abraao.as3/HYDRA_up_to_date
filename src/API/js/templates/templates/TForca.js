var TExercices = require('./TExercices');
var TKeyboard = require('./TKeyboard');
//var ScreenManager = require('./controller/ScreenManager');
//var PIXI = require('PIXI');

/**
 * @classdesc TForca Class
 * @memberof module:templates/templates
 * @exports TForca
 * @constructor
 */
function TForca() {
    TExercices.call(this);
    TKeyboard.call(this);

	this._txt_resposta = [];
    this._palpites = [];
    this._palpitesErro = [];
    this._contCorrects = 0;
    this._totalErros = null;
    this._txt_palpitesErro = null;

    this._game = null;
}

TForca.prototype = Object.create(TExercices.prototype);
TForca.prototype.constructor = TForca;
module.exports = TForca;

/** @function
* @description init
*/
TForca.prototype.init = function() {
    TExercices.prototype.init.call(this);
	TKeyboard.prototype.init.call(this);

    //this._pixi = new PIXIConfig();

    this._game = new PIXI.Container();
    //this._game.width = 100;
    //this._game.height = 100;

    this.obj = new PIXI.Graphics();
    this.obj.beginFill(0xFFFF00);
    this.obj.drawRect(0, 0, 100, 100);
    this.obj.endFill();

    //PIXI.Stage().addChild(this._game);

    this._game.addChild(this.obj);

    this._txt_resposta = this.layersDisplay.getMap('interactions');
    this._txt_palpitesErro = this.layersDisplay.getMapByID('text', this._config.forca.idPalpiteErrado);
    this._totalErros = this._config.forca.totalPalpiteErros;

    for (var i = 0; i < this._txt_resposta.length; i++) {
        var c_resposta = this._txt_resposta[i];
        c_resposta.total_letras = 0;

        var length_correct = c_resposta.config.correct.length;
        var txt = "";
        for (var j = 0; j < length_correct; j++) {
            if (c_resposta.config.correct[j] == " ") {
                txt += "  ";
            }
            else {
                txt += "_ ";
                c_resposta.total_letras += 1;
            }
        }

        this._txt_resposta[i].setText(txt, this._txt_resposta[i].children[0]._custonStyle);
        this._txt_resposta[i].qtdCont = 0;
    }

	this._corrects = [];
};

/** @function
* @description open
*/
TForca.prototype.open = function() {
    TExercices.prototype.open.call(this);
    TKeyboard.prototype.open.call(this);
};

/** @function
* @description _initInteractions
* @private
*/
TForca.prototype._initInteractions = function() {
    TExercices.prototype._initInteractions.call(this);
	TKeyboard.prototype._initInteractions.call(this);

    this._focussedInput.component.hideCursor();
};

/** @function
* @description _focus
* @private
*/
TForca.prototype._focus = function(e) {
    TExercices.prototype._focus.call(this, e);
	TKeyboard.prototype._focus.call(this, e);
};

/** @function
* @description _keyPressed
* @private
*/
TForca.prototype._keyPressed = function(e) {
    if(e.value == " ") {
        return;
    }
    this.verify(e);
};

/** @function
* @description addInputEvents
* @param arrInputs {Object} arrInputs
*/
TForca.prototype.addInputEvents = function(arrInputs) {
	TKeyboard.prototype.addInputEvents.call(this, arrInputs);
};

/** @function
* @description removeInputEvents
* @param arrInputs {Object} arrInputs
*/
TForca.prototype.removeInputEvents = function(arrInputs) {
	TKeyboard.prototype.removeInputEvents.call(this, arrInputs);
};

/** @function
* @description addKeyboardEvents
*/
TForca.prototype.addKeyboardEvents = function() {
	TKeyboard.prototype.addKeyboardEvents.call(this);
};

/** @function
* @description removeKeyboardEvents
*/
TForca.prototype.removeKeyboardEvents = function() {
	TKeyboard.prototype.removeKeyboardEvents.call(this);
};

/** @function
* @description openCorrect
*/
TForca.prototype.openCorrect = function() {
    var self = this;

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            self.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

/** @function
* @description openFail
*/
TForca.prototype.openFail = function() {
    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
            self.checkRoadMethod();
            self.removeKeyboardEvents();
            self.removeInputEvents();
            this._interface.openRefresh();
            this._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self.checkRoadMethod();
            self._interface.openRefresh();
            self._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, 1000);
    }

};

/** @function
* @description verify
*/
TForca.prototype.verify = function(e) {

    if (this._palpites.indexOf(e.value) >= 0) {
        return;
    }

    this._palpites.push(e.value);

    for (var i = 0; i < this._txt_resposta.length; i++) {
        var txt = this._txt_resposta[i].config.correct;
        var newtxt = this._txt_resposta[i].field.children[0]._text;
        var cont = 0;

        var len = txt.match(new RegExp(e.value, 'g'));
        len = len === null?0:len.length;

        if (len > 0) {
            for (var j = 0; j < newtxt.length; j+=2) {
                if (newtxt[j] === "_") {
                    if (e.value === txt[cont]) {
                        var a = newtxt.substr(0,j);
                        var b = e.value;
                        var c = newtxt.substr(j+1,newtxt.length - (j+1));
                        newtxt = a+b+c;
                        this._txt_resposta[i].qtdCont++;
                        this._txt_resposta[i].setText(newtxt, this._txt_resposta[i].children[0]._custonStyle);

                        if (this._txt_resposta[i].qtdCont >= this._txt_resposta[i].total_letras) {
                            this._contCorrects++;

                            if (this._contCorrects >= this._txt_resposta.length) {
                                this.removeKeyboardEvents();
                                this._txt_resposta[i].setStyle({"strokeThickness": 3, "stroke": "#1ba218"});
                                this.openCorrect();
                            }
                        }
                    }
                }
                cont++;
            }
        }
        else {
            this.emit('AnimateForca');
            this._palpitesErro.push(e.value);

            var txtErro = this._palpitesErro.toString();
            txtErro = txtErro.replace(/,/g,' ');
            this._txt_palpitesErro.setText(txtErro, this._txt_palpitesErro.children[0]._custonStyle);
            this._txt_palpitesErro.setStyle({"strokeThickness": 3, "stroke": "#a21818"});

            if (this._totalErros === false) {
                return;
            }
            else {
                this._totalErros -= 1;
                if (this._totalErros <= 0) {
                    this.removeKeyboardEvents();
                    this.openFail();
                }
            }
        }
    }
};

/** @function
* @description close
*/
TForca.prototype.close = function() {
    TExercices.prototype.close.call(this);
    TKeyboard.prototype.close.call(this);
};

/** @function
* @description reset
*/
TForca.prototype.reset = function() {
    TExercices.prototype.reset.call(this);
	TKeyboard.prototype.reset.call(this);

    for (var i = 0; i < this._txt_resposta.length; i++) {
        var c_resposta = this._txt_resposta[i];
        c_resposta.total_letras = 0;

        var length_correct = c_resposta.config.correct.length;
        var txt = "";
        for (var j = 0; j < length_correct; j++) {
            if (c_resposta.config.correct[j] == " ") {
                txt += "  ";
            }
            else {
                txt += "_ ";
                c_resposta.total_letras += 1;
            }
        }

        this._txt_resposta[i].setText(txt, this._txt_resposta[i].children[0]._custonStyle);
        this._txt_resposta[i].qtdCont = 0;
    }

	this._corrects = [];
    this._palpites = [];
    this._palpitesErro = [];
    this._totalErros = this._config.forca.totalPalpiteErros;
    this._txt_palpitesErro.setText("");
};
