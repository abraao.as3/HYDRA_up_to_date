var ITemplate = require('../core/ITemplate');
var behavior = require('../../behavior/index');

/**
* configuração do template:
* @example
*
* Falta criar exemplo.
*
* @classdesc TLivroPopUp Class
* @memberof module:templates/templates
* @exports TLivroPopUp
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @see module:animation.Point
* @constructor
*/
function TLivroPopUp() {
    ITemplate.call(this);

    this.end = false;

    TLivroPopUp.prototype._imagespopup = null;

}

TLivroPopUp.prototype = Object.create(ITemplate.prototype);
TLivroPopUp.prototype.constructor = TLivroPopUp;
module.exports = TLivroPopUp;

/** @function
* @description init
*/
TLivroPopUp.prototype.init = function() {
	this.setSequencies(this._config.sequences, this._SoundManager);

    ITemplate.prototype.init.call(this);

    var self = this;

    for (var i = 0; i < self._config.imagespopup.length; i++) {

      self._imagespopup = self._config.imagespopup[i];

      for (var j = 0; j < self._imagespopup.closedImage.length; j++) {
        self._imagespopup.closedImage[j] = self.layersDisplay.getMapByID('images', self._imagespopup.closedImage[j]);

        self._imagespopup.closedImage[j].alpha = 1;
      }

      for (var k = 0; k < self._imagespopup.popedImage.length; k++) {
        self._imagespopup.popedImage[k] = self.layersDisplay.getMapByID('images', self._imagespopup.popedImage[k]);

        self._imagespopup.popedImage[k].alpha = 0;
      }
    }
};

/** @function
* @description start
*/
TLivroPopUp.prototype.start = function(){

    ITemplate.prototype.start.call(this);
    this.fadeStart();
};

/** @function
* @description open
*/
TLivroPopUp.prototype.open = function() {

    ITemplate.prototype.open.call(this);

    this.fadeOpen();

    this.FadePopUp(this._imagespopup);

};

/** @function
* @description FadePopUp
*/
TLivroPopUp.prototype.FadePopUp = function(e){

  TweenMax.to(e.closedImage,0.85,{alpha:0});
  TweenMax.to(e.popedImage,0.85,{alpha:1});

  // var id = setInterval(function () {
  //   if (e.closedImage.alpha > 0) {
  //     e.closedImage.alpha -= 0.005;
  //   }
  //   else {
  //     e.closedImage.alpha = 0;
  //     clearInterval(id);
  //   }
  //
  // }, 60/1000);
  //
  // var ido = setInterval(function () {
  //   if (e.popedImage.alpha < 1){
  //     e.popedImage.alpha += 0.005;
  //   }
  //   else {
  //     e.popedImage.alpha = 1;
  //     clearInterval(ido);
  //   }
  //
  // }, 60/1000);

};

/** @function
* @description reset
*/
TLivroPopUp.prototype.reset = function(){
    ITemplate.prototype.reset.call(this);

    var self = this;

    for (var j = 0; j < self._imagespopup.closedImage.length; j++) {
      self._imagespopup.closedImage[j].alpha = 1;
    }

    for (var k = 0; k < self._imagespopup.popedImage.length; k++) {
      self._imagespopup.popedImage[k].alpha = 0;
    }

    this._end = false;
};

/** @function
* @description _initInteractions
* @private
*/
TLivroPopUp.prototype._initInteractions = function(){
    ITemplate.prototype._initInteractions.call(this);
    var self = this;

    this._unblock();
    this._interface.once('refreshOpened', function(){
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function(){
            self._interface.closeRefresh();
            self.fadeRestart();
            self._blocked();
            self.on('sequenceComplete', function(){
                self._unblock();
                self._interface.openRefresh();
            });
        });
    });

    console.log("init interactions.....");

    this._interface.openRefresh();
};

/** @function
* @description _blocked
* @private
*/
TLivroPopUp.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

/** @function
* @description _unblock
* @private
*/
TLivroPopUp.prototype._unblock = function() {
    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }
    this.emit('parallax');
};
