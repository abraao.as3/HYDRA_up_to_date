var ITemplate = require('../core/ITemplate');
var behavior = require('../../behavior/index');
var SoundManager = require("../../media/SoundManager");

/**
* configuração do template:
* <br/>`Veja as opções completas em {@link module:animation.Point}`
* @example
*
{
    "path": "telas/002/",
    "backgroundIndex": 2,
    "template": {
        "type": "dialog",
        "config": {
            "text": {
                "pt-BR": [{
                    "text": "<a>Drica</a>, por\nque tu estás <a>vestida</a> assim?",
                    "person": "Joaquim",
                    "audioID": 0,
                    "style": {
                        "a": {
                            "fill": "#c06140"
                        }
                    }
                }, {
                    "text": "Porque quero ser <a>cientista!</a> E para começar nosso aprendizado",
                    "person": "Drica",
                    "audioID": 1,
                    "style": {
                        "a": {
                            "fill": "#c06140"
                        }
                    }
                }]
            },
            "audio": {
                "pt-BR": [{
                    "name": "2_1",
                    "id": 0
                }, {
                    "name": "2_2",
                    "id": 1
                }]
            },
            "sequences": [
                {
                    "id":0,
                    "audio": 0,
                    "images": [3],
                    "mouth": [0],
                    "sequences": [
                        {
                            "id": 0,
                            "txt": [0],
                            "delay": 2
                        }
                    ]
                },
                {
                    "id": 1,
                    "images": [4],
                    "txt": [1],
                    "audio": 1
                }
            ]
        }
    }
}
* @classdesc Classe que define o Template de dialogo simples
* @memberof module:templates/templates
* @exports TDialog
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @see module:animation.Point
* @constructor
*/
function TDialog() {
    ITemplate.call(this);

    this._end = false;
    this._countAudioExtra = 0;
    this._sequencesConfigured = false;
}

TDialog.prototype = Object.create(ITemplate.prototype);
TDialog.prototype.constructor = TDialog;
module.exports = TDialog;

/** @function
 * @description init
 */
TDialog.prototype.init = function() {
	this.setSequencies(this._config.sequences, this._SoundManager);

    ITemplate.prototype.init.call(this);

    this._extraAudioNum = this._config.extraAudioNum;

    this._persistInteractions = this._config.persistInteractions;

    this.fadeStart();
};

/** @function
 * @description start
 */
TDialog.prototype.start = function() {
    // if(!this._sequencesConfigured){
    //     this._sequencesConfigured = true;
    //     this.setSequencies(this._config.sequences, this._SoundManager);
    // }

    ITemplate.prototype.start.call(this);
};

/** @function
 * @description open
 */
TDialog.prototype.open = function() {

    ITemplate.prototype.open.call(this);

    var self = this;

    this.fadeOpen();
};

TDialog.prototype.close = function() {
    ITemplate.prototype.close.call(this);

    SoundManager.stopAll();
    // this.forceEnd();
};

TDialog.prototype.reset = function() {
    ITemplate.prototype.reset.call(this);

    if (this._persistInteractions) {
        this._blocked(false, true);
    }

    // this.forceEnd();
    // this.fadeStop();

    this._end = false;
    this._countAudioExtra = 0;

    var buttons = this.layersDisplay.getMap("button");
    if (buttons !== null) {
        for (var i = 0; i < buttons.length; i++) {
            var b = buttons[i];
            b._clicked = false;
            b.removeListener('playSound');
        }
    }
};

/** @function
 * @description _initInteractions
 * @private
 */
TDialog.prototype._initInteractions = function() {
    ITemplate.prototype._initInteractions.call(this);
    var self = this;

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._interface.closeRefresh();
            self.fadeRestart();
            self._blocked(true);
            self.on('sequenceComplete', function() {
                self._unblock();
                self._interface.openRefresh();
            });
        });
    });

    this._interface.openRefresh();

    if (this._extraAudioNum !== undefined && this._extraAudioNum !== null && this._extraAudioNum > 0) {
        var buttons = this.layersDisplay.getMap("button");
        for (var i = 0; i < buttons.length; i++) {
            var b = buttons[i];
            if (!b.listeners('playSound', true) && b._clicked === false) {
                b.once('playSound', this._audioButtonClicked, this);
            }
        }

        return;
    }

    if (this._popups === undefined || this._popups === null) {
        this._finalizePage();
    }
};

/** @function
 * @description popupOpenStarted
 */
TDialog.prototype.popupOpenStarted = function(e) {
    ITemplate.prototype.popupOpenStarted.call(this, e);
    this._interface.closeRefresh();
};

/** @function
 * @description popupCloseCompleted
 */
TDialog.prototype.popupCloseCompleted = function(e) {
    ITemplate.prototype.popupCloseCompleted.call(this, e);

    if (!this._end)
        this._interface.openRefresh();
};

/** @function
 * @description popupsComplete
 */
TDialog.prototype.popupsComplete = function() {
    ITemplate.prototype.popupsComplete.call(this);

    this._end = true;

    if (this._extraAudioNum !== undefined && this._extraAudioNum !== null && this._extraAudioNum > 0) {
        if (this._countAudioExtra == this._extraAudioNum) {
            this._finalizePage();
        }
    } else {
        this._finalizePage();
    }
};

/** @function
 * @description _audioButtonClicked
 * @private
 */
TDialog.prototype._audioButtonClicked = function(e) {
    e.target._clicked = true;
    this._countAudioExtra++;

    if (this._countAudioExtra == this._extraAudioNum) {
        if (this._popups !== undefined && this._popups !== null && this._popups.length > 0) {
            if (this._end) {
                this._finalizePage();
            }
        } else {
            this._finalizePage();
        }
    }
};

/** @function
 * @description _finalizePage
 * @private
 */
TDialog.prototype._finalizePage = function() {
    if (!this._persistInteractions) {
        this._blocked(false, true);
    }
    this._interface.removeListener('refreshOpened');
    if (this._interface.refreshButton !== undefined && this._interface.refreshButton !== null) this._interface.refreshButton.removeEvents();
    this._interface.closeRefresh(true);

    this.checkRoadMethod();
};
