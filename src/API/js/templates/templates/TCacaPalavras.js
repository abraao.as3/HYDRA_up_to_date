var TExercices = require('./TExercices');
var behavior = require('../../behavior/index');
var CrossWord = require('../../display/simple/CrossWord');

/**
* @classdesc cacaPalavras
* @constructor
*/
function TCacaPalavras() {
    TExercices.call(this);

    this._countCorrects = 0;
}

TCacaPalavras.prototype = Object.create(TExercices.prototype);
TCacaPalavras.prototype.constructor = TCacaPalavras;
module.exports = TCacaPalavras;

TCacaPalavras.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this._cwConfig = this._config.cacaPalavras;
    var ib = this._layersDisplay.getObjects('images');
    for (var i = 0; i < ib.length; i++) {
        if (ib[i].index == this._cwConfig.imageBase) {
            this._imgBase = ib[i];
            break;
        }
    }

    var tb = this._layersDisplay.getMap('text');
    for (i = 0; i < tb.length; i++) {
        if (tb[i].index == this._cwConfig.textBase) {
            this._txtBase = tb[i];
            break;
        }
    }

    this._crossWord = new CrossWord(
        this._cwConfig.numberRow || 0,
        this._cwConfig.numberCol || 0, {
            x: this._imgBase.x - this._imgBase.width / 2,
            y: this._imgBase.y - this._imgBase.height / 2,
            w: this._imgBase.width,
            h: this._imgBase.height
        }, this._cwConfig.correctWords);
    this._imgBase.visible = false;

    this._crossWord.lineColor = this._cwConfig.lineColor;
    this._crossWord.debug = this._cwConfig.debug;
    this._crossWord.fontStyle = this._cwConfig.fontStyle;
    this._crossWord.upperCase = this._cwConfig.upperCase;
    this._crossWord.orientation = this._cwConfig.orientation;
    this._crossWord.textBase = this._txtBase;
    this.addChild(this._crossWord);
};

TCacaPalavras.prototype.start = function() {
    TExercices.prototype.start.call(this);

    this._crossWord.create();
};

TCacaPalavras.prototype._initInteractions = function() {
    var self = this;
    TExercices.prototype._initInteractions.call(this);

    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self._blocked();
            self.fadeRestart();
            self.on('sequenceComplete', function() {
                self._unblock();
            });
        });
    });
    this._interface.openRefresh();

    this._unblock();
    this._block = false;

    this._crossWord.addEvents();
    this._crossWord.on('correct', this._setOk, this);
    this._crossWord.on('incorrect', this._setFail, this);
};

TCacaPalavras.prototype._setOk = function() {
    this._countCorrects++;
    if (this._countCorrects >= this._cwConfig.correctWords.length) {
        this._interface._refreshButton.removeListener('clicked');
        this._interface.closeRefresh();
        this._crossWord.removeEvents();

        if (this.correct) {
            this.correct.timeline.play();
            this._blocked();
            this.correct.once('pointComplete', function() {
                this._blocked();
                this.checkRoadMethod();
            }, this);
        } else {
            setTimeout(function() {
                self.checkRoadMethod();
            }, 1000);
        }
    }
};

TCacaPalavras.prototype._setFail = function() {
    var self = this;
    if (!self._block) {
        self._interface.closeRefresh(true);
        self._block = true;
        self._blocked();

        if (self.fail) {
            self.fail.timeline.play();
            self._blocked();
            self.fail.once('pointComplete', function() {
                self.emit('exercicioIncorreto');
                self._unblock();
            }, self);
        } else {
            this.emit('exercicioIncorreto');
            setTimeout(function() {
                self._unblock();
                self._block = false;
            }, 1000);
        }
    }
};

TCacaPalavras.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }

    this._interface.openRefresh();
};

TCacaPalavras.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

TCacaPalavras.prototype.reset = function() {
    TExercices.prototype.reset.call(this);

    var self = this;

    self._blocked(true);

    this._interface.closeRefresh();
    //this._blocked(true);
    this._crossWord.reset();
    if (this.fail) this.fail.reset();
    if (this.correct) this.correct.reset();

    this._countCorrects = 0;

    this._crossWord.removeEvents();
    this._crossWord.removeListener('correct');
    this._crossWord.removeListener('incorrect');
    this.removeListener('sequenceComplete');
};
