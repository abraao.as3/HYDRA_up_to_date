var TExercices = require('./TExercices');

/*
 * configuração do template:
 * @example
 * @classdesc Classe que define o Template de drag
 * @memberof module:templates/templates
 * @exports TAbacus
 * @implements module:templates/core.ITemplate
 * @extends module:templates/core.ITemplate
 * @constructor
 */

function TAbacus() {

/* config.json documentation is in the end of this script */

    TExercices.call(this);

    this._drags = [];//drag of points of abacus

}

TAbacus.prototype = Object.create(TExercices.prototype);
TAbacus.prototype.constructor = TAbacus;
module.exports = TAbacus;

TAbacus.prototype.init = function() {

    TExercices.prototype.init.call(this);

    // this.configureDrags();
    // var config = this._config;
    // var images = this.layersDisplay.getMap('images');

};

TAbacus.prototype.start = function() {
    TExercices.prototype.start.call(this);
};


TAbacus.prototype.open = function() {
    TExercices.prototype.open.call(this);

};

TAbacus.prototype._initInteractions = function() {
  TExercices.prototype._initInteractions.call(this);
    var self = this;

    this._interface.once('refreshOpened', function() {

        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self.fadeRestart();
            self.removeDragEvents();
            self.once('sequenceComplete', function() {
                self.addDragEvents();
            });
        });
    });

    this._interface.openRefresh();
    this.addDragEvents();

};

/**
 * Metodo que adiciona eventos aos drags
 */
TAbacus.prototype.addDragEvents = function() {for (var i = 0; i < this._drags.length; i++) {this._drags[i].addEvents();}};

/**
 * Metodo que remove eventos dos drags
 */
TAbacus.prototype.removeDragEvents = function() {for (var i = 0; i < this._drags.length; i++) {this._drags[i].removeEvents();}};

/**
 * Configura os caracteristicas do drags e areasDrags baseadas no config.json
 */
TAbacus.prototype.configureDrags = function() {
    var self = this;
    var config = this._config;

    var images = this.layersDisplay.getObjects('images');
    var texts = this.layersDisplay.getObjects('text');

    this._drags = this.layersDisplay.getObjects('drag');
    var layerSup = this.layersDisplay.getLayer('sup');
    for (i = 0; i < config.drags.length; i++) {
        var d = this._drags[i];

        d.id = i;
        d.setResetOnFail(false);
        d.on('positionUpdated', self.positionUpdated, self);

    }

};

TAbacus.prototype.positionUpdated = function(e) {

    var sensor = e.target.getSensor();

    var x = (e.target.x - (e.target.width/2))+sensor.x;
    var y = (e.target.y - (e.target.height/2))+sensor.y;
    var width = sensor.width;
    var height = sensor.height;

    //Check if has a contact with positive feed
    for (var i = 0; i < this.feedsO.length; i++) {
      var cFeed = this.feedsO[i];

      var hit = cFeed.hasContent(x, y, width, height) && cFeed.visible ? true : false;
      if (hit) {this.CorrectValidation(cFeed);}
      else{cFeed.timeToFeed = this.timeToFeedO;}
    }

};


TAbacus.prototype.CorrectValidation = function(e) {

  // var self = this;
  //
  // e.timeToFeed -= 60;//60 fps
  //
  // if(!e.hit && e.timeToFeed <= 0){
  //
  //     e.hit = true;
  //     e.timeToFeed = this.timeToFeedO;
  //     e.openContour();
  //     self.qtdCorrects += 1;
  //     if(self.qtdCorrects >= self.feedsO.length){//SHOW POSITIVE FEED
  //
  //       self.resetDragPosition(this._drags[0], 300);
  //       self.correct.reset();
  //       self.correct.timeline.play();
  //       self.removeDragEvents();
  //
  //       self.SeTAbacus(2);
  //
  //       this._interface._refreshButton.visible = false;
  //
  //       self.correct.once('pointComplete', function() {
  //           this._interface._refreshButton.visible = true;
  //           self.emit('parallax');
  //       }, self);
  //
  //     }
  //   }

};

TAbacus.prototype.WrongValidation = function(e) {

  // var self = this;
  //
  // e.timeToFeed -= 60;//60 fps
  //
  // if(!e.hit && e.timeToFeed <= 0){
  //   this.resetDragPosition(this._drags[0], 300);
  //
  //   e.hit = true;
  //   e.timeToFeed = this.timeToFeedI;
  //   e.openContour();
  //   this._drags[0]._onUp();
  //   self.removeDragEvents();
  //
  //   this._interface._refreshButton.visible = false;
  //
  //   if (self.fail) {
  //       self.fail.timeline.play();
  //       self.fail.once('pointComplete', function() {
  //           this._interface._refreshButton.visible = true;
  //           this.emit('exercicioIncorreto');
  //           self.addDragEvents();
  //           this.fail.reset();
  //           e.closeContour();
  //           e.hit = false;
  //       }, self);
  //   } else {
  //     this.emit('exercicioIncorreto');
  //       setTimeout(function() {
  //           self.addDragEvents();
  //           this.fail.reset();
  //           e.closeContour();
  //           e.hit = false;
  //       }, 300);
  //   }
  //
  // }

};

TAbacus.prototype.resetDragPosition = function(e, time) {

  setTimeout(function() {//reset the drag position

    TweenMax.killTweensOf(e);

    TweenMax.to(e.position, 1.5, {
        x: Math.round(e._initialX),
        y: Math.round(e._initialY),
        ease: Back.easeOut
    });
  }, time);

};

TAbacus.prototype.reset = function() {
  TExercices.prototype.reset.call(this);

  this.ResetScreen();

};


TAbacus.prototype.close = function() {};

TAbacus.prototype.ResetScreen = function() {

  // this.qtdCorrects = 0;
  //
  // this.resetDragPosition(this._drags[0], 300);
  // this.removeDragEvents();
  // this._drags[0]._onUp();
  // for (var i = 0; i < this.feedsO.length; i++) {this.feedsO[i].closeContour(); this.feedsO[i].hit = false;}

};

//TAbacus.prototype.GetValidValue = function (default_value, c_value, blank) {
function GetValidValue(default_value, c_value, blank) {
//default_value = value if dont have a individual value (c_value)
//c_value       = individual value
//blank         = if dont have c_value, return "", without test default_value
    if ( !(IsValue(c_value)) ) {
        if ( !( IsValue(default_value) ) || blank) { return ""; }
        else { return default_value; }
    }
    return c_value;
}

// TAbacus.prototype.IsValue = function (e) {return e != "" && typeof e !== "undefined" ? true : false;}
function IsValue(e) {return e !== "" && typeof e !== "undefined" ? true : false;}

/**** BINOCULAR EXAMPLE CONFIG.json


*****/
