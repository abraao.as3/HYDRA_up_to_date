var TExercices = require('./TExercices');
var SnapGrid = require('../../display/complex/SnapGrid');

/**
* configuração do template:
* @example
*
* Falta criar exemplo.
*
 * @classdesc TLine Class
 * @memberof module:templates/templates
 * @exports TLine
 * @constructor
 */
function TLine() {
    TExercices.call(this);
    this._block = false;
}

TLine.prototype = Object.create(TExercices.prototype);
TLine.prototype.constructor = TLine;
module.exports = TLine;

TLine.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this._totalCorrects = 0;

    this._snapGrid = new SnapGrid();
    this.addChild(this._snapGrid);

    var im = this.layersDisplay.getObjects("images");
    var imToPoints = [];
    for (var i = 0; i < im.length; i++) {
        for (var j = 0; j < this._config.lines.points.length; j++) {
            if (im[i].index == this._config.lines.points[j]) {
                imToPoints.push(im[i]);
                break;
            }
        }
    }

    this._snapGrid.createPoint(imToPoints);
    this._snapGrid.createPointRelation(this._config.lines.relation);
};

/** @function
* @description start
*/
TLine.prototype.start = function() {
    TExercices.prototype.start.call(this);

    this._countCorrect = 0;
};

/** @function
* @description open
*/
TLine.prototype.open = function() {
    TExercices.prototype.open.call(this);
};

/** @function
* @description _initInteractions
* @private
*/
TLine.prototype._initInteractions = function() {
    var self = this;
    TExercices.prototype._initInteractions.call(this);

    this._snapGrid.addEvents();
    this._snapGrid.on('correct', this.openCorrect, this);
    this._snapGrid.on('incorrect', this.openFail, this);

    this._interface.once('refreshOpened', function(){
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function(){
            self._blocked();
            self.fadeRestart();
            self._snapGrid.block();
            // self._block = true;
            self.on('sequenceComplete', function(){
                self._unblock();
                self._snapGrid.unblock();
            });
        });
    });

    this._interface.openRefresh();

    this._unblock();
    this._block = false;
};

/** @function
* @description reset
*/
TLine.prototype.reset = function() {
    TExercices.prototype.reset.call(this);

    var self = this;

    this._snapGrid.removeEvents();
    this._snapGrid.clear();

    self._blocked(true);
};

/** @function
* @description openCorrect
*/
TLine.prototype.openCorrect = function(){
    var self = this;

    this._interface._refreshButton.removeListener('clicked');
    this._interface.closeRefresh();
    //this._blocked(true);

    if(this.correct){
        this.correct.once('pointComplete', function() {
            self._blocked();
            this.checkRoadMethod();
        }, this);
        this.correct.reset();
        this.correct.timeline.play();
        self._blocked();
    }
    else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }
};

/** @function
* @description openFail
*/
TLine.prototype.openFail = function(){
    var self = this;

    this._interface.closeRefresh(true);
    //this._blocked();
    this._snapGrid.block();
    if(this.fail){
        this.fail.once('pointComplete', function(){
            self.emit('exercicioIncorreto');
            this._unblock();
            this._snapGrid.unblock();
        }, this);
        this.fail.reset();
        this.fail.timeline.play();
        self._blocked();
    }
    else{
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            self._snapGrid.unblock();
            self._unblock();
        }, 1000);
    }
};

/** @function
* @description _blocked
* @private
*/
TLine.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

/** @function
* @description _unblock
* @private
*/
TLine.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._block = false;
    }

    this._interface.openRefresh();
};
