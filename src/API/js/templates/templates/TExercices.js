var ITemplate = require('../core/ITemplate');
var Point = require('../../animation/Point');

/**
 * @classdesc TExercices Class
 * @memberof module:templates/templates
 * @exports TExercices
 * @constructor
 */
function TExercices() {
    ITemplate.call(this);

    this.isExercise = true;
}

TExercices.prototype = Object.create(ITemplate.prototype);
TExercices.prototype.constructor = TExercices;
module.exports = TExercices;

/** @function
* @description init
*/
TExercices.prototype.init = function() {
    this.setSequencies(this._config.sequences, this._SoundManager);

    if (this._config.response !== undefined) {
        // if (this._config.response.fail !== undefined) {
        //     this.fail = new Point(this._config.response.fail, this.layersDisplay, this.soundManager);
        //     this.fail.start();
        // }
        // if (this._config.response.correct !== undefined) {
        //     this.correct = new Point(this._config.response.correct, this.layersDisplay, this.soundManager);
        //     this.correct.start();
        // }

        for(var i in this._config.response){
            this[i] = new Point(this._config.response[i], this.layersDisplay, this.soundManager);
            this[i].start();
        }
    }

    ITemplate.prototype.init.call(this);
};

/** @function
* @description start
*/
TExercices.prototype.start = function() {
    ITemplate.prototype.start.call(this);
    this.fadeStart();
};

/** @function
* @description open
*/
TExercices.prototype.open = function() {
    var self = this;

    ITemplate.prototype.open.call(this);

    this.fadeOpen();
};

/** @function
* @description _initInteractions
* @private
*/
TExercices.prototype._initInteractions = function() {
    ITemplate.prototype._initInteractions.call(this);
};

/** @function
* @description reset
*/
TExercices.prototype.reset = function(){
	ITemplate.prototype.reset.call(this);
    for(var i = 0; i < this._buttons.length; i++){
        this._buttons[i]._block = false;
    }
};
