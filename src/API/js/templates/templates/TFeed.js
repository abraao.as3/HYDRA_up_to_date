var TExercices = require('./TExercices');

/**
 * @classdesc TFeed Class
 * @memberof module:templates/templates
 * @exports TFeed
 * @constructor
 */
function TFeed() {
    TExercices.call(this);
    this._block = false;
}

TFeed.prototype = Object.create(TExercices.prototype);
TFeed.prototype.constructor = TFeed;
module.exports = TFeed;

/** @function
 * @description init
 */
TFeed.prototype.init = function() {
    TExercices.prototype.init.call(this);

    this._totalCorrects = 0;

    for (var i = 0; i < this._config.button.length; i++) {
        if (this._config.button[i].correct) this._totalCorrects++;
    }

    if(this._config.buttonGroup !== undefined && this._config.buttonGroup !== null){
        for(i = 0; i < this._config.buttonGroup.length; i++){
            var arr = this._config.buttonGroup[i];
            this._buttons[arr[0]].buttonGroup = this._buttons[arr[1]];
            this._buttons[arr[1]].buttonGroup = this._buttons[arr[0]];
        }
    }
};

/** @function
 * @description start
 */
TFeed.prototype.start = function() {
    TExercices.prototype.start.call(this);

    this._countCorrect = 0;
};

/** @function
 * @description open
 */
TFeed.prototype.open = function() {
    TExercices.prototype.open.call(this);
    var self = this;

    self._unclicked();

};

/** @function
 * @description _initInteractions
 * @private
 */
TFeed.prototype._initInteractions = function() {
    var self = this;
    TExercices.prototype._initInteractions.call(this);
    this._interface.once('refreshOpened', function() {
        this._refreshButton.addEvents();
        this._refreshButton.on('clicked', function() {
            self.fadeRestart();
            //self._block = true;
            //self._blocked(true);
            self._blocked();

            self.on('sequenceComplete', function() {
                //self._block = false;
                self._unblock();
            });
        });
    });

    this._interface.openRefresh();

    this._unblock();
    this._block = false;
};

/** @function
 * @description openFail
 * @param target {Object} target
 * @param self {Object} self
 */
TFeed.prototype.openFail = function(target, self) {

    if (!target._block) {
        self._interface.closeRefresh(true);
        target._block = true;
        self._blocked();
        target.openContour();
        if (self.fail) {
            self.fail.reset();
            self.fail.timeline.play();
            self.fail.once('pointComplete', function() {
                self.emit('exercicioIncorreto');
				if(target.buttonGroup) {
					target.buttonGroup.closeContour();
				}
                target.buttonGroup.closeContour();
                self._unblock();
                target._clicked = false;
            }, self);
        } else {
            self.emit('exercicioIncorreto');
            setTimeout(function() {
                target.closeContour();
				if(target.buttonGroup) {
					target.buttonGroup.closeContour();
				}
                self._unblock();
                target._clicked = false;
            }, 1000);
        }
    }
};

/** @function
 * @description openCorrect
 * @param target {Object} target
 * @param self {Object} self
 */
TFeed.prototype.openCorrect = function(target, self) {

    if (!target._clicked) {
        //target._block = true;
        //target._blocked(true);
        target._clicked = true;
        target._block = true;
        target.buttonGroup._clicked = true;
        target.buttonGroup._block = true;
        self._countCorrect++;
        target.openContour();
        //target.isLock = true;
        self._verify();
    }
};

/** @function
 * @description _unblock
 * @private
 */
TFeed.prototype._unblock = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        if (this._buttons[i]._clicked === false) {
            this._buttons[i]._block = false;
        }
    }

    this._interface.openRefresh();
};

/** @function
 * @description _unclicked
 * @private
 */
TFeed.prototype._unclicked = function() {
    if (this.fail) this.fail.reset();

    for (var i = 0; i < this._buttons.length; i++) {
        if (this._buttons[i].isLock === true) continue;
        this._buttons[i]._clicked = false;
    }

};

/** @function
 * @description _blocked
 * @private
 */
TFeed.prototype._blocked = function(all) {
    var a = all || false;
    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i]._block = true;
        if (a && this._buttons[i].animating === true) {
            this._buttons[i].stopAnimation();
        }
    }
};

/** @function
 * @description _verify
 * @private
 */
TFeed.prototype._verify = function() {
    var self = this;

    if (this._countCorrect >= this._totalCorrects) {
        self._blocked(true);

        if (this._interface.refreshButton !== undefined && this._interface.refreshButton !== null) {
            this._interface._refreshButton.removeListener('clicked');
            this._interface.closeRefresh();
        }

        this.removeListener('sequenceComplete');

        if (self.correct) {
            self.correct.reset();
            self.correct.timeline.play();
            self.correct.once('pointComplete', function() {
                self._blocked(true);

                this.checkRoadMethod();
            }, self);
        } else {
            setTimeout(function() {
                self._blocked(true);

                self.checkRoadMethod();
            }, 1000);
        }

        //return;
    }
    //this._unblock();
    //this._block = false;
};

/** @function
 * @description reset
 */
TFeed.prototype.reset = function() {
    TExercices.prototype.reset.call(this);

    var self = this;

    self._countCorrect = 0;

    for (var i = 0; i < this._buttons.length; i++) {
        this._buttons[i].closeContour();
    }

    self._blocked(true);

};
