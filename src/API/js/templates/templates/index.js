/**
* @module templates/templates
*/
var templates = module.exports = {

    empty: require('./TEmpty'),

    clock: require('./TClock'),

    dialog: require('./TDialog'),

    complexDialog: require('./TComplexDialog'),

    end: require('./TEnd'),

    video: require('./TVideo'),

    drag: require('./TDrag'),

    dragAnimation: require('./TDragAnimation'),

    feed: require('./TFeed'),

    hover: require('./THover'),

    movieClip: require('./TMovieClip'),

    line: require('./TLine'),

    paint: require('./TPaint'),

    bucket: require('./TBucket'),

    cacaPalavras: require('./TCacaPalavras'),

    word: require('./TWord'),

    snapGrid: require('./TSnapGrid'),

    seteErros: require('./TSeteErros'),

    keyboard: require('./TKeyboard'),

	compass: require('./TCompass'),

    keyboardExercise: require('./TKeyboardExercise'),

    ballon: require('./TBallon'),

    livropopup: require('./TLivroPopUp'),

    crossWord: require('./TCrossword'),

    fotografia: require('./TFotografia'),

    memoria: require('./TMemoria'),

    binoculars: require('./TBinoculars'),

    forca: require('./TForca'),

    genius: require('./TGenius'),

    abacus: require('./TAbacus'),

    lineNumerical: require('./TLineNumerical')
};
