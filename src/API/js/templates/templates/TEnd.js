var ITemplate = require('../core/ITemplate');
var TweenMax = require('TweenMax');
var PIXI = require('PIXI');

/**
* configuração do template:
* @example
*
"template":{
    "type": "end",

}
*
* @classdesc Classe que define o Template da tela end
* @memberof module:templates/templates
* @exports TEnd
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @constructor
*/
function TEnd() {
    ITemplate.call(this);
}

TEnd.prototype = Object.create(ITemplate.prototype);
TEnd.prototype.constructor = TEnd;
module.exports = TEnd;

/** @function
* @description init
*/
TEnd.prototype.init = function() {

    var self = this;


};

/** @function
* @description open
*/
TEnd.prototype.open = function() {

    var self = this;


};
