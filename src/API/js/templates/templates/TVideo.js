var ITemplate = require('../core/ITemplate');
var Interface = require('../../controller/InterfaceHandler');
var VideoManager = require('../../media/VideoManager');
var Const = require('../../core/Const');
var PIXIConfig = require('../../core/PIXIConfig');
var $ = require('jquery');

/**
* configuração do template:
* @example
único vídeo com player

{
    "template": {
        "type": "video",
        "config": [{
            "width": 974,       //largura do vídeo
            "height": 524,      //Altura do vídeo
            "x": 0,             //Posição x do vídeo
            "y": 0,             //Posção y do vídeo
            "player": true,     //Player para controle
            "url": "M05_T01",   //url do vídeo - default pasta video
            "autoplay": false,  //início automático do vídeo
            "thumb": ""         //url do thumb - default pasta video
        }]
    }
}

vários vídeos sem player

{
    "template": {
        "type": "video",
        "config": [{
            "width": 400,
            "height": 250,
            "x": 0,
            "y": 0,
            "url": "M05_T01",
            "autoplay": true,
            "thumb": ""
        },{
            "width": 400,
            "height": 250,
            "x": 500,
            "y": 0,
            "url": "M05_T01",
            "autoplay": false,
            "thumb": ""
        }]
    }
}
*
* @classdesc Classe que define o Template de Vídeo
* @memberof module:templates/templates
* @exports TVideo
* @implements module:templates/core.ITemplate
* @extends module:templates/core.ITemplate
* @constructor
*/
function TVideo() {
    ITemplate.call(this);

    this._interface = Interface.getInstance();
    this._backgroundDOM = $(document.createElement('div'));
    this._backgroundDOM.css({
        position: 'absolute',
        overflow: 'hidden',
        zIndex: -100
    });
    this._imageBackground = null;
    this._canvasView = $(PIXIConfig.renderer.view);
    this._factorScale = 1;
}

TVideo.prototype = Object.create(ITemplate.prototype);
TVideo.prototype.constructor = TVideo;
module.exports = TVideo;

/** @function
* @description init
*/
TVideo.prototype.init = function() {
    this._default = {
        width: 974,
        height: 524,
        x: 0,
        y: 0,
        player: true,
        url: null,
        thumb: null
    };

    this._vconfig = this._config.videoConfig;
    this._nextPageMethod = this._config.nextPageMethod;

    for (var j = 0; j < this._vconfig.length; j++) {
        for (var i in this._default) {
            if (this._vconfig[j][i] === undefined) {
                this._vconfig[j][i] = this._default[i];
            }
        }
    }

    for (var k = 0; k < this._vconfig.length; k++) {
        if (this._vconfig[k].url === null) throw "Template Vídeo: A tela de vídeo precisa de uma url válida.";

        this._vconfig[k].url = Const.APPConfig.pastaTelas + 'video/' + Const.APPConfig.LANGUAGE.replace("-", "") + "/" + this._vconfig[k].url + ".mp4";
    }

    this._videoManager = new VideoManager(this._vconfig);
    this.addChild(this._videoManager);

    var layerBack = this.layersDisplay.getLayer('background');
    this._imageBackground = $(PIXIConfig.renderer.extract.image(layerBack));
    layerBack.visible = false;

    this._imageBackground.css({
        width: '100%',
        height: '100%',
        position: 'relative'
    });

    this._backgroundDOM.append(this._imageBackground);
    $('body').first().prepend(this._backgroundDOM);

    this._backgroundDOM.css('display', 'none');

};

/** @function
* @description start
*/
TVideo.prototype.start = function() {
    var self = this;
    this._videoPlayer = this._interface.videoPlayer;

    if (this._vconfig.length == 1) {
        this.addChild(this._videoPlayer);
        this._videoPlayer.startPlayer();

        this._videoManager.setUpdateEvent(this._updateVideo, this);
        self._videoManager.updateVol(1);
        this._videoManager.on('videoEnded', function() {
            self._videoManager.pause();
            self._videoPlayer.setSeekPosition(0);
            self._videoPlayer.ended();
            self.checkRoadMethod();
            // self.emit("allowNext");
        });

        this._videoPlayer.on('seekDown', function() {
            self._videoManager.pause();
        });

        this._videoPlayer.on('updateSeek', function(e) {
            self._videoManager.updateSeek(e.pct);
        });

        this._videoPlayer.on('updateVol', function(e) {
            self._videoManager.updateVol(e.pct);
        });


    }

    this._videoManager.start({
        x: this.x,
        y: this.y
    });

    this._enterframe = setInterval(function() {
        self._videoManager.updatePos({
            x: self.x,
            y: self.y
        });
    }, 1000/60);

    this._initEnterFrame();
    this._backgroundDOM.css('display', 'block');
    this._domResize();
};

/** @function
* @description start
*/
TVideo.prototype.open = function() {
    var self = this;

    clearInterval(this._enterframe);
    this._videoManager.updatePos({
        x: this.x,
        y: this.y
    });

    if (this._vconfig.length == 1) {
        if (this._vconfig[0].player === true) {
            this._videoPlayer.openPlayer();

            this._videoPlayer.on("play", function() {
                self._videoManager.play();
            });

            this._videoPlayer.on("pause", function() {
                self._videoManager.pause();
            });
        }

        if (this._vconfig[0].autoplay) {
            this._videoManager.play();
            this._videoPlayer._togglePlay();
        }
    }
    else{
        for(var i = 0; i < this._vconfig.length; i++){
            if(this._vconfig[i].autoplay === true){
                this._videoManager.playIndex(i);
            }
        }
    }

    this._endEnterFrame();
    // self.emit("allowNext");
};


TVideo.prototype.reset = function(){
    ITemplate.prototype.reset.call(this);
    console.log("reset");
    this._backgroundDOM.css('display', 'none');

    this._endEnterFrame();
};

/** @function
* @description close
*/
TVideo.prototype.close = function(){
    console.log("close tempo do video",this._videoManager.currentTime);

    this._videoManager.close();
    this._videoPlayer.removeAll();
    this._videoPlayer.removeListener();
    console.log("fechou");
    this._initEnterFrame();
    // this._backgroundDOM.css('display', 'none');
};

TVideo.prototype._domResize = function(){
    this._backgroundDOM.width(this._canvasView.width());
    this._backgroundDOM.height(this._canvasView.height());
    this._backgroundDOM.offset({
        left: this._canvasView.offset().left,
        top: this._canvasView.offset().top
    });
};

TVideo.prototype._domPosition = function(){
    var result = this.x * this._factorScale;
    this._imageBackground.css('left', result + 'px');
    // this._paint.moveBoard(result);
};

TVideo.prototype._finalizePage = function(){

};

TVideo.prototype._initEnterFrame = function(){
    var self = this;
    this._enterframe = setInterval(function(){
        self._domPosition();
    }, 1000 / 60);
};

TVideo.prototype._endEnterFrame = function(){
    clearInterval(this._enterframe);
    this._domPosition();
};



/** @function
* @description resize
* @param factorScale {number} factorScale
* @param canvasPos {number} canvasPos
*/
TVideo.prototype.resize = function(factorScale, canvasPos) {
    this._factorScale = factorScale;

    if (this._videoManager)
        this._videoManager.resize(factorScale, canvasPos);
    this._domResize();
};

TVideo.prototype._updateVideo = function(pct, self) {
    self._videoPlayer.setSeekPosition(pct);
};
