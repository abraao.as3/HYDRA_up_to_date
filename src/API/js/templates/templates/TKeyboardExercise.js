var TExercices = require('./TExercices');
var TKeyboard = require('./TKeyboard');

/**
 * @classdesc TKeyboardExercise Class
 * @memberof module:templates/templates
 * @exports TKeyboardExercise
 * @constructor
 */
function TKeyboardExercise() {
    TExercices.call(this);
    TKeyboard.call(this);

	// self = this;
}

TKeyboardExercise.prototype = Object.create(TExercices.prototype);
TKeyboardExercise.prototype.constructor = TKeyboardExercise;
module.exports = TKeyboardExercise;

/** @function
* @description init
*/
TKeyboardExercise.prototype.init = function() {
    TExercices.prototype.init.call(this);
	TKeyboard.prototype.init.call(this);

	this._corrects = [];
};

/** @function
* @description open
*/
TKeyboardExercise.prototype.open = function() {
    TExercices.prototype.open.call(this);
    TKeyboard.prototype.open.call(this);
};

/** @function
* @description _initInteractions
* @private
*/
TKeyboardExercise.prototype._initInteractions = function() {
    TExercices.prototype._initInteractions.call(this);
	TKeyboard.prototype._initInteractions.call(this);
};

/** @function
* @description _focus
* @private
*/
TKeyboardExercise.prototype._focus = function(e) {
	TKeyboard.prototype._focus.call(this, e);
};

/** @function
* @description _keyPressed
* @private
*/
TKeyboardExercise.prototype._keyPressed = function(e) {
	TKeyboard.prototype._keyPressed.call(this, e);

	var self = this;

	if(self._focussedInput.text.length >= self._focussedInput.component.config.correct.length) {
		self.verify();
	}
};

/** @function
* @description addInputEvents
* @param arrInputs {Object} arrInputs
*/
TKeyboardExercise.prototype.addInputEvents = function(arrInputs) {
	TKeyboard.prototype.addInputEvents.call(this, arrInputs);
};

/** @function
* @description removeInputEvents
* @param arrInputs {Object} arrInputs
*/
TKeyboardExercise.prototype.removeInputEvents = function(arrInputs) {
	TKeyboard.prototype.removeInputEvents.call(this, arrInputs);
};

/** @function
* @description addKeyboardEvents
*/
TKeyboardExercise.prototype.addKeyboardEvents = function() {
	TKeyboard.prototype.addKeyboardEvents.call(this);
};

/** @function
* @description removeKeyboardEvents
*/
TKeyboardExercise.prototype.removeKeyboardEvents = function() {
	TKeyboard.prototype.removeKeyboardEvents.call(this);
};

/** @function
* @description openCorrect
*/
TKeyboardExercise.prototype.openCorrect = function() {

    var self = this;

    if (self.correct) {
		self.correct.reset();
        self.correct.timeline.play();
        self.correct.once('pointComplete', function() {
            self.checkRoadMethod();
        }, self);
    } else {
        setTimeout(function() {
            self.checkRoadMethod();
        }, 1000);
    }

};

/** @function
* @description openFail
*/
TKeyboardExercise.prototype.openFail = function() {

    var self = this;

    if (self.fail) {
        self.fail.reset();
        self.fail.timeline.play();
        self.fail.once('pointComplete', function() {
            self.emit('exercicioIncorreto');
             self._focussedInput.text = "";
             self._focussedInput.component.setText(self._focussedInput.text);
            // this._interface.openRefresh();
            // this._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, self);
    } else {
        this.emit('exercicioIncorreto');
        setTimeout(function() {
            // self._interface.openRefresh();
            // self._interface._refreshButton.addEvents();
			self.emit("feedComplete");
        }, 1000);
    }

};

/** @function
* @description verify
*/
TKeyboardExercise.prototype.verify = function() {
	var self = this;

	var inputsToVerify = self._inputs.filter(function(input) {
		return self._corrects.indexOf(input) < 0; //não existe no array de acertos
	});

	self.removeKeyboardEvents();
	self.removeInputEvents(inputsToVerify);
	self._focussedInput.component.hideCursor();

	if (self._focussedInput.text == self._focussedInput.component.config.correct) {
		self._corrects.push(self._focussedInput);
		inputsToVerify.splice(inputsToVerify.indexOf(self._focussedInput),1);

		if(inputsToVerify.length === 0) {
			self.openCorrect();
		} else {
			self.addKeyboardEvents();
			self.addInputEvents(inputsToVerify);

			self._focussedInput = inputsToVerify[0];

			self._focussedInput.component.showCursor();
		}
	} else {
		self.openFail();

		self.once('feedComplete', function() {
			self.addKeyboardEvents();
			self.addInputEvents(inputsToVerify);
			self._focussedInput.component.showCursor();
		});
	}
};

/** @function
* @description close
*/
TKeyboardExercise.prototype.close = function() {
    TKeyboard.prototype.close.call(this);
};

/** @function
* @description reset
*/
TKeyboardExercise.prototype.reset = function() {
	TKeyboard.prototype.reset.call(this);
    this._corrects = [];
};
