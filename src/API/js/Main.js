require('../../telas/ScreenIndex');

var PIXIConfig = require('./core/PIXIConfig');
var OrderModule = require('./utils/OrderModule');
var ScreenManager = require('./controller/ScreenManager');
var Const = require('./core/Const');
var Bitmap = require('./display/core/Bitmap');
var Navigation = require('./controller/Navigation');
var Interface = require('./controller/InterfaceHandler');
var LMS = require('./io/LMS');

/**
 * @classdesc Classe principal, cria os objetos do aplicativo.
 * @constructor
 */
function Main() {
    var self = this;

    window.particles = [];
    window.interfaceParticles = [];

    //Listener para o evento de resize da janela
    window.onresize = function(e) {
        self.resize(e);
    };

    /**
     * instancia do controlador do PIXI e stage
     * @member {tls.core.PIXIConfig}
     * @private
     */
    this._pixi = new PIXIConfig();

    this._moduleProperties = OrderModule.getModule(window.location.href);
    this._moduleTelas = this._moduleProperties.telas;
    this._screenManager = new ScreenManager();
    this._screenManager.initialize(this._moduleTelas);
    this._screenManager.once('appConfigLoaded', this._configLanguage);
    this._screenManager.once('startScreen', this._addStartScreen, this);

    this._navigation = new Navigation(this._pixi);

    this._navigation.indexEx = this._moduleProperties.indexEx;

    this.resize();
}


Main.prototype.constructor = Main;
module.exports = Main;

/**
 * Recebe o evento de resize da janela
 */
Main.prototype.resize = function() {
    this._pixi.resize();
};

/**
 * Enterframe para renderizar o stage
 */
Main.prototype.update = function() {
    var self = this;


    this.now = Date.now();


    var dif = (this.now - this.elapsed) * 0.001;
    for (var i = 0; i < window.particles.length; i++) {
        window.particles[i].updateParticles(dif);
    }
    for (i = 0; i < window.interfaceParticles.length; i++) {
        window.interfaceParticles[i].updateParticles(dif);
    }

    this.elapsed = this.now;

    /**
     * Adiciona o loop para renderização
     * @member {Number}
     * @private
     */
    this.enterframe = requestAnimationFrame(function() {
        self.update();
    });

    this._pixi.update();
};

/**
 * Faz a configuração final do idioma padrão
 * @private
 */
Main.prototype._configLanguage = function(e) {
    var config = Const.APPConfig;
    if (config.LANGUAGE === undefined) {
        throw "LANGUAGE undefined: defina a propriedade LANGUAGE no arquivo appConfig.json ex: pt-BR ou auto";
    }

    if (config.LANGUAGE == "auto") {
        Const.APPConfig.LANGUAGE = navigator.language;
    } else {
        if (['pt', 'es', 'en', 'fr'].indexOf(config.LANGUAGE.split("-")[0]) == -1) {
            throw "LANGUAGE invalid: escola uma linguagem válida - pt-BR es-ES en-US fr-FR";
        }
    }
};

Main.prototype._addStartScreen = function(e) {
    //console.log(this._module);
    var self = this;
    var startScreen = e.screen;
    this.elapsed = Date.now();
    this.update();

	this._pixi.addChild(startScreen);

	var lms = new LMS();
    lms._numtelas = this._moduleTelas.length - 1;

	var nivelBrilho = lms.brilho !== "undefined" && lms.brilho !== "null" && lms.brilho.length > 0 ? lms.brilho : Const.BASE_BRIGHT; // lms sempre retorna os resultados como String, mesmo que seja null ou undefined
	var nivelVolume = lms.audio !== "undefined" && lms.audio !== "null" && lms.audio.length > 0 && lms.audio != "0" ? lms.audio : Const.BASE_SOUND_VOLUME; // lms sempre retorna os resultados como String, mesmo que seja null ou undefined

	var brightSim = new PIXI.Graphics();
	brightSim.beginFill(0x000000);
	brightSim.drawRect(0, 0, Const.BASE_WIDTH, Const.BASE_HEIGHT);
	brightSim.endFill();
	brightSim.alpha = 1-(nivelBrilho/100);

	this._screenManager._interface.brightSim = brightSim;
	this._pixi.addChild(this._screenManager._interface);

	startScreen.start();
	startScreen.open();
	self._screenManager.once('moduleComplete', function(e) {
	    self._initNavigation(startScreen, e.screens);
	}, this);

    self._screenManager.loadInterface(nivelBrilho, nivelVolume);
};

Main.prototype._initNavigation = function(startScreen, screens) {
    var self = this;
    var lms = new LMS();

    this._navigation.on('update', function(obj) {
        lms.tela = obj.lastIndex;
    });

    this._navigation.on('pontuacao', function(e) {
        lms.curso = this.percent;
    });

    this._navigation.on('endQuestions', function() {
        lms.completed();
    });

	this._screenManager.on("brightUpdate", function(e) {
		lms.brilho = e.bright;
	});

	this._screenManager.on("volumeUpdate", function(e) {
		lms.audio = e.volume;
	});


    var nota = lms.curso;
    if(isNaN(parseInt(nota))) nota = 0;

    nota = nota / 10;

    this._navigation.points = nota;

    var lastIndex = lms.tela;
    if(isNaN(parseInt(lastIndex))) lastIndex = 0;
    this._navigation.start(startScreen, screens, this._moduleTelas, lastIndex);

};
