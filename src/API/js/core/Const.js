/**
* @module core
*/
/**
 * @classdesc Constantes globais e configurações gerais
 * @memberof module:core
 * @property version {String} Versão do app
 * @property BASE_WIDTH {Number} Width do app
 * @property BASE_HEIGHT {Number} Height do app
 * @property BASE_SOUND_VOLUME {Number} Volume do app
 * @property BASE_BRIGHT {Object} Bright do app
 */
var Const = module.exports = {
    /**
     * @member {String}
     */
    version: '__VERSION__',
    /**
     * @member {Number}
     * @default 974
     */
    BASE_WIDTH: 974,
    /**
     * @member {Number}
     * @default 466
     */
    BASE_HEIGHT: 524,

	/**
     * @member {Number}
     * @default 100
     */
    BASE_SOUND_VOLUME: 100,

	/**
     * @member {Number}
     * @default 100
     */
    BASE_BRIGHT: 100,

    /**
     * @member {Object}
     */
    APPConfig: {}
};
