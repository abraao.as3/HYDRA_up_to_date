var Const = require('./Const');
var EventEmitter = require('EventEmitter');
var PIXI = require('PIXI');

/**
* @classdesc Essa classe é responsável pela inicialização do PIXI e manipulção de seus objetos
 * @memberof module:core
 * @extends EventEmitter
 * @exports PIXIConfig
 * @constructor
 */
function PIXIConfig() {
    EventEmitter.call(this);

    /**
     * O objeto render do PIXI
     * @member {WebGLRenderer|CanvasRenderer}
     * @private
     */
    this._pixi = PIXI.autoDetectRenderer(Const.BASE_WIDTH, Const.BASE_HEIGHT, {
        transparent: true
    });
    PIXIConfig.type = this._pixi.type;
    PIXIConfig.renderer = this._pixi;

    /**
     * configuração do DOM
     */
    this._pixi.view.style.position = 'absolute';
    this._pixi.view.style.width = Const.BASE_WIDTH + 'px';
    this._pixi.view.style.height = Const.BASE_HEIGHT + 'px';

    /**
     * Fundo de suporte para habilitar interação (paralax)
     * @member {PIXI.Graphics}
     * @private
     */
    this._back = new PIXI.Graphics();
    this._back.beginFill(0, 0);
    this._back.drawRect(0, 0, Const.BASE_WIDTH, Const.BASE_HEIGHT);
    this._back.endFill();

    /**
     * O stage que será renderizado
     * @member {PIXI.Container}
     * @private
     */
    this._stage = new PIXI.Container();
    this._stage.addChild(this._back);
    PIXIConfig.stage = this._stage;

    //adiciona o canvas no html
    document.body.appendChild(this._pixi.view);

    /**
     * Emitido quando termina de redimensionar o stage
     * @event resize
     * @memberof module:core.PIXIConfig
     */

     this._factorScale = 1;
     this._canvasPos = {x: 0, y: 0};
}

PIXIConfig.prototype = Object.create(EventEmitter.prototype);
PIXIConfig.prototype.constructor = PIXIConfig;
module.exports = PIXIConfig;

Object.defineProperties(PIXIConfig.prototype, {
    /**
     * retorna o canvas criado com PIXIConfig
     * @name view
     * @type {DOMElement(canvas)}
     * @memberof module:core.PIXIConfig#
     * @readonly
     */
    view: {
        get: function() {
            return this._pixi.view;
        }
    },

    /**
     * retorna o Objeto renderer do PIXI
     * @name render
     * @type {WebGLRenderer|CanvasRenderer}
     * @memberof module:core.PIXIConfig#
     * @readonly
     */
    render: {
        get: function() {
            return this._pixi;
        }
    }
});

/**
 * Adiciona um filho ao stage
 * @param child {PIXI.DisplayObject} Objeto a ser adicionado
 */
PIXIConfig.prototype.addChild = function(child) {
    this._stage.addChild(child);
};

/**
 * Adiciona um filho ao stage na posição identificada
 * @param child {PIXI.DisplayObject} Objeto a ser adicionado
 * @param index {Number} Posicao a ser colocado o item
 */
PIXIConfig.prototype.addChildAt = function(child, index) {
    this._stage.addChildAt(child, index);
};

/**
 * Remove um filho do stage
 * @param child {PIXI.DisplayObject} objeto a ser removido
 */
PIXIConfig.prototype.removeChild = function(child) {
    this._stage.removeChild(child);
};

/**
 *  Método responsável pela renderização
 */
PIXIConfig.prototype.update = function() {
    this._pixi.render(this._stage);
};

/**
 * @desc Método responsável pelo redimensionamento do stage
 * @fires resize
 */
PIXIConfig.prototype.resize = function() {
    var scaleX = window.innerWidth / Const.BASE_WIDTH;
    var scaleY = window.innerHeight / Const.BASE_HEIGHT;

    var factorScale = 1;

    var dom = this._pixi.view;

    //se a largura da página for menor ou igual à largura do stage
    //ajusta a largura da página em relação ao stage
    //ou
    //define o tamanho do stage para o tamanho padrão
    if (scaleX <= 1) {
        dom.style.width = Const.BASE_WIDTH * scaleX + 'px';
        dom.style.height = Const.BASE_HEIGHT * scaleX + 'px';


        factorScale = scaleX;
    } else {
        dom.style.width = Const.BASE_WIDTH + 'px';
        dom.style.height = Const.BASE_HEIGHT + 'px';

        factorScale = 1;
    }

    //se a altura da página for menor ou igual à altura do stage
    //e se a largura base * scaleY for menor ou igual à largura da janela
    //define a altura do stage em relação à altura da janela
    if (scaleY <= 1) {
        if (Const.BASE_WIDTH * scaleY <= window.innerWidth) {
            dom.style.width = Const.BASE_WIDTH * scaleY + 'px';
            dom.style.height = Const.BASE_HEIGHT * scaleY + 'px';

            factorScale = scaleY;
        }
    }

    //se o dom estiver invisivel, torna-o visível
    if (dom.style.display == 'none') {
        dom.style.display = 'inline';
    }

    //centraliza o stage na página
    dom.style.left = window.innerWidth / 2 - parseFloat(dom.style.width) / 2 + 'px';
    dom.style.top = window.innerHeight / 2 - parseFloat(dom.style.height) / 2 + 'px';

    this._factorScale = factorScale;
    this._canvasPos = new PIXI.Point(parseFloat(dom.style.left), parseFloat(dom.style.top));

    //emite um evento de resize
    this.emit('resize', {
        factorScale: this._factorScale,
        canvasPos: this._canvasPos
    });
};

PIXIConfig.renderer = null;
PIXIConfig.type = 1;
PIXIConfig.stage = null;
