var PIXI = require('PIXI');
var $ = require('jquery');
var Contour = require('../behavior/Contour');

/**
* @classdesc Base para todos os objetos
 * @memberof module:core
 * @extends PIXI.Container
 * @exports ObjectBase
 * @constructor
 */
function ObjectBase() {
    PIXI.Container.call(this);
    this.addPlugin(new Contour());
}

ObjectBase.prototype = Object.create(PIXI.Container.prototype);
ObjectBase.prototype.constructor = ObjectBase;
module.exports = ObjectBase;

/**
 * adiciona plugin com novas funcionalidades
 * @param plugin {class} plugin de extensão
 */
ObjectBase.prototype.addPlugin = function(plugin) {
    $.extend(this, plugin);
};
