/**
 * @module animation
 */

var EventEmitter = require('EventEmitter');
var TweenMax = require('TweenMax');
var InternalPoint = require('./Point');

/**
 * @memberof module:animation
 * @exports Point
 * @constructor
 */
function Point(config, layers, soundManager) {
    EventEmitter.call(this);

    var self = this;

    this._config = {};
    this._layers = layers;
    this._soundManager = soundManager;

    this.timelineComplete = false;

    this._childPoint = [];
    this._isFadeOut = true;

    this._timeline = new TimelineMax({
        paused: true,
        onComplete: function() {
            self._timelineEnd();
        }
    });

	this._timeline.addLabel("startPoint", 0);

    /**
     * @member {Object}
     * @property [id=null] {Number} ID deste Point dentro de seu FadePoint, null se não é utilizado na Tela
     * @property [mouth=null] {Number[]} as bocas que irão mexer ex: [0] / [0,1] / default: null (quando não tem boca para mexer)
     * @property [images=null] {Number[]} imagens que irão se executadas pelo fadeIn e/ou fadeOut ex: [0] / [0,1] / default: null (quando não tem imagens no ponto)
     * @property [txt=null] {Number[]} textos que irão se executadas pelo fadeIn e/ou fadeOut ex: [0] / [0,1] / default: null (quando não tem textos no ponto)
     * @property [audio=null] {Number} indice do audio que será execultado no ponto / default: null (quando não tem audio no ponto)
     * @property [pause=false] {Boolean} indica se a sequencia irá pausar no final do ponto
     * @property [fadeIn=0.5] {Number} indica o tempo de animação (em segundos) para fadeIn `do ponto` /  null indica que as imagens aparecerão sem fadeIn
     * @property [fadeOut=0.5] {Number} indica o tempo da animação (em segundos) para fadeOut  `do ponto` / null indica que as imagens não irão desaparecer
     * @property [stand=1] {Number} indica o tempo (em segundos) entre o fadeIn e o fadeOut ou tempo de persistencia do ponto
     * @property [delay=0.1] {Number} indica o tempo (em segundos) de espera para iniciar o ponto
     * @property [sequences=null] {Object} indica a criação de novos pontos dentro do ponto principal - o tempo total do ponto é igual o tempo total de todos os pontos internos
     * @property [action=null] {Object} executa uma action sempre no início do ponto
     */
    this._default = {
        id: null,
        mouth: null,
        imgsMouth: null,
        images: null,
        txt: null,
        audio: null,
        pause: false,
        sequences: null,
        stand: 0.1,
        fadeIn: 0.5,
        fadeOut: 0.5,
        delay: 0,
        contour: undefined,
        particles: undefined,
        action: null,
		movieClips: undefined,
        buttons: null
    };

    for (var i in this._default) {
        if (config[i] !== undefined) {
            this._config[i] = config[i];
        } else {
            this._config[i] = this._default[i];
        }
    }

    var objTofade = [];
    if (this._checkExistence('images')) {
        for (var j = 0; j < this._config.images.length; j++) {
            var images = this._layers.getMap('images');
            // TODO GetMapByID
            for (var z = 0; z < images.length; z++) {
                if (this._config.images[j] == images[z].index) {
                    objTofade.push(images[z]);
                }
            }
        }
    }

    if (this._checkExistence('txt')) {
        for (var k = 0; k < this._config.txt.length; k++) {
            var text = this._layers.getMap('text');
            // TODO GetMapByID
            for (var zt = 0; zt < text.length; zt++) {
                if (this._config.txt[k] == text[zt].index)
                    objTofade.push(text[zt]);
            }
        }
    }

    if (this._checkExistence('buttons')) {
        for (var p = 0; p < this._config.buttons.length; p++) {
            var buttons = this._layers.getMap('button');
            // TODO GetMapByID
            for (var xp = 0; xp < buttons.length; xp++) {
                if (this._config.buttons[p] == buttons[xp].index)
                    objTofade.push(buttons[xp]);
            }
        }
    }

    if (this._checkExistence('imgsMouth')) {
        var m = this._layers.getMap('mouths');
        for (var ix = 0; ix < this._config.imgsMouth.length; ix++) {

            var imgMouth = m[this._config.imgsMouth[ix]];
            imgMouth.alpha = 0;
            objTofade.push(imgMouth);
        }

    }

    var particleObjects = [];
	if(this._checkExistence('particles')) {
		for(var o = 0; o < this._config.particles.length; o++) {
			var particle = this._config.particles[o];
			for(var q = 0; q < this._layers.particleElements.length; q++) {
				if(particle.numberObject == this._layers.particleElements[q].index) {
					this._layers.particleElements[q].emitter.emit = false;
					this._layers.particleElements[q]._startTime = (particle.startTime !== undefined && particle.startTime !== null && !isNaN(particle.startTime) && particle.startTime >= 0 ? Number(particle.startTime) : 0);
					this._layers.particleElements[q]._endTime = (particle.endTime === false || (!isNaN(particle.endTime) && particle.endTime >= 0) ? particle.endTime : true);
					particleObjects.push(this._layers.particleElements[q]);
				}
			}
		}
	}

	this._particleObjects = particleObjects;

	var movieClipObjects = [];
	if(this._checkExistence('movieClips')) {
		var movieClips = this._layers.getMap('movieClips');

		for(var _q = 0; _q < this._config.movieClips.length; _q++) {
			var cfg = this._config.movieClips[_q];
			for(var r = 0; r < movieClips.length; r++) {
				if(cfg.index == movieClips[r].index) {
					var movieClip = movieClips[r];

					if(cfg.fadeIn !== undefined && cfg.fadeIn !== null) {
						movieClip.alpha = 0;
						movieClip._fadeIn = {
							startTime: cfg.fadeIn.startTime !== undefined && cfg.fadeIn.startTime !== null && !isNaN(cfg.fadeIn.startTime) ? cfg.fadeIn.startTime : 0,
							duration: cfg.fadeIn.duration !== undefined && cfg.fadeIn.duration !== null && !isNaN(cfg.fadeIn.duration) ? cfg.fadeIn.duration : 0
						};
					}

					movieClip._fadeOut = (cfg.fadeOut !== undefined && cfg.fadeOut !== null && (cfg.fadeOut === true || !isNaN(cfg.fadeOut)) ? cfg.fadeOut: false);

					movieClipObjects.push(movieClip);
				}
			}
		}
	}

	this._movieClipObjects = movieClipObjects;

    this._getContourItems();

    this._objToFade = objTofade;

    var delay = 0;
    if (this._config.delay !== null && !isNaN(this._config.delay) && this._config.delay > 0) {
        delay = this._config.delay;
    }

    if (this._config.fadeIn !== null && !isNaN(this._config.fadeIn) && this._config.fadeIn > 0) {
        //TweenMax.to(objTofade, 0, {alpha: 0});
    } else {
        this._config.fadeIn = 0;
    }

    this._timeline.add(TweenMax.to(objTofade, this._config.fadeIn, {
        alpha: 1,
        delay: delay,
        onStart: function() {
            self.emit('pointStarted', self);
            self._configContour();

            if(self._config.action !== undefined && self._config.action !== null){
                self._executeAction();
            }
			// if(self._particleObjects !== undefined && self._particleObjects !== null && self._particleObjects.length > 0) {
			// 	for(var q = 0; q < self._particleObjects.length; q++) {
			// 		self._particleObjects[q].startEmitter();
			// 	}
			// }
        }
    }), "startPoint+="+this._timeline.duration());


    if (this._config.fadeIn === 0) {
        this._timeline.add(function() {
            self.emit('pointStarted', self);
            self._configContour();

            if(self._config.action !== undefined && self._config.action !== null){
                self._executeAction();
            }
        });
    }

    var audioPlay = null;
    var stand = 0.1;
    var bocas = [];

    if (this._config.stand !== null && !isNaN(this._config.stand) && this._config.stand > 0) {
        stand = this._config.stand;
    }

    var audio = null;

    if (this._config.audio !== null && !isNaN(this._config.audio)) {
        audio = this._soundManager._sounds[this._config.audio];
        if (self._checkExistence('mouth')) {
            for (var l = 0; l < self._config.mouth.length; l++) {
                var mouths = self._layers.getMap('mouths');
                bocas.push(mouths[self._config.mouth[l]]);
            }
        }

        if (audio.duration !== undefined && audio.duration !== null) {
            configureAudio();
        } else {
            audio.once('loadedmetadata', configureAudio);
        }
    } else {
        self._timeline.add(TweenMax.to({}, stand, {}), "startPoint+="+this._timeline.duration());
        self._addFadeOut();
    }

    function configureAudio() {
        var duration = audio.duration;
        if (stand < duration)
            stand = duration;

        var audioPlay = function() {
            self._soundManager.once('soundComplete', function() {
                for (var m = 0; m < bocas.length; m++) {
                    bocas[m].gotoAndStop(bocas[m].stationary);

                    if (self._config.fadeOut === true &&
                        self._config.imgsMouth !== undefined &&
                        self._config.imgsMouth !== null &&
                        self._config.imgsMouth.length <= 0) {
                        bocas[m].visible = false;
                    }
                }

                if (self._isFadeOut) {
                    self._timeline.seek("fadeOut", true);
                } else {
                    self._timeline.seek(self._timeline.duration() - 0.1);
                }
            });

            self._soundManager.playSound(self._config.audio);
            for (var n = 0; n < bocas.length; n++) {
                bocas[n].visible = true;
                bocas[n].play();
            }
        };

        self._timeline.add(TweenMax.to({}, stand, {
            onStart: audioPlay
        }), "startPoint+="+self._timeline.duration());

        self._addFadeOut();
    }
}

Point.prototype = Object.create(EventEmitter.prototype);
Point.prototype.constructor = Point;
module.exports = Point;

Object.defineProperties(Point.prototype, {
    timeline: {
        get: function() {
            return this._timeline;
        }
    }
});

Point.prototype._configContour = function() {
    var self = this;

    if (self._config.contour !== undefined) {
        if (self._config.contour.action == "open") {
            fn = "openContour";
        } else if (self._config.contour.action == "close") {
            fn = "closeContour";
        }

        for (var z = 0; z < self._contours.length; z++) {
            self._contours[z][fn]();
        }
    }
};

Point.prototype._executeAction = function(){
    var action = this._config.action;

    if(action.popup !== undefined && action.popup !== null){
        var p = this._layers.getMap("popups")[action.popup.id];
        if(action.popup.function == "open"){
            this._layers.parent._openPopup({action: action.popup.id});
        }
        else if (true) {
            p.close();
        }
    }
};

Point.prototype._getContourItems = function() {
    var self = this;

    if (self._config.contour !== undefined) {
        var prop = "";
        if (self._config.contour.images !== undefined) {

            prop = "images";
        } else if (self._config.contour.button !== undefined) {

            prop = "button";
        } else if (self._config.contour.text !== undefined) {

            prop = "text";
        }


        for (var z = 0; z < self._config.contour[prop].length; z++) {
            var __imgs = self._layers.getMap(prop);
            for (var za = 0; za < __imgs.length; za++) {
                if (__imgs[za].index == self._config.contour[prop][z]) {
                    if (this._contours === undefined || this._contours === null) {
                        this._contours = [];
                    }

                    this._contours.push(__imgs[za]);
                    break;
                }
            }
        }
    }
};

Point.prototype._addFadeOut = function() {
    var self = this;

    if (this._config.fadeOut !== null && !isNaN(this._config.fadeOut) && this._config.fadeOut > 0) {
        this._timeline.add(TweenMax.to(this._objToFade, this._config.fadeOut, {
            alpha: 0,
            onStart: function() {
                self._configContour();

				// if(self._particleObjects !== undefined && self._particleObjects !== null && self._particleObjects.length > 0) {
				// 	for(var q = 0; q < self._particleObjects.length; q++) {
				// 		if(self._particleObjects[q]._fadeOut) {
				// 			self._particleObjects[q].stopEmitter();
				// 		}
				// 	}
				// }
            }
        }), "fadeOut");
    } else {
        this._isFadeOut = false;
    }

    if (this._config.sequences !== null) {
        var seq = this._config.sequences;
        for (var i = 0; i < seq.length; i++) {
            var p = new Point(this._config.sequences[i], this._layers, this._soundManager);
            this._childPoint.push(p);
            if (p.timelineComplete) {
                registerTL(p);
            } else {
                p.once('timelineComplete', evt);
            }
        }
    } else {
		if(this._particleObjects !== undefined && this._particleObjects !== null && this._particleObjects.length > 0) {
			var startParticle = function(particle) {
				var timeLinePosition = "startPoint+="+particle._startTime;

					self._timeline.add(function() {
						particle.startEmitter();
					}, timeLinePosition);
			};

			var stopParticle = function(particle) {
				if(particle._endTime !== false) {
					var timeLinePosition = particle._endTime === true ? "startPoint+="+self._timeline.duration() : "startPoint+="+particle._endTime;

					self._timeline.add(function() {
						particle.stopEmitter(true);
					}, timeLinePosition);
				}
			};

			for(var q = 0; q < self._particleObjects.length; q++) {
				startParticle(self._particleObjects[q]);
				stopParticle(self._particleObjects[q]);
			}
		}

		if(this._movieClipObjects !== undefined && this._movieClipObjects !== null && this._movieClipObjects.length > 0) {
			var fadeIn = function (clip) {
				if(clip._fadeIn !== undefined) {
					var timeLinePosition = "startPoint+="+clip._fadeIn.startTime;

					self._timeline.add(TweenMax.to(clip, clip._fadeIn.duration, {
						alpha: 1
					}), timeLinePosition);

					timeLinePosition = "startPoint+="+(clip._fadeIn.startTime+clip._fadeIn.duration);
					self._timeline.add(TweenMax.to({},(clip._stationary+1)/(clip.animationSpeed*60), {
						onStart: function() {
							clip.play();
						}
					}),timeLinePosition);
				} else {
					self._timeline.add(TweenMax.to({},(clip._stationary+1)/(clip.animationSpeed*60), {
						onStart: function() {
							clip.play();
						}
					}), "startPoint");
				}
			};

			var fadeOut = function (clip) {
				if(clip._fadeOut !== false) {
					var timeLinePosition = "startPoint+="+self._timeline.duration();

					self._timeline.add(TweenMax.to(clip, clip._fadeOut === true || clip._fadeOut < 0.1 ? 0.1 : clip._fadeOut, {
						alpha: 0
					}), timeLinePosition);
				}
			};

			for(var idx = 0; idx < this._movieClipObjects.length; idx++) {
				fadeIn(this._movieClipObjects[idx]);
				fadeOut(this._movieClipObjects[idx]);
			}
		}

        this.timelineComplete = true;
        this.emit('timelineComplete');
    }

    function evt(e) {
        registerTL(this);
    }

    function registerTL(p) {
        p.timeline.paused(false);
        self._timeline.add(p.timeline, 0);
        self.timelineComplete = true;
        self.emit('timelineComplete');
    }
};

Point.prototype._checkExistence = function(type) {
    if (Object.prototype.toString.call(this._config[type]) === '[object Array]' && this._config[type].length > 0) {
        return true;
    } else {
        return false;
    }
};

Point.prototype._timelineEnd = function() {
    var self = this;

	// if(!this._isFadeOut && this._particleObjects !== undefined && this._particleObjects !== null && this._particleObjects.length > 0) {
	// 	for(var r = 0; r < this._particleObjects.length; r++) {
	// 		if(self._particleObjects[r]._fadeOut) {
	// 			this._particleObjects[r].stopEmitter();
	// 		}
	// 	}
	// }

    this.emit('pointComplete');
};

Point.prototype.start = function() {
    if (this._config.fadeIn !== null && !isNaN(this._config.fadeIn) && this._config.fadeIn > 0) {
        TweenMax.to(this._objToFade, 0, {
            alpha: 0
        });
    }

    for (var i = 0; i < this._childPoint.length; i++) {
        this._childPoint[i].start();
    }
};

Point.prototype.play = function() {
    this._timeline.play();
    if (this._childPoint !== null && this._childPoint !== undefined) {
        for (var j = 0; j < this._childPoint.length; j++) {
            this._childPoint[j].play();
        }
    }
};

Point.prototype.forceEnd = function() {
    if (this._particleObjects !== undefined && this._particleObjects !== null && this._particleObjects.length > 0) {
        for (var r = 0; r < this._particleObjects.length; r++) {
            this._particleObjects[r].stopEmitter(true, true);
        }
    }

    this._timeline.seek(this._timeline.duration(), true);

    if (this._contours !== undefined && this._contours !== null) {
        for (var i = 0; i < this._contours.length; i++) {
            try {
                this._contours[i].closeContour();
            } catch (err) {
                console.log(" objeto sem contorno");
            }
        }
    }
};

Point.prototype.reset = function(kill) {
    this.start();
    if (this._soundManager !== undefined && this._soundManager !== null) {
        this._soundManager.stop();
        // this._soundManager.removeListener('soundComplete');
    }

    this._timeline.pause();
    this._timeline.kill();


    if (this._particleObjects !== undefined && this._particleObjects !== null && this._particleObjects.length > 0) {
        for (var r = 0; r < this._particleObjects.length; r++) {
            this._particleObjects[r].stopEmitter(true, true);
        }
    }

	if(this._movieClipObjects !== undefined && this._movieClipObjects !== null && this._movieClipObjects.length > 0) {
		for(var s = 0; s < this._movieClipObjects.length; s++) {
			this._movieClipObjects[s].gotoAndStop(0);
		}
	}

    if (this._contours !== undefined && this._contours !== null) {
        for (var i = 0; i < this._contours.length; i++) {
            try {
                this._contours[i].closeContour();
            } catch (err) {
                console.log(" objeto sem contorno");
            }
        }
    }

    if (this._childPoint !== null && this._childPoint !== undefined) {
        for (var j = 0; j < this._childPoint.length; j++) {
            this._childPoint[j].reset();
        }
    }

    this._timeline.seek(0, true);
};
