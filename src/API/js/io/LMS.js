require('scormApi');

var scorm = pipwerks.SCORM;

var tela,
    porcurso,
	brilho,
	audio,
    _numtelas;

function LMS() {

    scorm.version = "1.2";
    scorm.connection.initialize();

    this.MINIMUN_AVAL = 3;

    // window.onunload = window.onbeforeunload = window.onpagehide = function() {
    //     if (scorm.connection.isActive) {
    //         pipwerks.UTILS.trace("Finalizando conexão no Unload");
    //         scorm.save();
    //         scorm.quit();
    //     }
    // };

    // tela = scorm.data.get("cmi.core.lesson_location");
    if(window.top.getCurrentScreen !== undefined) {
        tela = window.top.getCurrentScreen();
    }
    // porcurso = scorm.data.get("cmi.core.score.raw");
	// brilho = scorm.data.get("cmi.suspend_data");
	// audio = scorm.data.get("cmi.student_preference.audio");
	brilho = '';
	audio = '';
}

LMS.prototype.constructor = LMS;
module.exports = LMS;

Object.defineProperties(LMS.prototype, {
    tela: {
        get: function() {
            return scorm.data.get("cmi.core.lesson_location");
        },

        set: function(value) {
            var pct = (value / _numtelas).toFixed(2);

            console.log(pct);
            //
            // if(window.top.setCurrentScreen !== undefined){
            //     window.top.setCurrentScreen(value, pct);
            // }
            //
            this.curso = pct;

            scorm.data.set("cmi.core.lesson_location", value);
            scorm.save();
        }
    },
	curso: {
        get: function() {
            return porcurso;
        },

        set: function(value) {
            value = Math.round(value * 100);
            console.log("enviando valor para o curso....." + value);
            scorm.data.set("cmi.core.score.raw", value);

            if (value >= 100) {
                scorm.data.set("cmi.core.score.ra,,w", 100);
                scorm.data.set("cmi.core.lesson_status", "completed");
            }

            scorm.save();
        }
    },
	brilho: {
		get: function() {
			return brilho;
		},
		set: function(value) {
			// brilho = value;
			// scorm.data.set("cmi.suspend_data", brilho);
			// scorm.save();
		}
	},
	audio: {
		get: function() {
			return audio;
		},
		set: function(value) {
			// audio = value;
			// scorm.data.set("cmi.student_preference.audio", audio);
			// scorm.save();
		}
	},

    _numtelas: {
        set: function(value){
            _numtelas = value;
        }
    }
});

LMS.prototype.completed = function() {
    // scorm.data.set("cmi.core.lesson_status", "completed");
    // scorm.save();
};
