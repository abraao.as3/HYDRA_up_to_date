var PIXI = require('PIXI');
var $ = require('jquery');
var Const = require('../core/Const');
var behavior = require('../behavior/index');

/**
* @classdesc Classe responsável por criar e administrar os vídeos
* @memberof module:media
* @constructor
* @extends PIXI.Container
* @param config {Object[]} array de objetos com as  configurações do vídeo
*/
function VideoManager(config) {
    PIXI.Container.call(this);

    //armazena o config
    this._config = config;

    //armazena a porcentagem do seek
    this._seek = 0;

    this._bts = [];
    this._factorScale = 1;
    this._library = [];
    this._count = 0;

    //Elemento DOM referete ao stage para armazenar a página de vídeos
    this._container = $(document.createElement('div'));
    this._container.css({
        width: Const.BASE_WIDTH + 'px',
        height: Const.BASE_HEIGHT + 'px',
        position: 'absolute',
        //border: '1px solid red',
        overflow: 'hidden'
    });

    //Elemento DOM que irá armazenar os vídeos - É a página de vídeo
    this._containerVideos = $(document.createElement('div'));
    this._containerVideos.css({
        width: '100%',
        height: '100%',
        position: 'absolute',
        overflow: 'hidden',
        //border: '1px solid blue',
    });

    for (var i = 0; i < this._config.length; i++) {
        var video = $(document.createElement('video'));
        var source = $(document.createElement('source'));
        source.attr('src', this._config[i].url);
        video.css({
            position: 'absolute'
        });

        video.width(this._config[i].width);
        video.height(this._config[i].height);
        video.offset({
            left: this._config[i].x,
            top: this._config[i].y
        });

        if (this._config[i].thumb !== null && this._config[i].thumb !== undefined && this._config[i].thumb.length > 0) {
            video.attr('poster', this._config[i].thumb);
        }

        video.append(source);
        this._containerVideos.append(video);

        var videobt = new PIXI.Graphics();
        videobt.beginFill(0xff0000, 0);
        videobt.drawRect(0, 0, this._config[i].width, this._config[i].height);
        videobt.endFill();
        videobt.x = this._config[i].x;
        videobt.y = this._config[i].y;
        videobt.id = i;

        this.addChild(videobt);
        this._bts.push(videobt);

        this._library.push(video[0]);
    }

    this._container.append(this._containerVideos);

    /**
     * Emitido quando todos os vídeos foram assistidos
     * @event completed
     * @memberof module:media.VideoManager
     */

     /**
      * Emitido quando um vídeo unitário é assistido
      * @event videoEnded
      * @memberof module:media.VideoManager
      */
}

VideoManager.prototype = Object.create(PIXI.Container.prototype);
VideoManager.prototype.constructor = VideoManager;
module.exports = VideoManager;

/**
* Método responsável por iniciar os objetos
* @param pos {object} objeto que armazena a posição da tela
* @param pos.x {number} posição x
* @param pos.y {number} posição y
*/
VideoManager.prototype.start = function(pos) {
    var self = this;
    this._canvas = $('canvas');
    $('body').prepend(this._container);

    this._container.css({
        width: this._canvas.width() + 'px',
        height: this._canvas.height() + 'px'
    });
    this._container.offset({
        left: this._canvas.offset().left,
        top: this._canvas.offset().top
    });

    this.resize(this._canvas.width() / Const.BASE_WIDTH);
    this.updatePos(pos);

    if (this._config.length > 1) {
        for (var i = 0; i < this._bts.length; i++) {
            var bt = this._bts[i];
            $.extend(bt, new behavior.Clickable());
            bt.on('clicked', this._btClicked, this);
            bt._selectionEffect = false;
            bt.addEvents();

            var video = this._containerVideos.find('video')[i];
            video.onended = end_video;
        }
    }

    function end_video() {
        self._endvideo(this, self);
    }
};

/**
* Método responsável por atualizar a posição da tela
* @param pos {object} objeto que armazena a posição da tela
* @param pos.x {number} posição x
* @param pos.y {number} posição y
*/
VideoManager.prototype.updatePos = function(pos) {
    this._containerVideos.css({
        left: pos.x * this._factorScale + 'px',
        top: pos.y * this._factorScale + 'px'
    });
};

VideoManager.prototype._endvideo = function(video, self) {
    self._count++;
    video.currentTime = 0.1;

    if (self._count >= self._library.length) {
        self.emit("completed");
    }
};

/**
* Método responsável por redimencionar a tela
* @param factorScale {number} valor da escala (porcentagem)
*/
VideoManager.prototype.resize = function(factorScale) {
    this._factorScale = factorScale;
    var videos = this._containerVideos.find('video');
    for (var i = 0; i < videos.length; i++) {
        var video = $(videos[i]);
        video.css({
            width: this._config[i].width * factorScale + 'px',
            height: this._config[i].height * factorScale + 'px',
            left: this._config[i].x * factorScale + 'px',
            top: this._config[i].y * factorScale + 'px'
        });
    }
    if(this._canvas !== undefined)
    this._container.css({
        width: this._canvas.width() + 'px',
        height: this._canvas.height() + 'px',
        left: this._canvas.offset().left + 'px',
        top: this._canvas.offset().top + 'px'
    });
};

VideoManager.prototype._btClicked = function(e) {
    this.playIndex(e.target.id);
};

/**
* Inicia a reprodução do vídeo unitário
* @param [play=0] {number} posição da reprodução em porcentagem
*/
VideoManager.prototype.play = function(seek) {
    var video = this._containerVideos.find('video')[0];
    var s = seek || this._seek;
    try{
        video.currentTime = s * video.duration;
    }
    catch(err){
        
    }
    video.play();
};

/**
* Inicia ou pausa a reprodução de um vídeo
* @param index {number} index do vídeo a ser reproduzido
*/
VideoManager.prototype.playIndex = function(index) {
    var self = this;
    var video = this._containerVideos.find('video')[index];
    if (video.paused) {
        video.play();
    } else {
        video.pause();
    }

};

/**
* Pausa a reprodução de um vídeo unitário
*/
VideoManager.prototype.pause = function() {
    var video = this._containerVideos.find('video')[0];
    video.pause();
    this._seek = video.currentTime / video.duration;
};

/**
* atualiza a posição da reprodução
* @param pct {number} posição em porcentagem
*/
VideoManager.prototype.updateSeek = function(pct) {
    this._seek = pct;
};

/**
* altera o volume do vídeo
* @param pct {number} posição em porcentagem
*/
VideoManager.prototype.updateVol = function(pct) {
    var self = this;
    var video = this._containerVideos.find('video')[0];
    video.volume = pct <= 0.03 ? 0 : pct;
};

/**
* Inicia os eventos
* @param func {function} função chamada ao ocorrer um evento
* @param context {object} contexto para a função
* @fires videoEnded
*/
VideoManager.prototype.setUpdateEvent = function(func, context) {
    var self = this;
    var video = this._containerVideos.find('video')[0];
    video.ontimeupdate = function() {
        func(video.currentTime / video.duration, context);
    };

    video.onended = function() {
        self._seek = 0;
        this.currentTime = 0.1;
        self.emit('videoEnded');
    };
};

/**
* autoriza a reprodução automática dos vídeos
*/
VideoManager.prototype.autorizeAll = function() {
    for (var i = 0; i < this._library.length; i++) {
        var video = this._library[i];
        video.muted = true;
        video.play();
        video.pause();
        video.currentTime = 0.1;
        video.muted = false;
    }
};

/**
* encerra as atividades dos vídeos
*/
VideoManager.prototype.close = function() {
    for (var i = 0; i < this._library.length; i++) {
        var video = this._library[i];
        video.pause();
        video.currentTime = 0.1;
        console.log("video.currentTime",video.currentTime);
    }

    this._container.remove();

    if (this._config.length > 1) {
        for (var j = 0; j < this._bts.length; j++) {
            var bt = this._bts[j];
            bt.removeListener('clicked');
            bt.removeEvents();
        }
    }
};
