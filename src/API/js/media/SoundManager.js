/**
* @module media
*/

var EventEmitter = require('EventEmitter');
var Scream = require('Scream');

/**
  * @classdesc Classe que define o controlador de som e suas caracteristicas
  * @memberof module:media
  * @extends EventEmitter
  * @exports SoundManager
  * @constructor
  */
function SoundManager() {
    EventEmitter.call(this);

    this._initialized = false;

    this._sounds = [];
}

SoundManager.prototype = Object.create(EventEmitter.prototype);
SoundManager.prototype.constructor = SoundManager;
module.exports = SoundManager;

/**
 * Adiciona um unico som a ser tocado
 *
 * @param url Local onde este som está armazenado
 * @public
 */
SoundManager.prototype.addSingleSound = function(url, settings) {
    var s = new Scream.Audio(url);
    if(settings !== undefined && settings !== null){
        s.volume(settings.volume / 100);
    }
    this._sounds.push(s);
};

/**
 * Toca um unico som, o da posição indicada no parametro do método
 *
 * @param index posição do array de sons que será tocada
 * @public
 */
SoundManager.prototype.playSound = function(index) {
    var self = this;
    SoundManager.stopAll();

    var s = this._sounds[index];
    s.once('play', function(){
		self.emit('soundStart', {target:{index:index, sound: self}});
    });
    s.once('end', function(){
		self.emit('soundComplete', {target:{index:index, sound: self}});
    });
    s.play();
};

/**
 * Para todos os sons que estãos endo tocados no momento
 *
 * @public
 */
SoundManager.prototype.stop = function() {
    SoundManager.stopAll();
};

/**
 * Para todos os sons que estãos endo tocados no momento
 *
 * @public
 */
SoundManager.stopAll = function() {
    for(var i in Scream._map){
        Scream._map[i].stop();
    }
};
