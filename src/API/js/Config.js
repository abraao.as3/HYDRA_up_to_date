var $ = require('jquery');
// document.body.style.background = 'url(telas/back/htmlBack.jpg) no-repeat';
$('body').css({
    margin: '0px',
    width: '100%',
    height: '100%'
});

var appBackground = $(document.createElement('div'));
$('body').append(appBackground);
appBackground.css({
    width: '100%',
    height: '100%',
    position: 'absolute',
    zIndex: -1000,
    overflow: 'hidden'
});


var Font = require('Font');
var Main = require('./Main');
/**
 * @memberof Config
 * @class
 */
function Config() {
    var fontLibrary = [{
        name: 'arcena',
        url: 'fonts/arcena.ttf'
    }, {
        name: 'playtime',
        url: 'fonts/playtime.ttf'
    }, {
        name: 'maria',
        url: 'fonts/maria_lucia.ttf'
    }, {
        name: 'century',
        url: 'fonts/century.ttf'
    }, {
        name: 'escolar',
        url: 'fonts/escolar8.ttf'
    }, {
        name: 'hindlight',
        url: 'fonts/Hind-Light.ttf'
    }, {
        name: 'hindbold',
        url: 'fonts/Hind-Bold.ttf'
    }, {
        name: 'hindregular',
        url: 'fonts/Hind-Regular.ttf'
    }];

    var count = 0;

    for (var i = 0; i < fontLibrary.length; i++) {
        var f = new Font();
        f.fontFamily = fontLibrary[i].name;
        f.onload = tempLoad;
        f.src = fontLibrary[i].url;
    }

    function tempLoad() {
        count++;

        if (count >= fontLibrary.length) {
            fontsComplete();
        }
    }

    function fontsComplete() {
        new Main();
    }

    window.tls = {};
}
Config();
