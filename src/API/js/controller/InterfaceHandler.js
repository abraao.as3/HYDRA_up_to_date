var Interfaces = require('../ui/interfaces/index');
var Const = require('../core/Const');

/**
 * @classdesc Essa classe possui um método getInstance que retorna uma nova interface com base no tema selecionado no APPConfig
 * @memberof module:controller.InterfaceHandler
 * @exports InterfaceHandler
 * @constructor
 */
function InterfaceHandler() {

}

InterfaceHandler.prototype = Object.create(PIXI.Container.prototype);
InterfaceHandler.prototype.constructor = InterfaceHandler;
module.exports = InterfaceHandler;

/**
   * Retorna uma instância da interface com base no tema do curso
   *
   * @public
   */
InterfaceHandler.getInstance = function() {
    return Interfaces[Const.APPConfig.InterfaceTheme].getInstance();
};
