/**
 * @module controller
 */
var $ = require('jquery');
var PIXI = require('PIXI');
var ObjectFactory = require('../display/core/ObjectFactory');
var TemplateFactory = require('../templates/core/TemplateFactory');
var Const = require('../core/Const');
var SoundManager = require('../media/SoundManager');
var Interface = require('../controller/InterfaceHandler');
var Bitmap = require('../display/core/Bitmap');
var SeekBar = require('../ui/SeekBar');

/**
 * @classdesc Classe que controla a criação das telas
 * @memberof module:controller
 * @exports ScreenManager
 * @constructor
 */
function ScreenManager() {
    PIXI.Container.call(this);

    /**
     * Armazena a Interface que irá gerar os componentes de interface
     * @member {Object}
     * @type {module:ui.Interface}
     * @private
     */
    this._interface = null;

    /**
     * Armazena o módulo requisitado
     * @member {Array}
     * @private
     */
    this._module = null;

    /**
     * Armazena as instancias das telas
     * @member {Object}
     * @private
     */
    this._screens = {};

    /**
     * Armazena um lista de back que serão usados no modulo
     * @member {Object}
     * @private
     */
    this._backs = {};

    this._moduleCount = 0;

    this._soundVol = null;

    this._bright = null;

    /**
     * Emitido quando termina de carregar o appConfig.json
     * @event appConfigLoaded
     * @memberof module:controller.ScreenManager
     */

    /**
     * Emitido quando termina de carregar a leta  inicial
     * @event startScreen
     * @memberof module:controller.ScreenManager
     * @type {object}
     * @property {module:templates/core.ITemplate} startScreen - envia a Tela montada
     * @see {module:templates/core.ITemplate}
     */
}

ScreenManager.prototype = Object.create(PIXI.Container.prototype);
ScreenManager.prototype.constructor = ScreenManager;
module.exports = ScreenManager;

/**
 * inicia o carregamento do módulo
 * @param module {Array} array contendo o nome das páginas a serem carregadas
 * @fires appConfigLoaded
 */
ScreenManager.prototype.initialize = function(module) {
    this._module = module;
    this._loadInitialConfig();
};

/**
 * Método que carrega a configuração do sistema
 * @private
 */
ScreenManager.prototype._loadInitialConfig = function() {
    var self = this;

    $.getJSON('telas/appConfig.json', function(data) {

        Const.APPConfig = data;
        self._interface = Interface.getInstance();

        self.emit('appConfigLoaded');
        self.once('screenCreated', self._addStart, self);
        self._loadScreen('start');
    });
};

ScreenManager.prototype.loadInterface = function(nivelBrilho, nivelVolume) {
    var self = this;

    this._bright = nivelBrilho;
    this._soundVol = nivelVolume;

    var componentesInterface = new PIXI.loaders.Loader();

    $.getJSON('telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/buttonsCfg.json', function(data) {

        var buttonCfg = data;

        if (buttonCfg.next !== undefined && buttonCfg.next === true) {
            componentesInterface.add('navegacaoNext', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/next.png');
        }

		if (buttonCfg.nextInativo !== undefined && buttonCfg.nextInativo === true) {
            componentesInterface.add('navegacaoNextInativo', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/nextInativo.png');
        }

        if (buttonCfg.back !== undefined && buttonCfg.back === true) {
            componentesInterface.add('navegacaoBack', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/back.png');
        }

		if (buttonCfg.backInativo !== undefined && buttonCfg.backInativo === true) {
            componentesInterface.add('navegacaoBackInativo', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/backInativo.png');
        }

        if (buttonCfg.fechar !== undefined && buttonCfg.fechar === true) {
            componentesInterface.add('closeButton', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/fechar.png');
        }

		if (buttonCfg.navButtons !== undefined && buttonCfg.navButtons === true) {
            componentesInterface.add('navButtons', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/navButtons/sprites.json');
        }

        if (buttonCfg.configBotao !== undefined && buttonCfg.configBotao === true) {
            componentesInterface.add('configBotao', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/configBotao.png');
            componentesInterface.add('configMenu', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/menuOpcoes/menuOpcoes.json');
        }

        if (buttonCfg.ajuda_botao !== undefined && buttonCfg.ajuda_botao === true) {
            componentesInterface.add('ajudaBotao', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/ajuda_botao.png');
            componentesInterface.add('ajudaMenu', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/ajuda_info.png');
        }

        if (buttonCfg.refresh !== undefined && buttonCfg.refresh === true) {
            componentesInterface.add('refreshButton', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/refresh.png');
        }

        if (buttonCfg.backPontos !== undefined && buttonCfg.backPontos === true) {
            componentesInterface.add('backpontos', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/backPontos.png');
        }

		if (buttonCfg.logo !== undefined && buttonCfg.logo === true) {
            componentesInterface.add('logo', 'telas/commom/interfaces/' + Const.APPConfig.InterfaceTheme + '/logo.png');
        }


        componentesInterface.add('particleObject', 'telas/commom/particles/dotParticle.png');
        componentesInterface.add('particleStarTexture', 'telas/commom/particles/star.png');
        componentesInterface.add('particleShinyTexture', 'telas/commom/particles/shiny.png');

        componentesInterface.add('hand', 'telas/commom/hand/mao1.png');
        componentesInterface.add('clickingHand', 'telas/commom/hand/mao2.png');

        componentesInterface.load(function(loader, resource) {
			if(buttonCfg.logo !== undefined && buttonCfg.logo === true){
                self._interface.logo = new Bitmap(resource.logo.texture);
            }

			if (buttonCfg.nextInativo !== undefined && buttonCfg.nextInativo === true) {
				self._interface.navegacaoSeguinteInativo = new Bitmap(resource.navegacaoNextInativo.texture);
			}

            if (buttonCfg.next !== undefined && buttonCfg.next === true) {
                self._interface.navegacaoSeguinte = new Bitmap(resource.navegacaoNext.texture);
            }

			if (buttonCfg.backInativo !== undefined && buttonCfg.backInativo === true) {
				self._interface.navegacaoAnteriorInativo = new Bitmap(resource.navegacaoBackInativo.texture);
			}

            if (buttonCfg.back !== undefined && buttonCfg.back === true) {
                self._interface.navegacaoAnterior = new Bitmap(resource.navegacaoBack.texture);
            }

            if (buttonCfg.ajuda_botao !== undefined && buttonCfg.ajuda_botao === true) {
                self._interface.ajudaMenu = new Bitmap(resource.ajudaMenu.texture);
                self._interface.ajudaBotao = new Bitmap(resource.ajudaBotao.texture);
            }

            if (buttonCfg.refresh !== undefined && buttonCfg.refresh === true) {
                self._interface.refreshButton = new Bitmap(resource.refreshButton.texture);
            }

            if (buttonCfg.fechar !== undefined && buttonCfg.fechar === true) {
                self._interface.closeButton = new Bitmap(resource.closeButton.texture);
            }

			if (buttonCfg.navButtons !== undefined && buttonCfg.navButtons === true) {
				var navButtons = {};
				for(var texture in resource.navButtons.textures) {
					if(texture === 'navBackground.png') {
						navButtons['navBackground'] = new Bitmap(resource.navButtons.textures[texture]);
						navButtons['navBackground'].x = resource.navButtons.data.frames[texture].spriteSourceSize.x;
						navButtons['navBackground'].y = resource.navButtons.data.frames[texture].spriteSourceSize.y;
					} else {
						var buttonName = texture.slice(0, texture.indexOf('_'));
						var buttonStateName = texture.slice(texture.indexOf('_')+1, texture.indexOf('.'));

						var navButtonState = new Bitmap(resource.navButtons.textures[texture]);
						navButtonState.x = resource.navButtons.data.frames[texture].spriteSourceSize.x;
						navButtonState.y = resource.navButtons.data.frames[texture].spriteSourceSize.y;

						var navIndex = buttonName.substr(-2);
						navButtonState.navIndex = isNaN(navIndex) ? navIndex : Number(navIndex);

						if(buttonName.match(/navButton[0-9]{2}/) !== null) {
							navButtonState.hasClick = true;
						}

						var navButton = navButtons[buttonName] !== undefined ? navButtons[buttonName] : {};
						navButton[buttonStateName] = navButtonState;

						navButtons[buttonName] = navButton;
					}
				}

				self._interface.navegacaoButtons = navButtons;
			}

            self._interface.hand = new Bitmap(resource.hand.texture);
            self._interface.clickingHand = new Bitmap(resource.clickingHand.texture);

            if (buttonCfg.backPontos !== undefined && buttonCfg.backPontos === true) {
                self._interface.backPontos = new Bitmap(resource.backpontos.texture);
                self._interface.pontos = new type.text.TextField(110, 1);
                self._interface.showPoints(false);
            }

            self._interface.particleObject = {
                bitmap: new Bitmap(resource.particleObject.texture),
                texture: [resource.particleStarTexture.texture, resource.particleShinyTexture.texture]
            };

            if (buttonCfg.configBotao !== undefined && buttonCfg.configBotao === true) {
                var configMenuTexture = [];
                self._cropSupport(resource, configMenuTexture, ['configMenu']);
                // console.log(menuOpcoesTexture);

                var configMenu = new PIXI.Container();

                var brilhoBarTexture = {};
                var volBarTexture = {};

                for (var i in configMenuTexture[0]) {
                    var prefix = i.split('_')[0];

                    if (prefix == "brilhoBar") {
                        brilhoBarTexture[i] = configMenuTexture[0][i];
                    } else if (prefix == "volBar") {
                        volBarTexture[i] = configMenuTexture[0][i];
                    } else {
                        var obj = configMenuTexture[0][i];
                        var bt = new Bitmap(obj.texture);

                        bt.x = obj.config.x;
                        bt.y = obj.config.y;

                        configMenu[i.replace('.png', '')] = bt;
                        configMenu.addChild(bt);
                    }
                }

                if (configMenu.background !== undefined && configMenu.background !== null) {
                    configMenu.background.interactive = true;
                }

                // console.log(brilhoBarTexture);
                configMenu.brilhoBar = new SeekBar([brilhoBarTexture], {
                    slideBar: false,
                    seekPosition: self._bright
                });
                configMenu.brilhoBar.addEvents();
                configMenu.addChild(configMenu.brilhoBar);

                // console.log(volBarTexture);
                configMenu.volBar = new SeekBar([volBarTexture], {
                    slideBar: false,
                    seekPosition: self._soundVol
                });
                configMenu.volBar.addEvents();
                configMenu.addChild(configMenu.volBar);

                // console.log(configMenu);
                self._interface.configMenu = configMenu;

                self._interface.configBotao = new Bitmap(resource.configBotao.texture);

                self._interface.setChildIndex(self._interface.brightSim, self._interface.children.length - 1);

                self._interface.configMenu.brilhoBar.on('seekIconDown', self._brightSeekDown, self);
                self._interface.configMenu.brilhoBar.on('seekIconUp', self._brightSeekUp, self);
                self._interface.configMenu.brilhoBar.on('seekPositionUpdate', self._brightSeekPositionUpdate, self);
            }




            self.loadModule();
        });

    });
};

ScreenManager.prototype.loadModule = function() {
    var self = this;

    if (this._moduleCount == this._module.length) {
        this._moduleCount = 0;
        //TODO falta carregar a tela fim.....

        if (self._interface.configMenu !== null) {
            self._interface.configMenu.volBar.on('seekIconDown', self._volSeekDown, self);
            self._interface.configMenu.volBar.on('seekIconUp', self._volSeekUp, self);
            self._interface.configMenu.volBar.on('seekPositionUpdate', self._volSeekPositionUpdate, self);
        }

        this.emit('moduleComplete', {
            screens: this._screens
        });
        return;
    }

    this.once('screenCreated', function(e) {
        this._screens[this._module[this._moduleCount]] = e.screen;
        this._moduleCount++;
        this.loadModule();
    }, this);
    this._loadScreen(this._module[this._moduleCount]);
};

/**
 * Responsável por carregar e criar a tela
 * @param name {String} nome da tela
 * @private
 **/
ScreenManager.prototype._loadScreen = function(name) {
    var self = this;
    //seleciona a tela pelo nome
    var t = new ScreenManager.LIBRARY[name]();
    var data;



    //carrega do json de configuração
    $.getJSON(Const.APPConfig.pastaTelas + t.config + Const.APPConfig.configTela, function(_data) {
        var lo = new PIXI.loaders.Loader();
        data = _data;
        var template = data.template.type;

        if (data.template.config.calculator === true) {
            lo.add('calculator', 'telas/commom/ui/calculator/calculator.json');
        }

        if (template == 'video') {
            lo.add('videoPlayer', 'telas/commom/ui/videoplayer/videoPlayer.json');
        }

        if (template == 'paint') {
            lo.add('paint_ui', 'telas/commom/ui/paint/paint.json');
            lo.add(Const.APPConfig.pastaTelas + t.config + Const.APPConfig.sprites, Const.APPConfig.pastaTelas + t.config + Const.APPConfig.sprites);
        }

        if (template == 'keyboard' || template == 'word' || template == 'keyboardExercise' || template == 'crossWord' || template == 'forca' || template == 'lineNumerical') {
            if (template == 'word') {
                lo.add('backgroundWord', 'telas/back/word.png');
            }
            if (Const.APPConfig.LANGUAGE !== 'fr-FR') {
                lo.add('keyboard', 'telas/commom/ui/keyboard/keyboard.json');
            } else {
                lo.add('keyboard', 'telas/commom/ui/keyboard/frFR/keyboard.json');
            }

        }
        if (template != 'video' && template != 'paint') {
            var url = Const.APPConfig.pastaTelas + t.config + Const.APPConfig.sprites;
            lo.add(url, url);
        }

        //verifica se ja tem o background carregado, se não tiver inclui na lista do loader
        if (self._backs[data.backgroundIndex] === undefined && data.backgroundIndex !== undefined) {
            lo.add('background', 'telas/back/' + Const.APPConfig.backgrounds[data.backgroundIndex]);
        }

        if (data.template.config.controller !== undefined) {

            if (data.template.config.controller.right !== undefined) {

                if (data.template.config.controller.right.type == 'analog') {
                    lo.add('rightAnalogController', 'telas/commom/ui/controller/rightAnalog.json');
                }

                if (data.template.config.controller.right.type == 'digital') {
                    lo.add('rightDigitalController', 'telas/commom/ui/controller/rightDigital.json');
                }
            }

            if (data.template.config.controller.left !== undefined) {

                if (data.template.config.controller.left.type == 'analog') {
                    lo.add('leftAnalogController', 'telas/commom/ui/controller/leftAnalog.json');
                }

                if (data.template.config.controller.left.type == 'digital') {
                    lo.add('leftDigitalController', 'telas/commom/ui/controller/leftDigital.json');
                }
            }

        }
        // lo.add('controller', 'telas/commom/ui/controller/controller.json');

		if(data.template.config.animacoes !== undefined && data.template.config.animacoes !== null) {
			data.template.config.animacoes.forEach(function(animacao) {
				var url = Const.APPConfig.pastaTelas + t.config + animacao.path + animacao.animationDataFile;
				lo.add(url,url);
				url = Const.APPConfig.pastaTelas + t.config + animacao.path + animacao.textureDataFile;
				lo.add(url,url);
				url = Const.APPConfig.pastaTelas + t.config + animacao.path + animacao.textureImgFile;
				lo.add(url,url);
			});
		}


        //carrega os sprites da tela
        lo.load(function(loader, resource) {
            var result = [];

            if (self._backs[data.backgroundIndex] === undefined && data.backgroundIndex !== undefined) {
                self._backs[data.backgroundIndex] = resource.background.texture;
            }

            //carrega o sprite da tela
            if (template != 'video' && template != 'paint' && template) {
                self._cropSupport(resource, result, Const.APPConfig.sprites, Const.APPConfig.pastaTelas + t.config);
            }

            if (resource.calculator !== undefined && self._interface._calculator === null) {

                var calculatorTexture = [];
                self._cropSupport(resource, calculatorTexture, ['calculator']);
                self._interface.calculator = ObjectFactory.generateCalculator(calculatorTexture);
            }

            if (resource.rightAnalogController !== undefined && self._interface.rightAnalogController === null) {

                var rightAnalogControllerTexture = [];
                self._cropSupport(resource, rightAnalogControllerTexture, ['rightAnalogController']);
                self._interface.rightAnalogController = ObjectFactory.generateAnalogController(rightAnalogControllerTexture);

            }

            if (resource.leftAnalogController !== undefined && self._interface.leftAnalogController === null) {

                var leftAnalogControllerTexture = [];
                self._cropSupport(resource, leftAnalogControllerTexture, ['leftAnalogController']);
                self._interface.leftAnalogController = ObjectFactory.generateAnalogController(leftAnalogControllerTexture);

            }

            if (resource.leftDigitalController !== undefined && self._interface.leftDigitalController === null) {

                var leftDigitalControllerTexture = [];
                self._cropSupport(resource, leftDigitalControllerTexture, ['leftDigitalController']);
                self._interface.leftDigitalController = ObjectFactory.generateDigitalController(leftDigitalControllerTexture);

            }

            if (resource.rightDigitalController !== undefined && self._interface.rightDigitalController === null) {

                var rightDigitalControllerTexture = [];
                self._cropSupport(resource, rightDigitalControllerTexture, ['rightDigitalController']);
                self._interface.rightDigitalController = ObjectFactory.generateDigitalController(rightDigitalControllerTexture);

            }

            if (template == 'video' && self._interface.videoPlayer === null) {
                var videoPlayerTexture = [];
                self._cropSupport(resource, videoPlayerTexture, ['videoPlayer']);
                self._interface.videoPlayer = ObjectFactory.generateVideoPlayer(videoPlayerTexture);
            }

            if (template == 'paint' && self._interface._paint === null) {
                var paintTexture = [];
                self._cropSupport(resource, paintTexture, ['paint_ui']);
                self._interface._paint = ObjectFactory.generatePaint(paintTexture);
                self._cropSupport(resource, result, Const.APPConfig.sprites, Const.APPConfig.pastaTelas + t.config);
            }

            if (template == 'ballon' && self._interface._ballon === null) {
                self._interface.ballon = ObjectFactory.generateBallon();
            }

            if ((template == 'keyboard' || template == 'word' || template == 'keyboardExercise' || template == 'crossWord' || template == 'forca' || template == 'lineNumerical') && self._interface._keyboard === null) {
                var keyboardTexture = [];

                self._cropSupport(resource, keyboardTexture, ['keyboard']);
                self._interface._keyboard = ObjectFactory.generateKeyboard(keyboardTexture);
            }

			if(data.template.config.animacoes !== undefined && data.template.config.animacoes !== null) {
				var animacoes = [];
				data.template.config.animacoes.forEach(function(animacaoCfg) {
					var obj = {
						animationData: resource[Const.APPConfig.pastaTelas + t.config + animacaoCfg.path + animacaoCfg.animationDataFile],
						textureData: resource[Const.APPConfig.pastaTelas + t.config + animacaoCfg.path + animacaoCfg.textureDataFile],
						textureImg: resource[Const.APPConfig.pastaTelas + t.config + animacaoCfg.path + animacaoCfg.textureImgFile],
						config: animacaoCfg
					};
					animacoes.push(obj);
				});

				result.push({'animacoes': animacoes});
			}

            var tela = TemplateFactory.getTemplate(template);
            var layer = ObjectFactory.generateObjects(result, data.template.config, data.path);

            if (template != 'word') {
                layer.layersDisplay.getLayer('background').addChild(new PIXI.Sprite(self._backs[data.backgroundIndex]));
            } else {
                if (data.backgroundIndex !== undefined) {
                    layer.layersDisplay.getLayer('background').addChild(new PIXI.Sprite(self._backs[data.backgroundIndex]));
                } else {
                    layer.layersDisplay.getLayer('background').addChild(new PIXI.Sprite(resource.backgroundWord.texture));
                }
            }

            //adiciona os audios na tela
            if (data.template.config.audio !== undefined) {
                self._ballonConfig = [];
                var path = Const.APPConfig.pastaTelas + t.config + "/";

                var audioIdioma = data.template.config.audio[Const.APPConfig.LANGUAGE];

                for (var audio in audioIdioma) {

                    audio = audioIdioma[audio];


                    if (audio !== undefined) {
                        if (tela.soundManager === undefined || tela.soundManager === null) tela.setSoundManager(new SoundManager());
                        //if (audio.name.length == 1){
                        if (Const.APPConfig.debug) {
                            tela.soundManager.addSingleSound("../curso/telas/commom/debug.mp3", {
                                volume: self._soundVol
                            });
                        } else {
                            tela.soundManager.addSingleSound(path + Const.APPConfig.LANGUAGE.replace('-', '') + "/" + audio.name + ".mp3", {
                                volume: self._soundVol
                            });
                        }

                        // }else {
                        //     if (audio.name.length > 1) {
                        //         var urls = [];
                        //
                        //         for (var k = 0; k < audio.name.length; k++) {
                        //             urls.push(path + Const.APPConfig.LANGUAGE.replace('-', '') + "/" + audio.name[i]);
                        //         }
                        //         tela.soundManager.addMultipleSound(urls);
                        //     }
                        // }
                    }
                }
            }

            tela.setLayers(layer);
            tela.setConfig(data.template.config);
            tela.addPlugin(t);

            tela.init();

            self.emit('screenCreated', {
                screen: tela
            });
        });
    });
};

/**
 * Método manipulador de evento, é executado quando a telainicial termina de ser carregada
 * @private
 */
ScreenManager.prototype._addStart = function(e) {
    /**
     * @fires startScreen
     * @type {object}
     * @property {module:templates.core.ITemplate} startScreen - envia a Tela montada
     * @see {module:templates.core.ITemplate}
     */
    this.emit('startScreen', {
        screen: e.screen
    });
};


/**
 * Armazena as telas do curso
 * @type {Object}
 */
ScreenManager.LIBRARY = {};

/**
 * Registra uma tela
 * @param name {String} nome que identifica a telas
 * @param screen {class} classe Tela
 */
ScreenManager.registerScreen = function(name, screen) {
    if (ScreenManager.LIBRARY[name] === undefined) {
        ScreenManager.LIBRARY[name] = screen;
    }
};

ScreenManager.prototype._cropSupport = function(resource, result, sprites, path) {
    var obj = {};

    var _name = '';

    if (path !== undefined && path !== null) _name = path + sprites;
    else _name = sprites;

    for (var j in resource[_name].textures) {
        //monta as configurações dos objetos
        obj[j] = {
            config: {
                x: resource[_name].data.frames[j].spriteSourceSize.x,
                y: resource[_name].data.frames[j].spriteSourceSize.y,
                w: resource[_name].data.frames[j].frame.w,
                h: resource[_name].data.frames[j].frame.h
            },
            texture: resource[_name].textures[j]
        };
    }

    result.push(obj);
};

ScreenManager.prototype._brightSeekDown = function() {};

ScreenManager.prototype._brightSeekUp = function() {
    this.emit("brightUpdate", {
        bright: this._bright
    });
};

ScreenManager.prototype._brightSeekPositionUpdate = function(e) {
    this._bright = e.pct >= 0.1 ? Math.round(e.pct * 100) : 10;
    this._interface.brightSim.alpha = (1 - (this._bright / 100));
};

ScreenManager.prototype._volSeekDown = function() {};

ScreenManager.prototype._volSeekUp = function() {
    this.emit("volumeUpdate", {
        volume: this._soundVol
    });
};

ScreenManager.prototype._volSeekPositionUpdate = function(e) {
    this._soundVol = Math.round(e.pct * 100);

    var sounds = [];
    for (var s in this._screens) {
        if (this._screens[s].soundManager !== undefined && this._screens[s].soundManager !== null) {
            sounds = sounds.concat(this._screens[s].soundManager._sounds);
        }
    }

    sounds.forEach(function(sound) {
        sound.volume(this._soundVol/100);
    }, this);
};
