var PIXI = require('PIXI');
var EventEmitter = require('EventEmitter');
var TweenMax = require('TweenMax');
var Const = require('../core/Const');
var Interface = require('../controller/InterfaceHandler');
var behavior = require('../behavior/index');
var lms = require();


/**
 * @classdesc Classe para navegação das telas
 * @memberof module:controller
 * @extends PIXI.Container
 * @exports Navigation
 * @constructor
 */
function Navigation(pixi) {

    EventEmitter.call(this);

    this.indexEx = -1;

    /**
     * Referencia a classe PIXIConfig deste curso
     * @type {module:core.PIXIConfig}
     * @public
     */
    this._pixi = pixi;

    this._points = 0;

    this._previousScreen = null;
    this._currentScreen = null;
    this._nextScreen = null;

    this._telas = null;

    this._atualIndex = 0;
    this._highestIndex = 0;

    this._lastScreenVisited = 0;

    this._parallaxTimer = null;

    this._interface = null;

    this._nextMask = new PIXI.Graphics();

    this._nextMask.beginFill(0xff0000, 0.5);
    this._nextMask.drawRect(0, 0, Const.BASE_WIDTH, Const.BASE_HEIGHT);
    this._nextMask.endFill();

    this._currentMask = new PIXI.Graphics();

    this._currentMask.beginFill(0xff0000, 0.5);
    this._currentMask.drawRect(0, 0, Const.BASE_WIDTH, Const.BASE_HEIGHT);
    this._currentMask.endFill();

    this._previousMask = new PIXI.Graphics();

    this._previousMask.beginFill(0xff0000, 0.5);
    this._previousMask.drawRect(0, 0, Const.BASE_WIDTH, Const.BASE_HEIGHT);
    this._previousMask.endFill();

}

Navigation.prototype = Object.create(EventEmitter.prototype);
Navigation.prototype.constructor = Navigation;
module.exports = Navigation;



Object.defineProperties(Navigation.prototype, {

    /**
     * (GET) Retorna a porcentagem do curso ja realizada pelo aluno
     * @memberof module:Navigation
     * @type {number}
     * @public
     */
    percent: {
        get: function() {
            //var pct = this._highestIndex / this.this._telas_order.length * 100; this line count the pct based on the screen Number

            var pct = this._points / Const.APPConfig.pontuacaoMaxima;

            return pct;
        }
    },
    /**
     * (GET) Retorna em qual Tela o aluno esta
     * @memberof module:Navigation
     * @type {number}
     * @public
     */
    paginaAtual: {
        get: function() {
            return this._atualIndex;
        }
    },

    points: {
        get: function() {
            return this._points;
        },

        set: function(value) {
            this._points = value;
        }
    }
});

/**
 * Executa tela inicial, possui o método getInstance, controla os clicked para avançar e voltar, ordena os index das telas.
 * @param telaInicial {Object} Referencia a tela inicial do Index
 * @param telas {Object} Referencia as telas do Index
 * @param order {Object} Ordena os Index
 * @param lastIndex {Object} Busca no Index a ultima tela
 */
Navigation.prototype.start = function(telaInicial, telas, order, lastIndex) {
    var self = this;

    this._interface = Interface.getInstance();

    this._telas = telas;
    this._telas_order = order;

    self._atualIndex = lastIndex;
    self._highestIndex = lastIndex;

    telaInicial.once("avancar", function() {
		self._interface.updatePoints(self.points);

        self._interface.navegacaoSeguinte.on("clicked", function() {
            self._currentScreen.emit("avancarTela");
        });

        self._interface.navegacaoAnterior.on("clicked", function() {
            self._currentScreen.emit("voltarTela");
        });

		if(self._interface.navegacaoButtons) {
			for(var navBtn in self._interface.navegacaoButtons) {
				if(navBtn === 'navBackground') continue;

				var btnObject = self._interface.navegacaoButtons[navBtn];
				if(btnObject.ativo !== undefined && btnObject.ativo !== null && btnObject.ativo.hasClick === true) {
					btnObject.ativo.on("clicked", function(e) {
						self._currentScreen.emit("navegarTela", {navIndex: e.target.navIndex});
					}, this);
				}
			}
		}

        //TODO
        //colocar letra minuscula nesse metodo
        // if (telas[order[self._atualIndex]].soundManager !== undefined && telas[order[self._atualIndex]].soundManager !== null) {
        //     telas[order[self._atualIndex]].soundManager.AutorizeSounds();
        // }

        if (telas[order[self._atualIndex]]._videoManager !== undefined && telas[order[self._atualIndex]]._videoManager !== null) {
            telas[order[self._atualIndex]]._videoManager.autorizeAll();
        }

        self.forcedTransition(telaInicial, telas[order[self._atualIndex]], Const.APPConfig.startTransition);
    });

    this._pixi.on('resize', function(e) {
        if (self._currentScreen !== null) {
            self._currentScreen.resize.apply(self._currentScreen, [e.factorScale, e.canvasPos]);
        }
    });

    telaInicial.loaded();
};


Navigation.prototype.forcedTransition = function(telaAtual, nextScreen, direction) {
    var self = this;

    if (Const.APPConfig.transitionMethod === undefined || Const.APPConfig.transitionMethod === "slide") {
        var positionX = (direction == 'right') ? '-=' : '+=';
        nextScreen.x = (direction == 'right') ? Const.BASE_WIDTH : -Const.BASE_WIDTH;

        nextScreen.mask = this._nextMask;
        this._nextMask.x = nextScreen.x;

        telaAtual.mask = this._currentMask;
        this._currentMask.x = 0;

        this._pixi.addChildAt(nextScreen, 0);

        nextScreen.resize(self._pixi._factorScale, self._pixi._canvasPos);
        nextScreen.start();

        TweenMax.to([telaAtual, nextScreen, this._nextMask, this._currentMask], Const.APPConfig.transitionAnimationDuration, {
            x: positionX + Const.BASE_WIDTH,
            ease: Quad.easeIn,
            onComplete: function() {
                telaAtual.mask = null;
                nextScreen.mask = null;
                self.finalizeTransition(telaAtual, nextScreen);
            }
        });
    } else if(Const.APPConfig.transitionMethod === "blink"){
        nextScreen.x = telaAtual.x;

        this._pixi.addChildAt(nextScreen, 0);

        TweenMax.to([telaAtual], Const.APPConfig.transitionAnimationDuration, {
            alpha:0,
            ease: Quad.easeIn,
            onComplete: function() {
                self.finalizeTransition(telaAtual, nextScreen);
            }
        });
    }

};

Navigation.prototype.finalizeTransition = function(telaAtual, nextScreen) {
    var self = this;

    telaAtual.reset();

    self._pixi._stage.removeChild(telaAtual);
    telaAtual.alpha = 1;


    self._currentScreen = nextScreen;


    self.initializeScreen();
};

Navigation.prototype.initializeScreen = function() {
    var self = this;
	var showNavBar = false;

    if (this._highestIndex < this._atualIndex) {
        this._highestIndex = this._atualIndex;
        this.emit("update", {lastIndex: this._highestIndex});
    }


    if (self._atualIndex >= self.indexEx && self.indexEx != -1) {
        self._interface.showPoints(true);

        self._currentScreen.isAssessment = true;

        self._currentScreen.once("exercicioIncorreto", function(obj) {
            this.close();
            self._interface.hideButtons();
            self.prepareTransition(this, true, false);

            setTimeout(function() {
                self._atualIndex++;
                self.forcedTransition(self._currentScreen, self._telas[self._telas_order[self._atualIndex]], 'right');
            }, Const.APPConfig.tempoExercicioFinal * 1000);
        });
    }

	for(var navBtn in self._interface.navegacaoButtons) {
		if(navBtn === 'navBackground') continue;

		var btnObject = self._interface.navegacaoButtons[navBtn];
		if(btnObject.padrao.navIndex === self._atualIndex) {
			showNavBar = true;

			if(btnObject.ativo !== undefined && btnObject.ativo !== null && btnObject.ativo.hasClick === true) {
				btnObject.ativo.removeEvents();
			}

			btnObject.padrao.emit('btnAtual',{stateId: 'btnAtual'});
		}
	}

	self._currentScreen.once("navegarTela", function(obj) {
		this.close();
        self._interface.hideButtons();

		if (self._atualIndex >= self.indexEx && self.indexEx != -1) self.prepareTransition(this, true, true);
        else self.prepareTransition(this, false);

		var transitionDirection = obj.navIndex <= self._atualIndex ? 'left' : 'right';
		setTimeout(function() {
            self._atualIndex = obj.navIndex;
            self.forcedTransition(self._currentScreen, self._telas[self._telas_order[self._atualIndex]], transitionDirection);
        }, Const.APPConfig.tempoExercicioFinal * 1000);
	});

    self._currentScreen.once("avancarTela", function(obj) {
        this.close();
        self._interface.hideButtons();
        if (self._atualIndex >= self.indexEx && self.indexEx != -1) self.prepareTransition(this, true, true);
        else self.prepareTransition(this, false);




        setTimeout(function() {
            self._atualIndex++;
            self.forcedTransition(self._currentScreen, self._telas[self._telas_order[self._atualIndex]], 'right');
        }, Const.APPConfig.tempoExercicioFinal * 1000);


    });

    self._currentScreen.once("voltarTela", function() {
        this.close();
        self._interface.hideButtons();
        self.prepareTransition(this, false);

        self._atualIndex--;
        self.forcedTransition(this, self._telas[self._telas_order[self._atualIndex]], 'left');
    });

    self._currentScreen.once("parallax", function() {

        this.close();
        self._interface.hideButtons();

        if (self._atualIndex >= self.indexEx && self.indexEx != -1) self.prepareTransition(this, true, true);
        else self.prepareTransition(this, false);

        if (self._atualIndex >= self.indexEx && self.indexEx != -1) {
            if (Const.APPConfig.particleOnExercise) {
                self._interface.starStorm();
            }
            setTimeout(function() {
                self._atualIndex++;
                self.forcedTransition(self._currentScreen, self._telas[self._telas_order[self._atualIndex]], 'right');
            }, Const.APPConfig.tempoExercicioFinal * 1000);
        } else {
            if (self._currentScreen.isExercise) {
                if (Const.APPConfig.particleOnExercise) {
                    self._interface.starStorm();
                }
            }
            self._interface.teachAdvance();
            self.startParallax();
        }
    });

    self._currentScreen.once("allowNext", function() {

        self._interface._navegacaoSeguinte.visible = true;
		if(self._interface.navegacaoButtons && showNavBar) {
			self._interface.showNavButtons();
		}
    });

    //stop all particles
    window.particles = [];

    self._interface.showButtons = true;

    if (self._atualIndex >= self.indexEx && self.indexEx != -1) {
        self._interface.showButtons = false;
        self._interface.showPoints(true);
    }

	if(self._currentScreen._titulo !== 0) {
		self._interface.titulo = self._currentScreen._titulo;
	}

    self.checkNavigationButtons();
    self._currentScreen.open();
};
/**
 * Emite o evento que dispara a nota do aluno para o Interface
 *
 * @method module:controller.Navigation#calculatePoints
 * @memberof module:controller.Navigation
 * @public
 */
Navigation.prototype._dispatchScore = function() {
    this.emit('pontuacao');
};
/**
 * Executa método Parallax
 *
 * @method module:controller.Navigation#startParallax
 * @memberof module:controller.Navigation
 * @public
 */
Navigation.prototype.startParallax = function() {
    var self = this;


    self._currentScreen._lockY = true;

    if (self._atualIndex === 0) {
        self._currentScreen._maxX = 0;
    }

    if (self._atualIndex === self._telas_order.length - 1) {
        self._currentScreen._minX = 0;
    }

    //não é primeira tela  adiciona tela a esquerda
    if (self._atualIndex !== 0) {
        self._previousScreen = self._telas[self._telas_order[self._atualIndex - 1]];
        self._previousScreen.x = -Const.BASE_WIDTH;
        self._previousScreen.resize(self._pixi._factorScale, self._pixi._canvasPos);
        self._previousScreen.start();

        self._previousScreen.mask = this._previousMask;
        this._previousMask.x = self._previousScreen.x;

        self._pixi.addChildAt(self._previousScreen, 0);
    } else {
        self._previousScreen = null;
    }

    //não é ultima tela  adiciona tela a direita
    if (self._atualIndex !== self._telas_order.length - 1) {
        self._nextScreen = self._telas[self._telas_order[self._atualIndex + 1]];
        self._nextScreen.x = Const.BASE_WIDTH;
        self._nextScreen.resize(self._pixi._factorScale, self._pixi._canvasPos);
        self._nextScreen.start();

        self._nextScreen.mask = this._nextMask;
        this._nextMask.x = self._nextScreen.x;

        self._pixi.addChildAt(self._nextScreen, 0);
    } else {
        self._nextScreen = null;
    }


    self._currentScreen.mask = this._currentMask;
    this._currentMask.x = self._currentScreen.x;

    self._currentScreen.on("positionUpdated", function(e) {
        clearTimeout(self._parallaxTimer);
        this.x = this.x; //usa o setter do x para continuar corrindo posicao dos elementos
        this.mask.x = this.x;
        try {
            self._previousScreen.x = -Const.BASE_WIDTH + this.x;
            self._previousScreen.mask.x = -Const.BASE_WIDTH + this.x;
        } catch (g) {}
        try {
            self._nextScreen.x = Const.BASE_WIDTH + this.x;
            self._nextScreen.mask.x = Const.BASE_WIDTH + this.x;
        } catch (g) {}

    });

    self._currentScreen.on("dragStart", function() {
        //hides the hand
        self._interface.hideHand();
    });

    self._currentScreen.on("dragReleased", function() {

        clearTimeout(self._parallaxTimer);

        if (this.x <= -Const.BASE_WIDTH + 100) {
            self.parallaxAdvance();
            return;
        }
        if (this.x >= +Const.BASE_WIDTH - 100) {
            self.parallaxRetrocede();
            return;
        }
        if (this.x > -100 && this.x < 100) {
            self.screenCentralize();
            return;
        }

        self._parallaxTimer = setTimeout(function() {

            if (self._currentScreen.x < 0) {
                self.parallaxAdvance();
            }
            if (self._currentScreen.x > 0) {
                self.parallaxRetrocede();
            }

        }, 2000);


    });

    self._currentScreen.addParallaxEvents();

};
/**
 * Centraliza tela
 *
 * @method module:controller.Navigation#screenCentralize
 * @memberof module:controller.Navigation
 * @public
 */
Navigation.prototype.screenCentralize = function() {
    var self = this;

    TweenMax.to([self._currentScreen, self._currentScreen.mask], 0.2, {
        x: 0,
        ease: Quad.easeIn,
        onUpdate: function() {
            if (self._atualIndex !== 0) {
                self._previousScreen.mask.x = -Const.BASE_WIDTH + self._currentScreen.x;
                self._previousScreen.x = -Const.BASE_WIDTH + self._currentScreen.x;
            }
            if (self._atualIndex !== self._telas_order.length - 1) {
                self._nextScreen.mask.x = Const.BASE_WIDTH + self._currentScreen.x;
                self._nextScreen.x = Const.BASE_WIDTH + self._currentScreen.x;
            }
        }
    });

};
/**
 * Avança tela parallax
 *
 * @method module:controller.Navigation#parallaxAdvance
 * @memberof module:controller.Navigation
 * @public
 */
Navigation.prototype.parallaxAdvance = function() {
    var self = this;

    self._currentScreen.removeParallaxEvents();
    self._currentScreen.removeListener("positionUpdated");
    self._currentScreen.removeListener("dragReleased");


    TweenMax.to([self._currentScreen, self._currentScreen.mask], 0.5, {
        x: -Const.BASE_WIDTH,
        ease: Quad.easeIn,
        onUpdate: function() {
            if (self._atualIndex !== 0) {
                self._previousScreen.x = -Const.BASE_WIDTH + self._currentScreen.x;
                self._previousScreen.mask.x = -Const.BASE_WIDTH + self._currentScreen.x;
            }
            if (self._atualIndex !== self._telas_order.length - 1) {
                self._nextScreen.x = Const.BASE_WIDTH + self._currentScreen.x;
                self._nextScreen.mask.x = Const.BASE_WIDTH + self._currentScreen.x;
            }
        },
        onComplete: function() {

            if (self._atualIndex !== 0) {
                self._pixi._stage.removeChild(self._previousScreen); // remove tela anterior
                self._previousScreen.mask = null;
            }

            if (self._nextScreen !== undefined) {
                self._nextScreen.mask = null;
            }

            self._pixi._stage.removeChild(self._currentScreen); // remove tela atual
            self._currentScreen.mask = null;

            self._currentScreen.reset();

            self._currentScreen = self._nextScreen;

            self._atualIndex++;

            self.initializeScreen();


        }
    });
};

/**
 * Volta para tela anterior, parallax
 *
 * @method module:controller.Navigation#parallaxAdvance
 * @memberof module:controller.Navigation
 * @public
 */
Navigation.prototype.parallaxRetrocede = function() {
    var self = this;

    self._currentScreen.removeParallaxEvents();
    self._currentScreen.removeListener("positionUpdated");
    self._currentScreen.removeListener("dragReleased");
    TweenMax.to([self._currentScreen, self._currentScreen.mask], 0.5, {
        x: +Const.BASE_WIDTH,
        ease: Quad.easeIn,
        onUpdate: function() {
            if (self._atualIndex !== 0) {
                self._previousScreen.mask.x = -Const.BASE_WIDTH + self._currentScreen.x;
                self._previousScreen.x = -Const.BASE_WIDTH + self._currentScreen.x;
            }
            if (self._atualIndex !== self._telas_order.length - 1) {
                self._nextScreen.mask.x = Const.BASE_WIDTH + self._currentScreen.x;
                self._nextScreen.x = Const.BASE_WIDTH + self._currentScreen.x;
            }
        },
        onComplete: function() {
            if (self._atualIndex !== self._telas_order.length - 1) {
                self._pixi._stage.removeChild(self._nextScreen); // remove tela posterior
                self._nextScreen.mask = null;
            }


            self._pixi._stage.removeChild(self._currentScreen); // remove tela atual
            self._currentScreen.mask = null;

            if (self._previousScreen !== undefined) {
                self._previousScreen.mask = null;
            }

            self._currentScreen.reset();

            self._currentScreen = self._previousScreen;

            self._atualIndex--;

            self.initializeScreen();


        }
    });


};
/**
 *
 *
 * @method module:controller.Navigation#calculatePoints
 * @memberof module:controller.Navigation
 * @param points {number} Numero representando pontos a serem adicionados
 * @public
 */
Navigation.prototype.calculatePoints = function(points) {

    this._points += points;

    this._dispatchScore();
    this._interface.updatePoints(points);
};

Navigation.prototype.prepareTransition = function(screen, countPoints, correct) {

    var self = this;

    screen.removeListener("parallax");
    screen.removeListener("voltarTela");
    screen.removeListener("avancarTela");
	screen.removeListener("navegarTela");
    screen.removeListener("exercicioIncorreto");

    self._interface.showPoints(false);

    if (countPoints) {
        if (correct) {
            if (Const.APPConfig.particleOnExercise) {
                self._interface.starStorm();
            }
            if (screen._config.points !== undefined) self.calculatePoints(screen._config.points);
            else self.calculatePoints(Const.APPConfig.defaultEvaluationPoints);
        } else {
            if (screen._config.pointsOnError) self.calculatePoints(screen._config.pointsOnError);
            else self.calculatePoints(Const.APPConfig.defaultEvaluationPointsOnError);
        }
    }

    if (self._interface.ajudaBotao !== null) self._interface.ajudaBotao.visible = false;
	if (self._interface.titulo !== null) self._interface.titulo = null;

	for(var navBtn in self._interface.navegacaoButtons) {
		if(navBtn === 'navBackground') continue;

		var btnObject = self._interface.navegacaoButtons[navBtn];

		if(btnObject.padrao.navIndex === self._atualIndex) {
			if(btnObject.ativo !== undefined && btnObject.ativo !== null && !btnObject.ativo.interactive && btnObject.ativo.hasClick) {
				btnObject.ativo.addEvents();
			}
			btnObject.padrao.emit('btnAtivo',{stateId: 'btnAtivo'});
		}
	}
};


Navigation.prototype.checkNavigationButtons = function() {
    var self = this;

    if (self._interface.showButtons === false) {
        self._interface.navegacaoAnterior.visible = false;
        self._interface.navegacaoSeguinte.visible = false;
        self._interface.ajudaBotao.visible = true;
        self._interface.configBotao.visible = true;
        return;
    }

    if (this._highestIndex > this._atualIndex) {
        self._interface.navegacaoSeguinte.visible = self._atualIndex != self._telas_order.length - 1;
		self._interface.showNavButtons();
    }
    self._interface.navegacaoAnterior.visible = self._atualIndex !== 0;

    if (self._interface.ajudaBotao !== null) {
        self._interface.ajudaBotao.visible = true;
    }

    if (self._interface.configBotao !== null) {
        self._interface.configBotao.visible = true;
    }

	if (self._interface.navegacaoAnteriorInativo !== null) {
        self._interface.navegacaoAnteriorInativo.visible = true;
    }

	if (self._interface._navegacaoSeguinteInativo !== null) {
        self._interface._navegacaoSeguinteInativo.visible = true;
    }

};
