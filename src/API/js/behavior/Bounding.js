var PIXI = require('PIXI');
var Dragable = require('./Dragable');
var $ = require('jquery');

function Bounding() {
    this._sizeCorner = 20;

    this._leftGroup = [];
    this._midleGroupH = [];
    this._rightGroup = [];

    this._topGroup = [];
    this._bottomGroup = [];
    this._midleGroupV = [];
}

Bounding.prototype.constructor = Bounding;
module.exports = Bounding;

Bounding.prototype.createBounds = function() {
    this._boundsW = this.width;
    this._boundsH = this.height;

    this._checkPoint();

    this._line = new PIXI.Graphics();
    this._line.lineStyle(1, 0);
    this._line.drawRect(0, 0, this._boundsW, this._boundsH);
    this._line.endFill();
    this._line.x = this.x + this._checkPointX;
    this._line.y = this.y + this._checkPointY;
    this.parent.addChild(this._line);

    this._tl = this._createRect();
    this._cl = this._createRect();
    this._bl = this._createRect();
    this._leftGroup.push(this._tl, this._cl, this._bl);

    this._tm = this._createRect();
    this._cm = this._createRect();
    this._bm = this._createRect();
    this._midleGroupV.push(this._tm, this._cm, this._bm);

    this._tr = this._createRect();
    this._cr = this._createRect();
    this._br = this._createRect();
    this._rightGroup.push(this._tr, this._cr, this._br);

    this._topGroup.push(this._tl, this._tm, this._tr);
    this._midleGroupH.push(this._cl, this._cm, this._cr);
    this._bottomGroup.push(this._bl, this._bm, this._br);

    console.log(this._topGroup);

    this._verticalAlign();
    this._horizontalAlign();

    this.pivot.set(this._checkPointX, this._checkPointY);
    this._moveLine();
};

Bounding.prototype.addBoundsEvents = function() {
    this._cl.addEvents();
    this._cl._lockY = true;
    this._cl.on('positionUpdated', function(e) {
        this._updateLeft(e, this._leftGroup);
    }, this);

    this._cr.addEvents();
    this._cr._lockY = true;
    this._cr.on('positionUpdated', function(e) {
        this._updateLeft(e, this._rightGroup);
    }, this);

    this._tm.addEvents();
    this._tm._lockX = true;
    this._tm.on('positionUpdated', function(e){
        this._updateTop(e, this._topGroup);
    }, this);

    this._bm.addEvents();
    this._bm._lockX = true;
    this._bm.on('positionUpdated', function(e){
        this._updateTop(e, this._bottomGroup);
    }, this);

    this._tl.addEvents();
    this._tl.on('positionUpdated', function(e){
        this._updateLeft(e, this._leftGroup);
        this._updateTop(e, this._topGroup);
    }, this);

    this._bl.addEvents();
    this._bl.on('positionUpdated', function(e){
        this._updateLeft(e, this._leftGroup);
        this._updateTop(e, this._bottomGroup);
    }, this);

    this._tr.addEvents();
    this._tr.on('positionUpdated', function(e){
        this._updateLeft(e, this._rightGroup);
        this._updateTop(e, this._topGroup);
    }, this);

    this._br.addEvents();
    this._br.on('positionUpdated', function(e){
        this._updateLeft(e, this._rightGroup);
        this._updateTop(e, this._bottomGroup);
    }, this);

    this._cm.addEvents();
    this._cm.on('positionUpdated', this._updateMove, this);
};

Bounding.prototype.removeBoundsEvents = function(){
    this._cl.removeEvents();
    this._cl.removeListener('positionUpdated');

    this._cr.removeEvents();
    this._cr.on('positionUpdated');

    this._tm.removeEvents();
    this._tm.removeListener('positionUpdated');

    this._bm.removeEvents();
    this._bm.removeListener('positionUpdated');

    this._tl.removeEvents();
    this._tl.removeListener('positionUpdated');

    this._bl.removeEvents();
    this._bl.removeListener('positionUpdated');

    this._tr.removeEvents();
    this._tr.removeListener('positionUpdated');

    this._br.removeEvents();
    this._br.removeListener('positionUpdated');

    this._cm.removeEvents();
    this._cm.removeListener('positionUpdated');
};

Bounding.prototype._createRect = function() {
    var r = new PIXI.Graphics();
    r.beginFill(0xffffff);
    r.lineStyle(1, 0);
    r.drawRect(0, 0, this._sizeCorner, this._sizeCorner);
    r.endFill();
    this.parent.addChild(r);

    $.extend(r, new Dragable());
    return r;
};

Bounding.prototype._checkPoint = function(){
    var p = 10000;
    if(this._checkPointY === undefined || this._checkPointY === null){
        for(var i = 0; i < this.children.length; i++){
            if(this.children[i].y < p) p = this.children[i].y;
        }

        this._checkPointY = p;
    }

    p = 10000;
    if(this._checkPointX === undefined || this._checkPointX === null){
        for(var j = 0; j < this.children.length; j++){
            if(this.children[j].x < p) p = this.children[j].x;
        }

        this._checkPointX = p;
    }
};

Bounding.prototype._verticalAlign = function(){
    for(var i = 0; i < 3; i++){
        var t = this._topGroup[i];
        var m = this._midleGroupH[i];
        var b = this._bottomGroup[i];

        t.y = this.y + this._checkPointY - t.height / 2;
        m.y = this.y + this.height / 2 + this._checkPointY - m.height / 2;
        b.y = this.y + this.height + this._checkPointY - b.height / 2;
    }
};

Bounding.prototype._horizontalAlign = function(){
    for(var i = 0; i < 3; i++){
        var l = this._leftGroup[i];
        var c = this._midleGroupV[i];
        var r = this._rightGroup[i];

        l.x = this.x + this._checkPointX - l.width / 2;
        c.x = this.x + this.width / 2 + this._checkPointX - c.width / 2;
        r.x = this.x + this.width + this._checkPointX - r.width / 2;
    }
};

Bounding.prototype._updateLeft = function(e, group){
    this._boundsW = this._cr.x - this._cl.x;
    this._updateLine();
    this._moveLine();
    for(var i = 0; i < 3; i++){
        if(group[i] != e.target){
            group[i].x = e.target.x;
        }

        this._midleGroupV[i].x = this._cl.x + this._boundsW / 2;
    }
};

Bounding.prototype._updateTop = function(e, group){
    this._boundsH = this._bm.y - this._tm.y;
    this._updateLine();
    this._moveLine();
    for(var i = 0; i < 3; i++){
        if(group[i] != e.target){
            group[i].y = e.target.y;
        }

        this._midleGroupH[i].y = this._tm.y + this._boundsH / 2;
    }
};

Bounding.prototype._updateMove = function(e){
    for(var i = 0; i < 3; i++){
        this._leftGroup[i].x = e.target.x - this._boundsW / 2;
        this._rightGroup[i].x = e.target.x + this._boundsW / 2;
        if(this._midleGroupV[i] != e.target){
            this._midleGroupV[i].x = e.target.x;
        }

        this._topGroup[i].y = e.target.y - this._boundsH / 2;
        this._bottomGroup[i].y = e.target.y + this._boundsH / 2;

        if(this._midleGroupH[i] != e.target){
            this._midleGroupH[i].y = e.target.y;
        }
    }

    this._moveLine();
};

Bounding.prototype._updateLine = function(){
    this._line.clear();
    this._line.lineStyle(1, 0);
    this._line.drawRect(0, 0, this._boundsW, this._boundsH);
    this._line.endFill();

    this.width = this._boundsW;
    this.height = this._boundsH;
};

Bounding.prototype._moveLine = function(){
    this._line.x = this._cl.x + this._cl.width / 2;
    this._line.y = this._tm.y + this._tm.height / 2;

    this.x = this._line.x;
    this.y = this._line.y;
};

Bounding.prototype.destroyBounds = function(){
    var parent = this.parent;

    this.removeBoundsEvents();

    var dirt = [
        this._line,
        this._tl,
        this._cl,
        this._bl,
        this._tm,
        this._cm,
        this._bm,
        this._tr,
        this._cr,
        this._br
    ];

    for(var i = 0; i < dirt.length; i++ ){
        var o = dirt[i];
        parent.removeChild(o);
        o.destroy();
        o = null;
    }

    dirt.splice(0, dirt.length);
    dirt = null;

    this._leftGroup.splice(0, this._leftGroup.length);
    this._midleGroupV.splice(0, this._midleGroupV.length);
    this._rightGroup.splice(0, this._rightGroup.length);
    this._topGroup.splice(0, this._topGroup.length);
    this._midleGroupH.splice(0, this._midleGroupH.length);
    this._bottomGroup.splice(0, this._bottomGroup.length);
};
