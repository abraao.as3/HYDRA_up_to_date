var PIXI = require('PIXI');
var PIXIConfig = require('../core/PIXIConfig');

/**
 * @classdesc Plugin que configura o arraste
 * @memberof module:behavior
 * @exports Dragable
 * @constructor
 */

function Dragable() {

    this._lockX = false;
    this._lockY = false;

    this._minX = -1;
    this._minY = -1;
    this._maxX = -1;
    this._maxY = -1;

    this._obstacles = [];
    this._colidi = false;
    this._reset = true;
    this._resetOnFail = true;
    this._isHover = false;
    this._block = false;

    this._receptors = [];
    this._usePoint = true;

    /**
     * Emitido quando o é pressionado
     * @event dragStart
     * @memberof module:behavior.Dragable
     */

    /**
     * Emitido quando o é solto
     * @event dragReleased
     * @memberof module:behavior.Dragable
     */

    /**
     * Emitido quando o drag é solto em cima de um receptor
     * @event correct
     * @memberof module:behavior.Dragable
     */

    /**
     * Emitido quando o é solto fora de um receptor
     * @event incorrect
     * @memberof module:behavior.Dragable
     */

    /**
     * Emitido quando o Drag é movido
     * @event positionUpdated
     * @property {Object} Object containing the xDif and yDif dif from the original position
     * @memberof module:behavior.Dragable
     */

    /**
     * Emitido quando o Drag colide com algum obstaculo
     * @event colision
     * @property {Object} Obstaculo que colidiu com o drag
     * @memberof module:behavior.Dragable
     */



}

Dragable.prototype.constructor = Dragable;
module.exports = Dragable;

/**
 * Metodo que adiciona evento nos elementos injetados
 */
Dragable.prototype.addEvents = function() {
    this.interactive = true;
    this.buttonMode = true;

    this.on('mousedown', this._onDown);
    this.on('touchstart', this._onDown);

    this.on('mouseup', this._onUp);
    this.on('touchend', this._onUp);

    this.on('mouseupoutside', this._onUp);
    this.on('touchendoutside', this._onUp);

    this.on('mousemove', this._onMove);
    this.on('touchmove', this._onMove);

};

/**
 * Metodo que remove evento nos elementos injetados
 */
Dragable.prototype.removeEvents = function() {
    this.interactive = false;
    this.buttonMode = false;

    this.removeListener('mousedown', this._onDown);
    this.removeListener('touchstart', this._onDown);

    this.removeListener('mouseup', this._onUp);
    this.removeListener('touchend', this._onUp);

    this.removeListener('mouseupoutside', this._onUp);
    this.removeListener('touchendoutside', this._onUp);

    this.removeListener('mousemove', this._onMove);
    this.removeListener('touchmove', this._onMove);
};

/**
 * Metodo que captura a posição de click do mouse para usar de referencia para calculos futuros
 * @param e {Object} Retorno do evento onDown do pixi
 * @fires dragStart Emitido quando o é pressionado
 * @private
 */
Dragable.prototype._onDown = function(e) {
    if (this._isOff) return;
    this._moveEnabled = true;

    this._colidi = false;

    this._data = e.data;
    this._startingPosition = this._data.getLocalPosition(this);

    this.emit("dragStart", {target: this});
};

/**
 * Metodo que ao soltar o item verifica se há colisão com algum deceptor para este elemento
 * @param e {Object} Retorno do evento onUp do pixi
 * @fires dragReleased Emitido quando o é solto
 * @fires correct Emitido quando o drag é solto em cima de um receptor
 * @fires incorrect Emitido quando o é solto fora de um receptor
 * @private
 */
Dragable.prototype._onUp = function(e) {
    this._moveEnabled = false;

    if (this._colidi) return;
    if (this._isOff) return;
    this.emit("dragReleased", {target: this});

    this._isMove = false;
    this._data = null;

    var t = this._checkReceptors();
    if (t.result) {
        var r = this._receptors[t.index];

        if (this._isAdjust)
            this._adjust(r);

        this.emit("correct", e);
    } else {

        if (this._reset)
            this.resetPosition();

        this.emit("incorrect", e);
        this.clearContainers(this);
    }

};

/**
 * Metodo que verifica se o drag está em cima de algum receptor e atribui este drag ao receptor
 * @private
 */
Dragable.prototype._checkReceptors = function() {
    var result = false;
    var index = -1;


    if (!this._usePoint)
        for (var i = 0; i < this._receptors.length; i++) {
            if (this.x - this.width / 2 > this._receptors[i].x &&
                this.x + this.width / 2 < this._receptors[i].x + this._receptors[i].width &&
                this.y - this.height / 2 > this._receptors[i].y &&
                this.y + this.height / 2 < this._receptors[i].y + this._receptors[i].height) {
                if (this._lockArea) {
                    if (this._receptors[i].lock !== undefined && this._receptors[i].lock !== null) continue;
                    else this._receptors[i].lock = true;
                }
                index = i;
                result = true;
                this.addDrag(this, this._receptors[i]);
                break;
            }
        }
    else
        for (var j = 0; j < this._receptors.length; j++) {
            if (this.x + this.width / 2 > this._receptors[j].x - this._receptors[j].width / 2 &&
                this.x - this.width / 2 < this._receptors[j].x + this._receptors[j].width / 2 &&
                this.y + this.height / 2 > this._receptors[j].y - this._receptors[j].height / 2 &&
                this.y - this.height / 2 < this._receptors[j].y + this._receptors[j].height / 2) {

                if (this._lockArea) {
                    if (this._receptors[j].lock !== undefined && this._receptors[j].lock !== null) continue;
                    else this._receptors[j].lock = true;
                }
                index = j;
                result = true;

                this.addDrag(this, this._receptors[j]);
                break;
            }
        }

    return {
        result: result,
        index: index
    };
};


/**
 * Adiciona o drag ao area drag, mas antes disso verifica se esse drag estava dentro de outro area drag e o remove.
 * Um drag pode estar apenas dentro de um receptor em um dado momento
 * @param drag {module:display/simple.Drag} Drag a ser adicionado
 * @param area {module:display/simple.AreaDrag} Area Drag destino
 */
Dragable.prototype.addDrag = function(drag, area) {

    this.clearContainers(drag);

    area.receiveDrag(drag);
};


/**
 * Remove drag de outros areas drag para n ter um drag em 2 areas
 * @param drag {module:display/simple.Drag} Drag a ser adicionado
 */
Dragable.prototype.clearContainers = function(drag) {
    //this._areas = area;

    for (var i = 0; i < this._receptors.length; i++) {
        for (var j = 0; j < this._receptors[i]._containerDrags.length; j++) {
            if (drag === this._receptors[i]._containerDrags[j]) {
                this._receptors[i].removeDrag(j);
                this._receptors[i]._align(j);
                break;
            }
        }
    }
};


/**
 * Metodo que move o item para a direção do cursor
 * @public
 * @fires positionUpdated Envia um sinal contendo a nova posição x e y do objeto
 *
 */
Dragable.prototype._onMove = function(e) {

var self = this;

    if (!this._moveEnabled) return;

    if (!this._isHover) {
        if (this._isOff) return;
        if (this._colidi) return;
    }

    var newPosition = this._data.getLocalPosition(this.parent);

    var lock = false;

    var xDif = 0;
    var yDif = 0;

    if (!this._lockX) {

        var controleX = newPosition.x - this._startingPosition.x;

        if (this._minX != -1) {
            if (controleX < this._minX)
                lock = true;
        }


        if (this._maxX != -1) {
            if (controleX > this._maxX)
                lock = true;
        }

        if (!lock) {
            xDif = newPosition.x - this._startingPosition.x;
            this.x = xDif;
        } else {
            if (controleX < this._minX) {
                this.x = this._minX;

            } else if (controleX > this._maxX) {
                this.x = this._maxX;
            }

        }


    }

    lock = false;

    if (!this._lockY) {

        var controleY = newPosition.y - this._startingPosition.y;

        if (this._minY != -1) {

            if (controleY < this._minY)
                lock = true;
        }

        if (this._maxY != -1) {
            if (controleY > this._maxY)
                lock = true;

        }

        if (!lock) {
            yDif = newPosition.y - this._startingPosition.y;
            this.y = yDif;
        } else {
            if (controleY < this._minY) {
                this.y = this._minY;

            } else if (controleY > this._maxY) {
                this.y = this._maxY;
            }
        }

    }

    if (this._obstacles.length !== 0) this.colidi = this._checkObstacles();

    this.emit("positionUpdated", {
        xDif: xDif,
        yDif: yDif,
        target: this
    });

};

/**
 * Metodo que verifica se há colisão entre um drag e um obstaculo
 * @fires colision Evento determina a colisao com algum objeto
 * @private
 */
Dragable.prototype._checkObstacles = function() {
    var self = this;
    for (var i = 0; i < this._obstacles.length; i++) {
        var o = this._obstacles[i];

            if (self._isHover) {
                if (this._obstacles[i].hasContent(
                    this._sensor.x,
                    this._sensor.y,
                    this._sensor.width,
                    this._sensor.height
                )) {

                 self.emit("colision", {
                     "drag": self,
                     "object": o
                 });
                 return true;
                }

            } else {
                if (this.checkColision(o)) {

                    self._isMove = false;
                    self.resetPosition();
                    self._colidi = true;

                 self.emit("colision", {
                     "drag": self,
                     "object": o
                 });
                 return true;
                }
            }
    }

    return false;
};

Dragable.prototype.checkColision = function(o) {
    return this.advancedColision(this, o, false, false);
};

Dragable.prototype.advancedColision = function(drag, obstacle, dragIsCentered, obstacleIsCentered) {

    var x = drag.x;
    var y = drag.y;
    var x2 = obstacle.x;
    var y2 = obstacle.y;

    var w = drag.width,
        h = drag.height,
        w2 = obstacle.width,
        h2 = obstacle.height;


    // deal with the drag being centred
    if (dragIsCentered) {
        // fast rounding, but positive only
        x -= (w / 2);
        y -= (h / 2);
    }

    // deal with the obstacle being centred
    if (obstacleIsCentered) {
        x2 -= (w2 / 2);
        y2 -= (h2 / 2);
    }
    // we need to avoid using floats, as were doing array lookups
    x = Math.round(x);
    y = Math.round(y);
    x2 = Math.round(x2);
    y2 = Math.round(y2);

    // find the top left and bottom right corners of overlapping area
    var xMin = Math.max(x, x2),
        yMin = Math.max(y, y2),
        xMax = Math.min(x + w, x2 + w2),
        yMax = Math.min(y + h, y2 + h2);

    // Sanity collision check, we ensure that the top-left corner is both
    // above and to the left of the bottom-right corner.
    if (xMin >= xMax || yMin >= yMax) {
        return false;
    }

    var xDiff = xMax - xMin,
        yDiff = yMax - yMin;


    var pixels = PIXIConfig.renderer.extract.pixels(drag),
        pixels2 = PIXIConfig.renderer.extract.pixels(obstacle);


    var pixelX = -1;
    var pixelY = -1;

    // if the area is really small,
    // then just perform a normal image collision check
    if (xDiff < 4 && yDiff < 4) {
        for (pixelX = xMin; pixelX < xMax; pixelX++) {
            for (pixelY = yMin; pixelY < yMax; pixelY++) {
                if (
                    (pixels[((pixelX - x) + (pixelY - y) * w) * 4 + 3] !== 0) &&
                    (pixels2[((pixelX - x2) + (pixelY - y2) * w2) * 4 + 3] !== 0)
                ) {
                    return true;
                }
            }
        }
    } else {
        /* What is this doing?
         * It is iterating over the overlapping area,
         * across the x then y the,
         * checking if the pixels are on top of this.
         *
         * What is special is that it increments by incX or incY,
         * allowing it to quickly jump across the image in large increments
         * rather then slowly going pixel by pixel.
         *
         * This makes it more likely to find a colliding pixel early.
         */

        // Work out the increments,
        // it's a third, but ensure we don't get a tiny
        // slither of an area for the last iteration (using fast ceil).
        var incX = xDiff / 4.0,
            incY = yDiff / 4.0;
        incX = (~~incX === incX) ? incX : (incX + 1 | 0);
        incY = (~~incY === incY) ? incY : (incY + 1 | 0);

        for (var offsetY = 0; offsetY < incY; offsetY++) {
            for (var offsetX = 0; offsetX < incX; offsetX++) {
                for (pixelY = yMin + offsetY; pixelY < yMax; pixelY += incY) {
                    for (pixelX = xMin + offsetX; pixelX < xMax; pixelX += incX) {
                        if (
                            (pixels[((pixelX - x) + (pixelY - y) * w) * 4 + 3] !== 0) &&
                            (pixels2[((pixelX - x2) + (pixelY - y2) * w2) * 4 + 3] !== 0)
                        ) {
                            return true;
                        }
                    }
                }
            }
        }
    }

    return false;

};

/**
 * Seta os receptores deste drag
 * @param receptors {Objects} receptores para este drag
 */
Dragable.prototype.setReceptors = function(receptors) {
    this._receptors = receptors;
};

/**
 * Seta o usePoint deste drag
 * @param usePoint {Boolean} determina se o ponto de elemento será considerado inteiro ou apenas o ponto de ancora para detectar colisao
 */
Dragable.prototype.setUsePoint = function(usePoint) {
    this._usePoint = usePoint;
};

/**
 * Seta o is adjust desse drag
 * @param isAdjust {Boolean} determina se o elemento se ajustara ao receptor
 */
Dragable.prototype.setIsAdjust = function(isAdjust) {
    this._isAdjust = isAdjust;
};

/**
 * Seta o is hover desse drag
 * @param isHover {Boolean} determina se o elemento e um drageable do tipo Hover
 */
Dragable.prototype.setIsHover = function(isHover) {
    this._isHover = isHover;
};

/**
 * Seta o resetOnFail deste drag
 * @param resetOnFail {Boolean} determina se o elemento voltará a seu lugar caso não seja detectado um receptor ao solta-lo
 */
Dragable.prototype.setResetOnFail = function(resetOnFail) {
    this._resetOnFail = resetOnFail;
};

/**
 * Responsavel por centralizar o drag em seu receptor
 * @param receptor {Object} contem os pontos que serão utilizados  para ajustar esse elemento
 * @private
 */
Dragable.prototype._adjust = function(receptor) {

    TweenMax.to(this, 0.5, {
        x: Math.round(receptor.x),
        y: Math.round(receptor.y),
        ease: Back.easeOut
    });
};

/**
 * Responsavel por voltar o elemento a posição original
 */
Dragable.prototype.resetPosition = function() {
    var self = this;

    if (this._isOff) return;

    if (self._resetOnFail) {
        setTimeout(function() {

            TweenMax.killTweensOf(self);

            TweenMax.to(self.position, 0.5, {
                x: Math.round(self._initialX),
                y: Math.round(self._initialY),
                ease: Back.easeOut
            });
        }, 300);

    }

};
