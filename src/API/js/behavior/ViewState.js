/*global TimelineMax*/
var TweenMax = require('TweenMax');

function ViewState(statesConfig, layers) {
	this._statesConfig = statesConfig !== undefined ? statesConfig : null;
	this._states = {};
	this._layers = layers !== undefined ? layers : null;

	// this.loadStates();
}

ViewState.prototype.constructor = ViewState;
module.exports = ViewState;

ViewState.prototype.addViewState = function(stateId) {
	if(this._setupStateSettings !== undefined && this._setupStateSettings !== null) {
		if (this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null || this._setupStateSettings.stateObjects.length === 0) {
			throw {name: "StateObjectNotFound", message: "ViewState.selectViewState - Não exite nenhum stateObject carregado"};
		} else  {
			var lastStateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
			if(lastStateObject._isStateIntialized !== true) {
				this.initializeStateObject();
			}
		}
	}

	this._setupStateSettings = {
		name: stateId,
		enterStateEvents: {
			events: [],
			// listener: ViewState.prototype._viewStateListener
			listener: function(e) {
				this._viewStateListener({stateId: e.stateId !== undefined && e.stateId !== null ? e.stateId : stateId});
			}
		}
	};

	this._states[stateId] = this._setupStateSettings;

	return this;
};

/**
 * Este metodo seleciona um viewState e inicializa suas configurações
 *
 * @function
 * @param stateName {String} nome do viewState a ser configurado
 * @return {ViewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws StateObjectNotFound
 * @throws EnterStateEventNotFound
 */
ViewState.prototype.selectViewState = function(stateName) {
	if(this._setupStateSettings !== undefined && this._setupStateSettings !== null) {
		if (this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null || this._setupStateSettings.stateObjects.length === 0) {
			throw {name: "StateObjectNotFound", message: "ViewState.selectViewState - Não exite nenhum stateObject carregado"};
		} else  {
			var lastStateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
			if(lastStateObject._isStateIntialized !== true) {
				this.initializeStateObject();
			}
		}
	}

	this._setupStateSettings = { name: stateName };

	switch(stateName) {
	case ViewState.States.HOVERIN:
			this._setupStateSettings.enterStateEvents = {
				events:[{emitter: this, name: 'mouseover'}],
				listener: ViewState.prototype._onHoverIn
			};
			this._states.hoverInSettings = this._setupStateSettings;
			break;
		case ViewState.States.HOVEROUT:
			this._setupStateSettings.enterStateEvents = {
				events:[{emitter: this, name: 'mouseout'}],
				listener: ViewState.prototype._onHoverOut
			};
			this._states.hoverOutSettings = this._setupStateSettings;
			break;
		case ViewState.States.CLICKPRESSED:
			this._setupStateSettings.enterStateEvents = {
				events:[{emitter: this,name:'mousedown'}, {emitter: this, name: 'touchstart'}],
				listener: ViewState.prototype._onClickPressed
			};
			this._states.clickPressedSettings = this._setupStateSettings;
			break;
		case ViewState.States.CLICKRELEASED:
			this._setupStateSettings.enterStateEvents = {
				events:[{emitter: this, name: 'mouseup'}, {emitter: this, name: 'touchend'}, {emitter: this, name: 'mouseupoutside'}, {emitter: this, name: 'touchendoutside'}],
				listener: ViewState.prototype._onClickReleased
			};
			this._states.clickReleasedSettings = this._setupStateSettings;
			break;
		case ViewState.States.DISABLED:
			this._setupStateSettings.enterStateEvents = {
				events:[{emitter: this, name: 'viewStateDisabled'}],
				listener: ViewState.prototype._onDisabled
			};
			this._states.disabledSettings = this._setupStateSettings;
			break;
		case ViewState.States.ENABLED:
			this._setupStateSettings.enterStateEvents = {
				events:[{emitter: this, name: 'viewStateEnabled'}],
				listener: ViewState.prototype._onEnabled
			};
			this._states.enabledSettings = this._setupStateSettings;
			break;
		case ViewState.States.CUSTOM:
			this._setupStateSettings.enterStateEvents = {
				events: [],
				listener: ViewState.prototype._onCustom
			};
			this._states.customSettings = this._setupStateSettings;
			break;
		default: throw {name: "EnterStateEventNotFound", message: 'ViewState.selectViewState - enterStateEvent não encontrado: ' + stateName};
	}

	return this;
};

/**
 * Este metodo inclui eventos personalizados aos eventos padrões do viewState selecionado para serem ouvidos e servirem de gatilho p/ os estados configurados
 *
 * @function
 * @param emitterMapName {String} nome do map onde está o objeto que emitirá o evento que se deseja ouvir
 * @param emitterIndex {Number} indice dentro do Map que identifica o objeto que emitirá o evento que se deseja ouvir
 * @param emitterObject {EventEmitter} objeto que emitirá o evento que se deseja ouvir. Passando valor nesse parametro fara com que os parametros emitterMapName e emitterIndex sejam ignorados.
 * @param eventName {String} nome do evento que será emitido pelo objeto idenitificado que se deseja ouvir
 * @return {ViewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws ActualStateNotFound
 */
ViewState.prototype.addStateListener = function(emitterMapName, emitterIndex, emitterObject, eventName) {
	if(this._setupStateSettings === undefined || this._setupStateSettings === null) {
		throw {name: "ActualStateNotFound", message: "ViewState.addCustomListener - Configuração do estado atual não está definida"};
	}

	var emitObj = null;
	if(emitterObject !== undefined && emitterObject !== null) {
		emitObj = emitterObject;
	} else {
		if(emitterMapName == "screen") {
			emitObj = this._layers.parent;
		} else if(emitterMapName == "sound") {
			if(emitterIndex !== undefined && emitterIndex !== null) {
				emitObj = this._layers.parent.soundManager._sounds[emitterIndex];
			} else {
				emitObj = this._layers.parent.soundManager;
			}
		} else {
			var map = this._layers.getMap(emitterMapName);
			if(map !== null) {
				emitObj = map.find(function(element) {
					return element.index === emitterIndex;
				});
			}
		}
	}

	if(emitObj !== undefined && emitObj !== null) {
		this._setupStateSettings.enterStateEvents.events.push({emitter: emitObj, name: eventName});
	}

	return this;
};

/**
 * Este metodo registra o objeto que represetará um estados possivel para o ViewState
 *
 * @function
 * @param objectMapName {String} nome do map onde está o objeto que representa o estado
 * @param objectIndex {Number} indice dentro do Map que identifica o objeto responsavel por representar o estado
 * @param tObject {EventEmitter} objeto que emitirá o evento que se deseja ouvir. Passando valor nesse parametro fará com que os parametros objectMapName e objectIndex sejam ignorados.
 * @param addChild {Boolean} valor booleano indicando se o objeto represntande do estado deve ser adicionado como filho do objeto que pode assumir tal estado
 * @return {ViewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws ActualStateNotFound
 * @throws StateObjectNotFound
 * @throws MapNameNotFound
 */
ViewState.prototype.addStateObject = function(tObjectMapName, tObjectIndex, tObject, addChild) {
	if(this._setupStateSettings === undefined || this._setupStateSettings === null) {
		throw {name: "ActualStateNotFound", message: "ViewState.addStateObject - Configuração do estado atual não está definida"};
	}

	if(this._setupStateSettings.stateObjects !== undefined && this._setupStateSettings.stateObjects !== null && this._setupStateSettings.stateObjects.length > 0) {
		var lastStateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
		if(lastStateObject._isStateIntialized !== true) {
			this.initializeStateObject();
		}
	}

	var stateObject = null;
	if(tObject !== undefined && tObject !== null) {
		stateObject = tObject;
	} else {
		var objMap = null;
		if(tObjectMapName == 'particles') {
			objMap = this._layers.particleElements;
		} else {
			objMap = this._layers.getMap(tObjectMapName);
		}

		if(objMap !== null) {
			stateObject = objMap.find(function(element) {return element.index === tObjectIndex;});
		} else {
			throw {name: "MapNameNotFound", message: "ViewState.addStateObject - mapName não encontrado: "+tObjectMapName};
		}
	}

	if(stateObject !== null) {
		stateObject._isStateIntialized = false;
		if(addChild === true) {
			var parent = stateObject.parent;
			if(parent !== undefined && parent !== null) {
				parent.removeChild(stateObject);
			}

			stateObject.x = this.pivot.x === 0 ? 0 : Math.round(this.width / 2); // Round para não ter valor de posição com decimal. Isso gera problema na renderização
			stateObject.y = this.pivot.y === 0 ? 0 : Math.round(this.height / 2); // Round para não ter valor de posição com decimal. Isso gera problema na renderização

			this.addChild(stateObject);
			this.setChildIndex(stateObject, this.children.length-1);
		}

		if(stateObject.initialState === undefined || stateObject.initialState === null) {
			stateObject.initialState = null;
		}

		if(stateObject.stateConfig === undefined || stateObject.stateConfig === null) {
			stateObject.stateConfig = {};
		}

		stateObject.stateConfig[this._setupStateSettings.name] = {
			initialState: stateObject.initialState,
			targetState: null,
			effects: null
		};

		if(this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null) {
			this._setupStateSettings.stateObjects = [];
		}

		this._setupStateSettings.stateObjects.push(stateObject);
	} else {
		throw {name: "StateObjectNotFound", message: "ViewState.addStateObject - stateObject não encontrado: (mapName: '+objectMapName+', index: '+objectIndex+')"};
	}

	return this;
};

/**
 * Este metodo registra as configurações de inicializado/reset do objeto adicionado ao ViewState, representando um estado possivel
 *
 * @function
 * @param initialStateSettings {Object} Objeto contendo as configurações de incialização/reset do objeto adicionado ao ViewState, no formato "JSON"
 * @param objectIndex {Number} indice dentro do Map que identifica o objeto responsavel por representar o estado
 * @param addChild {Boolean} valor booleano indicando se o objeto represntande do estado deve ser adicionado como filho do objeto que pode assumir tal estado
 * @return {ViewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws ActualStateNotFound
 * @throws StateObjectNotFound
 */
ViewState.prototype.addInitialStateSettings = function(initialStateSettings) {
	if(this._setupStateSettings === undefined || this._setupStateSettings === null) {
		throw {name: "ActualStateNotFound", message: "ViewState.addInitialStateSettings - Configuração do estado atual não está definida"};
	} else if (this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null || this._setupStateSettings.stateObjects.length === 0) {
		throw {name: "StateObjectNotFound", message:"ViewState.addInitialStateSettings - Não exite nenhum stateObject carregado"};
	}

	var stateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
	stateObject.stateConfig[this._setupStateSettings.name].initialState = initialStateSettings !== undefined && initialStateSettings !== null ? initialStateSettings : null;

	return this;
};

/**
 * Este metodo registra as configurações de destino/target do objeto adicionado ao ViewState, representando um estado possivel
 *
 * @function
 * @param targetStateSettings {Object} Objeto contendo as configurações de destino/target do objeto adicionado ao ViewState, no formato "JSON"
 * @return {viewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws ActualStateNotFound
 * @throws StateObjectNotFound
 */
ViewState.prototype.addTargetStateSettings = function(targetStateSettings) {
	if(this._setupStateSettings === undefined || this._setupStateSettings === null) {
		throw {name: "ActualStateNotFound", message: "ViewState.addTargetStateSettings - Configuração do estado atual não está definida"};
	} else if (this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null || this._setupStateSettings.stateObjects.length === 0) {
		throw {name: "StateObjectNotFound", message:"ViewState.addTargetStateSettings - Não exite nenhum stateObject carregado"};
	}

	var stateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
	stateObject.stateConfig[this._setupStateSettings.name].targetState = targetStateSettings !== undefined && targetStateSettings !== null ? targetStateSettings : null;

	return this;
};

/**
 * Este metodo registra as configurações de efeitos referentes a transição e permanencia do objeto adicionado ao ViewState, representando um estado possivel
 *
 * @function
 * @param stateEffectsSettings {Object} Objeto contendo as configurações dos efeitos de transição e permanencia do objeto adicionado ao ViewState, no formato "JSON"
 * @return {ViewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws ActualStateNotFound
 * @throws StateObjectNotFound
 */
ViewState.prototype.addStateEffectsSettings = function(stateEffectsSettings) {
	if(this._setupStateSettings === undefined || this._setupStateSettings === null) {
		throw {name: "ActualStateNotFound", message: "ViewState.addStateEffectsSettings - Configuração do estado atual não está definida"};
	} else if (this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null || this._setupStateSettings.stateObjects.length === 0) {
		throw {name: "StateObjectNotFound", message:"ViewState.addStateEffectsSettings - Não exite nenhum stateObject carregado"};
	}

	var stateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
	stateObject.stateConfig[this._setupStateSettings.name].effects = stateEffectsSettings !== undefined && stateEffectsSettings !== null ? stateEffectsSettings : null;

	return this;
};

/**
 * Este metodo iniciliza as configurações do objeto adicionado ao ViewState, representando um estado possivel
 *
 * @function
 * @return {ViewState} Retorna a instancia atual de ViewState afim de permitir a chamada encadeada de métodos
 * @throws ActualStateNotFound
 * @throws StateObjectNotFound
 * @throws TargetStateNotFound
 */
ViewState.prototype.initializeStateObject = function() {
	if(this._setupStateSettings === undefined || this._setupStateSettings === null) {
		throw {name: "ActualStateNotFound", message: "ViewState.initializeStateObject - Configuração do estado atual não está definida"};
	} else if (this._setupStateSettings.stateObjects === undefined || this._setupStateSettings.stateObjects === null || this._setupStateSettings.stateObjects.length === 0) {
		throw {name: "StateObjectNotFound", message:"ViewState.initializeStateObject - Não exite nenhum stateObject carregado"};
	}

	var stateObject = this._setupStateSettings.stateObjects[this._setupStateSettings.stateObjects.length-1];
	if(stateObject._isStateIntialized !== true) {
		// if(stateObject.initialState === null) {
		// 	throw { name: "InitialStateNotFound", message: "ViewState.initializeStateObject - initialState não definido para o objeto identificado por: (displayType: "+stateObject.displayType+", index: "+stateObject.index+")"};
		// } else
		if(stateObject.stateConfig[this._setupStateSettings.name].targetState === null) {
			throw {name: "TargetStateNotFound", message: "ViewState.initializeStateObject - targetState não definido para o objeto identificado por: (displayType: "+stateObject.displayType+", index: "+stateObject.index+")"};
		} else {
			stateObject.stateConfig[this._setupStateSettings.name] = this._applyState(stateObject, stateObject.stateConfig[this._setupStateSettings.name], true);
			stateObject._isStateIntialized = true;
		}
	}

	return this;
};

/**
 * Este metodo recebe o JSON vindo do config das telas e chama os metódos auxiliares necessários para registrar as configurações dos objetos e adiciona-los ao ViewState, representando um estado possivel
 *
 * @function
 * @param stateEffectsSettings {Object} Objeto contendo as configurações dos efeitos de transição e permanencia do objeto adicionado ao ViewState, no formato "JSON"
 */
ViewState.prototype.loadStates = function(statesConfig) {
	this._statesConfig = statesConfig !== undefined ? statesConfig : this._statesConfig;
	
	this._statesConfig.forEach(function(stateCfg) {
		this.selectViewState(stateCfg.name);

		if(stateCfg.listenEvents !== undefined) {
			stateCfg.listenEvents.forEach(function(listenEvent) {
				this.addStateListener(listenEvent.emitter.mapName, listenEvent.emitter.index, null, listenEvent.name);
			}, this);
		}

		stateCfg.stateObjects.forEach(function(objectCfg) {
			objectCfg.indexes.forEach(function(index) {
				this.addStateObject(objectCfg.mapName, index, null, objectCfg.addChild);
				this.addInitialStateSettings(objectCfg.initialState);
				this.addTargetStateSettings(objectCfg.targetState);
				this.addStateEffectsSettings(objectCfg.effects);
				this.initializeStateObject();
				}, this);
		}, this);

		this._setupStateSettings.stateConfig = stateCfg;
	}, this);

	// this.addStateListeners();
	// this.removeStateListeners();
};

/**
 * Este metodo valida as configurações passadas tanto nas initialStateSettings quando nas targetStateSettings e Effects, verificando se as propriedades e metodos ha serem alteradas/invocadas existem no objeto "alvo". Caso não sejam passadas configurações referentes ao estado inicial do objeto (initialState), serão então armazenadas as configurações atuais das propriedas nas targetStateSettings.
 *
 * @function @private
 * @param targetObject {Object} Objeto "alvo" para aplicação das configurações dos efeitos de transição e permanencia informadas
 * @param settingsToValidade {Object} Objeto contendo as configurações dos efeitos de transição e permanencia do objeto adicionado ao ViewState
 * @return {Object} Objeto com as propriedades e metodos encontradas no objeto "alvo"
 * @throws InvalidTargetState
 */
ViewState.prototype._validadeSettings = function(targetObject, settingsToValidade) {
	var retorno = {
		targetState: null,
		initialState: null,
		effects: {
			fadeIn: false,
			stand: false,
			fadeOut: false
		},
		validatedSettings: false
	};

	if(settingsToValidade.validatedSettings) {
		retorno = settingsToValidade;
	} else {
		for(var topLevelProp in settingsToValidade) {
			if (topLevelProp === 'effects') {
				if(settingsToValidade.effects !== null) {
					retorno.effects.fadeIn = settingsToValidade.effects.fadeIn !== undefined && settingsToValidade.effects.fadeIn !== null && !isNaN(settingsToValidade.effects.fadeIn) ? settingsToValidade.effects.fadeIn : false;
					retorno.effects.stand = settingsToValidade.effects.stand !== undefined && settingsToValidade.effects.stand !== null && !isNaN(settingsToValidade.effects.stand) ? settingsToValidade.effects.stand : false;
					retorno.effects.fadeOut = settingsToValidade.effects.fadeOut !== undefined && settingsToValidade.effects.fadeOut !== null && !isNaN(settingsToValidade.effects.fadeOut) ? settingsToValidade.effects.fadeOut : false;
				}
			} else if(topLevelProp === 'targetState' || topLevelProp === 'initialState') {
				var stateToValidade = settingsToValidade[topLevelProp];

				if(stateToValidade !== null) {
					var validState = {
						properties: null,
						methods: null,
						objects: {},
						effects: {}
					};

					for(var stateProp in stateToValidade) {
						if(targetObject[stateProp] !== undefined && ((typeof targetObject[stateProp] === typeof stateToValidade[stateProp]) || (typeof targetObject[stateProp] === 'function' && Array.isArray(stateToValidade[stateProp])))) {
							if(typeof targetObject[stateProp] === 'function') {
								if(validState.methods === null) {
									validState.methods = {};
								}
								validState.methods[stateProp] = stateToValidade[stateProp];
							} else if(typeof targetObject[stateProp] === 'object') {
								validState.objects[stateProp] = stateToValidade[stateProp];
							} else {
								if(validState.properties === null) {
									validState.properties = {};
								}
								validState.properties[stateProp] = stateToValidade[stateProp];
							}
						} else {
							console.warn({name:"PropertyNotFound", message:"ViewState._validadeSettings - Propriedade \""+stateProp+"\" não encontrada no objeto identificado por: (displayType: "+targetObject.displayType+", index: "+targetObject.index+")"});
						}
					}

					retorno[topLevelProp] = validState;
				}
			}
		}

		if(retorno.targetState === null) {
			throw {name: "InvalidTargetState", message: "ViewState._validadeSettings: targetState invalido"};
		} else if( retorno.initialState === null) {
			retorno.initialState = {
				properties: null,
				methods: null,
				objects: {},
				effects: {}
			};

			for(var targetObjName in retorno.targetState.objects) {
				retorno.initialState.objects[targetObjName] = targetObject[targetObjName];
			}

			if(retorno.targetState.properties !== null) {
				for(var targetPropertyName in retorno.targetState.properties) {
					if(retorno.initialState.properties === null) {
						retorno.initialState.properties = {};
					}

					retorno.initialState.properties[targetPropertyName] = targetObject[targetPropertyName];
				}
			}
		}

		retorno.validatedSettings = true;
	}

	return retorno;
};

/**
 * Este metodo aplica as configurações passadas tanto nas initialStateSettings quando nas targetStateSettings e Effects, verificando se as propriedades e metodos ha serem alteradas/invocadas existem no objeto "alvo"
 *
 * @function @private
 * @param stateObject {Object} Objeto "alvo" para aplicação das configurações dos efeitos de transição e permanencia informadas
 * @param stateSettings {Object} Objeto contendo as configurações dos efeitos de transição e permanencia do objeto adicionado ao ViewState
 * @param initialize {Boolean} Indica se deve ser aplicada a configuração de inicialização (valor true) ou de alvo/targer (valor false)
 * @return {Object} Objeto com as propriedades e metodos aplicadas ao objeto "alvo"
 * @throws PropertyNotFound
 */
ViewState.prototype._applyState = function(stateObject, stateSettings, initialize) {
	var effectTimeline = new TimelineMax({ paused: true });
	var runningTimeline = null;

	effectTimeline.addLabel('startPoint',0);

	var settingsToApply = this._validadeSettings(stateObject, stateSettings);

	if(initialize) {
		for(var initializeMethodName in settingsToApply.initialState.methods) {
			stateObject[initializeMethodName].apply(stateObject,settingsToApply.initialState.methods[initializeMethodName]);
		}

		for(var initializePropertyName in settingsToApply.initialState.properties) {
			stateObject[initializePropertyName] = settingsToApply.initialState.properties[initializePropertyName];
		}

		for(var initializeObjectName in settingsToApply.initialState.objects) {
			stateObject[initializeObjectName] = settingsToApply.initialState.objects[initializeObjectName];
		}
	} else {
		if(settingsToApply.effects.fadeIn) {
			for(var targetMethodName in settingsToApply.targetState.methods) {
				stateObject[targetMethodName].apply(stateObject,settingsToApply.targetState.methods[targetMethodName]);
			}

			for(var targetObjectName in settingsToApply.targetState.objects) {
				var arrRunnigTweens = TweenMax.getTweensOf([stateObject, stateObject[targetObjectName]], true);

				for(var a = 0; a < arrRunnigTweens.length; a++) {
					var candidateTimeline = arrRunnigTweens[a].timeline,
						candidateTimeToGo = candidateTimeline.totalDuration()*(1-candidateTimeline.totalProgress()),
						runningTimeToGo = runningTimeline !== null ? runningTimeline.totalDuration()*(1-runningTimeline.totalProgress()) : null;

					if(runningTimeline === null || (candidateTimeToGo > runningTimeToGo)){
						runningTimeline = candidateTimeline;
					}
				}

				effectTimeline.add(TweenMax.to(stateObject[targetObjectName], settingsToApply.effects.fadeIn, settingsToApply.targetState.objects[targetObjectName]), 'startPoint');

			}

			if(settingsToApply.targetState.properties !== null){
				var propRunnigTweens = TweenMax.getTweensOf(stateObject, true);
				for(var b = 0; b < propRunnigTweens.length; b++) { // NÃO É ESPERADO QUE EXISTA MAIS DE UM ITEM NO ARRAY, MAS FEITO DENTRO DO LOOP POR PRECAUÇÃO
					var propCandidateTimeline = propRunnigTweens[b]._timeline,
						propCandidateTimeToGo = propCandidateTimeline.totalDuration()*(1-propCandidateTimeline.totalProgress()),
						propRunningTimeToGo = runningTimeline !== null ? runningTimeline.totalDuration()*(1-runningTimeline.totalProgress()) : null;

					if(runningTimeline === null || (propCandidateTimeToGo > propRunningTimeToGo)){
						runningTimeline = propCandidateTimeline;
					}
				}
				// settingsToApply.targetState.properties.onComplete = function() {
				// 	console.log('fadeIn onComplete', new Date());
				// 	delete settingsToApply.targetState.properties.onComplete;
				// };

				effectTimeline.add(TweenMax.to(stateObject, settingsToApply.effects.fadeIn, settingsToApply.targetState.properties), 'startPoint');
			}
			effectTimeline.addLabel('fadeIn');
		} else {
			for(var nfMethodToApply in settingsToApply.targetState.methods) {
				stateObject[nfMethodToApply].apply(stateObject,settingsToApply.targetState.methods[nfMethodToApply]);
			}

			for(var nfPropertyToApply in settingsToApply.targetState.properties) {
				stateObject[nfPropertyToApply] = settingsToApply.targetState.properties[nfPropertyToApply];
			}

			for(var nfObjectToApply in settingsToApply.targetState.objects) {
				stateObject[nfObjectToApply] = settingsToApply.targetState.objects[nfObjectToApply];
			}
		}

		if(settingsToApply.effects.fadeOut) {
			var fadeOutReferenceLabel = effectTimeline.getLabelBefore(effectTimeline.endTime());

			for(var fadeOutMethodName in settingsToApply.initialState.methods) {
				stateObject[fadeOutMethodName].apply(stateObject,settingsToApply.initialState.methods[fadeOutMethodName]);
			}

			for(var fadeOutObjectName in settingsToApply.initialState.objects) {
				arrRunnigTweens = TweenMax.getTweensOf([stateObject, stateObject[fadeOutObjectName]], true);

				for(var e = 0; e < arrRunnigTweens.length; e++) {
					candidateTimeline = arrRunnigTweens[e].timeline,
					candidateTimeToGo = candidateTimeline.totalDuration()*(1-candidateTimeline.totalProgress()),
					runningTimeToGo = runningTimeline !== null ? runningTimeline.totalDuration()*(1-runningTimeline.totalProgress()) : null;

					if(runningTimeline === null || (candidateTimeToGo > runningTimeToGo)){
						runningTimeline = candidateTimeline;
					}
				}

				settingsToApply.initialState.objects[fadeOutObjectName].delay = settingsToApply.effects.stand || 0;
				settingsToApply.initialState.objects[fadeOutObjectName].onComplete = function() {
					delete settingsToApply.initialState.objects[fadeOutObjectName].delay;
					delete settingsToApply.initialState.objects[fadeOutObjectName].onComplete;
				};

				effectTimeline.add(TweenMax.to(stateObject[fadeOutObjectName], settingsToApply.effects.fadeOut, settingsToApply.initialState.objects[fadeOutObjectName]), fadeOutReferenceLabel);

			}

			if(settingsToApply.initialState.properties !== null){
				propRunnigTweens = TweenMax.getTweensOf(stateObject, true);

				for(var f = 0; f < propRunnigTweens.length; f++) { // NÃO É ESPERADO QUE EXISTA MAIS DE UM ITEM NO ARRAY, MAS FEITO DENTRO DO LOOP POR PRECAUÇÃO
					propCandidateTimeline = propRunnigTweens[f]._timeline,
					propCandidateTimeToGo = propCandidateTimeline.totalDuration()*(1-propCandidateTimeline.totalProgress()),
					propRunningTimeToGo = runningTimeline !== null ? runningTimeline.totalDuration()*(1-runningTimeline.totalProgress()) : null;

					if(runningTimeline === null || (propCandidateTimeToGo > propRunningTimeToGo)){
						runningTimeline = propCandidateTimeline;
					}
				}

				settingsToApply.initialState.properties.delay = settingsToApply.effects.stand || 0;
				settingsToApply.initialState.properties.onComplete = function() {
					delete settingsToApply.initialState.properties.delay;
					delete settingsToApply.initialState.properties.onComplete;
				};

				effectTimeline.add(TweenMax.to(stateObject, settingsToApply.effects.fadeOut, settingsToApply.initialState.properties), fadeOutReferenceLabel);
			}
			effectTimeline.addLabel('fadeOut');
		} else if(settingsToApply.effects.stand) {
			var standCallBack = function() {
				// console.log('standCallBack', new Date());
				for(var standMethodName in settingsToApply.initialState.methods) {
					stateObject[standMethodName].apply(stateObject,settingsToApply.initialState.methods[standMethodName]);
				}

				for(var stantPropertyName in settingsToApply.initialState.properties) {
					stateObject[stantPropertyName] = settingsToApply.initialState.properties[stantPropertyName];
				}

				for(var standObjectName in settingsToApply.initialState.objects) {
					stateObject[standObjectName] = settingsToApply.initialState.objects[standObjectName];
				}
			}; //, settingsToApply.effects.stand*1000);

			var standReferenceLabel = effectTimeline.getLabelBefore(effectTimeline.endTime());
			effectTimeline.add(standCallBack, standReferenceLabel+'+='+settingsToApply.effects.stand);
			effectTimeline.addLabel('stand');
		}
	}

	if(effectTimeline.getLabelsArray().length > 1) {
		if(runningTimeline !== null && runningTimeline.isActive()) { // se ja existir uma timeline ativa, adiciona a timeline atual no final
			runningTimeline.add(effectTimeline);
		}

		effectTimeline.play();
	}

	return settingsToApply;
};

/**
 * Este metodo executado quando o evento HoverIn é lançado
 *
 * @function @private
 */
ViewState.prototype._onHoverIn = function() {
	if(!this._states.hoverInSettings.blocked) {
		this._states.hoverInSettings.stateObjects.forEach(function(stateObject) {
			if(stateObject.stateConfig[ViewState.States.HOVERIN] !== undefined && stateObject.stateConfig[ViewState.States.HOVERIN] !== null) {
				this._applyState(stateObject, stateObject.stateConfig[ViewState.States.HOVERIN]);
			} else {
				console.warn('ViewState._onHoverIn - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
			}
		}, this);
	}
};

/**
 * Este metodo executado quando o evento HoverOut é lançado
 *
 * @function @private
 */
ViewState.prototype._onHoverOut = function(e) {
	var localPos = e.data.getLocalPosition(this.objectToAlign ? this.objectToAlign : this);

	if(localPos.x <= this.width*0.1 || localPos.x >= this.width*0.9 || localPos.y <= this.height*0.1 || localPos.y >= this.height*0.9) {
		this._states.hoverInSettings.blocked = false;
		this._states.hoverOutSettings.stateObjects.forEach(function(stateObject) {
			if(stateObject.stateConfig[ViewState.States.HOVEROUT] !== undefined && stateObject.stateConfig[ViewState.States.HOVEROUT] !== null) {
				this._applyState(stateObject, stateObject.stateConfig[ViewState.States.HOVEROUT]);
			} else {
				console.warn('ViewState._onHoverOut - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
			}
		}, this);
	} else if(this._states.hoverInSettings !== undefined && this._states.hoverInSettings !== null) {
		this._states.hoverInSettings.blocked = true;
	}
};

/**
 * Este metodo executado quando o evento ClickPressed é lançado
 *
 * @function @private
 */
ViewState.prototype._onClickPressed = function() {
	this._states.clickPressedSettings.stateObjects.forEach(function(stateObject) {
		if(stateObject.stateConfig[ViewState.States.CLICKPRESSED] !== undefined && stateObject.stateConfig[ViewState.States.CLICKPRESSED] !== null) {
			this._applyState(stateObject, stateObject.stateConfig[ViewState.States.CLICKPRESSED]);
		} else {
			console.warn('ViewState._onClickPressed - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
		}
	}, this);
};

/**
 * Este metodo executado quando o evento ClickRelease é lançado
 *
 * @function @private
 */
ViewState.prototype._onClickReleased = function() {
	this._states.clickReleasedSettings.stateObjects.forEach(function(stateObject) {
		if(stateObject.stateConfig[ViewState.States.CLICKRELEASED] !== undefined && stateObject.stateConfig[ViewState.States.CLICKRELEASED] !== null) {
			this._applyState(stateObject, stateObject.stateConfig[ViewState.States.CLICKRELEASED]);
		} else {
			console.warn('ViewState._onClickReleased - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
		}
	}, this);
};

/**
 * Este metodo executado quando o evento viewStateDisabled é lançado
 *
 * @function @private
 */
ViewState.prototype._onDisabled = function() {
	this._states.disabledSettings.stateObjects.forEach(function(stateObject) {
		if(stateObject.stateConfig[ViewState.States.DISABLED] !== undefined && stateObject.stateConfig[ViewState.States.DISABLED] !== null) {
			this._applyState(stateObject, stateObject.stateConfig[ViewState.States.DISABLED]);
		} else {
			console.warn('ViewState._onClickReleased - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
		}
	}, this);
};

/**
 * Este metodo executado quando o evento viewStateEnabled é lançado
 *
 * @function @private
 */
ViewState.prototype._onEnabled = function() {
	this._states.enabledSettings.stateObjects.forEach(function(stateObject) {
		if(stateObject.stateConfig[ViewState.States.ENABLED] !== undefined && stateObject.stateConfig[ViewState.States.ENABLED] !== null) {
			this._applyState(stateObject, stateObject.stateConfig[ViewState.States.ENABLED]);
		} else {
			console.warn('ViewState._onClickReleased - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
		}
	}, this);
};

/**
 * Este metodo executado quando o evento configurado de forma personalizada é lançado
 *
 * @function @private
 */
ViewState.prototype._onCustom = function() {
	this._states.customSettings.stateObjects.forEach(function(stateObject) {
		if(stateObject.stateConfig[ViewState.States.CUSTOM] !== undefined && stateObject.stateConfig[ViewState.States.CUSTOM] !== null) {
			this._applyState(stateObject, stateObject.stateConfig[ViewState.States.CUSTOM]);
		} else {
			console.warn('ViewState._onClickReleased - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
		}
	}, this);
};

ViewState.prototype._viewStateListener = function(e) {
	var targetState = this._states[e.stateId];

	if(targetState !== undefined && targetState !== null) {
		targetState.stateObjects.forEach(function(stateObject) {
			if(stateObject.stateConfig[e.stateId] !== undefined && stateObject.stateConfig[e.stateId] !== null) {
				this._applyState(stateObject, stateObject.stateConfig[e.stateId]);
			} else {
				console.warn('ViewState._viewStateListener - targetState não definido para o objeto identificado por: (displayType: '+stateObject.displayType+', index: '+stateObject.index+')');
			}
		}, this);
	} else {
		console.warn('ViewState._viewStateListener - ViewState não definido: (stateId: '+e.stateId+')');
	}
};

/**
 * Metodo responsável por adicionar os listeners para os enventos configurados no ViewState
 *
 * @function
 */
ViewState.prototype.addStateListeners = function() {
	for(var stateName in this._states) {
		var state = this._states[stateName];

		this._setupStateSettings = state;
		this.initializeStateObject();

		if(state.enterStateEvents.events !== null) {
			state.enterStateEvents.events.forEach(function(event) {
				if(event.emitter.listeners(event.name).indexOf(state.enterStateEvents.listener) < 0) {
					event.emitter.on(event.name, state.enterStateEvents.listener, this);
				}
			}, this);
		} else {
			console.warn('ViewState.addStateListeners - Não foram configurados eventos para o estado '+stateName);
		}
	}

	// this.on("clicked", function(e){
	// 	console.log('teste listener p/ não remover');
	// });
};

/**
 * Metodo responsável por remover os listeners para os enventos configurados no ViewState
 *
 * @function
 */
ViewState.prototype.removeStateListeners = function() {
	var removeListener = function(stateEvents) {
		stateEvents.events.forEach(function(event) {
			event.emitter.removeListener(event.name, stateEvents.listener);
		});

		// if(this.eventNames().length === 0) {
		// 	this.interactive = false;
		// }
	};

	for(var stateName in this._states) {
		var state = this._states[stateName];
		removeListener.call(this,state.enterStateEvents);
		// state.enterStateEvents.events.forEach(removeEventListener, this);
		// if(state.leaveStateEvents !== undefined) {
		// 	state.leaveStateEvents.forEach(removeEventListener, this);
		// }
	}
};

/**
 * Metodo responsável por adicionar interatividade para os stateObjects configurados no ViewState
 *
 * @function
 */
ViewState.prototype.addStatesInteraction = function() {
	for(var stateName in this._states) {
		var state = this._states[stateName];
		state.enterStateEvents.events.forEach(function(event) {
			event.emitter.interactive = true;
		}, this);
	}

	// this.on("clicked", function(e){
	// 	console.log('teste listener p/ não remover');
	// });
};

/**
 * Metodo responsável voltar o ViewState para as configurações iniciais (initialStateSettings)
 *
 * @function
 * @throws InitialStateNotFound
 */
ViewState.prototype.resetInitialState = function() {
	for(var stateName in this._states) {
		var state = this._states[stateName];
		for(var a = 0; a < state.stateObjects.length; a++) {
			var stateObject = state.stateObjects[a];
			for(var stateNameCfg in stateObject.stateConfig) {
				this._applyState(stateObject, stateObject.stateConfig[stateNameCfg], true);
			}
		}
	}
};

ViewState.States = {};
ViewState.States.HOVERIN = 'hoverIn';
ViewState.States.HOVEROUT = 'hoverOut';
ViewState.States.CLICKPRESSED = 'clickPressed';
ViewState.States.CLICKRELEASED = 'clickReleased';
ViewState.States.DISABLED = 'disabled';
ViewState.States.ENABLED = 'enabled';
ViewState.States.CUSTOM = 'custom';
