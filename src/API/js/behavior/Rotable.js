var PIXI = require('PIXI');
var PIXIConfig = require('../core/PIXIConfig');

/**
 * @classdesc Plugin que configura o arraste
 * @memberof module:behavior
 * @exports Rotable
 * @constructor
 */

function Rotable() {
    this._isMove = false;
	this._totalPositions = 12;

    /**
     * Emitido quando o movimento que controla a rotação é iniciado
     * @event rotationStart
     * @memberof module:behavior.Rotable
     */

    /**
     * Emitido quando o movimento que controla a rotação é concluido
     * @event rotationEnd
     * @memberof module:behavior.Rotable
     */

}

Rotable.prototype.constructor = Rotable;
module.exports = Rotable;

Rotable.prototype.initObjectsToRotate = function() {
	var self = this;

	this._objectsToRotate = new PIXI.Container();
	var arrObjects = this.children.slice();
	this.removeChildren();

	arrObjects.forEach(function(obj) {
		this._objectsToRotate.addChild(obj);
	}, this);

	this._objectsToRotate.pivot.set(this._objectsToRotate.width/2, this._objectsToRotate.height/2);
	this._objectsToRotate.x += this._objectsToRotate.width/2;
	this._objectsToRotate.y += this._objectsToRotate.height/2;
	this.addChild(this._objectsToRotate);
};

/**
 * Metodo que adiciona evento nos elementos injetados
 */
Rotable.prototype.addEvents = function() {
    this.on('mouseup', this._onUp);
    this.on('touchend', this._onUp);

    this.on('mouseupoutside', this._onUp);
    this.on('touchendoutside', this._onUp);

    this.on('mousemove', this._onMove);
    this.on('touchmove', this._onMove);

	this.on('mousedown', this._onDown);
	this.on('touchstart', this._onDown);

	this.interactive = true;
    this.buttonMode = true;
};

/**
 * Metodo que remove evento nos elementos injetados
 */
Rotable.prototype.removeEvents = function() {
	this.interactive = false;
    this.buttonMode = false;

	this.removeListener('mousedown', this._onDown);
    this.removeListener('touchstart', this._onDown);

	this.removeListener('mousemove', this._onMove);
	this.removeListener('touchmove', this._onMove);

    this.removeListener('mouseup', this._onUp);
    this.removeListener('touchend', this._onUp);

    this.removeListener('mouseupoutside', this._onUp);
    this.removeListener('touchendoutside', this._onUp);
};

/**
 * Metodo que captura o angulo inicial para usar de referencia para calculos futuros
 * @param e {Object} Retorno do evento onDown do pixi
 * @fires rotationStart Emitido quando o é pressionado
 * @private
 */
Rotable.prototype._onDown = function(e) {
	this.emit("rotationStarted", {
        target: this
    });

    this._data = e.data;
    this._local = this._data.getLocalPosition(this);

	var _x = this._local.x-this.pivot.x;
    var _y = this._local.y-this.pivot.x;

    this._registerAngle = Math.atan2(_y,_x);
    this._registerRotation = this._objectsToRotate.rotation;

	this._isMove = true;
};

/**
 * Metodo que ao soltar o item calcula o angulo de rotação final
 * @param e {Object} Retorno do evento onUp do pixi
 * @fires rotationEnd Emitido quando o é solto
 * @private
 */
Rotable.prototype._onUp = function(e) {
	this._isMove = false;
    this._data = null;
	var angleStops = (360/this._totalPositions);
    var round = Math.round((this._objectsToRotate.rotation * PIXI.RAD_TO_DEG) / angleStops);
    var roundAngle = round * angleStops;

    var self = this;
    this.setRotation(roundAngle, function(){
		var res = roundAngle;
	    if (res < 0) {
	        res += 360;
	    } else if (res >= 360) {
			res = 0;
		}

		self.emit("rotationEnded", {target: self, angle:res});
	});

	// setTimeout(function() {
	// 	this.emit("rotationEnded", {target: this, angle:this._objectsToRotate.rotation*PIXI.RAD_TO_DEG});
	// }, 400);
};


/**
 * Metodo que captura o movimento do cursor e rotaciona o objeto de acordo com esse movimento
 * @param e {Object} Retorno do evento onMove do pixi
 * @private
 *
 */
Rotable.prototype._onMove = function(e) {
    if (this._isMove) {
        var newPosition = this._data.getLocalPosition(this);

        var _x = newPosition.x-this.pivot.x;
        var _y = newPosition.y-this.pivot.y;

        var _rad = Math.atan2(_y,_x);
        var dif = _rad - this._registerAngle;

        this._objectsToRotate.rotation = this._registerRotation + dif;
    }
};

/**
*
*/
Rotable.prototype.setRotation = function(angle, callback, ease, duration) {

    var clockEase = ( ease !== undefined ? ease : Back.easeOut );
    var animTime = ( duration !== undefined ? duration : 0.3 );
    
	TweenMax.to(this._objectsToRotate, animTime, {
		rotation: angle * PIXI.DEG_TO_RAD,
		ease: clockEase,
		onComplete: callback
	});

};

/**
*
*/
Rotable.prototype.setTotalPositions = function(totalPositions) {
	this._totalPositions = totalPositions;
};
