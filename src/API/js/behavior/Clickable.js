/**
 * @classdesc Plugin que configura um botão
 * @memberof module:behavior
 * @exports Clickable
 * @constructor
 */

function Clickable() {

    this._selectionEffect = true;
    this._block = false;

}

Clickable.prototype.constructor = Clickable;
module.exports = Clickable;

/**
 * Metodo que adiciona evento nos elementos injetados
 */
Clickable.prototype.addEvents = function() {
    this.interactive = true;
    this.buttonMode = true;

    this.on('mousedown', this._onDown);
    this.on('touchstart', this._onDown);
};

Clickable.prototype._addUpEvents = function() {
    this.on('mouseup', this._onUp);
    this.on('touchend', this._onUp);

    this.on('mouseupoutside', this._onUp);
    this.on('touchendoutside', this._onUp);
};

/**
 * Metodo que remove evento nos elementos injetados
 */
Clickable.prototype.removeEvents = function() {
    this.interactive = false;
    this.buttonMode = false;

    this.removeListener('mousedown', this._onDown);
    this.removeListener('touchstart', this._onDown);

    this._removeUpEvents();
};

Clickable.prototype._removeUpEvents = function() {
    this.removeListener('mouseup', this._onUp);
    this.removeListener('touchend', this._onUp);

    this.removeListener('mouseupoutside', this._onUp);
    this.removeListener('touchendoutside', this._onUp);
};

/**
 * Metodo que reduz o tamanho do elemento clickado
 * @private
 */
Clickable.prototype._onDown = function(e) {
    if(this._block) return;
    if (this._selectionEffect) {
        //diminui 20% na escala
        this.scale.set(this.scale.x - 0.2, this.scale.y - 0.2);
        // this.pivot.set(0, 0);
        // this.position.set(0, 0);
        //this.scale.set(0.8,0.8);

    }

    this._addUpEvents();

};

/**
 * Metodo que retorna o tamanho do elemento clickado ao tamanho original
 * @private
 */
Clickable.prototype._onUp = function(e) {
    this._removeUpEvents();
    if (this._selectionEffect) {
        //volta a escala original
        this.scale.set(this.scale.x + 0.2, this.scale.y + 0.2);
    }

    //Dispara o evento indicando o click
    this.emit("clicked", e);
};
