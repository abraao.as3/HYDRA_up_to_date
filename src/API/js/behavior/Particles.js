
require('Particles');
var PIXI = require('PIXI');

/**
 * @classdesc Plugin configura particulas
 * @memberof module:behavior
 * @exports Particles
 * @constructor
 * @param container {PIXI.container} coleção de objetos de exibição
 * @param particleCfg {PIXI.container}
 * @param particleCfg.alpha {Object} opacidade do objeto, escala de opacidade valores de 0 a 1
 * @param [particleCfg.alpha.start=0.9] {Number} alfa inicial de todas as partículas
 * @param [particleCfg.alpha.end=0.1] {Number} alfa final de todas as partículas
 * @param particleCfg.scale {Object} dimensiona a escala das particulas, valores de 0 a 1
 * @param [particleCfg.scale.start=1] {Number} escala inicial de todas as partículas
 * @param [particleCfg.scale.end=0.3] {Number} escala final de todas as partículas
 * @param particleCfg.color {Object} define a cor
 * @param [particleCfg.color.start="fb1010"] {Number} cor inicial de todas as partículas
 * @param [particleCfg.color.end="1010fb"] {Number} cor final de todas as partículas
 * @param particleCfg.speed {Object} multiplicador mínimo para a velocidade de uma partícula no início e fim
 * @param [particleCfg.speed.start=100] {Number} velocidade inicial
 * @param [particleCfg.speed.end=200] {Number} velocidade final
 * @param particleCfg.startRotation {Object} rotação de partida para uma partícula, em graus
 * @param [particleCfg.startRotation.min=0] {Number} rotação de partida mínima para uma partícula, em graus
 * @param [particleCfg.startRotation.max=360] {Number} rotação de partida máxima para uma partícula, em graus
 * @param particleCfg.rotationSpeed {Object} velocidade de rotação para uma partícula,em graus por segundo. Isso só visualmente gira a partícula, não muda de direção de movimento
 * @param [particleCfg.rotationSpeed.min=0] {Number} velocidade de rotação mínima para uma partícula
 * @param [particleCfg.rotationSpeed.max=0] {Number} velocidade de rotação máxima para uma partícula
 * @param particleCfg.lifetime {Object} tempo de vida para uma partícula
 * @param [particleCfg.lifetime.min=2] {Number} o tempo de vida mínimo para uma partícula, em segundos
 * @param [particleCfg.lifetime.max=6] {Number} o tempo de vida máximo para uma partícula, em segundos
 * @param [particleCfg.frequenci=0.40] {Number} tempo entre partículas geradas, em segundos
 * @param [particleCfg.emitterLifetime=0] {Number} tempo para emitir as particulas
 * @param [particleCfg.maxParticles=1000] {Number} definir o maximo de partículas
 * @param [particleCfg.addAtBack=false] {Bollean} define se as partículas devem ser adicionas na parte final da lista
 * @param [particleCfg.spawnType="circle"] {String} formato que as partículas seram geradas
 * @param particleCfg.spawnCircle {PIXI.Circle} formato definido como circulo
 * @param [particleCfg.spawnCircle.x=0] {Number} tamanho x
 * @param [particleCfg.spawnCircle.y=0] {Number} tamanho y
 * @param [particleCfg.spawnCircle.r=10] {Number} tamanho r
 * @param image {PIXI.image}
 */
function Particles(container, particleCfg, image) {

    this.particles = true;
    /**
     * Configura as dimensões, tempo, rotação, cor, frequencia e velocidade das particulas
     */

    if (image === undefined || image === null) image = [PIXI.Texture.fromImage('telas/commom/particles/basicParticle.png')];

    if (particleCfg === undefined) {
        particleCfg = {
            alpha: {
                start: 0.9,
                end: 0.1
            },
            scale: {
                start: 1,
                end: 0.3
            },
            color: {
                start: "fb1010",
                end: "1010fb"
            },
            speed: {
                start: 100,
                end: 200
            },
            startRotation: {
                min: 0,
                max: 360
            },
            rotationSpeed: {
                min: 0,
                max: 0
            },
            lifetime: {
                min: 2,
                max: 6
            },
            frequency: 0.40,
            emitterLifetime: 0,
            maxParticles: 1000,
            pos: {
                x: 0,
                y: 0
            },
            addAtBack: false,
            spawnType: "circle",
            spawnCircle: {
                x: 0,
                y: 0,
                r: 10
            }
        };
    }



    this.emitter = new PIXI.particles.Emitter(
        //objeto de exibição para adicionar as partículas
        container,

        //texturas
        image,

        //configuracao das particulas
        particleCfg
	);

	this._particleTimeout = null;
}

Particles.prototype.constructor = Particles;
module.exports = Particles;

/**
     * Funcao atualiza as imagens das particulas animadas na tela.
     * @memberof module: behavior
     * @public
     */

Particles.prototype.updateParticles = function(dif) {

    this.emitter.update(dif);

};

/**
     * Funcao chamada para limpar as particulas
     * @memberof module: behavior
     * @public
     */

Particles.prototype.cleanup = function() {

    this.emitter.cleanup();

};

Particles.prototype.startEmitter = function() {
	if(this._particleTimeout !== null) {
		clearTimeout(this._particleTimeout);
		this._particleTimeout = null;
	}

	this.emitter.emit = true;

	if(window.particles.indexOf(this) < 0) {
		window.particles.push(this);
	}
};

Particles.prototype.stopEmitter = function(withCleanup, forced) {
	var self = this;
	this.emitter.emit = false;

	var func = function() {
		if(self._particleTimeout !== null) {
			clearTimeout(self._particleTimeout);
			self._particleTimeout = null;
		}

		var particlePos = window.particles.indexOf(self);
		if(particlePos >= 0) {
			window.particles.splice(particlePos,1);
		}

		if(withCleanup) {
			self.cleanup();
		}
	};

	if(forced) {
		func();
	} else {
		this._particleTimeout = setTimeout(func, (this.emitter.maxLifetime+0.5)*1000);
	}
};
