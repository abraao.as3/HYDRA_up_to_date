var PIXI = require('PIXI');
var PIXIConfig = require('../core/PIXIConfig');
var behavior = require('./index');
var ObjectBase = require('../core/ObjectBase');
var Scroll = require('../display/simple/Scroll');

function Scrollable(obj_scroll, all_objs, parentContainer) {
    this._config = obj_scroll;

    this.scroll = this.CreateScrollContainer(obj_scroll);
    this.AddObjectsIntoScroll(this.scroll, all_objs, obj_scroll, parentContainer);
}

Scrollable.prototype.constructor = Scrollable;
module.exports = Scrollable;

Scrollable.prototype.CreateScrollContainer = function(obj_scroll){
    //create a scroll area container
    var s = new ObjectBase();

    s.autoAdjust = true;

    s.sW = obj_scroll.width + obj_scroll.marginX;//width of scroll screen
    s.sH = obj_scroll.height + obj_scroll.marginY;//height of scroll screen

    //X
    s.initX = 99999999;//init x of first object of scroll
    s.endX = -99999999;//end x of last object of scroll
    s.porcX = 0;//current porcentage of horizontal scroll view(0 <-> 1)
    s.pixelX = 0;//current pixel of scroll view (initX <-> endX)
    s.marginX = 5;//margem of right and left sides
    s.scrollHorizontal = false;//has horizontal scroll bar
    s.difX = 0;//diference between init and end of X

    //Y
    s.initY = 99999999;//init y of first object of scroll
    s.endY = -99999999;//end y of last object of scroll
    s.porcY = 0;//current porcentage of vertical scroll view(0 <-> 1)
    s.pixelY = 0;//current pixel of scroll view (initY <-> endY)
    s.marginY = 5;//margem of top and bottom sides
    s.scrollVertical = false;//has vertical scroll bar
    s.difY = 0;//diference between init and end of Y

    return s;
};

Scrollable.prototype.AddObjectsIntoScroll = function(s,objs, obj_scroll, parentContainer){//the objs are bitmap or type(texts)

    var objContainer = new ObjectBase();
    for (var i = 0; i < objs.length; i++) {var o = objs[i]; objContainer.addChild(o);}//add all objs

    s.addChild(objContainer);

    var changeLimitHorizontal = false;
    var changeLimitVertical = false;

    for (var i = 0; i < objs.length; i++) {//verify if need to change a limits

        var obj_pos = {};
        var obj_size = {};

        if(objs[i]._displayType === "type"){
            obj_size = objs[i].field;
            obj_pos.x = objs[i].x + objs[i].width/2;
            obj_pos.y = objs[i].y + objs[i].height/2;
        }
        else{
            obj_pos = objs[i];
            obj_size = objs[i];
        }

        this._isScrollable = true;
        // objs[i]._scroll = obj_scroll;

        //x limits
        if(obj_pos.x - obj_size.width/2 < s.initX || obj_pos.x + obj_size.width/2 > s.endX){
            if(obj_pos.x - obj_size.width/2 < s.initX){s.initX = obj_pos.x - obj_size.width/2;}
            if(obj_pos.x + obj_size.width/2 > s.endX){s.endX = obj_pos.x + obj_size.width/2;}
            changeLimitHorizontal = true;
        }

        //y limits
        if(obj_pos.y - obj_size.height/2 < s.initY || obj_pos.y + obj_size.height/2 > s.endY){
            if(obj_pos.y - obj_size.height/2 < s.initY){s.initY = obj_pos.y - obj_size.height/2;}
            if(obj_pos.y + obj_size.height/2 > s.endY){s.endY = obj_pos.y + obj_size.height/2;}
            changeLimitVertical = true;
        }

    }

    s.initX -= obj_scroll.marginX;
    s.endX += obj_scroll.marginX;

    s.initY -= obj_scroll.marginY;
    s.endY += obj_scroll.marginY;

    s.difX = s.endX - s.initX;
    s.difY = s.endY - s.initY;

    var btn_size = 25;
    var Hconfig = {
        //back layer of scroll
        scroll_barWidth : s.sW - btn_size,
        scroll_barHeight : btn_size,
        scroll_barColor : "0xBBBBBB",
        scroll_barAlpha : 1,
        scroll_barRadius : 15,
        //scroll point of current porcentage
        point_scrollWidth : btn_size,
        point_scrollHeight : btn_size,
        point_scrollColor : "0xFFFFFF",
        point_scrollAlpha : 0.5,
        point_scrollRadius : 15,
        //scroll of viewed bar
        viewed_scrollWidth : 0,
        viewed_scrollHeight : 0,
        viewed_scrollColor : 0,
        viewed_scrollAlpha : 0
    }
    var Vconfig = {
        //back layer of scroll
        scroll_barWidth : btn_size,
        scroll_barHeight : s.sH,
        scroll_barColor : "0xBBBBBB",
        scroll_barAlpha : 1,
        scroll_barRadius : 15,
        //scroll point of current porcentage
        point_scrollWidth : btn_size,
        point_scrollHeight : btn_size,
        point_scrollColor : "0xFFFFFF",
        point_scrollAlpha : 0.5,
        point_scrollRadius : 15,
        //scroll of viewed bar
        viewed_scrollWidth : 0,
        viewed_scrollHeight : 0,
        viewed_scrollColor : 0,
        viewed_scrollAlpha : 0
    }

    var NeedCreateHorizontalBar = this.NeedCreateHorizontalBar(s);
    var NeedCreateVerticalBar = this.NeedCreateVerticalBar(s);
    var autoAdjust = obj_scroll.autoAdjust;

    if(autoAdjust === true){
        if(NeedCreateVerticalBar === false){s.sH = s.difY + obj_scroll.marginY;}
        if(NeedCreateHorizontalBar === false){s.sW = s.difX;}
    }

    if(changeLimitHorizontal && NeedCreateHorizontalBar === true){
        Hconfig.scroll_barWidth += changeLimitVertical === true ? Vconfig.scroll_barWidth : 0;
        this.AdjustHorizontalLimit(s,Hconfig);
    }else{s.scrollHorizontal = false;}

    if(changeLimitVertical && NeedCreateVerticalBar === true){
        Vconfig.scroll_barHeight += changeLimitHorizontal === true ? Hconfig.scroll_barHeight : 0;
        this.AdjustVerticalLimit(s,Vconfig);
    }else{s.scrollVertical = false;}

    obj_scroll.x = s.x;
    obj_scroll.y = s.y;
    obj_scroll.width = s.width;
    obj_scroll.height = s.height;

    // this.CreateMask(s, obj_scroll, parentContainer);

};

Scrollable.prototype.NeedCreateHorizontalBar = function(s){return s.difX > s.sW && s.scrollHorizontal === false? true : false;};
Scrollable.prototype.NeedCreateVerticalBar = function(s){return s.difY > s.sH && s.scrollVertical === false ? true : false;};

Scrollable.prototype.AdjustHorizontalLimit = function(s, config){
    s.scrollHorizontal = true;
    var hBar = new Scroll();
    hBar.CreateHorizontalScrollBar(s, config, "bottom");
};

Scrollable.prototype.AdjustVerticalLimit = function(s,config){
    s.scrollVertical = true;
    var vBar = new Scroll();
    vBar.CreateVerticalScrollBar(s, config, "right");
};

Scrollable.prototype.CreateMask = function(v_scroll, v_obj_scroll, parentContainer){
    var s = v_scroll;

    //cria um retangulo de referencia
    var rect = v_obj_scroll.rect;

    var scrollMask = new PIXI.Graphics();
    // parentContainer.addChild(scrollMask);

    //set rect values
    var r = new PIXI.Graphics();
    r.lineStyle(rect.lineSize, rect.lineColor, rect.lineAlpha);
    r.beginFill(0xFFFFFF, 0);
    r.drawRoundedRect(0,0,s.sW, s.sH,rect.roundedAngle);
    r.endFill();
    r.position.set(s.initX,s.initY);

    s.addChild(r);

    parentContainer.addChild(s);
    s.mask = scrollMask;
    s.addChild(scrollMask)

    scrollMask.clear();
    scrollMask.beginFill(0x00ff00,0);
    scrollMask.drawRoundedRect(0, 0, s.sW, s.sH,rect.roundedAngle);
    scrollMask.endFill();
    scrollMask.position.set(s.initX, s.initY);

    //deixa o scroll como filho do container
    var t = this;
    var s = t.scroll;
    var ps = s.parent;
    ps.removeChild(s);
    t.addChild(s);
};
