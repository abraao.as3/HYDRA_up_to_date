var EventEmitter = require("EventEmitter");
var Point = require('../animation/Point');

/**
 * @classdesc Plugin que exibe imagens, textos e sons nas telas
 * @memberof module:behavior
 * @exports FadePoint
 * @constructor
 */
function FadePoint() {

    EventEmitter.call(this);
    this._currentIndex = 0;
    this._timeline = new TimelineMax();
    this._sequences = [];
    this._isPause = false;

    this._points = [];
    this._fadePointCount = 0;
    this._firstStart = false;
}

FadePoint.prototype = Object.create(EventEmitter.prototype);
FadePoint.prototype.constructor = FadePoint;
module.exports = FadePoint;

FadePoint.prototype.setSequencies = function(sequences, soundManager, layersDisplay) {
    this._sequences = sequences || [];
    var layers = layersDisplay || this.layersDisplay;
    this._layersDisplay = layers;

    for (var i = 0; i < this._sequences.length; i++) {
        var point = new Point(this._sequences[i], layers, soundManager);
        this._points.push(point);
    }
};

FadePoint.prototype.fadeStart = function() {
    for (var i = 0; i < this._points.length; i++) {
        var p = this._points[i];
        p.start();
    }
};

FadePoint.prototype.fadeOpen = function() {
    this.fadeRestart();
};

FadePoint.prototype._playing = function() {
    var self = this;

    this._firstStart = true;
    if (this._fadePointCount >= this._points.length) {
        var bocas = this._layersDisplay.getObjects('mouths');
        for (i = 0; i < bocas.length; i++) {
            bocas[i].gotoAndStop(bocas[i].stationary);
        }

		this.emit('sequenceComplete', {target: this});
        return;
    }

    var point = this._points[this._fadePointCount];
    point.once('pointComplete', function(e) {
        self._fadePointCount++;

        if (self._fadePointCount > self._points.length) return;
        if (self._fadePointCount < self._points.length) {
            if (self._sequences[self._fadePointCount - 1].pause !== undefined && self._sequences[self._fadePointCount - 1].pause === true && this._isPause === false) {
                this._isPause = true;
                this.emit('sequencePaused');
                return;
            }
        }

        self._playing();
    });

    this._points[this._fadePointCount].timeline.play();
};

FadePoint.prototype.forceEnd = function() {
    for (var i = 0; i < this._points.length; i++) {
        this._points[i].forceEnd();
    }

    this._fadePointCount = this._points.length;
    this._playing();
};

FadePoint.prototype.fadeResume = function() {
    if (this._isPause === true) {
        this._isPause = false;
        this._playing();
    }
};

FadePoint.prototype.fadeRestart = function() {
    this.fadeStop();

    this._playing();
};

FadePoint.prototype.fadeStop = function() {
    this._isPause = false;
    this._fadePointCount = 0;
    for (var i = 0; i < this._points.length; i++) {
        this._points[i].reset();
        this._points[i].removeListener('pointComplete');
    }
};
