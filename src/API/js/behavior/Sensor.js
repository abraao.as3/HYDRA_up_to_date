var PIXI = require('PIXI');

/**
 * @classdesc Plugin que adiciona um sensor a elementos
 * @memberof module:behavior
 * @exports Sensor
 * @constructor
 */
function Sensor(x, y, width, height, debug) {

    this._sensor = new PIXI.Rectangle(x, y, width, height);

}

Sensor.prototype.constructor = Sensor;
module.exports = Sensor;

/**
 * Metodo que desenha o sensor
 */
Sensor.prototype.drawSensor = function() {

    this._debug = new PIXI.Graphics();

    this._debug.beginFill("0xFF00FE", 0.5);
    this._debug.drawRect(this._sensor.x, this._sensor.y, this._sensor.width, this._sensor.height);
    this._debug.endFill();

    this.addChild(this._debug);

};


/**
 * Metodo que retorna o sensor
 */
Sensor.prototype.getSensor = function() {

    return this._sensor;

};
