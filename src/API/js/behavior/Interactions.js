var TweenMax = require('TweenMax');
var Clickable = require('./Clickable');
var FadePoint = require('./FadePoint');
var Point = require('../animation/Point');

/**
 * @classdesc Plugin que configura interações da tela pelo config
 * @memberof module:behavior
 * @exports Interactions
 * @constructor
 */
function Interactions() {

    this._blockClicks = false;
    this._countIntPoints = 0;
    this._pointController = [];
    this._idSeqAtual = -1;
    this._pointInicial = false;

}

Interactions.prototype.constructor = Interactions;
module.exports = Interactions;

/**
 * Passa por toda a array de interactions do config e pega imagens ou textos e  adiciona os behaviours neles
 */
Interactions.prototype.prepareBehaviours = function() {

    var self = this;

    for (var x in self._config.interactions) {

        var int = self._config.interactions[x];

        self._dealWithIntType(int);
        if (!!self._config.interactions[x].action) {

            //Sou uma interaction de imagem ou de texto ? ou ambos ?
            var count = 0;

        } else {
            throw "Interaction sem action !";
        }
    }

};

/**
 * Metodo usado para fazer a checagem de aqual tipo de action este interaction possui
 */
Interactions.prototype._dealWithIntType = function(e) {

    var self = this;
    var types = ['fadePoints', 'clickable'];

    if (types.indexOf(e.type) !== -1) {
        switch (e.type) {
            case 'fadePoints':

                var tempController = new FadePoint();
                tempController.id = self._countIntPoints;

                self._countIntPoints++;
                tempController._config = e;

                for (var i = 0; i < e.points.length; i++) {
                    tempController._points.push(new Point(e.points[i], self.layersDisplay, self.soundManager));

                    //recursivamente
                    if (!!e.points[i].interactions) {
                        for (var f = 0; f < e.points[i].interactions.length; f++) {

                            self._dealWithIntType(e.points[i].interactions[f]);

                        }
                    }
                }

                tempController.fadeStart();
                this._pointController.push(tempController);

                break;

            case 'clickable':

                //checo se tem imagem e/ou texto e bindo os eventos de mouse para trigeraremo metodo vinculado

                if (!!e.images) {

                    for (count = 0; count < e.images.length; count++) {

                        var tmpImg = e.images[count];

                        //TODO GetMapByID
                        var arrImg = self.layersDisplay.getMap('images');

                        for (var j = 0; j < arrImg.length; j++) {

                            if (arrImg[j].index == tmpImg) {
                                tmpImg = arrImg[j];
                                tmpImg.actionClick = e.action;
                                if (tmpImg.actionClick.name == "playSequence") {
                                    tmpImg._idFadePoint = (self._countIntPoints - 1);
                                    tmpImg.alpha = e.alpha || 0;
                                }
                            }
                        }
                        if (tmpImg.hasOwnProperty('alpha')) {
                            self._addBehaviour(tmpImg, e.type);
                        } else {
                            throw 'Imagens nas interactions nao contem na layer';
                        }
                    }
                }

                if (!!e.txt) {

                    for (count = 0; count < e.txt.length; count++) {

                        var tmpTxt = e.txt[count];

                        // TODO GetMapByID
                        var arrTxt = self.layersDisplay.getLayer('text').children;
                        for (var k = 0; k < arrTxt.length; k++) {
                            if (arrTxt[k].index == tmpTxt) {
                                tmpTxt = arrTxt[k];
                                tmpTxt.actionClick = e.action;
                            }
                        }

                        if (tmpTxt.hasOwnProperty('alpha')) {
                            self._addBehaviour(tmpTxt, e.type);
                        } else {
                            throw 'Textos nas interactions nao contem na layer';
                        }
                    }
                }

                break;

            default:
        }
    }

};

/**
 * Recursivamente percorre pelo interactions para ver se os pontos deste mesmo possuem outros interactions
 */
Interactions.prototype._manageIntFadePoints = function(int) {

    var ints = this.layersDisplay.getLayer('interactions').children;

    for (var i = 0; i < ints.length; i++) {
        if (ints[i].hasOwnProperty('actionClick')) {
            if (!ints[i]._block) {
                ints[i].addEvents();

                ints[i].once('mousedown', this._handleIntMouse, {
                    ctx: this
                });

                ints[i].once('shutInteractions', this._destroyEvents, {
                    ctx: this,
                    bitmap: ints[i]
                });
            }
        }
    }

};

/**
 * Adiciona eventos comuns da interface e um evento especial para interactions iniciadas por esta classe
 */
Interactions.prototype._addIntEvents = function() {

    var ints = this.layersDisplay.getLayer('interactions').children;

    for (var i = 0; i < ints.length; i++) {
        if (ints[i].hasOwnProperty('actionClick')) {
            if (!ints[i]._block) {
                ints[i].addEvents();

                ints[i].once('mousedown', this._handleIntMouse, {
                    ctx: this
                });

                ints[i].once('shutInteractions', this._destroyEvents, {
                    ctx: this,
                    bitmap: ints[i]
                });
            }
        }
    }

};

/**
 * Remove os eventos instanciados na funcao addIntEvents desta classe
 */
Interactions.prototype._removeIntEvents = function() {

    var ints = this.layersDisplay.getLayer('interactions').children;

    for (var i = 0; i < ints.length; i++) {
        ints[i].emit('shutInteractions');
    }

};

/**
 * Recebe o objeto e o behaviour a ser adicionado
 */
Interactions.prototype._addBehaviour = function(obj, behave) {


    // TODO temporariamente o default é o clickable
    var bhv = behave || 'clickable';

    if (!obj || obj === undefined) {
        throw 'Objeto vazio nao pode receber behaviour';
    }

    switch (bhv) {
        case 'clickable':
            obj.addPlugin(new Clickable());
            obj._selectionEffect = false;
            obj._block = false;
            if (this.layersDisplay.getLayer('interactions').children.indexOf(obj) <= -1) {
                this.layersDisplay.getLayer('interactions').addChild(obj);
                this.layersDisplay.addMap('interactions', obj);
            }
            break;


    }

};

/**
 * Desliga os eventos das interactions configuradas por esta classe
 * E somente utilizada pela funcao _removeIntEvents para nao declarar funcao dentro do evento
 */
Interactions.prototype._destroyEvents = function() {

    // var arrInts = this.ctx.layersDisplay.getLayer('interactions').children;

    this.bitmap.removeEvents();

    this.bitmap.off('mousedown');

    this.bitmap.off('shutInteractions');

    // for (var i = 0; i < arrInts.length; i++) {
    //
    //     if (arrInts[i].hasOwnProperty('actionClick')) {
    //         console.log('removendo eventos no destroy', i);
    //         arrInts[i].removeEvents();
    //     }
    // }

};

/**
 * Configura qual acao vai ser utilizada quando as interactions forem clicadas
 */
Interactions.prototype._handleIntMouse = function(e) {
    if(!this.ctx._autorizeClick) return;
    var self = this;

    e.target._clicked = true;
    if (!e.target._blocked) {

        if (!!e.target.actionClick) {

            if (this.ctx.hasOwnProperty("__" + e.target.actionClick.name)) {

                this.ctx["__" + e.target.actionClick.name](e.target);

            } else {
                throw 'Action ' + e.target.actionClick.name + ' nao encontrada no Interactions';
            }

        } else {
            throw 'Aconteceu algo e nao consegui pegar action para uma interaction';
        }
    }

    if (this.ctx._config.blockClicks) {
        e.target._blocked = true;
    } else {
        e.target._blocked = false;
    }

};

/**
 * Macumba do ddy que manipula a timeline de fadepoints
 * serio
 */
Interactions.prototype._arrangePoints = function(prevFade, nextFade, idSeq) {

    var self = this;

    if (self._config.pointHistory) {
        self._pointTimeline.push({
            fade: prevFade.id,
            points: self._points
        });
    }

    self._points = [];

    for (var i = 0; i < nextFade._config.sequences.length; i++) {

        if (nextFade._config.sequences[i].idSeq === idSeq) {

            self._sequences = [];

            for (var l = 0; l < nextFade._config.sequences[i].order.length; l++) {

                var idNextPoint = nextFade._config.sequences[i].order[l];
                self._points.push(nextFade._points[idNextPoint]);
                self._sequences = self._points;

            }
        }
    }

};

/**
 * ACTION function configurada para ser chamada no mousedown/touchdown
 * Muda o visible dos objetos dentro da array enviada por parametro para true
 */
Interactions.prototype.__show = function(e) {

    var count = 0;
    e = e.actionClick;

    if (!!e.images) {
        for (count = 0; count < e.images.length; count++) {

            var tmpImg = this.layersDisplay.getMapByID('images', e.images[count]);
            tmpImg.alpha = 1;

        }
    }

    if (!!e.txt) {
        for (count = 0; count < e.txt.length; count++) {

            var tmpTxt = this.layersDisplay.getMapByID('text', e.txt[count]);
            tmpTxt.alpha = 1;

        }
    }

};

/**
 * ACTION function configurada para ser chamada no mousedown/touchdown
 * Muda o visible dos objetos dentro da array enviada por parametro para false
 */
Interactions.prototype.__hide = function(e) {

    var count = 0;
    e = e.actionClick;

    if (!!e.images) {
        for (count = 0; count < e.images.length; count++) {

            var tmpImg = this.layersDisplay.getMapByID('images', e.images[count]);
            tmpImg.alpha = 0;

        }
    }

    if (!!e.txt) {
        for (count = 0; count < e.txt.length; count++) {

            var tmpTxt = this.layersDisplay.getMapByID('text', e.txt[count]);
            tmpTxt.alpha = 0;

        }
    }

};

/**
 * ACTION function configurada para ser chamada no mousedown/touchdown
 * Metodo utilizado para forcar o feed negativo ao se clicar em uma das interactions configuradas
 */
Interactions.prototype.__feedFail = function(e) {

    var count = 0;
    this.openFail();

};

/**
 * ACTION function configurada para ser chamada no mousedown/touchdown
 * Metodo chamado pelos objetos clicados para mostrar esta interaction(alpha = 1, visible nao funciona com clicks)
 */
Interactions.prototype.__showSelf = function(e) {

    var count = 0;

    TweenMax.to(e, 1, {
        alpha: 1
    });
    TweenMax.to(e.scale, 0.3, {
        x: 1.5,
        repeat: 1,
        yoyo: true
    });
    TweenMax.to(e.scale, 0.3, {
        y: 1.5,
        repeat: 1,
        yoyo: true
    });


};

/**
 * ACTION function configurada para ser chamada no mousedown/touchdown
 * Metodo chamado para chamar uma sequencia dentro do fadepoint da interaction passada por parametro
 */
Interactions.prototype.__playSequence = function(e) {
    var sameSeq = false;
    this._fadePointCount = 0;

    var prevFade = null;
    var nextFade = null;

    if (this._idSeqAtual == e.actionClick.idSeq) {
        sameSeq = true;
    } else {
        this._idSeqAtual = e.actionClick.idSeq;
    }

    for (var i = 0; i < this._pointController.length; i++) {
        if (this._pointController[i].id === e._idFadePoint) {

            nextFade = this._pointController[i];
        }
        if (this._pointController[i].id === this._fadeAtual) {

            prevFade = this._pointController[i];
        }
    }


    if (this._config.hidePrevPoint) {
        if (this._pointInicial) {
            for (var x = 0; x < this._points.length; x++) {
                this._points[x].forceEnd();
                this._points[x].reset();
                this._points[x].removeListener('pointComplete');
            }
        }

    } else {
        for (var j = 0; j < this._points.length; j++) {
            this._points[j]._timeline.seek(this._points[j]._timeline.duration(), true);
        }
    }

    this._pointInicial = true;

    this._arrangePoints(prevFade, nextFade, this._idSeqAtual);

    // for (i = 0; i < this._points.length; i++) {
    //     console.log("estou aqui........");
    //     this._points[i].forceEnd();
    //     this._points[i].reset();
    //     this._points[i].removeListener('pointComplete');
    // }

    this._fadeAtual = e._idFadePoint;

    if (!!this._SoundManager) {
        this._SoundManager.stop();
        this._SoundManager.removeListener('soundComplete');
    }

    var self = this;

    self._verifyProgress();

    self._customPlay = function() {

        self._firstStart = true;

        var point = self._points[self._fadePointCount];

        if (point !== undefined) {
            if (!point.listeners('pointComplete', true)) {

                point.once('pointComplete', function(e) {

                    self._fadePointCount++;

                    self._customPlay();

                });
            }
            self._points[self._fadePointCount].timeline.play();
        }

    };

    self.popLine = function() {

        this.fadeStop();
        this._points = [];
        if (this._pointTimeline.length > 0) {

            var newLine = this._pointTimeline.pop();

            this._points = newLine.points;

            this._fadeAtual = newLine.fade;

            self._customPlay();

            self.once('sequenceComplete', popLine);

        }
    };

    self._customPlay();

    if (self._config.pointHistory) {
        self.once('sequenceComplete', popLine);
    }


};
