/**
 * @classdesc dimensiona a escala das imagens
 * @memberof module:behavior
 * @exports Scale
 * @constructor
 */
var TweenMax = require('TweenMax');
function Scale() {

    this._selectionEffect = true;
    this._block = false;
    console.log("Scale");
}

Scale.prototype.constructor = Scale;
module.exports = Scale;


Scale.prototype.scaleObj = function(tempo, imgScale)
{
    TweenMax.to(this.scale,tempo,{x:imgScale,y:imgScale});
};

Scale.prototype.scaleXObj = function(tempo, imgScale)
{
    TweenMax.to(this.scale,tempo,{x:imgScale});
};

Scale.prototype.scaleYObj = function(tempo, imgScale)
{
    TweenMax.to(this.scale,tempo,{y:imgScale});
};
