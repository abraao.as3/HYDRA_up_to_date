var PIXI = require('PIXI');
var $ = require('jquery');
var TweenMax = require('TweenMax');
var Bitmap = require('./../display/core/Bitmap');

function Zoom(object) {
    this._rect = new PIXI.Graphics();
    this._scrollMask = new PIXI.Graphics();
    this._rectWhite = new PIXI.Graphics();
    this._container = new PIXI.Container();
    this._container.visible = false;
    this._texture = null;
    if (object !== null && object !== undefined) {
        this._imgZoom = object;
        this._imgZoom.x = 0;
        this._imgZoom.y = 0;
    }

}

Zoom.prototype.constructor = Zoom;
module.exports = Zoom;

Zoom.prototype.zoomOut = function(cfg) {
    this._config = cfg;
    this.initialize(cfg);
};

Zoom.prototype.zoomIn = function(cfg) {
    this._config = cfg;
    this.initialize(cfg);
    this.eventZoomIn(cfg);
};

Zoom.prototype.eventZoomIn = function(cfg) {

    this.createMask(cfg);

    this._container.width = this._sprite.width + 2 * (this._imgZoom.width + this._sprite.width);
    this._container.height = this._sprite.height + 2 * (this._imgZoom.height + this._sprite.height);

    this._originalX = this.x;
    this._originalY = this.y;

    this.parent.addChild(this._container);

    this._container.x = this._originalX + this._sprite.width - this._imgZoom.width;
    this._container.y = this._originalY + this._sprite.height - this._imgZoom.height;

    this._positionW = this._imgZoom.width / this._sprite.width;
    this._positionY = this._imgZoom.height / this._sprite.height;
};

Zoom.prototype.createMask = function() {
    var rect = {
        lineSize: this._config.sizeLine,
        lineColor: this._config.colorBorder,
        lineAlpha: 1,
        width: this._config.wRect,
        height: this._config.hRect,
        roundedAngle: 0
    };


    this._rectWhite.lineStyle(rect.lineSize, rect.lineColor, rect.lineAlpha);

    this._rectWhite.beginFill(0xFFFFFF, 1);
    this._rectWhite.drawRect(0, 0, rect.width, rect.height);
    this._rectWhite.endFill();

    this._scrollMask.lineStyle(0);
    this._scrollMask.beginFill(0x00ff00, 0);
    this._scrollMask.drawRect(0, 0, rect.width - rect.lineSize, rect.height - rect.lineSize);
    this._scrollMask.endFill();

    this._scrollMask.pivot.set(rect.width / 2, rect.height / 2);
    this._rectWhite.pivot.set(this._rectWhite.width / 2, this._rectWhite.height / 2);

    this.addChild(this._container);

    this._imgZoom.visible = true;

    this._container.addChild(this._scrollMask);
    this._imgZoom.mask = this._scrollMask;
    this._container.addChild(this._rectWhite);
    this._container.addChild(this._imgZoom);

};

Zoom.prototype.onButtonDown = function() {
    if (this._config.type == "zoomIn") {
        this._container.visible = true;

    } else if (this._config.type == "zoomOut") {

        this.scale.x = this._config.xScale;
        this.scale.y = this._config.yScale;
    }

};

Zoom.prototype.onButtonUp = function() {
    if (this._config.type == "zoomIn") {
        this._container.visible = false;
    } else if (this._config.type == "zoomOut") {
        this.scale.x = 1;
        this.scale.y = 1;
    }
};

Zoom.prototype.initialize = function(cfg) {
    this.interactive = true;
    this
        .on('mousedown', this.onButtonDown)
        .on('touchstart', this.onButtonDown)
        .on('mousemove', this.onPointerMove)
        .on('mouseup', this.onButtonUp)
        .on('mouseupoutside', this.onButtonUp)
        .on('touchend', this.onButtonUp)
        .on('touchendoutside', this.onButtonUp);
};

Zoom.prototype.onPointerMove = function(eventData) {
    var mouseLocalX = eventData.data.global.x - this._originalX + this._sprite.width / 2;
    var mouseLocalY = eventData.data.global.y - this._originalY + this._sprite.height / 2;

    if (!(mouseLocalX >= 0 &&
            mouseLocalY >= 0 &&
            mouseLocalX <= this._sprite.width &&
            mouseLocalY <= this._sprite.height)) {
        this._container.visible = false;
        return;
    }


    this._rectWhite.x = mouseLocalX;
    this._rectWhite.y = mouseLocalY;
    this._scrollMask.x = mouseLocalX;
    this._scrollMask.y = mouseLocalY;

    this._scrollMask.visible = true;


    this._imgZoom.x = mouseLocalX + (this._imgZoom.width / 2 - (mouseLocalX * this._positionW));
    this._imgZoom.y = mouseLocalY + (this._imgZoom.height / 2 - (mouseLocalY * this._positionY));
};
