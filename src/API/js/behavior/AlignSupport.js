/**
* @module behavior
*/
/**
* @classdesc Plugin que dá suporte ao alinhamento de objetos
* @memberof module:behavior
* @exports AlignSupport
* @constructor
*/
function AlignSupport() {
    /**
    * Objeto que será alinhado em relação ao objeto que adiciona o plugin
    * @member {PIXI.DisplayObject}
    * @default null
    */
    this.objectToAlign = null;
    this.atualAlign = {x: "left", y: "left"};
}

AlignSupport.prototype.constructor = AlignSupport;
module.exports = AlignSupport;

/**
* Alinha o objeto definido na horizontal e na vertical em relação ao objeto extendido
* @example
* this.objectToAlign = new PIXI.Container();
*
* this.align(AlignSupport.HORIZONTAL.LEFT, AlignSupport.VERTICAL.CENTER);
*
* @param horizontal {AlignSupport.HORIZONTAL} o alinhamento horizontal do objeto
* @param vertical {AlignSupport.VERTICAL} o alinhamento vertical do objeto
*/
AlignSupport.prototype.align = function(horizontal, vertical){
    switch (horizontal) {
        case 'left':
            this._left();
            break;
        case 'center':
            this._hcenter();
            break;
        case 'right':
            this._right();
            break;

    }

    switch (vertical) {
        case 'top':
            this._top();
            break;
        case 'center':
            this._vcenter();
            break;
        case 'bottom':
            this._bottom();
            break;

    }

    this.atualAlign.x = horizontal;
    this.atualAlign.y = vertical;
};

/**
* Alinha o objeto pela esquerda
* @private
*/
AlignSupport.prototype._left = function(){
    this.objectToAlign.x = 0;
};

/**
* Alinha o objeto pela direita
* @private
*/
AlignSupport.prototype._right = function(){
    this.objectToAlign.x = -this.objectToAlign.width;
};

/**
* Alinha o objeto pelo centro horizontal
* @private
*/
AlignSupport.prototype._hcenter = function(){
    this.objectToAlign.x = -Math.round(this.objectToAlign.width / 2);
};

/**
* Alinha o objeto pelo topo
* @private
*/
AlignSupport.prototype._top = function(){
    this.objectToAlign.y = 0;
};

/**
* Alinha o objeto por baixo
* @private
*/
AlignSupport.prototype._bottom = function(){
    this.objectToAlign.y = -this.objectToAlign.height;
};

/**
* Alinha o objeto pelo centro verticals
* @private
*/
AlignSupport.prototype._vcenter = function(){
    this.objectToAlign.y = -Math.round(this.objectToAlign.height / 2);
};

/**
* @constant
* @member {Object}
* @property {String} LEFT left
* @property {String} CENTER center
* @property {String} RIGHT right
*/
AlignSupport.HORIZONTAL = {
    LEFT: 'left',
    CENTER: 'center',
    RIGHT: 'right'
};

/**
* @constant
* @member {Object}
* @property {String} TOP top
* @property {String} CENTER center
* @property {String} BOTTOM bottom
*/
AlignSupport.VERTICAL = {
    TOP: 'top',
    CENTER: 'center',
    BOTTOM: 'bottom'
};
