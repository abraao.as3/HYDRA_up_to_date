var Outline = require('../utils/Outline');
var PIXI = require('PIXI');
var PIXIConfig = require('../core/PIXIConfig');
var Const = require('../core/Const');
var TweenMax = require('TweenMax');

function Contour(){
    this._contourn = null;
}

Contour.prototype.constructor = Contour;
module.exports = Contour;

Contour.prototype.createContour = function(thickness, color){
    this.contourTxt(thickness, color);
    // var self = this;
    //
    // var rt1 = PIXI.RenderTexture.create(Const.BASE_WIDTH, Const.BASE_HEIGHT);
    // PIXIConfig.renderer.render(this, rt1);
    //
    // var sp = new PIXI.Sprite(rt1);
    // var container = new PIXI.Container();
    //
    // var _bounds = this.getBounds();
    // this._baseX = _bounds.x;
    // this._baseY = _bounds.y;
    //
    // sp.x = -_bounds.x;
    // sp.y = -_bounds.y;
    //
    // container.addChild(sp);
    //
    // var rt2 = PIXI.RenderTexture.create(this.width, this.height);
    // PIXIConfig.renderer.render(container, rt2);
    //
    // var src = PIXIConfig.renderer.extract.image(rt2);
    // document.body.appendChild(src);
    //
    // container.destroy();
    // sp.destroy(true);
    // rt1.destroy(true);
    // rt2.destroy(true);
    //
    // new Outline(src, thickness, color, function(res){
    //     self._contourn = res;
    //
    //     var p = self.toLocal(new PIXI.Point(self._baseX, self._baseY));
    //     if(isNaN(p.x)){
    //         var b = self.getLocalBounds();
    //         p = {x: b.x, y: b.y};
    //     }
    //     res.x = p.x - thickness;
    //     res.y = p.y - thickness;
    //     res.alpha = 0;
    //     self.addChildAt(res, 0);
    //     self.emit('contourComplete');
    // });
};

Contour.prototype.contourTxt = function(thickness, color){
    var self = this;

    var rt1 = PIXI.RenderTexture.create(Const.BASE_WIDTH, Const.BASE_HEIGHT);

    PIXIConfig.renderer.render(this, rt1);
    var _bounds = self.getBounds();
    self._baseX = _bounds.x;
    self._baseY = _bounds.y;

    var flp = PIXIConfig.renderer.extract.canvas(rt1);
    new Outline(flp, thickness, color, function(res){
        self._contourn = res;

        var p = self.toLocal(new PIXI.Point(self._baseX, self._baseY));
        if(isNaN(p.x)){
            var b = self.getLocalBounds();
            p = {x: b.x, y: b.y};
        }
        res.x = p.x - thickness;
        res.y = p.y - thickness;
        res.alpha = 0;
        self.addChildAt(res, 0);
        self.emit('contourComplete');
    }, _bounds);

};

Contour.prototype.openContour = function(cor){
    if(this._contourn === undefined || this._contourn === null) return;
    if(cor !== undefined)
    {
        console.log('olha a cor aeee');
        this._contourn.tint = cor;
    }

    console.log("Estou aqui................");

    this._contourn.alpha = 0;
    TweenMax.to(this._contourn, 0.3, {alpha: 1});
};

Contour.prototype.closeContour = function(){
    if(this._contourn === undefined || this._contourn === null) return;
    
    TweenMax.to(this._contourn, 0.3, {alpha: 0});
};
