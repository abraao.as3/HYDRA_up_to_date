var TweenMax = require('TweenMax');
var PIXI = require('PIXI');

/**
 * @classdesc Plugin animação pulsar
 * @memberof module:behavior
 * @exports Pulsating
 * @constructor
 */
function Pulsating(){
}

Pulsating.prototype.constructor = Pulsating;
module.exports = Pulsating;

    /**
    * Faz uma animação constante no formato de uma pulsação
    * @public
    */
Pulsating.prototype.animate = function(){
    this._initialScale = new PIXI.Point(this.scale.x, this.scale.y);
    this.animating = true;

    this._tween = new TweenMax(this.scale, 0.3, {x: this._initialScale.x + 0.2, y: this._initialScale.y + 0.2, yoyo: true, repeat: -1});
	this._tween.play();
};

/**
 * Para a animação de pulso
 * @public
 */
Pulsating.prototype.stopAnimation = function(){
	if(this._tween.isActive()) {
		this._tween.kill();
		this.scale.set(this._initialScale.x, this._initialScale.y);
	}
};
