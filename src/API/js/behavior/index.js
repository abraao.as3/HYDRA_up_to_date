var behavior = module.exports = {

    AlignSupport: require('./AlignSupport'),

    Clickable: require('./Clickable'),

    FadePoint: require('./FadePoint'),

    Interactions: require('./Interactions'),

    Dragable: require('./Dragable'),

    Contour: require('./Contour'),

    Pulsating: require('./Pulsating'),

    Particles: require('./Particles'),

    Bounding: require('./Bounding'),

    Sensor: require('./Sensor'),

	Rotable: require('./Rotable'),

	Scale: require('./Scale'),

    Zoom: require('./Zoom'),

	ViewState: require('./ViewState')

};
