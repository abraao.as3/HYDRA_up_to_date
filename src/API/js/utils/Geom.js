/**
  * @classdesc Classe que define os poligonos
  * @memberof module:utils
  * @extends Pixi.Container
  * @constructor
  * @param grid {Object} grade onde os pontos irão fixar
  * @param star {Object} ponto de inicio
  */
function Geom() {

}

Geom.prototype.constructor = Geom;
module.exports = Geom;

Geom.d3_geom_contourDx = [1, 0, 1, 1, -1, 0, -1, 1, 0, 0, 0, 0, -1, 0, -1, NaN];
Geom.d3_geom_contourDy = [0, -1, 0, 0, 0, -1, 0, 0, 1, -1, 1, 1, 0, -1, 0, NaN];

Geom.contour = function(grid, start) {
    var s = start || Geom.d3_geom_contourStart(grid), // starting point
        c = [], // Contorno polígono
        x = s[0], // Posição x atual
        y = s[1], // Posição y atual
        dx = 0, // Próxima direção x
        dy = 0, // Próxima direção y
        pdx = NaN, // Direção x anterior
        pdy = NaN, // Direção y anterior
        i = 0;

    do {
        /**
        *Determinar índice de quadrados de marcha
        * @memberof module:utils.Geom
        * @type {object}
        */
        i = 0;
        if (grid(x - 1, y - 1)) i += 1;
        if (grid(x, y - 1)) i += 2;
        if (grid(x - 1, y)) i += 4;
        if (grid(x, y)) i += 8;
        /**
        * Determine a próxima direção
        * @memberof module:utils.Geom
        * @type {object}
        */

        if (i === 6) {
            dx = pdy === -1 ? -1 : 1;
            dy = 0;
        } else if (i === 9) {
            dx = 0;
            dy = pdx === 1 ? -1 : 1;
        } else {
            dx = Geom.d3_geom_contourDx[i];
            dy = Geom.d3_geom_contourDy[i];
        }
        /**
        *Atualizar contorno polígonoção
        * @memberof module:utils.Geom
        * @type {object}
        */
        if (dx != pdx && dy != pdy) {
            c.push([x, y]);
            pdx = dx;
            pdy = dy;
        }

        x += dx;
        y += dy;
    } while (s[0] != x || s[1] != y);

    return c;
};

Geom.d3_geom_contourStart = function(grid) {
    var x = 0,
        y = 0;
        /**
         Procurar um ponto de partida; Começar na origem
         E proceder ao longo de diagonais de expansão externa
        * @memberof module:utils.Geom
        * @type {object}
        */

    while (true) {
        if (grid(x, y)) {
            return [x, y];
        }
        if (x === 0) {
            x = y + 1;
            y = 0;
        } else {
            x = x - 1;
            y = y + 1;
        }
    }
};
