var PIXI = require('PIXI');
/**
 *
 * @class
 * @extends Pixi.Container
 * @memberof module:utils
  */
function Sillhuette() {

}

Sillhuette.prototype.constructor = Sillhuette;
module.exports = Sillhuette;

Sillhuette.draw = function(src, colorRGB) {
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    canvas.width = src.width;
    canvas.height = src.height;

    ctx.drawImage(src, 0, 0);

    var imgData = ctx.getImageData(0, 0, src.width, src.height);
    var pix = imgData.data;
    var red = colorRGB[0];
    var green = colorRGB[1];
    var blue = colorRGB[2];

    for (var i = 0, n = pix.length; i < n; i += 4) {
        // if (i > 3) {
        //     if ((Math.abs(pix[i - 3] - pix[i]) > 10) &&
        //         (Math.abs(pix[i - 2] - pix[i + 1]) > 10) &&
        //         (Math.abs(pix[i - 1] - pix[i + 2]) > 10)
        //     ) {
        //
        //         pix[i] = red;
        //         pix[i + 1] = green;
        //         pix[i + 2] = blue;
        //
        //     }
        // } else {
        //     if (pix[i] < 250 && pix[i + 1] < 250 && pix[i + 2] < 250) {
        //         pix[i] = red;
        //         pix[i + 1] = green;
        //         pix[i + 2] = blue;
        //     }
        // }
        pix[i] = red;
        pix[i + 1] = green;
        pix[i + 2] = blue;
        pix[i + 3] = pix[i + 3];
    }

    ctx.putImageData(imgData, 0, 0);

    return  PIXI.Texture.fromCanvas(canvas);
};

Sillhuette.shadow = function(src){
    var canvas = document.createElement('canvas');
    var ctx = canvas.getContext('2d');

    canvas.width = src.width;
    canvas.height = src.height;

    ctx.shadowBlur = 15;
    ctx.shadowColor = "black";
    ctx.shadowOffsetX = 0;
    ctx.shadowOffsetY = 0;

    ctx.drawImage(src, 0, 0);

    return  PIXI.Texture.fromCanvas(canvas);
};
