var $ = require('jquery');
var PIXI = require('PIXI');
var Geom = require('./Geom');
var PIXIConfig = require('../core/PIXIConfig');

function Outline(src, thickness, color, responder, canvasCropConfig) {
    var self = this;

    this._responder = responder;
    this._color = "#" + color.toString(16).toUpperCase();
    this._thickness = thickness;
    this._src = src;

    this._canvas = document.createElement('canvas');
    this._ctx = this._canvas.getContext("2d");

    this._canvases = [];
    this._imageData = null;
    this._data = null;
    this._imageData1 = null;
    this._data1 = null;

    this._canvasW = null;
    this._canvasH = null;

    this._img = new Image();
    this._img.onload = function() {
        self._start();
    };

    if(canvasCropConfig){
        var canvasSupport = document.createElement('canvas');
        canvasSupport.width = canvasCropConfig.width;
        canvasSupport.height = canvasCropConfig.height;

        var imgSupport = new Image();
        imgSupport.onload = function(){
            canvasSupport.getContext("2d").drawImage(imgSupport,
                canvasCropConfig.x,
                canvasCropConfig.y,
                canvasCropConfig.width,
                canvasCropConfig.height,
                0,
                0,
                canvasCropConfig.width,
                canvasCropConfig.height);

            self._img.src = canvasSupport.toDataURL();

            canvasSupport.getContext("2d").clearRect(0, 0, canvasSupport.width, canvasSupport.height);
            canvasSupport = null;

            src.getContext("2d").clearRect(0, 0, src.width, src.height);
            src = null;
        };

        imgSupport.src = src.toDataURL();
    }
    else{
        this._img.src = this._src.src;
    }
}

Outline.prototype.constructor = Outline;
module.exports = Outline;

Outline.prototype._start = function() {
    // resize the main canvas to the image size
    this._canvas.width = this._canvasW = this._img.width + this._thickness * 2;
    this._canvas.height = this._canvasH = this._img.height + this._thickness * 2;

    // draw the image on the main canvas
    this._ctx.drawImage(this._img, this._thickness, this._thickness);

    // Move every discrete element from the main canvas to a separate canvas
    // The sticker effect is applied individually to each discrete element and
    // is done on a separate canvas for each discrete element
    while (this._moveDiscreteElementToNewCanvas()) {}

    // add the sticker effect to all discrete elements (each canvas)
    for (var i = 0; i < this._canvases.length; i++) {
        this._addStickerEffect(this._canvases[i], this._thickness);
        this._ctx.drawImage(this._canvases[i], 0, 0);
    }

    // redraw the original image
    //   (necessary because the sticker effect
    //    slightly intrudes on the discrete elements)
    this._ctx.drawImage(this._img, this._thickness, this._thickness);

    var temp = new PIXI.Sprite(PIXI.Texture.fromCanvas(this._canvas));

    var rt = PIXI.RenderTexture.create(temp.width, temp.height);
    PIXIConfig.renderer.render(temp, rt);

    var res = new PIXI.Sprite(rt);
    this._responder(res);

    temp.destroy(true);

    for(var j = 0; j < this._canvases.length; j++){
        this._canvases[j].getContext("2d").clearRect(0, 0, this._canvases[j].width, this._canvases[j].height);
        $(this._canvases[j]).remove();
    }

    this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
    $(this._canvas).remove();
};

// true/false function used by the edge detection method
Outline.prototype._defineNonTransparent = function(x, y) {
    return (this._data1[(y * this._canvasW + x) * 4 + 3] > 0);
};

// This function finds discrete elements on the image
// (discrete elements == a group of pixels not touching
//  another groups of pixels--e.g. each individual sprite on
//  a spritesheet is a discreet element)
Outline.prototype._moveDiscreteElementToNewCanvas = function() {
    var self = this;
    // get the imageData of the main canvas
    this._imageData = this._ctx.getImageData(0, 0, this._canvas.width, this._canvas.height);
    this._data1 = this._imageData.data;

    // test & return if the main canvas is empty
    // Note: do this b/ geom.contour will fatal-error if canvas is empty
    var hit = false;
    for (var i = 0; i < this._data1.length; i += 4) {
        if (this._data1[i + 3] > 0) {
            hit = true;
            break;
        }
    }

    if (!hit) {
        return false;
    }

    // get the point-path that outlines a discrete element
    var points = Geom.contour(function(x, y) {
        return self._defineNonTransparent(x, y);
    });

    var newCanvas = document.createElement('canvas');
    newCanvas.style.position = 'absolute';
    newCanvas.width = this._canvas.width;
    newCanvas.height = this._canvas.height;
    $('body').prepend(newCanvas);
    this._canvases.push(newCanvas);
    var newCtx = newCanvas.getContext('2d');

    // attach the outline points to the new canvas (needed later)
    newCanvas.outlinePoints = points;

    // draw just that element to the new canvas
    this._defineGeomPath(newCtx, points);
    newCtx.save();
    newCtx.clip();
    newCtx.drawImage(this._canvas, 0, 0);
    newCtx.restore();

    // remove the element from the main canvas
    this._defineGeomPath(this._ctx, points);
    this._ctx.save();
    this._ctx.clip();
    this._ctx.globalCompositeOperation = "destination-out";
    this._ctx.clearRect(0, 0, this._canvas.width, this._canvas.height);
    this._ctx.restore();

    $(newCanvas).remove();

    return true;
};

// utility function
// Defines a path on the canvas without stroking or filling that path
Outline.prototype._defineGeomPath = function(context, points) {
    context.beginPath();
    context.moveTo(points[0][0], points[0][1]);
    for (var i = 1; i < points.length; i++) {
        context.lineTo(points[i][0], points[i][1]);
    }
    context.lineTo(points[0][0], points[0][1]);
    context.closePath();
};

Outline.prototype._addStickerEffect = function(canvas, strokeWeight) {
    var url = canvas.toDataURL();
    var ctx1 = canvas.getContext('2d');
    var pts = canvas.OutlinePoints;
    this._addStickerLayer(ctx1, pts, strokeWeight);
    var imgx = new Image();
    imgx.onload = function() {
        ctx1.drawImage(imgx, 0, 0);
    };
    imgx.src = url;
};

Outline.prototype._addStickerLayer = function(context, _points, weight) {
    var self = this;
    this._imageData = context.getImageData(0, 0, this._canvas.width, this._canvas.height);
    this._data1 = this._imageData.data;

    var points = Geom.contour(function(x, y){
        return self._defineNonTransparent(x,y);
    });

    this._defineGeomPath(context, points);
    context.lineJoin = 'round';
    context.lineCap = 'round';
    context.strokeStyle = this._color;
    context.lineWidth = weight;
    context.stroke();
};
