
/**
* @class
* @memberof module:utils
*/
function OrderModule(){

}

OrderModule.prototype.constructor = OrderModule;
module.exports = OrderModule;

OrderModule.selectionPattern = /[\W \w \d]*\?select_module=/ig;

/**
* @param URL {String} location completo contendo o módulo solicitado
* @return {Array} retorna um array com o módulo construido
*/
OrderModule.getModule = function(URL){
    var indexEx;
    var simpleURL;
    if (URL.indexOf("indexEx")!== -1) {

        simpleURL = URL.slice(0, URL.indexOf("&indexE"));
        indexEx = URL.slice(URL.indexOf("xEx=") + 4);


    }else {

        simpleURL = URL;
        indexEx = -1;

    }
    var selection = simpleURL.replace(OrderModule.selectionPattern, '').split(',');

    var result = [];

    if(isNaN(parseInt(selection[0])))
    {
        return {telas:selection, indexEx: parseInt(indexEx)};
    }

    for(var i = 0; i < selection.length; i++)
    {
        if(selection[i].indexOf('_') != -1)
        {
            var values = selection[i].split('_');
            for(var j = parseInt(values[0]); j <= parseInt(values[1]); j++)
            {
                result.push(j);
            }

            continue;
        }
        result.push(parseInt(selection[i]));
    }

    return {telas:result, indexEx: parseInt(indexEx)};
};
