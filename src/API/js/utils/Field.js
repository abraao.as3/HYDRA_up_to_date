/**
* @module utils
*/
var PIXI = require('PIXI');
var TweenMax = require('TweenMax');


/**
 * Classe que define o CrosField e sua exibição
 *
 * @class
 * @extends Pixi.Container
 * @memberof module:utils
 * @param _w {number} largura largura do retangulo criado para o crossfield
 * @param _h {number} altura altura do retangulo criado para o crossfield
 */
function Field(_w, _h) {
    PIXI.Container.call(this);

    this._bounds = new PIXI.Rectangle(0, 0, _w, _h);

    this._back = new PIXI.Graphics();
    this._back.lineStyle(2, 0, 1);
    this._back.beginFill(0, 1);
    this._back.drawRect(0, 0, this._bounds.width, this._bounds.height);
    this._back.endFill();
    this._back.alpha = 0;
    this._back.pivot.set(this._back.width / 2, this._back.height / 2);

    this._border = new PIXI.Graphics();
    this._border.beginFill(0xFFFF33, 1);
    this._border.drawRect(0, 0, this._bounds.width, this._bounds.height);
    this._border.endFill();
    this._border.alpha = 0;
    this._border.pivot.set(this._border.width / 2, this._border.height / 2);

    this._sensibility = new PIXI.Graphics();
    this._sensibility.lineStyle(2, 0xff0000, 1);
    this._sensibility.beginFill(0xffffff, 0);
    this._sensibility.drawRect(0, 0, this._bounds.width - 10, this._bounds.height - 10);
    this._sensibility.endFill();
    this._sensibility.alpha = 0;
    this._sensibility.pivot.set(this._sensibility.width / 2, this._sensibility.height / 2);

    this.addChild(this._back);
    this.addChild(this._border);
    this.addChild(this._sensibility);

    this._isOn = false;
}

Field.prototype = Object.create(PIXI.Container.prototype);
Field.prototype.constructor = Field;
module.exports = Field;


Object.defineProperties(Field.prototype, {

    /**
    * (GET SET) insere dadaos modifica se está se o campo está ativo ou não.
    * @memberof module:utils.Field
    * @type {object}
    */
    turnOn: {
        get: function() {
            return this._isOn;
        },
        set: function(value) {
            var self = this;

            if (value === true) {
                this._border.visible = true;
                TweenMax.to(this._border, 0.2, {
                    alpha: 1
                });

                this._isOn = true;
            } else {
                this._isOn = false;
                TweenMax.to(this._border, 0.2, {
                    alpha: 0,
                    onComplete: function() {
                        self._border.visible = false;
                    }
                });
            }
        }
    },

    /**
    * permite o campo ficar visivel ou invisivel
    * @memberof module:utils.Field
    * @type {boolean}
    *@param value {boolean}
    *@public
    */
    debug: {
        set: function(value) {
            if (value === true) {
                this._back.alpha = 0.3;
                this._sensibility.alpha = 0.3;
            } else {
                this._back.alpha = 0;
                this._sensibility.alpha = 0;
            }
        }
    },
    /**
    * verifica o tamanho do retangulo permitindo receber ou não objetos.
    * @memberof module:utils.Field
    * @type {objetc}
    *@param value {boolean}
    *@public
    */
    sensibilityArea: {
        get: function() {
            return new PIXI.Rectangle(0, 0, this._sensibility.width, this._sensibility.height);
        },

        set: function(value) {
            this._sensibility.width = value.width;
            this._sensibility.height = value.height;
        }
    },
    /**
    * verifica o limite da area.
    * @memberof module:utils.Field
    * @type {objetc}
    *@public
    */
    lineColor: {
        get: function() {
            return this._border.lineColor;
        },

        set: function(value) {
            this._border.lineColor = value;
        }
    },
    /**
    * verifica o espaço.
    * @memberof module:utils.Field
    * @type {objetc}
    *@public
    */
    sensor: {
        get: function() {
            return this._sensibility;
        }
    }
});
/**
 * Método de dispose do crossField e suas dependencias para liberação da memória
 * @memberof module:utils.Field
 * @public
 */
Field.prototype.destroy = function() {
    this.removeChildren();
    this.removeAllListeners();

    TweenMax.killTweensOf(this._border);

    this._back.destroy();
    this._back = null;
    this._border.destroy();
    this._border = null;
    this._sensibility.destroy();
    this._sensibility = null;

    PIXI.Container.prototype.destroy.call(this);
};
