/**
 * @module ui
 */

var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');
var Const = require('../../core/Const');
var TweenMax = require('TweenMax');

/**
 * @classdesc Representa a calculator
 * @memberof module:ui/interfaces
 * @exports AbstractInterface
 * @constructor
 */
function AbstractInterface() {
    ObjectBase.call(this);

    this._videoPlayer = null;
    this._keyboard = null;
    this._paint = null;
    this._ballon = null;
    this._calculator = null;
    this._dataBoard = null;

    this._rightAnalogController = null;
    this._leftAnalogController = null;
    this._rightDigitalController = null;
    this._leftDigitalController = null;

	this._configBotao = null;
	this._configMenu = null;

	this._brightSim = null;

    this._ajudaBotao = null;
    this._ajudaMenu = null;
    this._closeButton = null;
    this._navegacaoSeguinte = null;
    this._navegacaoAnterior = null;
    this._showButtons = false;

	this._navegacaoAnteriorInativo = null;
	this._navegacaoSeguinteInativo = null;
	this._navegacaoButtons = null;
	this._logobar = null;
	this._titulo = null;

    this._refreshButton = null;

    this._backPontos = null;
    this._pontos = null;

    this._particleObject = null;

    this._hand = null;
    this._clickingHand = null;
    this._showHand = true;
}

AbstractInterface.prototype = Object.create(ObjectBase.prototype);
AbstractInterface.prototype.constructor = AbstractInterface;
module.exports = AbstractInterface;

Object.defineProperties(AbstractInterface.prototype, {
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    videoPlayer: {
        get: function() {
            return this._videoPlayer;
        },

        set: function(value) {
            this._videoPlayer = value;
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    navegacaoAnterior: {
        get: function() {
            return this._navegacaoAnterior;
        },
        set: function(value) {
            this._navegacaoAnterior = value;
            this._navegacaoAnterior.addPlugin(new behavior.Clickable());
            this._navegacaoAnterior.addEvents();

            this._navegacaoAnterior.align("center", "center");


            // TODO sustituir o 524 por window.height
            this._navegacaoAnterior.y += Const.BASE_HEIGHT - this._navegacaoAnterior.height / 2;
            this._navegacaoAnterior.x = this._navegacaoAnterior.width / 2;

            this._navegacaoAnterior.visible = false;

            this.addChild(value);
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    navegacaoSeguinte: {
        get: function() {
            return this._navegacaoSeguinte;
        },
        set: function(value) {
            this._navegacaoSeguinte = value;
            this._navegacaoSeguinte.addPlugin(new behavior.Clickable());
            this._navegacaoSeguinte.addEvents();

            this._navegacaoSeguinte.align("center", "center");

            this._navegacaoSeguinte.x = Const.BASE_WIDTH - this._navegacaoSeguinte.width / 2;
            this._navegacaoSeguinte.y += Const.BASE_HEIGHT - this._navegacaoSeguinte.height / 2;

            this._navegacaoSeguinte.visible = false;
            this.addChild(value);
        },
        configurable: true
    },
	navegacaoAnteriorInativo: {
        get: function() {
            return this._navegacaoAnteriorInativo;
        },
        set: function(value) {
            this._navegacaoAnteriorInativo = value;

			this._navegacaoAnteriorInativo.align("center", "center");

            this._navegacaoAnteriorInativo.y += Const.BASE_HEIGHT - this._navegacaoAnteriorInativo.height / 2;
            this._navegacaoAnteriorInativo.x = this._navegacaoAnteriorInativo.width / 2;

            this._navegacaoAnteriorInativo.visible = false;

            this.addChild(value);
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    navegacaoSeguinteInativo: {
        get: function() {
            return this._navegacaoSeguinteInativo;
        },
        set: function(value) {
            this._navegacaoSeguinteInativo = value;

            this._navegacaoSeguinteInativo.align("center", "center");

            this._navegacaoSeguinteInativo.x = Const.BASE_WIDTH - this._navegacaoSeguinteInativo.width / 2;
            this._navegacaoSeguinteInativo.y += Const.BASE_HEIGHT - this._navegacaoSeguinteInativo.height / 2;

            this._navegacaoSeguinteInativo.visible = false;
            this.addChild(value);
        },
        configurable: true
    },
    navegacaoButtons: {
		get: function() {
			return this._navegacaoButtons;
		},
		set: function(arr) {
			this._navegacaoButtons = arr;

			for(var navElement in this._navegacaoButtons) {
				if(navElement === 'navBackground') {
					this._navegacaoButtons[navElement].visible = false;
					this.addChildAt(this._navegacaoButtons[navElement], 0);
				} else {
					var navBtn = this._navegacaoButtons[navElement];

					if((navBtn.ativo !== undefined && navBtn.ativo !== null) || (navBtn.atual !== undefined && navBtn.atual !== null)) {
						navBtn.padrao.addPlugin(new behavior.ViewState());

						if(navBtn.ativo !== undefined && navBtn.ativo !== null && navBtn.atual !== undefined && navBtn.atual !== null) {
							if(navBtn.ativo.hasClick === true) {
								navBtn.ativo.addPlugin(new behavior.Clickable());
							}

							navBtn.padrao
								.addViewState('btnAtivo')
									.addStateListener(null, null, navBtn.padrao, 'btnAtivo')
										.addStateObject(null, null, navBtn.ativo, true)
											.addTargetStateSettings({alpha: 1})
											.addInitialStateSettings({alpha: 0})
										.initializeStateObject()
										.addStateObject(null, null, navBtn.atual, true)
											.addTargetStateSettings({alpha: 0})
											.addInitialStateSettings({alpha: 0})
										.initializeStateObject()
								.addViewState('btnAtual')
									.addStateListener(null, null, navBtn.padrao, 'btnAtual')
									.addStateObject(null, null, navBtn.ativo)
										.addTargetStateSettings({alpha: 0})
									.initializeStateObject()
										.addStateObject(null, null, navBtn.atual)
											.addTargetStateSettings({alpha: 1})
										.initializeStateObject();
						} else {
							if(navBtn.ativo !== undefined && navBtn.ativo !== null) {
								if(navBtn.ativo.hasClick === true) {
									navBtn.ativo.addPlugin(new behavior.Clickable());
								}

								navBtn.padrao.addViewState('btnAtivo')
												.addStateObject(null, null, navBtn.ativo, true)
													.addStateListener(null, null, navBtn.padrao, 'btnAtivo')
													.addTargetStateSettings({alpha: 1})
													.addInitialStateSettings({alpha: 0})
												.initializeStateObject();
							} else if(navBtn.atual !== undefined && navBtn.atual !== null) {
								navBtn.padrao.addViewState('btnAtual')
												.addStateObject(null, null, navBtn.atual, true)
													.addStateListener(null, null, navBtn.padrao, 'btnAtual')
													.addTargetStateSettings({alpha: 1})
													.addInitialStateSettings({alpha: 0})
												.initializeStateObject();
							}
						}

						navBtn.padrao.addStateListeners();
					}

					navBtn.padrao.visible = false;
					this.addChild(navBtn.padrao);
				}
			}
		}
	},
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    refreshButton: {
        get: function() {
            return this._refreshButton;
        },

        set: function(value) {
            this._refreshButton = value;

            this._refreshButton.addPlugin(new behavior.Clickable());
            this._refreshButton.align("center", "center");

            this._refreshButton.x = Const.BASE_WIDTH - this._refreshButton.width / 2;
            this._refreshButton.y = this._refreshButton.height / 2;

            this._refreshButtonvisible = false;
            this._refreshButton.alpha = 0;
            this.addChild(this._refreshButton);
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    closeButton: {
        get: function(){
            return this._closeButton;
        },

        set: function(value){
            this._closeButton = value;
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    particleObject: {
        get: function() {
            return this._particleObject;
        },

        set: function(value) {

            this._particleObject = value.bitmap;
            this._particleObject.addPlugin(new behavior.Particles(this._particleObject, Const.APPConfig.exerciseStarAnimationCFG, value.texture));

            this._particleObject.align("center", "center");

            this._particleObject.x = Const.BASE_WIDTH / 2;
            this._particleObject.y = Const.BASE_HEIGHT / 2;

            this._particleObject.visible = true;
            this._particleObject.alpha = 1;
            this.addChild(this._particleObject);
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    hand: {
        get: function() {
            return this._hand;
        },

        set: function(value) {
            this._hand = value;
            this._hand.visible = false;
            this.addChild(value);
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    showButtons: {
        get: function() {
            return this._showButtons;
        },

        set: function(value) {
            this._showButtons = value;
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    clickingHand: {
        get: function() {
            return this._clickingHand;
        },

        set: function(value) {
            this._clickingHand = value;
            this._clickingHand.visible = false;
            this.addChild(value);
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    calculator: {
        get: function() {
            return this._calculator;
        },

        set: function(value) {
            this._calculator = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    keyboard: {
        get: function() {
            return this._keyboard;
        },

        set: function(value) {
            this._keyboard = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    paint: {
        get: function() {
            return this._paint;
        },

        set: function(value) {
            this._paint = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Object}
     */
    dataBoard: {
        get: function() {
            return this._dataBoard;
        },

        set: function(value) {
            this._dataBoard = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    ballon: {
        get: function() {
            return this._ballon;
        },

        set: function(value) {
            this._ballon = value;

            this.addChild(value);
            value.visible = false;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    rightAnalogController: {
        get: function() {
            return this._rightAnalogController;
        },

        set: function(value) {
            this._rightAnalogController = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    leftAnalogController: {
        get: function() {
            return this._leftAnalogController;
        },

        set: function(value) {
            this._leftAnalogController = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    leftDigitalController: {
        get: function() {
            return this._leftDigitalController;
        },

        set: function(value) {
            this._leftDigitalController = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    rightDigitalController: {
        get: function() {
            return this._rightDigitalController;
        },

        set: function(value) {
            this._rightDigitalController = value;
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    configBotao: {
        get: function() {
            return this._configBotao;
        },
		set: function(value) {

            var self = this;

            this._configBotao = value;
            this._configBotao.addPlugin(new behavior.Clickable());
            this._configBotao.addEvents();

            this._configBotao.on("clicked", function() {
				self.configMenu.visible = !self.configMenu.visible;

				// self._ajudaBotao.visible = !self._ajudaBotao.visible;
				// if(!self._configMenu.visible && self._ajudaMenu.visible) {
				// 	self._ajudaMenu.visible = self._configMenu.visible;
				// }
                // self.ajudaMenu.interactive = !self.ajudaMenu.interactive;
				//
                // self._refreshButton.visible = false;
				//
				// if (!self.ajudaMenu.visible && self.refreshIsActive === true) {
                //     self._refreshButton.visible = true;
                // }
            });

            this._configBotao.align('center', 'center');
            this._configBotao.x += this._configBotao.width;
            this._configBotao.y += this._configBotao.height * 3;

            this._configBotao.visible = false;
            this.addChild(value);
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
	configMenu: {
		get: function() {
			return this._configMenu;
		},

		set: function(value) {
			this._configMenu = value;
            // this._configMenu.align('center', 'center');
            // this._configMenu.x += this._configMenu.width / 2;
            // this._configMenu.y += this._configMenu.height / 2;
            this._configMenu.visible = false;

            this.addChild(value);
		},
        configurable: true
	},

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    ajudaBotao: {
        get: function() {
            return this._ajudaBotao;
        },

        set: function(value) {

            var self = this;

            this._ajudaBotao = value;
            this._ajudaBotao.addPlugin(new behavior.Clickable());
            this._ajudaBotao.addEvents();

            this._ajudaBotao.on("clicked", function() {
				self.ajudaMenu.visible = !self.ajudaMenu.visible;
                self.ajudaMenu.interactive = !self.ajudaMenu.interactive;

                self._refreshButton.visible = false;

				if (!self.ajudaMenu.visible && self.refreshIsActive === true) {
                    self._refreshButton.visible = true;
                }
            });
            this._ajudaBotao.align('center', 'center');
            this._ajudaBotao.x += (this._ajudaBotao.width / 2);
            this._ajudaBotao.y += (this._ajudaBotao.height / 2);

            this._ajudaBotao.visible = false;
            this.addChild(value);
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    ajudaMenu: {
        get: function() {
            return this._ajudaMenu;
        },
        set: function(value) {
            this._ajudaMenu = value;
            this._ajudaMenu.align('center', 'center');
            this._ajudaMenu.x += this._ajudaMenu.width / 2;
            this._ajudaMenu.y += this._ajudaMenu.height / 2;
            this._ajudaMenu.visible = false;
            this._ajudaMenu.interactive = false;
            this.addChild(value);
        },
        configurable: true
    },

    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    backPontos: {
        get: function() {
            return this._backPontos;
        },

        set: function(value) {

            var self = this;

            this._backPontos = value;

            this._backPontos.align('center', 'center');
            this._backPontos.x = Const.BASE_WIDTH - this._backPontos.width / 2 - 100;
            this._backPontos.y += this._backPontos.height / 2 +10;

            this.addChild(value);
        },
        configurable: true
    },

    pontos: {
        get: function() {
            return this._pontos;
        },

        set: function(value) {
            var self = this;

            this._pontos = value;

            // 175 = backpontos.width
            this._pontos.x = this._backPontos.x - this._backPontos.width /2;
            this._pontos.y = this._backPontos.y - 10;
            this._pontos._width = this._backPontos.width;

            this._pontos.align = "center";

            this.addChild(value);
        },
        configurable: true
    },

    brightSim: {
		get: function() {
			return this._brightSim;
		},
		set: function(value) {
			this._brightSim = value;
			// this._brightSim.setParent(this);
			this.addChild(value);
		},
        configurable: true
	},
    logo:{
        get: function(){
            return this._logobar;
        },

        set: function(value){
            this._logobar = value;
            this.addChildAt(this._logobar, 0);
            // this.setChildIndex(this._ajudaMenu, 0);
        }
    },
	titulo: {
		get: function() {
			return this._titulo;
		},
		set: function(value) {
			if(value !== undefined && value !== null) {
				this._titulo = value;

				var parent = this._titulo.parent;
				if(parent !== null && parent !== undefined) {
					parent.removeChild(this._titulo);
				}

				this.addChild(this._titulo);
				// this._titulo.x = 280;
				// this._titulo.y = 41
			} else {
				this.removeChild(this._titulo);
				this._titulo = null;
			}

		}
	}
});

AbstractInterface.prototype.openRefresh = function(force) {
    if (this._refreshButton === null) return;

    var self = this;
    var f = force || false;

    this.refreshIsActive = true;
    this._refreshButton.visible = true;
    if (!f) {
        this._refreshButton._alpha = 0;
        TweenMax.to(this._refreshButton, 0.3, {
            alpha: 1,
            onComplete: function() {
                self.emit('refreshOpened');
            }
        });
    } else {
        this._refreshButton._alpha = 1;
        self.emit('refreshOpened');
    }
};

AbstractInterface.prototype.showPoints = function(show) {

    if (this._backPontos === null) return;

    this._backPontos.visible = show;
    this._pontos.visible = show;

};

AbstractInterface.prototype.blowBallon = function(xBallon, yBallon) {
    this._ballon.x = xBallon;
    this._ballon.y = yBallon;

    this._ballon.visible = true;
    this._ballon.gotoAndPlay(0);
    this._ballon.animationSpeed = 0.7;

    this._ballon.onComplete = function() {
      this.parent.visible = false;

    };

};

AbstractInterface.prototype.closeRefresh = function(force) {
    if (this._refreshButton === null) return;

    var self = this;
    var f = force || false;

    this.refreshIsActive = false;

    if (!f) {
        this._refreshButton._alpha = 0;
        TweenMax.to(this._refreshButton, 0.3, {
            alpha: 0,
            onComplete: function() {
                self._refreshButton.visible = false;
                self.emit('refreshClosed');
            }
        });
    } else {
        this._refreshButton._alpha = 0;
        this._refreshButton.visible = false;
        self.emit('refreshClosed');
    }
};

AbstractInterface.prototype.starStorm = function() {
    var self = this;
    window.interfaceParticles.push(self.particleObject);

    setTimeout(function() {

        window.AbstractInterfaceParticles = [];
        self.particleObject.cleanup();
    }, Const.APPConfig.exerciseStarAnimationDuration * 1000);
};

AbstractInterface.prototype.teachAdvance = function() {

    this._showHand = true;

    this.teachAdvanceMove();

};

AbstractInterface.prototype.hideHand = function() {
    var self = this;
    this._showHand = false;
    this._hand.visible = false;
    self._clickingHand.visible = false;
    TweenMax.killTweensOf(this._hand);
    TweenMax.killTweensOf(self._clickingHand);
};

AbstractInterface.prototype.hideButtons = function() {
    this._navegacaoSeguinte.visible = false;
    this._navegacaoAnterior.visible = false;
    this.closeRefresh();
};

AbstractInterface.prototype.showButtons = function() {
    this._navegacaoSeguinte.visible = true;
    this._navegacaoAnterior.visible = true;
};


AbstractInterface.prototype.updatePoints = function(points) {

    if (this._pontos !== null) {
        this._pontos.setText("<a>Points: " + points + "</a>", {a: {fill: "white"}});
    }
};

AbstractInterface.prototype.teachAdvanceMove = function() {

    var self = this;

    this._hand.visible = true;
    this._hand.x = Const.BASE_WIDTH / 2 + 25;
    this._hand.y = Const.BASE_HEIGHT - 30 - this._hand.height;
    this._hand.rotation = 0;

    this._clickingHand.x = Const.BASE_WIDTH / 2 + 40;
    this._clickingHand.y = Const.BASE_HEIGHT - 130 - this._clickingHand.height;

    TweenMax.to(self._hand, 1, {
        x: "+=32",
        y: "-=8",
        rotation: "-=0.7",
        onComplete: function() {
            self._hand.visible = false;
            self._clickingHand.visible = true;

            TweenMax.to(self._clickingHand, 2, {
                x: "-=110",
                onComplete: function() {
                    self._hand.visible = true;
                    self._clickingHand.visible = false;

                    self._hand.x -= 110;

                    TweenMax.to(self._hand, 1, {
                        x: "+=25",
                        y: "+=20",
                        rotation: "+=0.5",
                        onComplete: function() {
                            self._hand.visible = false;
                            self._clickingHand.visible = false;

                            setTimeout(function(){
                                if (self._showHand === true) {
                                    self.teachAdvanceMove();
                                }
                            },800);
                        }
                    });
                }
            });
        }
    });

};
