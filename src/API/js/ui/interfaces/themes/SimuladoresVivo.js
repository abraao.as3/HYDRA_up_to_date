var AbstractInterface = require('../AbstractInterface');
var behavior = require('../../../behavior/index');
var Const = require('../../../core/Const');
var TweenMax = require('TweenMax');

/**
 * @classdesc SimuladoresVivo é o tema da AbstractInterface
 * @memberof module:themes
 * @exports SimuladoresVivo
 * @constructor
 */
function SimuladoresVivo() {
    AbstractInterface.call(this);

    if (SimuladoresVivo._instance) {
        throw "Singleton:: use SimuladoresVivo.getInstance()";
    }

    SimuladoresVivo._instance = this;
}

SimuladoresVivo.prototype = Object.create(AbstractInterface.prototype);
SimuladoresVivo.prototype.constructor = SimuladoresVivo;
module.exports = SimuladoresVivo;

SimuladoresVivo._instance = null;

/**
   * Retorna uma instância do tema SimuladoresVivo
   *
   * @memberof module:themes.SimuladoresVivo
   * @public
   */
SimuladoresVivo.getInstance = function() {
    if (!SimuladoresVivo._instance) {
        new SimuladoresVivo();
    }

    return SimuladoresVivo._instance;
};
