var AbstractInterface = require('../AbstractInterface');
var behavior = require('../../../behavior/index');
var Const = require('../../../core/Const');
var TweenMax = require('TweenMax');


function OdontoPrev() {
    AbstractInterface.call(this);

    if (OdontoPrev._instance) {
        throw "Singleton:: use OdontoPrev.getInstance()";
    }

    OdontoPrev._instance = this;
}

OdontoPrev.prototype = Object.create(AbstractInterface.prototype);
OdontoPrev.prototype.constructor = OdontoPrev;
module.exports = OdontoPrev;

Object.defineProperties(OdontoPrev.prototype, {
    navegacaoAnterior: {
        get: function() {
            return this._navegacaoAnterior;
        },
        set: function(value) {
            this._navegacaoAnterior = value;
            this._navegacaoAnterior.addPlugin(new behavior.Clickable());
            this._navegacaoAnterior.addEvents();

            this._navegacaoAnterior.align("center", "center");

            this._navegacaoAnterior.y += Const.BASE_HEIGHT / 2 - this._navegacaoAnterior.height / 2;
            this._navegacaoAnterior.x = this._navegacaoAnterior.width / 2;

            this._navegacaoAnterior.visible = false;

            this.addChild(value);
        }
    },
    navegacaoSeguinte: {
        get: function() {
            return this._navegacaoSeguinte;
        },
        set: function(value) {
            this._navegacaoSeguinte = value;
            this._navegacaoSeguinte.addPlugin(new behavior.Clickable());
            this._navegacaoSeguinte.addEvents();

            this._navegacaoSeguinte.align("center", "center");

            this._navegacaoSeguinte.x = Const.BASE_WIDTH - this._navegacaoSeguinte.width / 2;
            this._navegacaoSeguinte.y += Const.BASE_HEIGHT / 2 - this._navegacaoSeguinte.height / 2;

            this._navegacaoSeguinte.visible = false;
            this.addChild(value);
        }
    }

});

OdontoPrev._instance = null;
OdontoPrev.getInstance = function() {
    if (!OdontoPrev._instance) {
        new OdontoPrev();
    }

    return OdontoPrev._instance;
};
