var AbstractInterface = require('../AbstractInterface');
var behavior = require('../../../behavior/index');
var Const = require('../../../core/Const');
// var TweenMax = require('TweenMax');

/**
 * @classdesc PortoSeguroMoto é o tema da AbstractInterface
 * @memberof module:themes
 * @exports PortoSeguroMoto
 * @constructor
 */
function PortoSeguroMoto() {
    AbstractInterface.call(this);

    if (PortoSeguroMoto._instance) {
        throw "Singleton:: use PortoSeguroMoto.getInstance()";
    }

    PortoSeguroMoto._instance = this;

	this._navegacaoAnteriorInativo = null;
	this._navegacaoSeguinteInativo = null;
	this._navegacaoButtons = null;
	this._logobar = null.
	this._titulo = null;
}

PortoSeguroMoto.prototype = Object.create(AbstractInterface.prototype);
PortoSeguroMoto.prototype.constructor = PortoSeguroMoto;
module.exports = PortoSeguroMoto;

Object.defineProperties(PortoSeguroMoto.prototype, {
	/**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    navegacaoAnteriorInativo: {
        get: function() {
            return this._navegacaoAnteriorInativo;
        },
        set: function(value) {
            this._navegacaoAnteriorInativo = value;

			this._navegacaoAnteriorInativo.align("center", "center");

            this._navegacaoAnteriorInativo.y += Const.BASE_HEIGHT - this._navegacaoAnteriorInativo.height / 2;
            this._navegacaoAnteriorInativo.x = this._navegacaoAnteriorInativo.width / 2;

            this._navegacaoAnteriorInativo.visible = false;

            this.addChild(value);
        },
        configurable: true
    },
    /**
     * @memberof module:ui/interfaces.AbstractInterface
     * @type {Number}
     */
    navegacaoSeguinteInativo: {
        get: function() {
            return this._navegacaoSeguinteInativo;
        },
        set: function(value) {
            this._navegacaoSeguinteInativo = value;

            this._navegacaoSeguinteInativo.align("center", "center");

            this._navegacaoSeguinteInativo.x = Const.BASE_WIDTH - this._navegacaoSeguinteInativo.width / 2;
            this._navegacaoSeguinteInativo.y += Const.BASE_HEIGHT - this._navegacaoSeguinteInativo.height / 2;

            this._navegacaoSeguinteInativo.visible = false;
            this.addChild(value);
        },
        configurable: true
    },
    navegacaoButtons: {
		get: function() {
			return this._navegacaoButtons;
		},
		set: function(arr) {
			this._navegacaoButtons = arr;

			for(var navElement in this._navegacaoButtons) {
				if(navElement === 'navBackground') {
					this._navegacaoButtons[navElement].visible = false;
					this.addChildAt(this._navegacaoButtons[navElement], 0);
				} else {
					var navBtn = this._navegacaoButtons[navElement];

					if((navBtn.ativo !== undefined && navBtn.ativo !== null) || (navBtn.atual !== undefined && navBtn.atual !== null)) {
						navBtn.padrao.addPlugin(new behavior.ViewState());

						if(navBtn.ativo !== undefined && navBtn.ativo !== null && navBtn.atual !== undefined && navBtn.atual !== null) {
							if(navBtn.ativo.hasClick === true) {
								navBtn.ativo.addPlugin(new behavior.Clickable());
							}

							navBtn.padrao
								.addViewState('btnAtivo')
									.addStateListener(null, null, navBtn.padrao, 'btnAtivo')
										.addStateObject(null, null, navBtn.ativo, true)
											.addTargetStateSettings({alpha: 1})
											.addInitialStateSettings({alpha: 0})
										.initializeStateObject()
										.addStateObject(null, null, navBtn.atual, true)
											.addTargetStateSettings({alpha: 0})
											.addInitialStateSettings({alpha: 0})
										.initializeStateObject()
								.addViewState('btnAtual')
									.addStateListener(null, null, navBtn.padrao, 'btnAtual')
									.addStateObject(null, null, navBtn.ativo)
										.addTargetStateSettings({alpha: 0})
									.initializeStateObject()
										.addStateObject(null, null, navBtn.atual)
											.addTargetStateSettings({alpha: 1})
										.initializeStateObject();
						} else {
							if(navBtn.ativo !== undefined && navBtn.ativo !== null) {
								if(navBtn.ativo.hasClick === true) {
									navBtn.ativo.addPlugin(new behavior.Clickable());
								}

								navBtn.padrao.addViewState('btnAtivo')
												.addStateObject(null, null, navBtn.ativo, true)
													.addStateListener(null, null, navBtn.padrao, 'btnAtivo')
													.addTargetStateSettings({alpha: 1})
													.addInitialStateSettings({alpha: 0})
												.initializeStateObject();
							} else if(navBtn.atual !== undefined && navBtn.atual !== null) {
								navBtn.padrao.addViewState('btnAtual')
												.addStateObject(null, null, navBtn.atual, true)
													.addStateListener(null, null, navBtn.padrao, 'btnAtual')
													.addTargetStateSettings({alpha: 1})
													.addInitialStateSettings({alpha: 0})
												.initializeStateObject();
							}
						}

						navBtn.padrao.addStateListeners();
					}

					navBtn.padrao.visible = false;
					this.addChild(navBtn.padrao);
				}
			}
		}
	},
    closeButton: {
        get: function(){
            return this._closeButton;
        },

        set: function(value){
            this._closeButton = value;
            this._closeButton.align('center', 'center');
            this._closeButton.x = 911;
            this._closeButton.y = 35;
            this.addChild(value);

            this._closeButton.addPlugin(new behavior.Clickable());
            this._closeButton.addEvents();

            this._closeButton.once('clicked', function(){
                window.open(window.location, '_self').close();
            });
        },
        configurable: true
    },
    ajudaMenu: {
        get: function() {
            return this._ajudaMenu;
        },
        set: function(value) {
            this._ajudaMenu = value;
            this._ajudaMenu.align('center', 'center');
            this._ajudaMenu.x += this._ajudaMenu.width / 2;
            this._ajudaMenu.y += this._ajudaMenu.height / 2;
            this._ajudaMenu.visible = false;
            this._ajudaMenu.interactive = false;
            this.addChild(value);
        },
        configurable: true
    },
    ajudaBotao: {
        get: function() {
            return this._ajudaBotao;
        },

        set: function(value) {

            var self = this;

            this._ajudaBotao = value;
            this._ajudaBotao.addPlugin(new behavior.Clickable());
            this._ajudaBotao.addEvents();

            this._ajudaBotao.on("clicked", function() {
				self.ajudaMenu.visible = !self.ajudaMenu.visible;
                self.ajudaMenu.interactive = !self.ajudaMenu.interactive;
            });
            this._ajudaBotao.align('center', 'center');
            this._ajudaBotao.x = 845;
            this._ajudaBotao.y = 35;

            this._ajudaBotao.visible = true;
            this.addChild(value);
        },
        configurable: true
    },
    logo:{
        get: function(){
            return this._logobar;
        },

        set: function(value){
            this._logobar = value;
            this.addChildAt(this._logobar, 0);
            // this.setChildIndex(this._ajudaMenu, 0);
        }
    },
	titulo: {
		get: function() {
			return this._titulo;
		},
		set: function(value) {
			if(value !== undefined && value !== null) {
				this._titulo = value;

				var parent = this._titulo.parent;
				if(parent !== null && parent !== undefined) {
					parent.removeChild(this._titulo);
				}

				this.addChild(this._titulo);
				// this._titulo.x = 280;
				// this._titulo.y = 41
			} else {
				this.removeChild(this._titulo);
				this._titulo = null;
			}

		}
	}
});

PortoSeguroMoto._instance = null;

/**
   * Retorna uma instância do tema PortoSeguroMoto
   *
   * @memberof module:themes.PortoSeguroMoto
   * @public
   */
PortoSeguroMoto.getInstance = function() {
    if (!PortoSeguroMoto._instance) {
        new PortoSeguroMoto();
    }

    return PortoSeguroMoto._instance;
};

PortoSeguroMoto.prototype.hideButtons = function() {
	AbstractInterface.prototype.hideButtons.call(this);

	for(var navElement in this._navegacaoButtons) {
		var navBtn = this._navegacaoButtons[navElement];

		if(navElement === 'navBackground') {
			navBtn.visible = false;
		} else {
			navBtn.padrao.visible = false;
		}
	}
};

PortoSeguroMoto.prototype.showNavButtons = function() {
	for(var navElement in this._navegacaoButtons) {
		var navBtn = this._navegacaoButtons[navElement];

		if(navElement === 'navBackground') {
			navBtn.visible = true;
		} else {
			navBtn.padrao.visible = true;
		}
	}
};
