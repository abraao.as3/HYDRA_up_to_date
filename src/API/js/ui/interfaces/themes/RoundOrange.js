var AbstractInterface = require('../AbstractInterface');
var behavior = require('../../../behavior/index');
var Const = require('../../../core/Const');
var TweenMax = require('TweenMax');

/**
 * @classdesc RoundOrange é o tema da AbstractInterface
 * @memberof module:themes
 * @exports RoundOrange
 * @constructor
 */
function RoundOrange() {
    AbstractInterface.call(this);

    if (RoundOrange._instance) {
        throw "Singleton:: use RoundOrange.getInstance()";
    }

    RoundOrange._instance = this;
}

RoundOrange.prototype = Object.create(AbstractInterface.prototype);
RoundOrange.prototype.constructor = RoundOrange;
module.exports = RoundOrange;

RoundOrange._instance = null;

/**
   * Retorna uma instância do tema RoundOrange
   *
   * @memberof module:themes.RoundOrange
   * @public
   */
RoundOrange.getInstance = function() {
    if (!RoundOrange._instance) {
        new RoundOrange();
    }

    return RoundOrange._instance;
};
