 var AbstractInterface = require('../AbstractInterface');
 var behavior = require('../../../behavior/index');
 var Const = require('../../../core/Const');
 var TweenMax = require('TweenMax');

 /**
  * @classdesc Modern é o tema da AbstractInterface
  * @memberof module:themes
  * @exports Modern
  * @constructor
  */
function Modern() {
    AbstractInterface.call(this);


    if (Modern._instance) {
        throw "Singleton:: use Modern.getInstance()";
    }

    Modern._instance = this;
}

Modern.prototype = Object.create(AbstractInterface.prototype);
Modern.prototype.constructor = Modern;
module.exports = Modern;

Modern._instance = null;

/**
   * Retorna uma instância do tema Modern
   *
   * @memberof module:themes.Modern
   * @public
   */
Modern.getInstance = function() {
    if (!Modern._instance) {
        new Modern();
    }

    return Modern._instance;
};
