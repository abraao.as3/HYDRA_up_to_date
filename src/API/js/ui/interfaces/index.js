/**
* @module ui/interfaces
*/
var interfaces = module.exports = {

    roundOrange: require('./themes/RoundOrange'),

    modern: require('./themes/Modern'),

    odontoPrev: require('./themes/OdontoPrev'),

    simuladoresVivo: require('./themes/SimuladoresVivo')
};
