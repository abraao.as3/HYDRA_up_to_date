var EventEmiter = require('EventEmitter');
var $ = require('jquery');
var Const = require('../../core/Const');
var d3 = require('D3');

function DataBoard(){
    EventEmiter.call(this);
    this._visualise = $(document.createElement('div'));
    this._visualise.attr("id","visualise");
    this._visualise.css({
        width: Const.BASE_WIDTH + 'px',
        height: Const.BASE_HEIGHT + 'px',
        position: 'absolute',
        overflow: 'hidden'

    });
 var first = $('div').first();
    first.after(this._visualise);

    this.board = null;
    this.ctx = null;
    this._imageBackground = null;

    this._factorScale = 1;
}

DataBoard.prototype = Object.create(EventEmiter.prototype);
DataBoard.constructor = DataBoard;
module.exports = DataBoard;

Object.defineProperties(DataBoard.prototype, {
    eraser:{
        get: function(){
            if(this.ctx.globalCompositeOperation == "destination-out") return true;
            else return false;
    },

    set: function(value){
            if(this.ctx !== null){
                if(value === true){
                    this.ctx.globalCompositeOperation = "destination-out";
                }
                else{
                    this.ctx.globalCompositeOperation = "source-over";
                }
            }
        }
    }

});


DataBoard.prototype.init = function(colorBall, ballRadius, inLine,otLine,posUser,ticks,imageBase,strokeTickness, fontSize, fontColor, lineColor){

    this._svg = d3.select('#visualise')
        .append("svg")

        .attr("width", Const.BASE_WIDTH)
        .attr("height", Const.BASE_HEIGHT);
        this._posLinha = [{
            x:(imageBase.x - imageBase.width / 2),
            y: (imageBase.y - imageBase.height / 2)}];

    this._visualise.prop({
        width: Const.BASE_WIDTH,
        height: Const.BASE_HEIGHT
    });

    this._visualise.visible = false;

    this._canvas = this._visualise.find('canvas');
    this._canvas.css({
        width: '100%',
        height: '100%',
        position: 'relative'
    });
    var elementsArr = [];
    elementsArr = ["svg", "circle", "rect", "line","style"];

    for (var i = 0; i < elementsArr.length; i++) {
    document.createElementNS("http://www.w3.org/2000/svg", elementsArr[i]);
}

    this.width ='100%';
    this.height = '100%';

    this.criaLinha(inLine, otLine, ticks,imageBase, strokeTickness, fontSize, fontColor, lineColor);
    this.criaBola(colorBall, ballRadius, posUser, inLine, otLine,imageBase);
}


DataBoard.prototype.start = function(){
    this.width ='100%';
    this.height = '100%';

    this._autorizeNext = false;
    this._autorizeDraw = true;
};


DataBoard.prototype.criaLinha = function(inLine, otLine, ticks,imageBase, strokeTickness,
fontSize, fontColor, lineColor){

    var xScale = d3.scaleLinear()
    .domain([inLine, otLine])
    .range([0, imageBase.width]);


    if(Array.isArray(ticks))
    {
        var xAxis = d3.axisBottom()
                .scale(xScale)
                .tickSize([50])
                .tickValues(ticks);
    }else{
        var xAxis = d3.axisBottom()
                .scale(xScale)
                .tickSize([50])
                .ticks(ticks);
    }
    this._svg.append('svg:g').call(xAxis)
    .attr("transform", "translate("+this._posLinha[0].x+"," +this._posLinha[0].y +")");

    // $('g').css({
    //     'stroke-width':strokeTickness,
    //     'font-size': fontSize
    // });
    $('.tick').find("text").css({
        'fill' : fontColor,
         'font-size': fontSize
    });

    $('.tick').find("line").css({
        'stroke-width':strokeTickness
    });
    $('.domain').css({
        'stroke-width':strokeTickness
    })


};


DataBoard.prototype.criaBola = function(colorBall, ballRadius,posUser,inLine, otLine,imageBase){

    var dataset= [0,150];
    var posBola = (posUser-inLine)/(otLine-inLine)*imageBase.width;
    var circles = this._svg.selectAll("circle").data(dataset);

        circles
            .enter()
            .insert("circle")
            .attr("cx", this._posLinha[0].x + posBola)
            .attr("cy", this._posLinha[0].y)
            .attr("r", ballRadius)
            .style("fill", colorBall);

};

DataBoard.prototype.setSize = function(x, y, w, h){
    this._visualise.css({
        left: x + 'px',
        top: y + 'px',
        width: w + 'px',
        height: h + 'px'
    });
};

DataBoard.prototype.domResize = function(factorScale, canvasPos){

    this.setSize(canvasPos.x, canvasPos.y, Const.BASE_WIDTH * factorScale, Const.BASE_HEIGHT * factorScale);
};

DataBoard.prototype.moveBoard = function(_left){
    this._canvas.css({
        'left': _left + 'px'
    });
};

DataBoard.prototype.reset = function(){
    this._autorizeNext = true;
    this._autorizeDraw = false;

    this.clear();

    this._visualise.remove();
};
