var EventEmiter = require('EventEmitter');
var $ = require('jquery');
var Const = require('../../core/Const');
var Sketch = require('Sketch');
var PIXIConfig = require('../../core/PIXIConfig');

/**
  * @classdesc Classe que define o quadro do Paint
  * @memberof module:ui/paint
  * @extends Pixi.Container
  * @exports PaintBoard
  * @constructor
  */
function PaintBoard(){
    EventEmiter.call(this);

    this._dom = $(document.createElement('div'));
    this._dom.css({
        position: 'absolute',
        overflow: 'hidden'
    });

    this.board = null;
    this.ctx = null;
}

PaintBoard.prototype = Object.create(EventEmiter.prototype);
PaintBoard.constructor = PaintBoard;
module.exports = PaintBoard;

Object.defineProperties(PaintBoard.prototype, {
    /**
     * @memberof module:ui/paint.PaintBoard
     * @type {string}
     */
    eraser:{
        get: function(){
            if(this.ctx.globalCompositeOperation == "destination-out") return true;
            else return false;
        },

        set: function(value){
            if(this.ctx !== null){
                if(value === true){
                    this.ctx.globalCompositeOperation = "destination-out";
                }
                else{
                    this.ctx.globalCompositeOperation = "source-over";
                }
            }
        }
    }
});

/**
 * inicializa a classe
 */
PaintBoard.prototype.init = function(){
    this._dom.prop({
        width: Const.BASE_WIDTH,
        height: Const.BASE_HEIGHT
    });

    this.board = Sketch.create({
        container: this._dom[0],
        fullscreen: false,
        width: Const.BASE_WIDTH,
        height: Const.BASE_HEIGHT,
        type: Sketch.CANVAS,
        autoclear: false
    });
    this._canvas = this._dom.find('canvas');
    this._canvas.css({
        width: '100%',
        height: '100%',
        position: 'relative'
    });
    this.ctx = this._dom.find('canvas')[0].getContext('2d');
};

/**
 * executa a classe
 */
PaintBoard.prototype.start = function(){
    var self = this;
    this._autorizeNext = false;
    this._autorizeDraw = true;

    $('body').prepend(this._dom);
};

/**
 * atribui tamanho
 * @param x {number} ponto x
 * @param y {number} ponto y
 * @param w {number} largura
 * @param h {number} altura
 */
PaintBoard.prototype.setSize = function(x, y, w, h){
    this._dom.css({
        left: x + 'px',
        top: y + 'px',
        width: w + 'px',
        height: h + 'px'
    });
};

PaintBoard.prototype.moveBoard = function(_left){
    this._canvas.css({
        'left': _left + 'px'
    });
};

PaintBoard.prototype.clear = function(){
    this.board.clear();
};

PaintBoard.prototype.reset = function(){
    this._autorizeNext = true;
    this._autorizeDraw = false;

    this.clear();

    this._dom.remove();
};
