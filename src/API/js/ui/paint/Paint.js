/**
 * @module ui/paint
 */
var ObjectBase = require('../../core/ObjectBase');
var PaintBoard = require('./PaintBoard');
var PaintToolBar = require('./PaintToolBar');
var Const = require('../../core/Const');
var PIXI = require('PIXI');
var behavior = require('../../behavior/index');
var PIXIConfig = require('../../core/PIXIConfig');

/**
  * @classdesc Classe que define o Paint
  * @memberof module:ui/paint
  * @extends Pixi.Container
  * @exports Paint
  * @constructor
  * @param textures {PIXI.Texture} Textura para formar a imagem
  */
function Paint(textures){
    var self = this;
    ObjectBase.call(this);

    this._board = new PaintBoard();
    this._originPoint = {x: 0, y: 0};
    this._factorScale = 1;

    this._move = false;
    this._draw = false;
    this._linedraw = false;

    this._isForm = false;

    this._touchSupport = new PIXI.Graphics();
    this._touchSupport.beginFill(0);
    this._touchSupport.drawRect(0,0,Const.BASE_WIDTH, Const.BASE_HEIGHT);
    this._touchSupport.endFill();
    this._touchSupport.alpha = 0;

    this._toolbar = new PaintToolBar(textures);
    this._toolbar.x = this._touchSupport.width - this._toolbar.width;
    this._toolbar.y = this._touchSupport.height / 2 - this._toolbar.height / 2;

    this._tempboard = new PIXI.Graphics();
    this._tempContainer = new ObjectBase();
    this._tempContainer.addPlugin(new behavior.Bounding());

    this.addChild(this._tempboard);
    this.addChild(this._touchSupport);
    this.addChild(this._toolbar);

    var lo = new PIXI.loaders.Loader();
    lo.add('stamp', 'telas/commom/ui/paint/stamps.json');
    lo.add('person', 'telas/commom/ui/paint/person.json');
    lo.load(function(loader, resources){
        var s = resources.stamp.textures;
        self._stamps = [];
        for(var i in s){
            var index = parseInt(i.replace(/\D/g, ""));
            self._stamps[index] = s[i];
        }

        var p = resources.person.textures;
        self._persons = [];
        for(i in p){
            var index_ = parseInt(i.replace(/\D/g, ""));
            self._persons[index_] = p[i];
        }
    });
}

Paint.prototype = Object.create(ObjectBase.prototype);
Paint.prototype.constructor = Paint;
module.exports = Paint;

/**
 * inicializa classe
 */
Paint.prototype.init = function(){
    this._board.init();
};

/**
 * executa classe
 */
Paint.prototype.start = function(){
    var self = this;

    this._board.start();
    this._touchSupport.interactive = true;
    // this._toolbar.close(3, function(){
    //     // self._toolbar.addEvents();
    // });

    this._toolbar.on('toolSelected', this._toolSelected, this);
    this._toolbar.on('formSelected', this._drawForms, this);
    this._toolbar.on('stampSelected', this._drawStamps, this);
    this._toolbar.on('personSelected', this._drawPersons, this);
    this._toolbar.on('clicked', this._onToolClicked, this);
};

Paint.prototype.openToolbar = function(addEvents){
    var self = this;
    this._toolbar.open(0, function(){
        if(addEvents){
            self._toolbar.addEvents();
        }
        else{
            self._toolbar.removeEvents();
        }
    });
};

Paint.prototype.closeToolbar = function(addEvents){
    var self = this;
    this._toolbar.close(0, function(){
        if(addEvents){
            self._toolbar.addEvents();
        }
        else{
            self._toolbar.removeEvents();
        }
    });
};

/**
 * metodo adiciona eventos
 */
Paint.prototype.addDragEvents = function(){
    this._touchSupport.on('mousedown', this._onDown, this);
    this._touchSupport.on('touchstart', this._onDown, this);

    this._touchSupport.on('mouseup', this._onUp, this);
    this._touchSupport.on('touchend', this._onUp, this);

    this._touchSupport.on('mouseupoutside', this._onUp, this);
    this._touchSupport.on('touchendoutside', this._onUp, this);

    this._touchSupport.on('mousemove', this._onMove, this);
    this._touchSupport.on('touchmove', this._onMove, this);
};

/**
 * metodo remove eventos
 */
Paint.prototype.removeDragEvents = function(){
    this._touchSupport.removeListener('mousedown');
    this._touchSupport.removeListener('touchstart');

    this._touchSupport.removeListener('mouseup');
    this._touchSupport.removeListener('touchend');

    this._touchSupport.removeListener('mouseupoutside');
    this._touchSupport.removeListener('touchendoutside');

    this._touchSupport.removeListener('mousemove');
    this._touchSupport.removeListener('touchmove');
};

/**
 * metodo adiciona eventos
 */
Paint.prototype.addLineEvents = function(){
    this._touchSupport.on('mousedown', this._onLineDown, this);
    this._touchSupport.on('touchstart', this._onLineDown, this);

    this._touchSupport.on('mouseup', this._onLineUp, this);
    this._touchSupport.on('touchend', this._onLineUp, this);

    this._touchSupport.on('mouseupoutside', this._onLineUp, this);
    this._touchSupport.on('touchendoutside', this._onLineUp, this);

    this._touchSupport.on('mousemove', this._onLineMove, this);
    this._touchSupport.on('touchmove', this._onLineMove, this);
};

Paint.prototype.removeLineEvents = function(){
    this._touchSupport.removeListener('mousedown');
    this._touchSupport.removeListener('touchstart');

    this._touchSupport.removeListener('mouseup');
    this._touchSupport.removeListener('touchend');

    this._touchSupport.removeListener('mouseupoutside');
    this._touchSupport.removeListener('touchendoutside');

    this._touchSupport.removeListener('mousemove');
    this._touchSupport.removeListener('touchmove');
};

Paint.prototype.removeAllDrawEvents = function(){
    this.removeDragEvents();
    this.removeLineEvents();
};

/**
 * metodo que controla tecla pressionada
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._onDown = function(e){
    this._move = true;

    this._setDrawConfig();

    this._board.board.beginPath();
    this._board.board.moveTo(e.data.global.x, e.data.global.y);

    if(!this._board.eraser)
    this.emit('drawinboard');
};

/**
 * metodo que controla tecla pressionada
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._onUp = function(e){
    this._move = false;
};

/**
 * metodo que controla tecla pressionada
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._onMove = function(e){
    if(this._move){

        this._board.board.lineTo(e.data.global.x, e.data.global.y);
        this._board.board.stroke();
    }
};

/**
 * metodo que controla tecla pressionada
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._onLineDown = function(e){
    this._move = true;
    e.target.data = new PIXI.Point(e.data.global.x, e.data.global.y);

    this.emit('drawinboard');
};

/**
 * metodo que controla tecla pressionada
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._onLineUp = function(e){
    this._move = false;
    this._setDrawConfig();
    this._board.board.beginPath();
    this._board.board.moveTo(e.target.data.x, e.target.data.y);
    this._board.board.lineTo(e.data.global.x, e.data.global.y);
    this._board.board.stroke();

    this._tempboard.clear();
};

/**
 * metodo que controla tecla pressionada
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._onLineMove = function(e){
    if(this._move){
        this._tempboard.clear();
        this._tempboard.lineStyle(this._toolbar.dot, this._toolbar.color);
        this._tempboard.moveTo(e.target.data.x, e.target.data.y);
        this._tempboard.lineTo(e.data.global.x, e.data.global.y);
        this._tempboard.endFill();
    }
};

/**
 * metodo ...
 * @memberof Paint
 * @private
 */
Paint.prototype._setDrawConfig = function(){
    this._board.board.lineCap = 'round';
    this._board.board.lineJoin = 'round';
    this._board.board.fillStyle = this._board.board.strokeStyle = this._toolbar.color.toString(16).toUpperCase() === "0" ? "black" : "#" + this._toolbar.color.toString(16).toUpperCase();
    this._board.board.lineWidth = this._toolbar.dot;
};

/**
 * metodo ...
 * @memberof Paint
 * @private
 */
Paint.prototype._onToolClicked = function(){
    if(this._isForm){
        this._saveForm();
    }
};

/**
 * metodo ...
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._toolSelected = function(e){
    this._noDraw();
    this._board.eraser = false;

    switch (e.name) {
        case 'pencil':
            if(this._draw === false){
                this.addDragEvents();
                this._draw = true;
            }
            break;
        case 'eraser':
            if(this._draw === false){
                this.addDragEvents();
                this._draw = true;
            }
            this._board.eraser = true;
            break;
        case 'line':
            if(this._linedraw === false){
                this.addLineEvents();
                this._linedraw = true;
            }
            break;
    }
};

/**
 * metodo ...
 * @memberof Paint
 * @private
 */
Paint.prototype._noDraw = function(){
    this.removeDragEvents();
    this._draw = false;
    this._linedraw = false;
};

/**
 * metodo ...
 * @param factorScale {number} fator para escala
 * @param canvasPos {object}
 */
Paint.prototype.resize = function(factorScale, canvasPos){
    this._factorScale = factorScale;
    this._board.setSize(canvasPos.x, canvasPos.y, Const.BASE_WIDTH * factorScale, Const.BASE_HEIGHT * factorScale);
};

/**
 * metodo ...
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._drawForms = function(e){
    this._toolbar.clearSelection();
    this._isForm = true;
    this._noDraw();
    this._board.eraser = false;

    this._tempboard.lineStyle(this._toolbar.dot, this._toolbar.color);

    switch (e.form) {
        case 'square':
            this._drawSquare();
            break;
        case 'circle':
            this._drawCircle();
            break;
        case 'roundsquare':
            this._drawRoundedRect();
            break;
        case 'triangle':
            this._drawTriangle();
            break;
        case 'pentagon':
            this._drawPentagon();
            break;
        case 'hexagon':
            this._drawHexagon();
            break;
            case 'star':
                this._drawStar();
                break;
    }

    this._addTempContainer();
    this.emit("drawinboard");
};

/**
 * metodo ...
 * @memberof Paint
 * @private
 */
Paint.prototype._addTempContainer = function(){
    this.addChildAt(this._tempContainer, 0);

    this._tempContainer.x = Const.BASE_WIDTH / 2 - this._tempContainer.width / 2;
    this._tempContainer.y = Const.BASE_HEIGHT / 2 - this._tempContainer.height / 2;

    this._tempContainer.createBounds();
    this._tempContainer.addBoundsEvents();
};

/**
 * metodo desenha quadrado
 * @memberof Paint
 * @private
 */
Paint.prototype._drawSquare = function(){
    this._tempboard.drawRect(this._toolbar.dot / 2, this._toolbar.dot / 2, 250, 250);
    this._tempboard.endFill();

    this._drawFormOnScreen(0, 0);
};

/**
 * metodo desenha circulo
 * @memberof Paint
 * @private
 */
Paint.prototype._drawCircle= function(){
    this._tempboard.drawCircle(this._toolbar.dot / 2 + 125, this._toolbar.dot / 2 + 125, 125);
    this._tempboard.endFill();

    this._drawFormOnScreen(0, 0);
};

/**
 * metodo desenha retangulo arredondado
 * @memberof Paint
 * @private
 */
Paint.prototype._drawRoundedRect = function(){
    this._tempboard.drawRoundedRect(this._toolbar.dot / 2, this._toolbar.dot / 2, 250, 250, 30);
    this._tempboard.endFill();

    this._drawFormOnScreen(0, 0);
};

/**
 * metodo desenha triangulo
 * @memberof Paint
 * @private
 */
Paint.prototype._drawTriangle = function(){
    var fx = this._toolbar.dot * 0.35;
    var fy = this._toolbar.dot * 0.15;
    this._tempboard.drawPolygon([
        new PIXI.Point(125 + this._getDotX(fx),  this._getDotY(fy)),
        new PIXI.Point(this._getDotX(fx), 250 + this._getDotY(fy)),
        new PIXI.Point(250 + this._getDotX(fx), 250 + this._getDotY(fy)),
        new PIXI.Point(125 + this._getDotX(fx), this._getDotY(fy))
    ]);

    this._drawFormOnScreen(this._getDotX(fx) * 0.4, this._getDotY(fy) * 0.3);
};

/**
 * metodo desenha pentagono
 * @memberof Paint
 * @private
 */
Paint.prototype._drawPentagon = function(){
    var fx = this._toolbar.dot * 0.1;
    var fy = -this._toolbar.dot * 0.3;
    this._tempboard.drawPolygon([
        new PIXI.Point(125 + this._getDotX(fx), this._getDotY(fy)),
        new PIXI.Point(this._getDotX(fx), 91 + this._getDotY(fy)),
        new PIXI.Point(50 + this._getDotX(fx), 236 + this._getDotY(fy)),
        new PIXI.Point(203 + this._getDotX(fx), 236 + this._getDotY(fy)),
        new PIXI.Point(250 + this._getDotX(fx), 91 + this._getDotY(fy)),
        new PIXI.Point(125 + this._getDotX(fx), this._getDotY(fy))
    ]);

    this._drawFormOnScreen(this._getDotX(fx) * 1.25, this._getDotY(fy) * 1.15);
};

/**
 * metodo desenha hexagono
 * @memberof Paint
 * @private
 */
Paint.prototype._drawHexagon = function(){
    var fx = 0;
    var fy = -this._toolbar.dot * 0.4;
    this._tempboard.drawPolygon([
        new PIXI.Point(107 + this._getDotX(fx), 0 + this._getDotY(fy)),
        new PIXI.Point(0 + this._getDotX(fx), 62 + this._getDotY(fy)),
        new PIXI.Point(0 + this._getDotX(fx), 187 + this._getDotY(fy)),
        new PIXI.Point(107 + this._getDotX(fx), 250 + this._getDotY(fy)),
        new PIXI.Point(215 + this._getDotX(fx), 187 + this._getDotY(fy)),
        new PIXI.Point(215 + this._getDotX(fx), 62 + this._getDotY(fy)),
        new PIXI.Point(107 + this._getDotX(fx), 0 + this._getDotY(fy))
    ]);

    this._drawFormOnScreen(this._getDotX(fx) * 2, this._getDotY(fy) * 1.35);
};

/**
 * metodo desenha estrela
 * @memberof Paint
 * @private
 */
Paint.prototype._drawStar = function(){
    var fx = this._toolbar.dot * 0.55;
    var fy = this._toolbar.dot * 0.15;
    this._tempboard.drawPolygon([
        new PIXI.Point(124 + this._getDotX(fx), 0 + this._getDotY(fy)),
        new PIXI.Point(86 + this._getDotX(fx), 78 + this._getDotY(fy)),
        new PIXI.Point(0 + this._getDotX(fx), 91 + this._getDotY(fy)),
        new PIXI.Point(62 + this._getDotX(fx), 152 + this._getDotY(fy)),
        new PIXI.Point(47 + this._getDotX(fx), 238 + this._getDotY(fy)),
        new PIXI.Point(124 + this._getDotX(fx), 197 + this._getDotY(fy)),
        new PIXI.Point(201 + this._getDotX(fx), 238 + this._getDotY(fy)),
        new PIXI.Point(187 + this._getDotX(fx), 152 + this._getDotY(fy)),
        new PIXI.Point(250 + this._getDotX(fx), 91 + this._getDotY(fy)),
        new PIXI.Point(163 + this._getDotX(fx), 78 + this._getDotY(fy)),
        new PIXI.Point(124 + this._getDotX(fx), 0 + this._getDotY(fy))
    ]);

    this._drawFormOnScreen(-this._getDotX(fx) * 0.15, -this._getDotY(fy) * 0.1);
};

/**
 * metodo obter ponto x
 * @memberof Paint
 * @param fx {number} ponto x
 * @private
 */
Paint.prototype._getDotX = function(fx){
    return this._toolbar.dot / 2 + fx;
};

/**
 * metodo obter ponto y
 * @memberof Paint
 * @param fy {number} ponto y
 * @private
 */
Paint.prototype._getDotY = function(fy){
    return this._toolbar.dot + fy;
};

/**
 * metodo desenhar formulario na tela
 * @memberof Paint
 * @param fx {number} ponto x
 * @param fy {number} ponto y
 * @private
 */
Paint.prototype._drawFormOnScreen = function(fx, fy){
    var rt = PIXI.RenderTexture.create(this._tempboard.width - fx, this._tempboard.height - fy);
    PIXIConfig.renderer.render(this._tempboard, rt);

    this._tempboard.clear();
    this._tempContainer.addChild(new PIXI.Sprite(rt));
};

/**
 * metodo ...
 * @memberof Paint
 * @private
 */
Paint.prototype._saveForm = function(){
    if(!this._isForm) return;

    this._isForm = false;
    this._tempContainer.destroyBounds();

    var rt1 = PIXI.RenderTexture.create(Const.BASE_WIDTH, Const.BASE_HEIGHT);
    PIXIConfig.renderer.render(this._tempContainer, rt1);

    var src = PIXIConfig.renderer.extract.image(rt1);

    this._board.board.drawImage(src, 0, 0);

    this.removeChild(this._tempContainer);
    var sp = this._tempContainer.removeChildAt(0);
    sp.destroy();
    sp = null;
    this._tempContainer.scale.set(1, 1);
    this._tempContainer.x = 0;
    this._tempContainer.y = 0;
    this._tempboard.clear();
};

/**
 * metodo ...
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._drawStamps = function(e){
    this._toolbar.clearSelection();
    this._noDraw();
    this._board.eraser = false;
    this._isForm = true;
    var s = new PIXI.Sprite(this._stamps[e.stampIndex]);
    s.tint = this._toolbar.color;
    this._tempContainer.addChild(s);
    this._addTempContainer();

    this.emit("drawinboard");
};

/**
 * metodo ...
 * @memberof Paint
 * @param e {object} evento
 * @private
 */
Paint.prototype._drawPersons = function(e){
    this._toolbar.clearSelection();
    this._noDraw();
    this._board.eraser = false;
    this._isForm = true;
    var p = new PIXI.Sprite(this._persons[e.personIndex]);
    this._tempContainer.addChild(p);
    this._addTempContainer();

    this.emit("drawinboard");
};

Paint.prototype.moveBoard = function(_left){
    this._board.moveBoard(_left);
};

Paint.prototype.clear = function(){
    this._board.clear();
};

Paint.prototype.reset = function(){
    this.clear();

    this._board.reset();

    this._toolbar.reset();

    this._toolbar.removeListener('toolSelected');
    this._toolbar.removeListener('formSelected');
    this._toolbar.removeListener('stampSelected');
    this._toolbar.removeListener('personSelected');
    this._toolbar.removeListener('clicked');
};
