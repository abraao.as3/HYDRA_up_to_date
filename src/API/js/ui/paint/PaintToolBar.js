var ObjectBase = require('../../core/ObjectBase');
var Bitmap = require('../../display/core/Bitmap');
var behavior = require('../../behavior/index');
var TweenMax = require('TweenMax');

/**
  * @classdesc Classe que define a barra de ferramentas do Paint
  * @memberof module:ui/paint
  * @extends Pixi.Container
  * @exports PaintToolBar
  * @constructor
  * @param textures {PIXI.Texture} Textura para formar a imagem
  */
function PaintToolBar(textures) {
    ObjectBase.call(this);

    var t = textures[0];
    this._all = {};
    this._tool = {};
    this._option = {};
    this._dotOption = {};
    this._formsOption = {};
    this._stampOption = {};
    this._personOption = {};
    this._p1 = {};
    this._p2 = {};
    this._p3 = {};
    this._p4 = {};
    this._p5 = {};
    this._p6 = {};
    this._back = null;
    this._arrow = null;
    this._pen = null;
    this._erase = null;
    this._prev = null;
    this._dotSizes = [5, 15, 30, 60];
    this._formsTypes = ['star', 'pentagon', 'hexagon', 'circle', 'square', 'roundsquare', 'triangle'];
    this._dotSelected = 5;
    this._colorSelected = 0;
    this._colorOp = null;

    this._state = "_p1";

    for (var i in t) {
        var name = i.replace('.png', '');
        var config = t[i].config;
        var b = new Bitmap(t[i].texture);
        b.align('center', 'center');
        b.x = config.x + b.width / 2;
        b.y = config.y + b.height / 2;
        b.name = name;
        this.addChild(b);

        if (name.indexOf('tool') !== -1) {
            this._tool[name] = b;
            b.selected = false;
            //b.addPlugin(new behavior.Contour());
        } else if (name.indexOf('option') !== -1) {
            this._option[name] = b;

            if (name.indexOf('dot') !== -1) {
                this._dotOption[name] = b;
            }

            if (name.indexOf('forms') !== -1) {
                this._formsOption[name] = b;
            }

            if(name.indexOf('stamp') !== - 1){
                this._stampOption[name] = b;
            }

            if(name.indexOf('person') !== - 1){
                this._personOption[name] = b;
            }
        }

        this._all[name] = b;
        if (name.indexOf('p1') !== -1) {
            this._p1[name] = b;
        }

        if (name.indexOf('p2') !== -1) {
            this._p2[name] = b;
            b.visible = false;
        }

        if (name.indexOf('p3') !== -1) {
            this._p3[name] = b;
            b.visible = false;
        }

        if (name.indexOf('p4') !== -1) {
            this._p4[name] = b;
            b.visible = false;
        }

        if(name.indexOf('p5') !== -1){
            this._p5[name] = b;
            b.visible = false;
        }

        if(name.indexOf('p6') !== -1){
            this._p6[name] = b;
            b.visible = false;
        }

        if (name.indexOf('menu') === -1) {
            b.addPlugin(new behavior.Clickable());
        } else {
            this.setChildIndex(b, 0);
            this._back = b;
        }
    }

    this._back.interactive = true;

    this._arrow = this._all.arrow_button;
    this._arrow.scale.set(-1, 1);
    this._open = true;

    this._prev = this._all.back_button;
    this._prev.addPlugin(new behavior.Clickable());
    this._prev.visible = false;

    this._selectDotOption(0);
    this._selectFormOption(0);

    this._colorOp = this._option.color_option_p1;
    this._selectColor(0);

    this._hasEvents = false;
}

PaintToolBar.prototype = Object.create(ObjectBase.prototype);
PaintToolBar.prototype.constructor = PaintToolBar;
module.exports = PaintToolBar;

Object.defineProperties(PaintToolBar.prototype, {
    /**
     * @memberof module:ui/paint.PaintToolBar
     * @type {number}
     * @readonly
     */
    dot: {
        get: function() {
            return this._dotSelected;
        }
    },

    /**
     * @memberof module:ui/paint.PaintToolBar
     * @type {string}
     * @readonly
     */
    color: {
        get: function() {
            return this._colorSelected;
        }
    }
});

/**
 * insere eventos ao input
 */
PaintToolBar.prototype.addEvents = function() {
    if(this._hasEvents) return;
    this._hasEvents = true;

    for (var i in this._tool) {
        var t = this._tool[i];

        if (
            i.indexOf('dot') != -1 ||
            i.match(/[\d+\w+]{6}(?=_tool_p3)/g) !== null ||
            i.indexOf('forms') != -1 ||
            i.indexOf('stamp') != -1 ||
            i.indexOf('person') != -1
        ) {
            continue;
        }
        // t.createContour(3, 0x1ba218);

        t.addEvents();
        t.on('clicked', this._onClickedTool, this);
    }

    for (i in this._dotOption) {
        var d = this._dotOption[i];
        d.addEvents();
        d.on('clicked', function() {
            this.emit('clicked');
            this._openPage(2);
        }, this);
    }

    this._colorOp.addEvents();
    this._colorOp.on('clicked', function() {
        this.emit('clicked');
        this._openPage(3);
    }, this);

    for (i in this._formsOption) {
        var f = this._formsOption[i];
        f.addEvents();
        f.on('clicked', function() {
            this.emit('clicked');
            this._openPage(4);
        }, this);
    }

    for(i in this._stampOption){
        var s = this._stampOption[i];
        s.addEvents();
        s.on('clicked', function(){
            this.emit('clicked');
            this._openPage(5);
        }, this);
    }

    for(i in this._personOption){
        var p = this._personOption[i];
        p.addEvents();
        p.on('clicked', function(){
            this.emit('clicked');
            this._openPage(6);
        }, this);
    }

    this._arrow.addEvents();
    this._arrow.on('clicked', this._toggle, this);
};

PaintToolBar.prototype.removeEvents = function(){
    if(!this._hasEvents) return;
    this._hasEvents = false;

    for (var i in this._tool) {
        var t = this._tool[i];

        if (
            i.indexOf('dot') != -1 ||
            i.match(/[\d+\w+]{6}(?=_tool_p3)/g) !== null ||
            i.indexOf('forms') != -1 ||
            i.indexOf('stamp') != -1 ||
            i.indexOf('person') != -1
        ) {
            continue;
        }

        t.removeEvents();
        t.removeListener('clicked');
    }

    for (i in this._dotOption) {
        var d = this._dotOption[i];
        d.removeEvents();
        d.removeListener('clicked');
    }

    this._colorOp.removeEvents();
    this._colorOp.removeListener('clicked');

    for (i in this._formsOption) {
        var f = this._formsOption[i];
        f.removeEvents();
        f.removeListener('clicked');
    }

    for(i in this._stampOption){
        var s = this._stampOption[i];
        s.removeEvents();
        s.removeListener('clicked');
    }

    for(i in this._personOption){
        var p = this._personOption[i];
        p.removeEvents();
        p.removeListener('clicked');
    }

    this._arrow.removeEvents();
    this._arrow.removeListener('clicked');
};

/**
* @memberof PaintToolBar
* @private
 */
PaintToolBar.prototype._toggle = function() {
    if (this._open === true) {
        this.close();
    } else {
        this.open();
    }
};

/**
* @memberof PaintToolBar
* @param e {object} evento
* @private
 */
PaintToolBar.prototype._onClickedTool = function(e) {
    this.emit('clicked');
    if (e.target.selected === false) {
        this._selectTool(e.target.name);
    }
};

/**
* @memberof PaintToolBar
* @param name {string}
* @private
 */
PaintToolBar.prototype._selectTool = function(name) {
    var t = this._all[name];
    t.openContour();
    t.selected = true;
    for (var i in this._tool) {
        if (i.indexOf('dot') != -1 || i.indexOf('p3') != -1) {
            continue;
        }
        if (i.indexOf('forms') != -1 || i.indexOf('p4') != -1) {
            continue;
        }
        if (i.indexOf('stamp') != -1 || i.indexOf('p5') != -1) {
            continue;
        }
        if (i.indexOf('person') != -1 || i.indexOf('p6') != -1) {
            continue;
        }
        if (i != name) {
            this._tool[i].closeContour();
            this._tool[i].selected = false;
        }
    }

    this.emit('toolSelected', {
        target: this,
        tool: t,
        name: name.substring(0, name.indexOf('_'))
    });
};

/**
* @param delay {number}
* @param onComplete {boolean}
 */
PaintToolBar.prototype.close = function(delay, onComplete) {
    var self = this;
    var d = delay || 0;

    if (this._open === true) {
        this._arrow.scale.set(1, 1);
        TweenMax.to(this, 0.3, {
            x: "+=" + (this._back.width - 18),
            delay: d,
            onComplete: function() {
                self._open = false;
                if(onComplete !== undefined) onComplete();
            }
        });
    }
};

/**
* @param delay {number}
 */
PaintToolBar.prototype.open = function(delay, onComplete) {
    var self = this;
    var d = delay || 0;
    if (this._open === false) {
        this._arrow.scale.set(-1, 1);
        TweenMax.to(this, 0.3, {
            x: "-=" + (this._back.width - 18),
            delay: d,
            onComplete: function() {
                self._open = true;
                if(onComplete !== undefined) onComplete();
            }
        });
    }
};

/**
* @memberof PaintToolBar
* @param index {number}
* @private
 */
PaintToolBar.prototype._selectDotOption = function(index) {
    var name = 'dot' + index + '_option_p1';

    for (var i in this._dotOption) {
        if (name == i) {
            this._dotOption[i].visible = true;
        } else {
            this._dotOption[i].visible = false;
        }
    }

    this._dotSelectedIndex = index;
    this._dotSelected = this._dotSizes[index];
};

/**
* @memberof PaintToolBar
* @param color {string}
* @private
 */
PaintToolBar.prototype._selectColor = function(color) {
    this._colorOp.sprite.tint = color;
    this._colorSelected = color;
};

/**
* @memberof PaintToolBar
* @param index {number}
* @private
 */
PaintToolBar.prototype._selectFormOption = function(index) {
    var name = 'forms' + index + '_option_p1';

    for (var i in this._formsOption) {
        if (name == i) {
            this._formsOption[i].visible = true;
        } else {
            this._formsOption[i].visible = false;
        }
    }

    this._formsSelectedIndex = index;
};

/**
* @memberof PaintToolBar
* @param index {number}
* @private
 */
PaintToolBar.prototype._openPage = function(index) {
    var toClose = this[this._state];
    var toOpen = this['_p' + index];
    for (var i in toClose) {
        toClose[i].visible = false;
    }
    for (i in toOpen) {
        toOpen[i].visible = true;
    }
    if (this._state.indexOf('1') == -1) {
        this['close' + this._state]();
    }


    if (index > 1) {
        this['start_p' + index]();
    } else {
        this._selectDotOption(this._dotSelectedIndex);
        this._selectFormOption(this._formsSelectedIndex);
    }

    this._state = '_p' + index;
};

/**
* @memberof PaintToolBar
* @private
 */
PaintToolBar.prototype._gobackEvents = function() {
    this._prev.visible = true;
    this._prev.addEvents();
    this._prev.on('clicked', function() {
        this._gobackremoveEvents();
        this._openPage(1);
    }, this);
};

/**
* @memberof PaintToolBar
* @private
 */
PaintToolBar.prototype._gobackremoveEvents = function() {
    this._prev.visible = false;
    this._prev.removeEvents();
    this._prev.removeListener('clicked');
};

/**
*
 */
PaintToolBar.prototype.start_p2 = function() {
    this._gobackEvents();

    for (var i in this._p2) {
        if (i.indexOf('tool') != -1) {
            var t = this._p2[i];
            t.addEvents();
            t.on('clicked', this._dotClicked, this);
        }
    }
};

/**
*
 */
PaintToolBar.prototype.close_p2 = function() {
    for (var i in this._p2) {
        if (i.indexOf('tool') != -1) {
            var t = this._p2[i];
            t.removeEvents();
            t.removeListener('clicked');
        }
    }
};

/**
*
 */
PaintToolBar.prototype.start_p3 = function() {
    this._gobackEvents();

    for (var i in this._p3) {
        var t = this._p3[i];
        t.addEvents();
        t.on('clicked', this._colorClicked, this);
    }
};

/**
*
 */
PaintToolBar.prototype.close_p3 = function() {
    for (var i in this._p3) {
        var t = this._p3[i];
        t.removeEvents();
        t.removeListener('clicked');
    }
};

/**
*
 */
PaintToolBar.prototype.start_p4 = function() {
    this._gobackEvents();

    for (var i in this._p4) {
        var t = this._p4[i];
        t.addEvents();
        t.on('clicked', this._formsClicked, this);
    }
};

/**
*
 */
PaintToolBar.prototype.close_p4 = function() {
    for (var i in this._p4) {
        var t = this._p4[i];
        t.removeEvents();
        t.removeListener('clicked');
    }
};

/**
*
 */
PaintToolBar.prototype.start_p5 = function(){
    this._gobackEvents();

    for (var i in this._p5) {
        var t = this._p5[i];
        t.addEvents();
        t.on('clicked', this._stampClicked, this);
    }
};

/**
*
 */
PaintToolBar.prototype.close_p5 = function() {
    for (var i in this._p5) {
        var t = this._p5[i];
        t.removeEvents();
        t.removeListener('clicked');
    }
};

/**
*
 */
PaintToolBar.prototype.start_p6 = function(){
    this._gobackEvents();

    for (var i in this._p6) {
        var t = this._p6[i];
        t.addEvents();
        t.on('clicked', this._personClicked, this);
    }
};

/**
*
 */
PaintToolBar.prototype.close_p6 = function() {
    for (var i in this._p6) {
        var t = this._p6[i];
        t.removeEvents();
        t.removeListener('clicked');
    }
};

/**
 * @memberof PaintToolBar
 * @param e {object} evento
 * @private
 */
PaintToolBar.prototype._dotClicked = function(e) {
    var self = this;
    this._selectDotOption(e.target.name.match(/(?=\w+)\d(?=\w+)/g)[0]);

    self._openPage(1);

};

/**
 * @memberof PaintToolBar
 * @param e {object} evento
 * @private
 */
PaintToolBar.prototype._colorClicked = function(e) {
    var color = parseInt('0x' + e.target.name.match(/[\d+ \w+]{6}(?=_t)/g)[0]);
    this._selectColor(color);

    this._openPage(1);
};

/**
 * @memberof PaintToolBar
 * @param e {object} evento
 * @private
 */
PaintToolBar.prototype._formsClicked = function(e) {
    var index = e.target.name.match(/(?=\w+)\d(?=\w+)/g)[0];
    this.emit("formSelected", {
        form: this._formsTypes[index],
        target: this
    });
    this._selectFormOption(index);

    this._openPage(1);
};

/**
 * @memberof PaintToolBar
 * @param e {object} evento
 * @private
 */
PaintToolBar.prototype._stampClicked = function(e){
    var index = e.target.name.match(/(?=\w+)\d(?=\w+)/g)[0];

    this.emit("stampSelected", {
        stampIndex: index,
        target: this
    });

    this._openPage(1);
};

/**
 * @memberof PaintToolBar
 * @param e {object} evento
 * @private
 */
PaintToolBar.prototype._personClicked = function(e){
    var index = e.target.name.match(/(?=\w+)\d(?=\w+)/g)[0];

    this.emit("personSelected", {
        personIndex: index,
        target: this
    });

    this._openPage(1);
};

/**
 *
 */
PaintToolBar.prototype.clearSelection = function() {
    for (var i in this._tool) {
        if (i.indexOf('dot') != -1 || i.indexOf('p3') != -1) {
            continue;
        }
        if (i.indexOf('forms') != -1 || i.indexOf('p4') != -1) {
            continue;
        }

        if (i.indexOf('stamp') != -1 || i.indexOf('p5') != -1) {
            continue;
        }

        if (i.indexOf('person') != -1 || i.indexOf('p6') != -1) {
            continue;
        }

        this._tool[i].closeContour();
        this._tool[i].selected = false;

    }
};

PaintToolBar.prototype.reset = function(){
    this._selectDotOption(0);
    this._selectFormOption(0);

    this._colorOp = this._option.color_option_p1;
    this._selectColor(0);

    this._openPage(1);

    this._p1.pencil_tool_p1.selected = false;
};
