/**
 * @module ui/keyboard/keyboardModules
 */
var PIXI = require('PIXI');
var Bitmap = require('../../../display/core/Bitmap');

/**
  * @classdesc Classe que define os Input e sua exibição
  * @memberof module:ui/keyboard/keyboardModules
  * @extends Pixi.Container
  * @exports BaseModule
  * @constructor
  * @param moduleURL {string} URL
  */
function BaseModule(moduleURL) {
    PIXI.Container.call(this);

    this._url = moduleURL;
    this._data = null;
    this._keys = [];


    this._loader = new PIXI.loaders.Loader();
    this._loader.add(moduleURL);
    this._loader.on('complete', this._onLoadComplete, this);
    this._loader.load();
}

BaseModule.prototype = Object.create(PIXI.Container.prototype);
BaseModule.prototype.constructor = BaseModule;
module.exports = BaseModule;

/**
 * metodo que controla ao termino da carga
 * @memberof BaseModule
 * @param e {object} evento
 * @private
 */
BaseModule.prototype._onLoadComplete = function(e) {
    this._data = e.resources[this._url].data;

    for (var i in this._data.frames) {
        var name = i.replace(/mod_\w+_|\Wpng/g, "");
        var k = new Bitmap(PIXI.Texture.fromFrame(i));
        k.name = name;

        k.x = this._data.frames[i].spriteSourceSize.x;
        k.y = this._data.frames[i].spriteSourceSize.y;
        if (name == 'back') {
            this.addChildAt(k, 0);
            k.interactive = true;
            continue;
        }

        this._keys.push(k);
        this.addChild(k);
    }
};

/**
 * metodo que controla ao clicar
 * @memberof BaseModule
 * @param e {object} evento
 * @private
 */
BaseModule.prototype._onClicked = function(e) {
    this.emit('clicked', {
        target: {
            name: e.target.name
        }
    });
};

/**
 * insere eventos ao BaseModule
 */
BaseModule.prototype.addEvents = function() {
    for (var i = 0; i < this._keys.length; i++) {
        var k = this._keys[i];
        //k.addEvents();
        //k.on('clicked', this._onClicked, this);
        k.interactive = true;
        k.buttonMode = true;
        k.on('mousedown', this._onDownKey, this);
        k.on('touchstart', this._onDownKey, this);

        k.on('mouseup', this._onUpKey, this);
        k.on('touchend', this._onUpKey, this);

        k.on('mouseupoutside', this._onOutKey, this);
        k.on('touchendoutside', this._onOutKey, this);
    }
};

/**
 * remove eventos ao BaseModule
 */
BaseModule.prototype.removeEvents = function() {
    for (var i = 0; i < this._keys.length; i++) {
        var k = this._keys[i];
        //k.removeEvents();
        //k.removeListener('clicked');
        k.interactive = false;
        k.buttonMode = false;
        k.removeListener('mousedown');
        k.removeListener('touchstart');

        k.removeListener('mouseup');
        k.removeListener('touchend');

        k.removeListener('mouseupoutside');
        k.removeListener('touchendoutside');
    }
};

/**
 * metodo que controla tecla pressionada
 * @memberof BaseModule
 * @param e {object} evento
 * @private
 */
BaseModule.prototype._onDownKey = function(e) {
    var k = e.target;
    this._removeDown(k);

    k.scale.set(0.8, 0.8);
};

/**
 * metodo que controla tecla pressionada
 * @memberof BaseModule
 * @param e {object} evento
 * @private
 */
BaseModule.prototype._onUpKey = function(e) {
    var k = e.target;

    k.scale.set(1, 1);

    this._onClicked({
        target: {
            name: k.name
        }
    });
};

/**
 * metodo que controla tecla
 * @memberof BaseModule
 * @param e {object} evento
 * @private
 */
BaseModule.prototype._onOutKey = function(e) {
    var k = e.target;

    k.scale.set(1, 1);

    // this._removeDown();
    var self = this;
    setTimeout(function() {
        self._addDown(k);
    }, 100);
};

/**
 * metodo que controla tecla pressionada
 * @memberof BaseModule
 * @param exclude {string} tecla
 * @private
 */
BaseModule.prototype._removeDown = function(exclude) {
    for (var i = 0; i < this._keys.length; i++) {
        var k = this._keys[i];
        if (k == exclude) continue;

        k.interactive = false;
        k.removeAllListeners();
    }
};

/**
 * metodo que controla tecla pressionada
 * @memberof BaseModule
 * @param exclude {string} tecla
 * @private
 */
BaseModule.prototype._addDown = function(exclude) {
    for (var i = 0; i < this._keys.length; i++) {
        var k = this._keys[i];
        if (k == exclude) continue;

        k.interactive = true;
        k.on('mousedown', this._onDownKey, this);
        k.on('touchstart', this._onDownKey, this);

        k.on('mouseup', this._onUpKey, this);
        k.on('touchend', this._onUpKey, this);

        k.on('mouseupoutside', this._onOutKey, this);
        k.on('touchendoutside', this._onOutKey, this);
    }
};
