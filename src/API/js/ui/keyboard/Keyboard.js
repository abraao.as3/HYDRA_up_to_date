var PIXI = require('PIXI');
var TweenMax = require('TweenMax');
var Bitmap = require('../../display/core/Bitmap');
var BaseModule = require('./keyboardModules/BaseModule');
var behavior = require('../../behavior/index');
var Const = require('../../core/Const');


function Keyboard(textures) {
    PIXI.Container.call(this);

    this.POS_Y = 412;
    this._textures = textures[0];
    this._type = 'default';
    this._keys = [];
    this._map = {};
    this._lower = [];
    this._upper = [];
    this._numeric = [];
    this._symbol = [];
    this._action = [];
    this._modules = [];
    this._currentKeyboard = "lower";

    this._timeToFireModule = 0.5;
    this._backClicked = false;
    this._moduleOpen = false;
    this._modeuleEventControl = true;

    for (var component in this._textures) {
        var k = new Bitmap(this._textures[component].texture);
        k.addPlugin(new behavior.Clickable());

        k.name = component.replace(/\w*_|\W{1}png/g, "");

        k.x = this._textures[component].config.x;
        k.y = this._textures[component].config.y;

        k.pivot.set(k.width / 2, k.height /2);
        k.x += k.width / 2;
        k.y += k.height / 2;

        if (component.indexOf('lower_') != -1) {
            this._lower.push(k);
        } else if (component.indexOf('upper_') != -1) {
            this._upper.push(k);
        } else if (component.indexOf('num_') != -1) {
            this._numeric.push(k);
        } else if (component.indexOf('symbol_') != -1) {
            this._symbol.push(k);
        } else {
            this._action.push(k);
        }

        this._map[k.name] = k;
        this._keys.push(k);
        this.addChild(k);
    }

    this._back = new PIXI.Graphics();
    this._back.beginFill(0, 0.8);
    this._back.drawRoundedRect(0, 0, this.width + 10, this.height + 10, 15);
    this._back.endFill();
    this._back.visible = false;

    this.addChild(this._back);

    var upperA = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_a.json');
    this._addModule('A', upperA);

    var upperE = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_e.json');
    this._addModule('E', upperE);

    var upperI = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_i.json');
    this._addModule('I', upperI);

    var upperO = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_o.json');
    this._addModule('O', upperO);

    var upperU = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_u.json');
    this._addModule('U', upperU);

    var upperC = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_c.json');
    this._addModule('C', upperC);

    var upperN = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_n.json');
    this._addModule('N', upperN);

    var upperS = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_s.json');
    this._addModule('S', upperS);

    var upperY = new BaseModule('telas/commom/ui/keyboard/modules/upper/mod_upper_y.json');
    this._addModule('Y', upperY);

    var lowera = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_a.json');
    this._addModule('a', lowera);

    var lowere = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_e.json');
    this._addModule('e', lowere);

    var loweri = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_i.json');
    this._addModule('i', loweri);

    var lowero = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_o.json');
    this._addModule('o', lowero);

    var loweru = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_u.json');
    this._addModule('u', loweru);

    var lowerc = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_c.json');
    this._addModule('c', lowerc);

    var lowern = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_n.json');
    this._addModule('n', lowern);

    var lowers = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_s.json');
    this._addModule('s', lowers);

    var lowery = new BaseModule('telas/commom/ui/keyboard/modules/lower/mod_lower_y.json');
    this._addModule('y', lowery);

    var interrogacao = new BaseModule('telas/commom/ui/keyboard/modules/punctuation/mod_interrogacao.json');
    this._addModule('\u003f', interrogacao);

    var exclamacao = new BaseModule('telas/commom/ui/keyboard/modules/punctuation/mod_exclamacao.json');
    this._addModule('\u0021', exclamacao);

    this.alpha = 0;
    this.visible = false;
}

Keyboard.prototype = Object.create(PIXI.Container.prototype);
Keyboard.prototype.constructor = Keyboard;
module.exports = Keyboard;

Object.defineProperties(Keyboard.prototype, {
    type: {
        get: function() {
            return this._type;
        },

        set: function(value) {
            if (value != this._type) {
                this._type = value;
                switch (value) {
                    case Keyboard.TYPE.DEFAULT:
                        this._clicked({
                            target: this._map.alfabetico
                        });
                        break;
                    case Keyboard.TYPE.NUMERIC:
                        this._clicked({
                            target: this._map.numerico
                        });
                        break;
                    case Keyboard.TYPE.SYMBOL:
                        this._clicked({
                            target: this._map.simbolos
                        });
                        break;
                    case Keyboard.TYPE.LOWER:
                        this._clicked({
                            target: this._map.alfabetico
                        });
                        break;
                    case Keyboard.TYPE.UPPER:
                        this._clicked({
                            target: this._map.alfabetico
                        });
                        break;
                }
            }
        }
    }
});

Keyboard.prototype._showNumeric = function() {
    for (i = 0; i < this._numeric.length; i++) {
        this._numeric[i].interactive = true;
        this._numeric[i].visible = true;
    }
};

Keyboard.prototype._hideNumeric = function() {
    for (i = 0; i < this._numeric.length; i++) {
        this._numeric[i].interactive = false;
        this._numeric[i].visible = false;
    }
};

Keyboard.prototype._showSymbol = function() {
    for (i = 0; i < this._symbol.length; i++) {
        this._symbol[i].interactive = true;
        this._symbol[i].visible = true;
    }
};

Keyboard.prototype._hideSymbol = function() {
    for (i = 0; i < this._symbol.length; i++) {
        this._symbol[i].interactive = false;
        this._symbol[i].visible = false;
    }
};

Keyboard.prototype._showLower = function() {
    for (var i = 0; i < this._lower.length; i++) {
        this._lower[i].interactive = true;
        this._lower[i].visible = true;
    }
};

Keyboard.prototype._hideLower = function() {
    for (var i = 0; i < this._lower.length; i++) {
        this._lower[i].interactive = false;
        this._lower[i].visible = false;
    }
};

Keyboard.prototype._showUpper = function() {
    for (var i = 0; i < this._upper.length; i++) {
        this._upper[i].interactive = true;
        this._upper[i].visible = true;
    }
};

Keyboard.prototype._hideUpper = function() {
    for (var i = 0; i < this._upper.length; i++) {
        this._upper[i].interactive = false;
        this._upper[i].visible = false;
    }
};

Keyboard.prototype._showAction = function() {
    for (var i = 0; i < this._action.length; i++) {
        this._action[i].interactive = true;
        this._action[i].visible = true;
    }
};

Keyboard.prototype._hideAction = function() {
    for (var i = 0; i < this._action.length; i++) {
        this._action[i].interactive = false;
        this._action[i].visible = false;
    }
};

Keyboard.prototype.showKey = function(key) {
    var k = this._map[key];
    k.visible = true;
    k.interactive = true;
};

Keyboard.prototype.hideKey = function(key) {
    var k = this._map[key];
    k.visible = false;
    k.interactive = false;
};

Keyboard.prototype.onlyNumeric = function() {
    this._showNumeric();
    this._hideLower();
    this._hideUpper();

    this.children[36].visible = false;
    this.children[36].interactive = false;

    this.children[37].visible = true;
    this.children[37].interactive = true;
    this.children[37].x = 570;

    for (var i = 0; i < 10; i++) {
        this.children[i].x = 45 + 52 * i;
    }

};

Keyboard.prototype.show = function() {
    var self = this;

    if (this._type == "default") this._clicked({
        target: this._map.alfabetico
    });


    if (this.parent === null || this.parent === undefined) return;

    this.visible = true;
    this.alpha = 1;

    this.x = Const.BASE_WIDTH / 2 - this.width / 2;
    this.y = this.POS_Y;

    TweenMax.from(this, 0.5, {
        y: "+=" + 200,
        alpha: 0,
        onComplete: function() {
            self.addEvents();
        }
    });
};

Keyboard.prototype.showWithAnimation = function(type) {
    var self = this;

    if (this._type == "default") this._clicked({
        target: this._map.alfabetico
    });


    if (this.parent === null || this.parent === undefined) return;

    this.visible = true;
    this.alpha = 1;

    this.x = Const.BASE_WIDTH / 2 - this.width / 2;
    this.y = this.POS_Y;

    TweenMax.from(this, 0.5, {
        y: "+=" + 200,
        alpha: 0,
        onComplete: function() {
            self.addEvents();
        }
    });

    for (var i = 0; i < this._lower.length; i++) {

        TweenMax.from(this._lower[i], 0.5, {
            delay: i* 0.05,
            y: "+=" + 200,
            alpha: 0
        });
    }

};

Keyboard.prototype.hide = function() {
    var self = this;
    for (var i = 0; i < self._keys.length; i++) {
        var k = self._keys[i];
        k.removeEvents();
        k.removeListener("clicked");
    }

    TweenMax.to(this, 0.3, {
        y: "+=" + 100,
        alpha: 0,
        onComplete: function() {
            self.visible = false;
            self.y = self.POS_Y;
            self.removeEvents();
        }
    });
};

Keyboard.prototype._clicked = function(e) {
    var k = e.target;

    var self = this;

    setTimeout(function() {

        self._modeuleEventControl = true;

    }, 200);



    if (this._backClicked === true) {
        this._backClicked = false;
        return;
    }

    if (k.name == "numerico" || k.name == "numericoDL") {
        this._hideLower();
        this._hideUpper();
        this._hideSymbol();
        this._hideAction();

        this._currentKeyboard = "numeric";

        this._showNumeric();

        if (this._type == Keyboard.TYPE.NUMERIC) {
            this._map.alfabetico.alpha = 0.5;
            this._map.alfabetico.isOff = true;
        }

        return;
    }

    if (k.name == "simbolos") {
        this._hideNumeric();
        this._hideAction();
        this._hideUpper();
        this._hideLower();
        this.showKey('alfabetico');
        this._showSymbol();

        this._currentKeyboard = "symbol";

        if (this._type == Keyboard.TYPE.SYMBOL) {
            this._map.alfabetico.alpha = 0.5;
            this._map.numericoDL.alpha = 0.5;
            this._map.alfabetico.isOff = true;
            this._map.numericoDL.isOff = true;
        }
        return;
    }

    if (k.name == "alfabetico") {
        this._hideNumeric();
        this._hideSymbol();
        this._showAction();
        this.hideKey('alfabetico');

        if (this._type != Keyboard.TYPE.UPPER) {
            this._hideUpper();
            this.hideKey('shiftFix');
            this._currentKeyboard = "lower";
            this._showLower();

            if (this._type == Keyboard.TYPE.LOWER) {
                this._map.shift.alpha = 0.5;
                this._map.shift.isOff = true;
            }
        } else {
            this._hideLower();
            this.hideKey('shift');
            this._currentKeyboard = "upper";
            this._showUpper();

            this._map.shiftFix.alpha = 0.5;
            this._map.shiftFix.isOff = true;
        }

        return;
    }

    if (k.name == "shift") {
        this._hideLower();
        this.hideKey("shift");

        this._currentKeyboard = "upper";

        this.showKey("shiftFix");
        this._showUpper();
        return;
    }

    if (k.name == "shiftFix") {
        this.hideKey("shiftFix");

        this._clicked({
            target: this._map.alfabetico
        });
        return;
    }

    if (k.name == "enter") {
        this.emit("keyPressed", {
            value: "\n"
        });
        return;
    }

    if (k.name == "espaco") {
        this.emit("keyPressed", {
            value: " "
        });
        return;
    }

    if (this._moduleOpen === true) {
        this._closeModule();
    }

    this.emit("keyPressed", {
        value: k.name
    });
};

Keyboard.prototype.removeEvents = function() {
    var self = this;
    for (var i = 0; i < self._keys.length; i++) {
        var k = self._keys[i];

        if (k.module !== undefined) {
            k.removeListener('mousedown');
            k.removeListener('touchstart');

            k.removeListener('mouseup');
            k.removeListener('touchend');
            k.removeListener('mouseupoutside');
            k.removeListener('touchendoutside');

            k.removeListener("clicked");
            continue;
        }

        k.removeEvents();
        k.removeListener("clicked");
    }

    for (i = 0; i < this._modules.length; i++) {
        var m = this._modules[i];
        m.removeListener('clicked');
    }
};

Keyboard.prototype.addEvents = function() {
    var self = this;
    for (var i = 0; i < self._keys.length; i++) {
        var k = self._keys[i];
        if (k.isOff === true) continue;

        if (k.module !== undefined) {
            k.interactive = true;
            k.buttonMode = true;
            k.on('mousedown', this._onDownModuleKey, this);
            k.on('touchstart', this._onDownModuleKey, this);

            k.on('mouseup', this._onUpModuleKey, this);
            k.on('touchend', this._onUpModuleKey, this);
            k.on('mouseupoutside', this._onUpModuleKey, this);
            k.on('touchendoutside', this._onUpModuleKey, this);

            k.on("clicked", self._clicked, this);
            continue;
        }

        k.addEvents();
        k.on("clicked", self._clicked, this);
    }

    for (i = 0; i < this._modules.length; i++) {
        var m = this._modules[i];
        m.on('clicked', this._clicked, this);
    }
};

Keyboard.prototype._onDownModuleKey = function(e) {
    var self = this;
    var k = e.target;

    this._timeKeyPressed = setTimeout(function() {
        self._modeuleEventControl = false;
        k.removeListener('mouseup');
        k.removeListener('touchend');
        k.removeListener('mouseupoutside');
        k.removeListener('touchendoutside');

        self._openModule(k);
        k.scale.set(1, 1);
    }, this._timeToFireModule * 1000);

    k.scale.set(0.8, 0.8);
};

Keyboard.prototype._onUpModuleKey = function(e) {


    if (this._modeuleEventControl) {
        clearTimeout(this._timeKeyPressed);
        k = e.target;
        k.scale.set(1, 1);
        k.emit('clicked', e);
    }
};

Keyboard.prototype._addModule = function(key, module) {
    var k = this._map[key];
    k.module = module;
    module.visible = false;
    this.addChild(module);
    this._modules.push(module);
};

Keyboard.prototype._openModule = function(key) {
    this._moduleOpen = true;
    var k = key;
    this._back.key = k;
    this._back.visible = true;
    this._back.interactive = true;
    this._back.on('mousedown', this._closeModuleFromBack, this);
    this._back.on('touchstart', this._closeModuleFromBack, this);
    k.module.visible = true;
    setTimeout(function() {
        k.module.addEvents();
    }, 500);
};

Keyboard.prototype._closeModuleFromBack = function() {
    this._backClicked = true;
    this._closeModule();
};

Keyboard.prototype._closeModule = function() {
    this._back.removeListener('mousedown');
    this._back.removeListener('touchstart');

    var k = this._back.key;
    k.module.removeEvents();
    k.module.visible = false;
    this._back.visible = false;

    this._moduleOpen = false;

    k.on('mouseup', this._onUpModuleKey, this);
    k.on('touchend', this._onUpModuleKey, this);
    k.on('mouseupoutside', this._onUpModuleKey, this);
    k.on('touchendoutside', this._onUpModuleKey, this);
};


Keyboard.TYPE = {
    DEFAULT: 'default',
    NUMERIC: 'numeric',
    SYMBOL: 'symbol',
    LOWER: 'lower',
    UPPER: 'upper'
};
