var ObjectBase = require('../core/ObjectBase');
var Bitmap = require('../display/core/Bitmap');
var PIXI = require('PIXI');
var behavior = require('../behavior/index');

/**
* @classdesc Representa o SeekBar
* @memberof module:ui
* @exports SeekBar
* @constructor
* @param textures {Object[]} objeto contendo as texturas referente ao seekBar
* @param config {Object[]} objeto contendo valores configuraveis do seekBar
*/
function SeekBar(textures, config) {
	ObjectBase.call(this);

	this._config = config || {slideBar: false};

	this._debug = config.debug || false;

	//grupo que armazena os botões
    this._buttons = {};

	//grupo que armazenas os seeks
    this._seekIcon = null;

	this._seekBarPath = null;

	this._seekBarFull = null;

	this._seekBarBack = null;

    //grupo que armazena os backs
    this._back = null;


    //grupo que armazena todos
    this._all = {};

	//para verificar se a barra de volume está aberta
    this._expanded = true;

	this._orientation = null;

	this._direction = null;

    var bts = ["iconOn", "iconOff"];

    //grupo que armazena os itens da barra de volume
    this._seekBar = [];

	var nameSufixRegexp = /(?=[^_]*$)(.+)(?=\.png$)/i; // captura o texto que estiver entre o ultimo "_" e a extensão do arquivo (no caso a extensão está fixa para ".png")
	var minX = null, minY = null;
    for (var i in textures[0]) {
        var obj = textures[0][i];
        var bt = new Bitmap(obj.texture);

		bt.x = obj.config.x;
        bt.y = obj.config.y;

		if(minX === null || minX > bt.x) { minX = bt.x; }
		if(minY === null || minY > bt.y) { minY = bt.y; }

        var name = nameSufixRegexp.exec(i)[0];
		// console.log(name);

		// var name = i.replace('.png', "");

        bt.name = name;

        this._all[name] = bt;

		if(bts.indexOf(name) !== -1) {
			this._buttons[name] = bt;
		}

        if (name == "seekIcon") {
            this._seekIcon = bt;
        }

		if(name == "seekBarPath") {
			this._seekBarPath = bt;
		}

		if(name == "seekBarFull") {
			this._seekBarFull = bt;
		}

		if(name == "seekBarBack") {
			this._seekBarBack = bt;
		}

        if (name == "background") {
            this._back = bt;
            this.addChildAt(bt, 0);
            continue;
        }

        this.addChild(bt);

      /**
       * Emitido quando o botão de seek é precionado
       * @event seekDown
       * @memberof module:ui.SeekBar
       */

       /**
        * Emitido quando o botão de seek é solto
        * @event seekUp
        * @memberof module:ui.SeekBar
        */

        /**
         * Emitido sempre quando a posição do botão de seek é alterada
         * @event updateSeek
         * @memberof module:ui.SeekBar
         * @property target {Object} referente ao target, quem espera o evento
         * @property parent {Object} referente parent do target
         * @property pct {Object} referente à porcentagem 0 - 1 `50% = 0.5`
         */
    }

    this._orientation = this._all.seekBarPath.width > this._all.seekBarPath.height ? "horizontal" : "vertical";

	if(this._orientation == "horizontal") {
		if(this._all.seekBarPath.x < this._all.iconOn.x) {
			this._direction = "left";
		} else {
			this._direction = "right";
		}
	} else {
		if(this._all.seekBarPath.y < this._all.iconOn.y) {
			this._direction = "up";
		} else {
			this._direction = "down";
		}
	}

	this.setChildIndex(this._seekBarBack, this.children.length - 1);
	this.setChildIndex(this._seekBarPath, this.children.length - 1);
	this.setChildIndex(this._seekBarFull, this.children.length - 1);
	this.setChildIndex(this._seekIcon, this.children.length - 1);

	if(this._all.iconOn !== undefined && this._all.iconOn !== null) {
		this.setChildIndex(this._all.iconOn, this.children.length - 1);
	}

	if(this._all.iconOff !== undefined && this._all.iconOff !== null) {
		this.setChildIndex(this._all.iconOff, this.children.length - 1);
	}

	this._seekIcon.addPlugin(new behavior.Dragable());
    this._seekIcon.align('center', 'center');
    this._seekIcon.x += this._seekIcon.width / 2;
    this._seekIcon.y += this._seekIcon.height / 2;

	this._seekIcon.addEvents();

	if(this._config.slideBar) {
		this._expanded = false;
	}

	for (var k in this._buttons) {
        var b = this._buttons[k];

		b.align('center', 'center');
        b.x += b.width / 2;
        b.y += b.height / 2;

        if (b.name == "iconOff") b.visible = false;

		if(this._config.slideBar) {
			b.addPlugin(new behavior.Clickable());
			b.addEvents();
		}
    }

	if (this._orientation == "horizontal") {
		this._seekIcon._lockY = true;
		this._seekIcon._minX = this._seekBarPath.x;
		this._seekIcon._maxX = this._seekBarPath.x + this._seekBarPath.width;

	    // _mascara.drawRect(0, 0, this._seekBarBack.width, 50);
    } else if(this._orientation == "vertical") {
		this._seekIcon._lockX = true;
		this._seekIcon._minY = this._seekBarPath.y;
		this._seekIcon._maxY = this._seekBarPath.y + this._seekBarPath.height;

		// _mascara.drawRect(0, 0, 50, this._seekBarBack.height);
	}

	var _mascara = new PIXI.Graphics();
	_mascara.beginFill(0xff0000, 0.3);
	_mascara.drawRect(0, 0, this._seekBarBack.width, this._seekBarBack.height);
	_mascara.endFill();

	_mascara.x = this._seekBarBack.x;
	_mascara.y = this._seekBarBack.y + this._seekBarBack.height / 2 - _mascara.height / 2;

    this.addChild(_mascara);

    this._seekBar.push(this._seekIcon, this._seekBarPath, this._seekBarFull, this._seekBarBack);

	if (this._orientation == "horizontal") {
		if(this._direction == "left") {
			this._seekBar[0].x = this._seekBar[0]._minX;
		    this._seekBar[2].align("right", "top");
		    this._seekBar[2].x = this._seekBar[0]._maxX;

			if(this._config.seekPosition !== undefined && this._config.seekPosition !== null) {
				this._seekBar[0].x += this._seekBarPath.width*(1-(this._config.seekPosition/100));
				this._seekBar[2].scale.x = (this._config.seekPosition/100);
			}

			if(!this._expanded) {
				TweenMax.to(this._seekBar, 0, {
					x: "+=" + this._seekBarBack.width
				});
			}
		} else if(this._direction == "right") {
			this._seekBar[0].x = this._seekBar[0]._maxX;
		    this._seekBar[2].align("left", "top");
		    this._seekBar[2].x = this._seekBar[0]._minX;

			if(this._config.seekPosition !== undefined && this._config.seekPosition !== null) {
				this._seekBar[0].x -= this._seekBarPath.width*(1-(this._config.seekPosition/100));
				this._seekBar[2].scale.x = (this._config.seekPosition/100);
			}

			if(!this._expanded) {
				TweenMax.to(this._seekBar, 0, {
					x: "-=" + this._seekBarBack.width
				});
			}
		}
	} else if(this._orientation == "vertical") {
		if(this._direction == "up") {
			this._seekBar[0].y = this._seekBar[0]._minY;
		    this._seekBar[2].align("left", "bottom");
		    this._seekBar[2].y = this._seekBar[0]._maxY;

			if(this._config.seekPosition !== undefined && this._config.seekPosition !== null) {
				this._seekBar[0].y += this._seekBarPath.height*(1-(this._config.seekPosition/100));
				this._seekBar[2].scale.y = (this._config.seekPosition/100);
			}

			if(!this._expanded) {
				TweenMax.to(this._seekBar, 0, {
					y: "+=" + this._seekBarBack.height
				});
			}
		} else if(this._direction == "down") {
			this._seekBar[0].y = this._seekBar[0]._maxY;
		    this._seekBar[2].align("left", "top");
		    this._seekBar[2].y = this._seekBar[0]._minY;

			if(this._config.seekPosition !== undefined && this._config.seekPosition !== null) {
				this._seekBar[0].y -= this._seekBarPath.height*(1-(this._config.seekPosition/100));
				this._seekBar[2].scale.y = (this._config.seekPosition/100);
			}

			if(!this._expanded) {
				TweenMax.to(this._seekBar, 0, {
					y: "-=" + this._seekBarBack.height
				});
			}
		}
	}

	for (var n = 0; n < this._seekBar.length; n++) {
		if(this._seekBar[n] !== undefined && this._seekBar[n] !== null) {
			this._seekBar[n].mask = _mascara;
		}
    }


    this.x = Math.round(minX);
    this.y = Math.round(minY);

	for (i = 0; i < this.children.length; i++) {
		this.children[i].x = this.children[i].x - this.x;
		this.children[i].y = this.children[i].y - this.y;

		if(this.children[i]._minX != -1) {
			this.children[i]._minX = this.children[i]._minX - this.x;
		}

		if(this.children[i]._maxX != -1) {
			this.children[i]._maxX = this.children[i]._maxX - this.x;
		}

		if(this.children[i]._minY != -1) {
			this.children[i]._minY = this.children[i]._minY - this.y;
		}

		if(this.children[i]._maxY != -1) {
			this.children[i]._maxY = this.children[i]._maxY - this.y;
		}
	}

	this.pivot.set(this.width / 2, this.height / 2);
    this.x += this.width / 2;
    this.y += this.height / 2;

	if (this._debug === true) {
        var graphics = new PIXI.Graphics();

        graphics.beginFill("0x00ff00", 0.5);
        graphics.drawRect(0, 0, this.width, this.height);
        graphics.endFill();
        graphics.x = 0;
        graphics.y = 0;
        this.addChild(graphics);
    }
}

SeekBar.prototype = Object.create(ObjectBase.prototype);
SeekBar.prototype.constructor = SeekBar;
module.exports = SeekBar;

Object.defineProperties(SeekBar.prototype, {
	/**
     * @memberof module:ui.SeekBar
     * @name debug
     */
	debug: {
		get: function() {
			return this._debug;
		},
		set: function(value) {
			this._debug = value;
		}
	}
});

/**
 * Metodo que adiciona eventos ao SeekBar
 */
SeekBar.prototype.addEvents = function() {
	for(var b in this._buttons) {
		this._buttons[b].on("clicked", this._toogleBar, this);
	}

	this._seekIcon.on("dragStart", this._seekDragStart, this);
	this._seekIcon.on("dragReleased", this._seekDragReleased, this);
	this._seekIcon.on("positionUpdated", this._seekPositionUpdated, this);
};

/**
 * @memberof SeekBar
 * @param e evento
 * @private
 */
SeekBar.prototype._toogleBar = function(e) {

	e.target.removeListener("clicked");

	var clickedObj = e.target;

	var self = this;
	var animationSettings = {
		target: this._seekBar,
		duration: 0.2
	};

	if(this._expanded) {
		animationSettings.properties = {
			onComplete: function() {
				self._expanded = false;
				clickedObj.on("clicked", self._toogleBar, self);
			}
		};

		if(this._orientation == "horizontal") {
			if(this._direction == "left") {
				animationSettings.properties.x = "+=" + this._seekBarBack.width;
			} else if(this._direction == "right") {
				animationSettings.properties.x = "-=" + this._seekBarBack.width;
			}
		} else if(this._orientation == "vertical") {
			if(this._direction == "up") {
				animationSettings.properties.y = "+=" + this._seekBarBack.height;
			} else if(this._direction == "down") {
				animationSettings.properties.y = "-=" + this._seekBarBack.height;
			}
		}
	} else {
		animationSettings.properties = {
			onComplete: function() {
				self._expanded = true;
				clickedObj.on("clicked", self._toogleBar, self);
			}
		};

		if(this._orientation == "horizontal") {
			if(this._direction == "left") {
				animationSettings.properties.x = "-=" + this._seekBarBack.width;
			} else if(this._direction == "right") {
				animationSettings.properties.x = "+=" + this._seekBarBack.width;
			}
		} else if(this._orientation == "vertical") {
			if(this._direction == "up") {
				animationSettings.properties.y = "-=" + this._seekBarBack.height;
			} else if(this._direction == "down") {
				animationSettings.properties.y = "+=" + this._seekBarBack.height;
			}
		}
	}

	TweenMax.to(animationSettings.target, animationSettings.duration, animationSettings.properties);
};

/**
 * @memberof SeekBar
 * @param e evento
 * @private
 */
SeekBar.prototype._seekDragStart = function(e) {
	// console.log("_seekDragStart");
	this.emit('seekIconDown');
};

/**
 * @memberof SeekBar
 * @param e evento
 * @private
 */
SeekBar.prototype._seekDragReleased = function(e) {
	// console.log("_seekDragReleased", this);
	this.emit('seekIconUp');
};

/**
 * @memberof SeekBar
 * @param scale {Number}
 * @private
 */
SeekBar.prototype._updateSeekBarFull = function(scale) {
	if(this._orientation == "horizontal") {
		this._seekBarFull.scale.x = scale;
	} else if(this._orientation == "vertical") {
		this._seekBarFull.scale.y = scale;
	}

	if(this._all.iconOff !== undefined && this._all.iconOff !== null) { // so troca os icones caso exista o "iconOff"
		if(scale < 0.01) {
			this._all.iconOff.visible = true;
			this._all.iconOn.visible = false;
		} else {
			this._all.iconOn.visible = true;
			this._all.iconOff.visible = false;
		}
	}
};

/**
 * @memberof SeekBar
 * @param e evento
 * @private
 */
SeekBar.prototype._seekPositionUpdated = function(e) {
	var t = e.target;
    var pct = null;

	if(this._orientation == "horizontal") {
		if(this._direction == "left") {
			pct = Math.round(((t._maxX - t.x) / (t._maxX - (t._minX)) * 100));
		} else if(this._direction == "right") {
			// pct = Math.round(((t.x - t._minX) / (t._maxX - (t._minX)) * 100));
			pct = Math.round(((t.x - t._minX) / (t._maxX - t._minX))*100);
		}
	} else if(this._orientation == "vertical") {
		if(this._direction == "up") {
			pct = Math.round(((t._maxY - t.y) / (t._maxY - (t._minY)) * 100));
		} else if(this._direction == "down") {
			pct = Math.round(((t.y - t._minY) / (t._maxY - (t._minY)) * 100));
		}
	}

    this._updateSeekBarFull(pct / 100);

    this.emit('seekPositionUpdate', {target: t, parent: this, pct: (pct / 100)});
};
