/**
 * @module ui/calculator
 */
var ObjectBase = require('../../core/ObjectBase');
var Const = require('../../core/Const');
var PIXI = require('PIXI');
var behavior = require('../../behavior/index');
var PIXIConfig = require('../../core/PIXIConfig');
var Bitmap = require('../../display/core/Bitmap');
var TextField = require('../../display/text/TextField');

/**
 * @classdesc Representa a calculator
 * @memberof module:ui/calculator
 * @extends PIXI.Container
 * @exports Calculator
 * @param {module:ui/calculator.textures}
 * @constructor
 */
function Calculator(textures) {
    var self = this;
    ObjectBase.call(this);

    this.keys = [];
    this.up = null;
    this.down = null;

    this.field = null;


    this.memory = "";
    this.fieldText = "";
    this.oper = "";

    this._textures = textures[0];


    //configurando teclas
    for (var t in this._textures) {

        var element = new Bitmap(this._textures[t].texture);

        element.x = this._textures[t].config.x;
        element.y = this._textures[t].config.y;

        element.pivot.set(element.width / 2, element.height / 2);
        element.x += element.width / 2;
        element.y += element.height / 2;

        if (t.indexOf('fundoC') === -1 && t.indexOf('fundoN') === -1) {

            element.name = t.replace(/\w*_|\W{1}png/g, "");

            element.addPlugin(new behavior.Clickable());

            this.keys.push(element);

            if (t.indexOf('up') === 0) {
                element.on("clicked", self.hide, self);
                element.visible = false;
                this.up = element;
            }

            if (t.indexOf('down') === 0) {
                element.on("clicked", self.show, self);
                this.down = element;
            }

            if (t.indexOf('down') !== 0 && t.indexOf('up') !== 0) {
                element.on("clicked", self.keyPressed, this, self);
            }
        } else {
            if (t.indexOf('fundoN') === -1) {
                //fundo da calculadora, deve ficar por tras
                this.addChildAt(element, 0);
                continue;
            } {
                //fundo dos numeros temos que adicionar o type.

                var text = new TextField(element.width, element.height);
                this.field = text;
                this.field.textAlign = "right";

                this.field.y = 8;
                this.field.setText("");

                element.addChild(this.field);

            }
        }

        this.addChild(element);
    }

    this.pivot.set(this.width / 2, this.height / 2);
    this.x = Const.BASE_WIDTH / 2;
    this.y += this.height / 2;
}

Calculator.prototype = Object.create(ObjectBase.prototype);
Calculator.prototype.constructor = Calculator;
module.exports = Calculator;

/**
 * Metodo que mostra a calculadora
 */
Calculator.prototype.show = function() {
    var self = this;

    self.removeEvents();

    TweenMax.to(self, 1, {
        y: "+=111",
        onComplete: function() {

            self.up.visible = true;
            self.down.visible = false;
            self.addEvents();

        }
    });
};

/**
 * Metodo que esconde a calculadora
 */
Calculator.prototype.hide = function() {
    var self = this;

    self.removeEvents();

    TweenMax.to(self, 1, {
        y: "-=111",
        onComplete: function() {

            self.up.visible = false;
            self.down.visible = true;
            self.addEvents();

        }
    });
};

/**
 * Metodo que adiciona eventos na calculadora
 */
Calculator.prototype.addEvents = function() {

    for (var i = 0; i < this.keys.length; i++) {
        this.keys[i].addEvents();
    }

};

/**
 * Metodo que remove eventos na calculadora
 */
Calculator.prototype.removeEvents = function() {

    for (var i = 0; i < this.keys.length; i++) {
        this.keys[i].removeEvents();
    }

};

/**
 * Metodo que insere texto na calculadora
 * @param texto
 */
Calculator.prototype.text = function(texto) {
    texto = texto.toString();

    texto = texto.slice(0, 15);

    this.field.setText(texto);
};

/**
 * Metodo que efetua calculo
 */
Calculator.prototype.calculate = function() {
    if (this.oper !== "") {
        if (this.oper == "+") {
            this.fieldText =  parseInt(this.memory) + parseInt(this.fieldText);
            this.text(this.fieldText);
        }

        if (this.oper == "-") {
            this.fieldText = parseInt(this.memory) - parseInt(this.fieldText);
            this.text(this.fieldText);
        }

        if (this.oper == "*") {
            this.fieldText = parseInt(this.fieldText) * parseInt(this.memory);
            this.text(this.fieldText);
        }

        if (this.oper == "/") {
            this.fieldText = parseInt(this.fieldText) / parseInt(this.memory);
            this.text(this.fieldText);
        }

        if (this.oper == "%") {
            this.fieldText = parseInt(this.memory) / parseInt(this.fieldText) * 100;
            this.text(this.fieldText);
        }
    }
};

/**
 * Metodo que adiciona eventos na calculadora
 * @param oper operacao a ser calculada
 */
Calculator.prototype.operation = function(oper) {


    if (this.memory !== "" && this.fieldText !== "") {
        this.calculate();
        this.memory = this.fieldText;
    } else {
        if (this.fieldText !== "" && this.fieldText !== "infinity" && this.fieldText !== "NaN") {
            this.memory = this.fieldText;
        }
    }
    this.oper = oper;

    this.fieldText = "";
};

/**
 * Metodo que verifica que acao foi pressionada
 * @param e {Object} evento
 */
Calculator.prototype.keyPressed = function(e) {

    if (isNaN(parseInt(e.target.name))) {

        //apertou uma opeção
        if (e.target.name == "clearField") {
            this.fieldText = "";
            this.text(this.fieldText);
            return;
        }

        if (e.target.name == "backspace") {
            this.fieldText = this.fieldText.slice(0, this.fieldText.length - 1);
            this.text(this.fieldText);
            return;
        }

        if (e.target.name == "point") {
            if (this.fieldText.indexOf(".") == -1) {
                this.fieldText = this.fieldText + ".";
                this.text(this.fieldText);
            }
            return;
        }

        if (e.target.name == "clearMemory") {
            this.oper = "";
            this.memory = "";
            this.fieldText = "";
            this.text(this.fieldText);
            return;
        }

        if (e.target.name == "fracao") {
            var conta = 1 / parseInt(this.fieldText);
            this.fieldText = conta.toString();

            this.text(this.fieldText);
            return;
        }

        if (e.target.name == "raiz") {
            var raiz = Math.sqrt(parseInt(this.fieldText));
            this.fieldText = raiz.toString();

            this.text(this.fieldText);
            return;
        }

        if (e.target.name == "igual") {
            this.calculate();

            this.oper = "";

            this.memory = this.fieldText;

            this.fieldText = "";

            return;
        }

        if (e.target.name == "adicao") {
            this.operation("+");
            return;
        }
        if (e.target.name == "subtracao") {
            this.operation("-");
            return;
        }
        if (e.target.name == "multiplicacao") {
            this.operation("*");
            return;
        }
        if (e.target.name == "diviso") {
            this.operation("/");
            return;
        }
        if (e.target.name == "porcentagem") {
            this.operation("%");
            return;
        }

    } else {
        //apertou um numero

        if (this.fieldText == "NaN" || this.fieldText == "Infinity") {
            this.fieldText = "";
        }

        this.fieldText = this.fieldText + e.target.name;
        this.text(this.fieldText);

    }

};
