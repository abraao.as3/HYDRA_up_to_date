/**
 * @module ui/controller
 */
var behavior = require('../../behavior/index');
var Const = require('../../core/Const');
var ObjectBase = require('../../core/ObjectBase');
var TweenMax = require('TweenMax');


/**
 * @classdesc Representa os controllers
 * @memberof module:ui/controller
 * @extends PIXI.Container
 * @exports AbstractController
 * @constructor
 */
function AbstractController() {
    ObjectBase.call(this);

    this._keys = [];

}

AbstractController.prototype = Object.create(ObjectBase.prototype);
AbstractController.prototype.constructor = AbstractController;
module.exports = AbstractController;

/**
 * Metodo ...
 */
AbstractController.prototype.comand = function() {


};

/**
 * Metodo que adiciona eventos
 */
AbstractController.prototype.addEvents = function() {


};

/**
 * Metodo que remove eventos
 */
AbstractController.prototype.removeEvents = function() {


};

/**
 * Metodo que mostra
 */
AbstractController.prototype.show = function() {


};

/**
 * Metodo que esconde
 */
AbstractController.prototype.hide = function() {


};
