var behavior = require('../../behavior/index');
var Const = require('../../core/Const');
var TweenMax = require('TweenMax');
var AbstractController = require('./AbstractController');
var Bitmap = require('../../display/core/Bitmap');

/**
 * @classdesc Representa os controllers
 * @memberof module:ui/controller
 * @extends PIXI.Container
 * @exports AnalogController
 * @param {module:ui/controller.textures}
 * @constructor
 */
function AnalogController(textures) {
    AbstractController.call(this);

    this._textures = textures[0];

    for(var t in this._textures){

        var element = new Bitmap(this._textures[t].texture);

        element.x = this._textures[t].config.x;
        element.y = this._textures[t].config.y;

        element.pivot.set(element.width / 2, element.height / 2);
        element.x += element.width / 2;
        element.y += element.height / 2;

        if (t.indexOf('back') !== -1) {

            element.addPlugin(new behavior.Clickable());

            this._keys.push(element);
        }

        this.addChild(element);
    }
}

AnalogController.prototype = Object.create(AbstractController.prototype);
AnalogController.prototype.constructor = AnalogController;
module.exports = AnalogController;
