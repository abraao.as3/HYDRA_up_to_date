var ObjectBase = require('../../core/ObjectBase');
var Bitmap = require('../../display/core/Bitmap');
var PIXI = require('PIXI');
var behavior = require('../../behavior/index');
var Const = require('../../core/Const');

/**
* @classdesc Representa o player de vídeo (incluso na interface do usuário)
* @see module:ui.Interface
* @memberof module:ui
* @constructor
* @param textures {Object[]} objeto contendo as texturas referente ao player
*/
function VideoPlayer(textures) {
    ObjectBase.call(this);

    //grupo que armazena os botões
    this._buttons = {};

    //grupo que armazenas os seeks
    this._seeks = {};

    //grupo que armazena os backs
    this._backs = {};

    //grupo que armazena todos
    this._all = {};

    /**
    * posição do tempo corrente em porcentagem
    * @member {number}
    */
    this.seekPosition = 0;
    /**
    * posição do volume corrente em porcentagem
    * @member {number}
    */
    this.seekVolPosition = 0;

    //para verificar se a barra de volume está aberta
    this._volOpen = false;

    var bts = ["play", "pause", "vol", "volOff"];

    //grupo que armazena os itens da barra de volume
    this._volBar = [];

    for (var i in textures[0]) {
        var obj = textures[0][i];
        var bt = new Bitmap(obj.texture);
        bt.x = obj.config.x;
        bt.y = obj.config.y;

        var name = i.replace('.png', "");
        bt.name = name;

        this._all[name] = bt;

        if (bts.indexOf(name) !== -1) {
            this._buttons[name] = bt;
        }

        if (name == "seek" || name == "seekVol") {
            this._seeks[name] = bt;
        }

        if (name.match(/back/ig) !== null) {
            this._backs[name] = bt;
            this.addChildAt(bt, 0);
            continue;
        }

        this.addChild(bt);

    /**
     * Emitido quando o botão de play é clicado
     * @event play
     * @memberof module:ui.VideoPlayer
     */

     /**
      * Emitido quando o botão de pause é clicado
      * @event pause
      * @memberof module:ui.VideoPlayer
      */

      /**
       * Emitido quando o botão de seek é precionado
       * @event seekDown
       * @memberof module:ui.VideoPlayer
       */

       /**
        * Emitido quando o botão de seek é solto
        * @event seekUp
        * @memberof module:ui.VideoPlayer
        */

        /**
         * Emitido sempre quando a posição do botão de seek é alterada
         * @event updateSeek
         * @memberof module:ui.VideoPlayer
         * @property target {Object} referente ao target, quem espera o evento
         * @property parent {Object} referente parent do target
         * @property pct {Object} referente à porcentagem 0 - 1 `50% = 0.5`
         */

         /**
          * Emitido quando o botão de seekVol é precionado
          * @event seekVolDown
          * @memberof module:ui.VideoPlayer
          */

          /**
           * Emitido quando o botão de seekVol é solto
           * @event seeVolkUp
           * @memberof module:ui.VideoPlayer
           */

           /**
            * Emitido sempre quando a posição do botão de seek do volume é alterada
            * @event updateVol
            * @memberof module:ui.VideoPlayer
            * @property target {Object} referente ao target, quem espera o evento
            * @property parent {Object} referente parent do target
            * @property pct {Object} referente à porcentagem 0 - 1 `50% = 0.5`
            */
    }

    this.setChildIndex(this._backs.background, 0);

    for (var j in this._seeks) {
        var s = this._seeks[j];
        this.setChildIndex(s, this.children.length - 1);

        s.addPlugin(new behavior.Dragable());
        s.align('center', 'center');
        s.x += s.width / 2;
        s.y += s.height / 2;

        s.addEvents();

        if (s.name == "seek") {
            s._lockY = true;
            s._minX = s.x;
            s._maxX = s.x + this._all.seekBar.width;

            var a1 = new PIXI.Graphics();
            a1.beginFill(0xff0000, 0);
            a1.drawRect(0, 0, 50, 50);
            a1.endFill();
            a1.x = -a1.width / 2;
            a1.y = -a1.height / 2;
            s.addChild(a1);
        } else {
            s._lockX = true;
            s._minY = this._all.seekBarVol.y;
            s._maxY = this._all.seekBarVol.y + this._all.seekBarVol.height;

            var a2 = new PIXI.Graphics();
            a2.beginFill(0xff0000, 0);
            a2.drawRect(0, 0, 30, 30);
            a2.endFill();
            a2.x = -a2.width / 2;
            a2.y = -a2.height / 2;
            s.addChild(a2);
        }
    }

    for (var k in this._buttons) {
        var b = this._buttons[k];
        b.addPlugin(new behavior.Clickable());
        b.align('center', 'center');
        b.x += b.width / 2;
        b.y += b.height / 2;

        if (b.name == "pause" || b.name == "volOff") b.visible = false;
    }

    //mascara para a barra de volume
    var _mascara = new PIXI.Graphics();
    _mascara.beginFill(0xff0000, 0.3);
    _mascara.drawRect(0, 0, 50, this._all.volBack.height);
    _mascara.endFill();
    _mascara.x = this._all.volBack.x + this._all.volBack.width / 2 - _mascara.width / 2;
    _mascara.y = this._all.volBack.y;

    this.addChild(_mascara);

    this._volBar.push(this._all.seekBarVol, this._all.seekVol, this._all.seekBarVolFull, this._all.volBack);
    this._volBar[1].y = this._volBar[1]._minY;
    this._volBar[2].align("left", "bottom");
    this._volBar[2].y += this._volBar[2].height;
    for (var n = 0; n < this._volBar.length; n++) {
        this._volBar[n].mask = _mascara;
    }
    TweenMax.to(this._volBar, 0, {
        y: "+=" + this._all.volBack.height
    });

    this.x = Const.BASE_WIDTH / 2 - this.width / 2;
    this.y = Const.BASE_HEIGHT;

    this._openPosition = Const.BASE_HEIGHT - this.height - 50;
    this.alpha = 0.9;
}

VideoPlayer.prototype = Object.create(ObjectBase.prototype);
VideoPlayer.prototype.constructor = VideoPlayer;
module.exports = VideoPlayer;

/**
* Coloca o player em sua posição inicial
*/
VideoPlayer.prototype.startPlayer = function(){
    this.y = Const.BASE_HEIGHT;
    console.log("startPlayer");
    this._updateSeekFull(0);
    this._updateSeekVolFull(0);
};

/**
* Abre o player na tela
*/
VideoPlayer.prototype.openPlayer = function() {
    console.log("openPlayer");
    this._buttons.vol.addEvents();
    this._buttons.volOff.addEvents();
    this._buttons.vol.on('clicked', this._toggleVol, this);
    this._buttons.volOff.on('clicked', this._toggleVol, this);
    this._all.seek.on('positionUpdated', this._getSeekPosition, this);
    this._all.seek.on('dragStart', this._seekDown, this);
    this._all.seek.on('dragReleased', this._seekUp, this);
    this._all.seekVol.on('positionUpdated', this._getSeekVolPosition, this);
    this._all.seekVol.on('dragStart', this._seekVolDown, this);
    this._all.seekVol.on('dragReleased', this._seekVolUp, this);

    this._buttons.play.addEvents();
    this._buttons.pause.addEvents();

    console.log("botao pause", this._buttons.pause.visible);
    console.log("botao play", this._buttons.play.visible);
    // this._buttons.play.visible = false;
    this._buttons.play.on('clicked', this._togglePlay, this);
    this._buttons.pause.on('clicked', this._togglePlay, this);

    TweenMax.to(this, 0.5, {y: this._openPosition});
};

/**
* Abre e fecha a barra de volume
* @private
*/
VideoPlayer.prototype._toggleVol = function(e) {
    e.target.removeListener('clicked');
    var t = e.target;
    var self = this;
    if (this._volOpen) {
        TweenMax.to(this._volBar, 0.2, {
            y: "+=" + this._all.volBack.height,
            onComplete: function() {
                self._volOpen = false;
                t.on('clicked', self._toggleVol, self);
            }
        });

    } else {
        TweenMax.to(this._volBar, 0.3, {
            y: "-=" + this._all.volBack.height,
            onComplete: function() {
                self._volOpen = true;
                t.on('clicked', self._toggleVol, self);
            }
        });
    }
};

/**
* Troca o botão de play/ pause
* @private
*/
VideoPlayer.prototype._togglePlay = function(e){
    console.log("_togglePlay", this._buttons.play.visible);
    if(this._buttons.play.visible)
    {
        this._buttons.play.visible = false;
        this._buttons.pause.visible = true;

        this.emit('play');
        console.log("play");
    }
    else {
        this._buttons.play.visible = true;
        this._buttons.pause.visible = false;

        this.emit('pause');
        console.log("pause");
    }
};

/**
* Muda a posição da barra cheia em relação à posição do seek
* @param scale {number} posição do seek em porcentagem
* @private
*/
VideoPlayer.prototype._updateSeekFull = function(scale){
    this._all.seekBarFull.scale.x = scale;
    this.seekPosition = scale * 100;
};

/**
* Muda a posição da barra cheia em relação à posição do seekVol
* @param scale {number} posição do seekVol em porcentagem
* @private
*/
VideoPlayer.prototype._updateSeekVolFull = function(scale){
    this._all.seekBarVolFull.scale.y = 1 - scale;
    this.seekVolPosition = scale * 100;

    if(1 - scale <= 0.05){
        this._buttons.vol.visible = false;
        this._buttons.volOff.visible = true;
    }
    else {
        this._buttons.vol.visible = true;
        this._buttons.volOff.visible = false;
    }
};

/**
* Ao precionar o seek o vídeo é pausado
* @private
* @fires seekDown
*/
VideoPlayer.prototype._seekDown = function(){
    this.emit('seekDown');
    this._buttons.play.visible = false;
    this._togglePlay();
};

/**
* Ao soltar o seek o vídeo começa a ser executado
* @private
* @fires seekUp
*/
VideoPlayer.prototype._seekUp = function(){
    this.emit('seekUp');
    this._buttons.play.visible = true;
    this._togglePlay();
};

/**
* @fires updateSeek
* @private
*/
VideoPlayer.prototype._getSeekPosition = function(e){
    var t = e.target;
    var pct = Math.round(((t.x - t._minX) / (t._maxX - (t._minX)) * 100));
    this._updateSeekFull(pct / 100);

    this.emit('updateSeek', {target: t, parent: this, pct: pct / 100});
};

/**
* @fires seekVolDown
* @private
*/
VideoPlayer.prototype._seekVolDown = function(){
    this.emit('seekVolDown');
};

/**
* @fires seeVolkUp
* @private
*/
VideoPlayer.prototype._seekVolUp = function(){
    this.emit('seeVolkUp');
};

/**
* @fires updateVol
* @private
*/
VideoPlayer.prototype._getSeekVolPosition = function(e){
    var t = e.target;
    var pct = Math.round(((t.y - t._minY) / (t._maxY - (t._minY)) * 100));
    this._updateSeekVolFull(pct / 100);

    this.emit('updateVol', {target: t, parent: this, pct: 1 - (pct / 100)});
};

/**
* Seta a posição do seek
* @param pct {number} valor da posição em porcentagem
*/
VideoPlayer.prototype.setSeekPosition = function(pct){
    var barPos = this._all.seekBar.width * pct;
    this._all.seek.x = barPos + this._all.seek._minX;

    this._getSeekPosition({target: this._all.seek});
};

/**
* Seta a posição do seek de volume
* @param pct {number} valor da posição em porcentagem
*/
VideoPlayer.prototype.setSeekVolPosition = function(pct){
    var barPos = this._all.seekBarVol.height * pct;
    this._all.seekVol.y = barPos + this._all.seekVol._minX;

    this._getSeekPosition({target: this._all.seekVol});
};

/**
* Método chamado quando termina a reprodução, serve para atualizar os botões de play/ pause
*/
VideoPlayer.prototype.ended = function(){
    this._togglePlay();
};

VideoPlayer.prototype.removeAll = function()
{
    console.log("removeAll");
    this._buttons.play.removeEvents();
    this._buttons.pause.removeEvents();
    this._buttons.play.removeListener('clicked');
    this._buttons.pause.removeListener('clicked');
    this._buttons.play.visible = true;
    this._buttons.pause.visible = false;
    console.log("botao pause", this._buttons.pause.visible);
    this.removeListener('updateVol');
    this.removeListener('seeVolkUp');
    this.removeListener('seekVolDown');
    this.removeListener('updateSeek');
    this.removeListener('seekUp');
    this.removeListener('seekDown');
    this.removeListener('pause');
    this.removeListener('play');

    this.seekPosition = 0;
    this.seekVolPosition = 0;
};
