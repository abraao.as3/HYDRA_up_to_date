var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
* @module display/buttons
*/
/**
* @classdesc Criação de botões
 * @memberof module:display/buttons
 * @extends ObjectBase
 * @exports Button
 * @constructor
 */
function Button() {
    ObjectBase.call(this);

    this._displayType = "button";
    this._config = null;
    this._action = [];
    this.soundManager = null;
    this.layersDisplay = null;
    this.popups = null;

    this._eventsAdded = false;

    this._clicked = false;

    this.addPlugin(new behavior.Clickable());
    this.addPlugin(new behavior.Pulsating());
    this.addPlugin(new behavior.AlignSupport());
}

Button.prototype = Object.create(ObjectBase.prototype);
Button.prototype.constructor = Button;
module.exports = Button;

Object.defineProperties(Button.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name displayType
     * @type {Object}
     * @memberof module:display/buttons.Button
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name config
     * @type {Object}
     * @memberof module:display/buttons.Button
     * @readonly
     */
    config: {
        get: function() {
            return this._config;
        },

        set: function(value) {
            this._config = value;
        }
    }
});

/** @function
* @description Inicia animação no elemento.
*/
Button.prototype.start = function() {
    if (this._config.animate === true) {
        this.animate();
    }

    if (this._config.action !== undefined && this._action !== null) {
        if (this._eventsAdded) return;
        this._eventsAdded = true;
        this.addEvents();

        // if (!this.listeners('clicked', true)) {
            this.on('clicked', function() {
                for (var i = 0; i < this._action.length; i++) {
                    this._action[i].fn(this, this._action[i].context, this._action[i].params);
                }
            }, this);
        // }
    }
};

/** @function
* @description Re inicia a classe.
*/
Button.prototype.reset = function() {
    this._eventsAdded = false;
    this.removeEvents();
    this.removeListener('clicked');
    this.stopAnimation();

};

/** @function
* @param name {string} nome
* @param action {object} ação
* @description Re inicia a classe.
*/
Button.prototype.defineAction = function(name, action) {
    var self = this;
    switch (name) {
        case "audio":
            this._action.push({
                fn: function(target, self) {
                    self.stopAnimation();
                    var mouths = self.layersDisplay.getObjects('mouths');
                    for (var i = 0; i < action.mouth.length; i++) {
                        mouths[action.mouth[i]].play();
                    }
                    // if (!self.soundManager.listeners('soundComplete', true)) {
                        self.soundManager.once('soundComplete', function() {
                            for (var j = 0; j < action.mouth.length; j++) {
                                mouths[action.mouth[j]].gotoAndStop(mouths[action.mouth[j]].stationary);
                            }
                        });
                    // }
                    self.emit('playSound', {target: this});
                    self.soundManager.playSound(action.id);
                },
                target: self,
                context: self
            });
            break;
        case "popup":
            this._action.push({
                fn: function(target, self) {
                    self.emit('openPopup', {target: self, action: action});
                },
                target: self,
                context: self
            });
            break;
        case "link":
            this._action.push({
                fn: function(target, self){
                    window.top.open(action, '_blank');
                },
                target: self,
                context: self
            });
            break;
    }
};

Button.ACTION_DEFAULT = ["audio", "popup", "link"];
