var display = module.exports = {

    Bitmap: require('./core/Bitmap'),

    text: require('./text/index'),

    core: require('./core/index'),

    simple: require('./simple/index'),

    complex: require('./complex/index'),

    //composed: require('./composed'),

    buttons: require('./buttons/index')


};
