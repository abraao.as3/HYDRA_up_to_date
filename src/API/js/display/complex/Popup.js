var ObjectBase = require('../../core/ObjectBase');
var PIXIConfig = require('../../core/PIXIConfig');
var Sillhuette = require('../../utils/Sillhuette');
var Interface = require('../../controller/InterfaceHandler');
var TweenMax = require('TweenMax');
var behavior = require('../../behavior/index');
var Layers = require('../core/Layers');
var Bitmap = require('../core/Bitmap');
var Const = require('../../core/Const');

/**
 * @classdesc Popup Class
 * @memberof module:display/complex
 * @extends ObjectBase
 * @exports Popup
 * @constructor
 * @example // modelo de codigo que abre e fecha o popup
 *{
     "id": 1,
     "images": [17],
     "txt": [6],
     "mouth": [0],
     "audio": 1
 }, {
     "id": 2,
     "action": {
         "popup": {
             "id": 0,
             "function": "open"
         }
     }
 }, {
     "id": 3,
     "action": {
         "delay": 5,
         "popup": {
             "id": 0,
             "function": "close"
         }
     }
 *
 */
function Popup() {
    ObjectBase.call(this);

    this._content = new Layers();

    this._radius = 100;
    this._closeSpace = 10;
    var _interface = Interface.getInstance();
    var texture = _interface.closeButton.sprite.texture.clone();
    this._close = new Bitmap(texture);
    this._close.addPlugin(new behavior.Clickable());

    this._timelineStart = false;
}

Popup.prototype = Object.create(ObjectBase.prototype);
Popup.prototype.constructor = Popup;
module.exports = Popup;

/** @function
* @description setBackground
* @param background {Object} background
*/
Popup.prototype.setBackground = function(background, config) {
    // var src = PIXIConfig.renderer.extract.image(background);
    // this._background = new PIXI.Sprite(Sillhuette.draw(src, [255, 255, 255]));
    // this._backgroundAlpha = 0.85;
    // this._background.interactive = true;
    // this.addChild(this._background);

    this._config = config;

    this._background = new PIXI.Graphics();
    this._background.beginFill(this._config.alphaBackground.colorAlpha);
    this._background.drawRect(0, 0, Const.BASE_WIDTH, Const.BASE_HEIGHT);
    this._background.interactive = true;
    this._background.endFill();
    this.addChild(this._background);
    this._background.on("mousedown", this._onClose, this);

};

/** @function
* @description setConfig
* @param config {Object} config
* @param layersDisplay {Object} layersDisplay
* @param soundManager {Object} soundManager
*/
Popup.prototype.setConfig = function(config, layersDisplay, soundManager) {
    this._config = config;
    this._ld = layersDisplay;
    this._sm = soundManager;

	this._keepOpenable = this._config.keepOpenable === true;

    var self = this;

    var imgs = this._ld.getMap("images");
    var txts = this._ld.getMap("text");
    var bts = this._ld.getMap("button");
	var movieClips = this._ld.getMap("movieClips");
    var scrolls = this._ld.getMap("scrolls");

    var id = null,
        j = 0,
        i = 0,
        parent = null,
		rect = null;

    if (this._config.images !== undefined && this._config.images !== null) {
        for (i = 0; i < this._config.images.length; i++) {
            id = this._config.images[i];
            for (j = 0; j < imgs.length; j++) {
                if (imgs[j].index == id) {
                    var img = imgs[j];
                    if (this._images === undefined || this._images === null)
                        this._images = [];
                    this._images.push(img);

                    parent = img.parent;
                    parent.removeChild(img);

                    this._content.getLayer('images').addChild(img);
                    this._content.addMap('images', img);
                }
            }
        }
    }

    if (this._config.txt !== undefined && this._config.txt !== null) {
        for (i = 0; i < this._config.txt.length; i++) {
            id = this._config.txt[i];
            for (j = 0; j < txts.length; j++) {
                if (txts[j].index == id) {
                    var txt = txts[j];
                    if (this._text === undefined || this._text === null)
                        this._text = [];
                    this._text.push(txt);

                    parent = txt.parent;
                    parent.removeChild(txt);

                    this._content.getLayer('text').addChild(txt);
                    this._content.addMap('text', txt);
                }
            }
        }
    }

    if(this._config.scrolls !== undefined && this._config.scrolls !== null){

        for (var i = 0; i < this._config.scrolls.length; i++) {

            var c_id = this._config.scrolls[i];
            var c_scroll = scrolls[c_id];

            if (this._scrolls === undefined || this._scrolls === null){this._scrolls = [];}
            this._scrolls.push(c_scroll);

            parent = c_scroll.parent;
            parent.removeChild(c_scroll);

            this._content.getLayer('scroll').addChild(c_scroll);
            this._content.addMap('scroll', c_scroll);
        }

    }

    if (this._config.buttons !== undefined && this._config.buttons !== null) {
        for (i = 0; i < this._config.buttons.length; i++) {
            id = this._config.buttons[i];
            for (j = 0; j < bts.length; j++) {
                if (bts[j].index == id) {
                    var b = bts[j];
                    if (this._bts === undefined || this._bts === null)
                        this._bts = [];
                    this._bts.push(b);
                    b.popupLinked = this.index;

                    parent = b.parent;
                    parent.removeChild(b);

                    this._content.getLayer('interactions').addChild(b);
                    this._content.addMap('buttons', b);
                }
            }
        }
    }

	if (this._config.background !== undefined && this._config.background !== null) {
		if(this._config.background.supImg !== undefined && this._config.background.supImg !== null) {
			for(i = 0; i < imgs.length; i++) {
				if(imgs[i].index == this._config.background.supImg) {
					rect = imgs[i];
                    imgs[this._config.background.supImg].interactive = true;
					parent = rect.parent;
					parent.removeChild(rect);

					break;
				}
			}
		}

		if(this._config.alphaBackground.backAlpha !== undefined && this._config.alphaBackground.backAlpha !== null) {
			this._backgroundAlpha = this._config.alphaBackground.backAlpha;
		}
	}

	if (this._config.movieClips !== undefined && this._config.movieClips !== null) {
		for(i = 0; i < this._config.movieClips.length; i++) {
			id = this._config.movieClips[i];
			for(j = 0; j < movieClips.length; j++) {
				if(movieClips[j].index == id) {
					var movieClip = movieClips[j];
					if(this._movieClips === undefined || this._movieClips === null) { this._movieClips = []; }

					this._movieClips.push(movieClip);

					parent = movieClip.parent;
					parent.removeChild(movieClip);

					this._content.getLayer("interactions").addChild(movieClip);
					this._content.addMap("interactions", movieClip);
					this._content.addMap("movieClips", movieClip);
				}
			}
		}
	}

    if (this._config.txt === undefined && this._config.images === undefined && this._config.buttons === undefined && this._config.movieClips === undefined) {
        throw "Popup: O popup está sem conteúdo";
    }

	var difX = null;
	var difY = null;

	if(rect === null) {
		var limitL = 2048;
	    var limitR = -2048;
	    var limitT = 2048;
	    var limitB = -2048;

	    var _children = this._content.getChildren();

	    for (i = 0; i < _children.length; i++) {
	        var c = _children[i];
            var bounds = c.getBounds();
            if (c._isScrollable) {
                var scroll = c.scroll;
                var bounds = {
                    "x":scroll.initX,
                    "y":scroll.initY,
                    "width":scroll.sW,
                    "height":scroll.sH
                }

            } else {
                var bounds = c.getBounds();
            }
            // console.log(c);
            // console.log(bounds);
	        if (bounds.x < limitL)
	            limitL = bounds.x;
	        if (bounds.x + bounds.width > limitR)
	            limitR = bounds.x + bounds.width;
	        if (bounds.y < limitT)
	            limitT = bounds.y;
	        if (bounds.y + bounds.height > limitB)
	            limitB = bounds.y + bounds.height;
	        }

	    var width = limitR - limitL;
	    var height = limitB - limitT;

	    rect = new PIXI.Graphics();
	    rect.beginFill(0xfefefe);
	    rect.drawRect(0, 0, width + this._radius, height + this._radius);
	    rect.endFill();
	    rect.x = limitL - this._radius / 2;
	    rect.y = limitT - this._radius / 2;

		var realCenterX = this._background.width / 2 - rect.width / 2;
	    var realCenterY = this._background.height / 2 - rect.height / 2;

	    difX = realCenterX - rect.x;
	    difY = realCenterY - rect.y;

	    var container = new PIXI.Container();
	    container.addChild(rect);

	    var rt = PIXI.RenderTexture.create(this._background.width, this._background.height);
	    PIXIConfig.renderer.render(container, rt);
	    this._rect = new PIXI.Sprite(Sillhuette.shadow(PIXIConfig.renderer.extract.image(rt)));

		this._close.align("center", "center");
	    this._close.x = rect.x + rect.width - this._close.width / 2 - this._closeSpace;
	    this._close.y = rect.y + this._closeSpace + this._close.width / 2;
	} else {
		this._rect = rect;
		difX = 0;
		difY = 0;

		this._close.align("center", "center");
	    this._close.x = rect.x + (rect.width/2) - this._close.width / 2 - this._closeSpace;
	    this._close.y = rect.y - (rect.height/2) + this._closeSpace + this._close.width / 2;
	}

	if(this._config.closeButton !== undefined && this._config.closeButton !== null) {
		if(this._config.closeButton.index !== undefined && this._config.closeButton.index !== null) {
			for (var z = 0; z < bts.length; z++) {
				var bt = bts[z];
				if(this._config.closeButton.index == bt.index) {
					var btParent = bt.parent;
					btParent.removeChild(bt);
					this._close = bt;
					break;
				}
			}
		}  else {
			if(this._config.closeButton.x !== undefined && this._config.closeButton.x !== null && !isNaN(this._config.closeButton.x)) {
				this._close.x = this._config.closeButton.x;
			}

			if(this._config.closeButton.y !== undefined && this._config.closeButton.y !== null && !isNaN(this._config.closeButton.y)) {
				this._close.y = this._config.closeButton.y;
			}
		}
	}

    this._content.getLayer("background").addChildAt(this._rect, 0);
    this._content.addMap("background", this._rect);

    this._content.getLayer("sup").addChild(this._close);
    this._content.addMap("sup", this._close);

    this._content.pivot.set(this._content.width / 2, this._content.height / 2);
    this._content.x = this._content.width / 2 + difX;
    this._content.y = this._content.height / 2 + difY;

    this.addChild(this._content);
};

Popup.prototype.init = function() {
    this._content.scale.set(0, 0);
    this._background.alpha = 0;
};

Popup.prototype.start = function(){
    if(!this._sequencesConfigured){
        this._sequencesConfigured = true;

        if (this._config.sequences !== undefined && this._config.sequences !== null) {
            this._sequences = new behavior.FadePoint();
            this._sequences.setSequencies(this._config.sequences, this._sm, this._content);
        }

        if (this._config.popSequences !== undefined && this._config.popSequences !== null) {
            for (i = 0; i < this._config.popSequences.length; i++) {
                var s = new behavior.FadePoint();
                s.setSequencies(this._config.popSequences[i].sequences, this._sm, this._content);
                if (this._popSequenses === undefined || this._popSequenses === null) {
                    this._popSequenses = [];
                }

                this._popSequenses.push(s);
            }
        }
    }
};

/** @function
* @description init
*/
Popup.prototype.init = function() {
    this._content.scale.set(0, 0);
    this._background.alpha = 0;
};

/** @function
* @description open
*/
Popup.prototype.open = function() {
    var self = this;

    if (!this._timelineStart) {
        this._timelineStart = true;

        if (this._sequences !== undefined && this._sequences !== null) {
            this._sequences.fadeStart();
        }

        if (this._popSequenses !== undefined && this._popSequenses !== null) {
            for (var i = 0; i < this._popSequenses.length; i++) {
                this._popSequenses[i].fadeStart();
            }
        }
    }

	if (this._bts !== undefined && this._bts !== null) {
        // for (var i = 0; i < this._bts.length; i++) {
        //     this._bts[i]._block = true;
        // }

		this.parent._blocked(true, true, this._bts);
    }



    this.emit('openStarted', {target: this});
    TweenMax.to(this._background, 0.3, {alpha: self._backgroundAlpha});
    TweenMax.to(this._content.scale, 0.5, {
        x: 1,
        y: 1,
        ease: Back.easeOut,
        delay: 0.3,
        onComplete: function() {
            self._initInteractions();
        }
    });
};

/** @function
* @description _initInteractions
* @private
*/
Popup.prototype._initInteractions = function() {
    this.emit('openCompleted', {target: this});

    this._close.addEvents();
    this._close.once('clicked', this._onClose, this);

    if (this._sequences !== undefined && this._sequences !== null) {
        this._sequences.fadeRestart();
        this._sequences.once("sequenceComplete", this._selfInteraction, this);
    } else {
        this._selfInteraction();
    }
};

Popup.prototype.close = function(){
    this._close.emit('clicked', {target: this._close});
};

/** @function
* @description _onClose
* @param e {Object} e
* @private
*/
Popup.prototype._onClose = function(e) {
    var self = this;
    //e.target.removeEvents();
    this.emit('closeStarted', {target: this});

    if (this._sequences !== undefined && this._sequences !== null) {
        this._sequences.fadeStop();
    }

    if (self._popSequenses !== undefined && self._popSequenses !== null) {
        for (var i = 0; i < self._popSequenses.length; i++) {
            self._popSequenses[i].fadeStop();
        }
    }

	if(self._bts !== undefined && self._bts !== null) {
		self.parent._blocked(true, true, self._bts);
	}

    TweenMax.to(this._content.scale, 0.3, {
        x: 0,
        y: 0,
        ease: Back.easeIn
    });
    TweenMax.to(this._background, 0.2, {
        alpha: 0,
        delay: 0.3,
        onComplete: function() {
            self.emit('closeCompleted', {target: self});
        }
    });
};

/** @function
* @description popSequences
* @param target {Object} target
* @param self {Object} self
* @param params {Object} params
*/
Popup.prototype.popSequences = function(target, self, params) {
	var hidePopupBts = function(e) {
		self.parent._unblock(self._bts, true);
	};

    for (var i = 0; i < self._popSequenses.length; i++) {
        if (i == params[0]) {
			self.parent._blocked(true, true, self._bts);
            self._popSequenses[i].fadeRestart();
			self._popSequenses[i].once("sequenceComplete", hidePopupBts);
        } else {
            if (self._popSequenses[i]._firstStart === true) {
                self._popSequenses[i].forceEnd();
            }
        }
    }
};

/** @function
* @description _selfInteraction
* @param e {Object} e
* @private
*/
Popup.prototype._selfInteraction = function(e) {
    if (this._bts !== undefined && this._bts !== null) {
        // for (var i = 0; i < this._bts.length; i++) {
        //     this._bts[i]._block = false;
        // }
		this.parent._unblock(this._bts, true);
    }
};
