var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
* @module display/complex
*/
/**
 * @classdesc Define um objeto que pode ser rotacionado
 * @memberof module:display/complex
 * @extends PIXI.Container
 * @exports Clock
 * @constructor
 * @param body {Object} body
 */
function Clock(body) {
    ObjectBase.call(this);

	this._displayType = "clock";

    this._hours = false;
    this._minutes = false;

    this._body = body;

    this.initClock();
}

Clock.prototype = Object.create(ObjectBase.prototype);
Clock.prototype.constructor = Clock;
module.exports = Clock;

Object.defineProperties(Clock.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name displayType
     * @type {Object}
     * @memberof module:display/complex.Clock
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * Bool para saber se as horas estao certas
     * @name hours
     * @type {Boolean}
     * @memberof module:display/complex.Clock
     * @readonly
     */
    hours: {
        get: function() {
            return this._hours;
        },
        set: function(param) {
            this._hours = param;
        }
    },

    /**
     * Bool para saber se os minutos estao certas
     * @name minutes
     * @type {Boolean}
     * @memberof module:display/complex.Clock
     * @readonly
     */
    minutes: {
        get: function() {
            return this._minutes;
        },
        set: function(param) {
            this._minutes = param;
        }
    }

});

/** @function
* @description initClock
*/
Clock.prototype.initClock = function (){

    //Variaveis de exercicio
    this._hours = false;
    this._minutes = false;

    //Adiciona os plugins para rotacionar os dois ponteiros
    this._body.pointerH.addPlugin(new behavior.Rotable());
    this._body.pointerM.addPlugin(new behavior.Rotable());

    //Inicializa os bitmaps deste clock para serem rotacionados
    this._body.pointerH.initObjectsToRotate();
    this._body.pointerM.initObjectsToRotate();

    // Ao resetar o clock, nos ja passaremos pelos ponteiros e REinicializaremos os pivots, caso aconteca algo VOODOO
    this._body.pointerH.children[0].pivot.set(0,this._body.pointerH.children[0].height/2);
    this._body.pointerM.children[0].pivot.set(0,this._body.pointerM.children[0].height/2);
    this._body.pointerH.children[0].x = 0;
    this._body.pointerM.children[0].x = 0;

    if (this._body.perpetual && !this._body.clockSequences) {
        this.perpetualRotation();
    }

};

/** @function
* @description addClockEvents
*/
Clock.prototype.addClockEvents = function (){

    this._body.pointerH.addEvents();
    this._body.pointerM.addEvents();

    this._body.pointerH.on("rotationEnded", this.checkAngles, this);
    this._body.pointerM.on("rotationEnded", this.checkAngles, this);

    //Reposiciona os ponteiros pois eles teem pivots diferentes
    this.resetClock();

};

/** @function
* @description removeClockEvents
*/
Clock.prototype.removeClockEvents = function (){

    this._body.pointerH.removeEvents();
    this._body.pointerM.removeEvents();

    this._body.pointerH.off("rotationEnded");
    this._body.pointerM.off("rotationEnded");

};

/** @function
* @description checkAngles
*/
Clock.prototype.checkAngles = function(e){

    if (this.config.pointerH === e.target) {
        if ((this.config.correctPositionH * 30) === e.angle) {
            this._hours = true;

        }  else { this._hours = false; }
    } else if (this.config.pointerM === e.target) {
        if ((this.config.correctPositionM * 30) === e.angle) {
            this._minutes = true;

        } else { this._minutes = false; }
    }

};

/** @function
* @description verifyClock
*/
Clock.prototype.verifyClock = function() {

    if (this._minutes && this._hours) {
        this.emit('correct');

    } else {
        this.emit('incorrect');

    }

};

/** @function
* @description resetClock
*/
Clock.prototype.resetClock = function (h, m){

    var initialH = h || this._body.initialPositionH;
    var initialM = m || this._body.initialPositionM;

    this._body.pointerH.setRotation(initialH * 30);
    this._body.pointerM.setRotation(initialM * 30);

};

/** @function
* @description resetClockAnimated
*/
Clock.prototype.resetClockAnimated = function (t, h, m){

    var self = this;
    var timer = t || 1;
    var nextH = h || 0;
    var nextM = m || 0;

    setTimeout(function(){

        self._body.pointerH.setRotation(nextH * 30, function(){}, Linear.easeNone, 0.3);
        self._body.pointerM.setRotation(nextM * 30, function(){}, Linear.easeNone, 0.3);

    }, timer * 1000);

};

/** @function
* @description perpetualRotation
*/
Clock.prototype.perpetualRotation = function (){

    if (this._body.pointerH.hasOwnProperty("setRotation") &&
        this._body.pointerM.hasOwnProperty("setRotation")) {

        this._body.pointerH.setRotation(360, function(e){
            this.restart();
        }, "Linear.easeNone", 110);

        this._body.pointerM.setRotation(360, function(e){
            this.restart();
        }, "Linear.easeNone", 10);

    }

};
