var ObjectBase = require('../../core/ObjectBase');
var PIXI = require('PIXI');
var LinePoint = require('../simple/LinePoint');
var Const = require('../../core/Const');
var resemble = require('resemble');
var PIXIConfig = require('../../core/PIXIConfig');

/**
 * @classdesc SnapGrid Class
 * @memberof module:display/complex
 * @extends PIXI.Graphics
 * @exports SnapGrid
 * @constructor
 */
function SnapGrid() {
    ObjectBase.call(this);

    this._board = new PIXI.Graphics();
    this._board.name = "board";
    this._tempBoard = new PIXI.Graphics();
    this._tempBoard.name = "tempBoard";

    this._points = [];
    this._activePoints = [];
    this._mapX = {};

    this._allowMove = false;

    this._countPointFull = 0;

    this.addChild(this._board);
    this.addChild(this._tempBoard);

    var aindex = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,x,w,x,y,z";
    this._alphaIndex = aindex.split(',');

    this._gridPoints = {};
    this._gridTargets = [];
    this._gridRelations = [];

    this._pointSize = 10;

    this._gridSteps = [];
}

SnapGrid.prototype = Object.create(ObjectBase.prototype);
SnapGrid.prototype.constructor = this;
module.exports = SnapGrid;

Object.defineProperties(SnapGrid.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name pointSize
     * @type {Object}
     * @memberof module:display/complex.SnapGrid
     * @readonly
     */
    pointSize: {
        get: function(){
            return this._pointSize;
        },

        set: function(value){
            this._pointSize = value;
        }
    }
});

/** @function
* @description createPoint
* @param ref {Object} ref
*/
SnapGrid.prototype.createPoint = function(ref) {
    var self = this;

    for (var i = 0; i < ref.length; i++) {
        var p = new LinePoint(ref[i]);
        p.id = i;
        p.x = ref[i].x;
        p.y = ref[i].y;
        this.addChild(p);
        this._points.push(p);

        this._createMap(p);
    }
};

/** @function
* @description createGrid
* @param rows {Object} rows
* @param cols {Object} cols
* @param size {Object} size
*/
SnapGrid.prototype.createGrid = function(rows, cols, size) {
    var celW = Math.round(size.w / cols);
    var celH = Math.round(size.h / rows);

    for (var i = 0; i < cols; i++) {
        for (var j = 0; j < rows; j++) {
            var p = new LinePoint({
                width: this._pointSize,
                height: this._pointSize
            });
            p.id = j * cols + i;
            this._points.push(p);
            p.x = (celW + p.width / 2) * i + size.x;
            p.y = (celH + p.height / 2) * j + size.y;
            this._gridPoints[this._alphaIndex[i] + j] = p;

            this._gridTargets.push(p.id);

            this.addChild(p);

            this._createMap(p);
        }
    }

    this.createGridRelation();
};

/** @function
* @description createPointRelation
* @param relation {Object} relation
*/
SnapGrid.prototype.createPointRelation = function(relation) {
    for (var i = 0; i < relation.length; i++) {
        var rel = relation[i];

        for (var j = 0; j < rel.points.length; j++) {
            if (rel.targets.length === 0) continue;
            var p = this._points[rel.points[j]];

            p.relation = rel.targets;
            this._activePoints.push(p);

            for (var k = 0; k < this._mapX[p.map.x][p.map.y].length; k++) {
                var item = this._mapX[p.map.x][p.map.y][k];
                if (item === p) {
                    this._mapX[p.map.x][p.map.y].splice(k, 1);
                    break;
                }
            }
        }
    }
};

/** @function
* @description createGridRelation
*/
SnapGrid.prototype.createGridRelation = function() {
    for (var j = 0; j < this._points.length; j++) {
        var p = this._points[j];
        p.relation = this._gridTargets;
        this._activePoints.push(p);
    }

};

/** @function
* @description addEvents
*/
SnapGrid.prototype.addEvents = function() {
    for (var i = 0; i < this._activePoints.length; i++) {
        var p = this._activePoints[i];
        p.addEvents();
        p.on('pointStart', this._pointStart, this);
        p.on('pointEnd', this._pointEnd, this);
        p.on('pointMove', this._pointMove, this);
        p.on('pointFull', this._pointFull, this);
    }
};

/** @function
* @description removeEvents
*/
SnapGrid.prototype.removeEvents = function() {
    for (var i = 0; i < this._activePoints.length; i++) {
        var p = this._activePoints[i];
        p.removeEvents();
        p.removeListener('pointStart');
        p.removeListener('pointEnd');
        p.removeListener('pointMove');
        p.removeListener('pointFull');
    }
};

/** @function
* @description _getMapPosition
* @param point {Object} point
* @private
*/
SnapGrid.prototype._getMapPosition = function(point) {
    var p = {
        x: Math.round((point.x / Const.BASE_WIDTH) * 100) / 2,
        y: Math.round((point.y / Const.BASE_HEIGHT) * 100) / 5
    };
    var x = Math.round(p.x).toString();
    var nameX = "";
    switch (x.length) {
        case 1:
            nameX = "0";
            this._checkMapX(nameX);
            break;
        case 2:
            nameX = x[0];
            this._checkMapX(nameX);
            break;
        case 3:
            nameX = x[0] + "0";
            this._checkMapX(nameX);
            break;
    }

    var y = Math.round(p.y).toString();
    var nameY = "";

    switch (y.length) {
        case 1:
            nameY = "0";
            break;
        case 2:
            nameY = y[0];
            break;
        case 3:
            nameY = y[0] + "0";
            break;
    }

    return {
        x: nameX,
        y: nameY
    };
};

/** @function
* @description _createMap
* @param point {Object} point
* @private
*/
SnapGrid.prototype._createMap = function(point) {
    var position = this._getMapPosition(point);

    if (this._mapX[position.x][position.y] === undefined) this._mapX[position.x][position.y] = [];

    point.map = position;
    this._mapX[position.x][position.y].push(point);
};

/** @function
* @description _checkMapX
* @param name {Object} name
* @private
*/
SnapGrid.prototype._checkMapX = function(name) {
    if (this._mapX[name] === undefined) this._mapX[name] = {};
};

/** @function
* @description _pointStart
* @param e {Object} e
* @private
*/
SnapGrid.prototype._pointStart = function(e) {
    this._allowMove = true;
    this._initTarget = e.target;
};

/** @function
* @description _pointEnd
* @param e {Object} e
* @param self {Object} self
* @private
*/
SnapGrid.prototype._pointEnd = function(e, self) {
    this._allowMove = false;

    this._tempBoard.clear();
    if (this._tempTarget !== null && this._tempTarget !== undefined) {
        this._tempTarget.close();

        if (this._initTarget.full === false) {
            this._drawLine(this._initTarget, this._tempTarget, this._board);
            this._initTarget.addTarget(this._tempTarget);
            this._tempTarget.addTarget(this._initTarget);
        }
    }

    this._initTarget = null;
    this._tempTarget = null;
};

/** @function
* @description _pointFull
* @param e {Object} e
* @private
*/
SnapGrid.prototype._pointFull = function(e) {
    this._countPointFull++;
    if (this._countPointFull >= this._activePoints.length) {
        this._verifyPoints();
    }
};

/** @function
* @description _pointMove
* @param e {Object} e
* @param self {Object} self
* @private
*/
SnapGrid.prototype._pointMove = function(e, self) {
    if (this._allowMove) {
        this._drawLine(self.position, e.data.global);

        var t = this._checkColision(e.data.global);
        if (t !== null) {
            t.open();
            this._tempTarget = t;
        } else {
            if (this._tempTarget !== null && this._tempTarget !== undefined) {
                this._tempTarget.close();
                this._tempTarget = null;
            }
        }
    }
};

/** @function
* @description _checkColision
* @param pos {Object} pos
* @private
*/
SnapGrid.prototype._checkColision = function(pos) {
    var map = this._getMapPosition(pos);
    var targets = null;

    if (this._mapX[map.x] === undefined) return null;

    if (this._mapX[map.x][map.y] === undefined) return null;

    targets = this._mapX[map.x][map.y];

    var result = null;

    for (var i = 0; i < targets.length; i++) {
        var t = targets[i];
        if (pos.x > t.x - t.width / 2 && pos.x < t.x + t.width / 2 &&
            pos.y > t.y - t.height / 2 && pos.y < t.y + t.height / 2) {
            result = t;
            break;
        }
    }

    return result;
};

/** @function
* @description _drawLine
* @param init {Object} init
* @param end {Object} end
* @param board {Object} board
* @private
*/
SnapGrid.prototype._drawLine = function(init, end, board) {
    var b = board || null;
    var currentBoard = null;
    if (b === null) {
        currentBoard = this._tempBoard;
        currentBoard.clear();
    }
    else{
        currentBoard = b;

        this._gridSteps.push({
            moveTo: {x: init.x, y: init.y},
            lineTo: {x: end.x, y: end.y}
        });
    }
    currentBoard.lineStyle(5, 0x62706f);
    currentBoard.moveTo(init.x, init.y);
    currentBoard.lineTo(end.x, end.y);
    currentBoard.endFill();
};

/** @function
* @description _verifyPoints
* @private
*/
SnapGrid.prototype._verifyPoints = function() {
    var incorrects = [];
    var corrects = [];
    for (var i = 0; i < this._activePoints.length; i++) {
        var p = this._activePoints[i];
        if (p.correct === false) {
            incorrects.push(p);
        } else {
            corrects.push(p);
        }
    }

    if (incorrects.length === 0) {
        this.emit("correct");
        return;
    } else {
        this.emit("incorrect");
    }

    this._board.clear();
    for (var j = 0; j < corrects.length; j++) {
        var _p = corrects[j];
        for (var k = 0; k < _p._targets.length; k++) {
            this._drawLine(_p, _p._targets[k], this._board);
        }
    }

    for (var l = 0; l < incorrects.length; l++) {
        var __p = incorrects[l];
        __p.clear();
        __p.addEvents();
        this._countPointFull--;
    }

    incorrects.splice(0, incorrects.length);
    corrects.splice(0, corrects.length);

    incorrects = null;
    corrects = null;
};

/** @function
* @description clear
*/
SnapGrid.prototype.clear = function(){
    this._board.clear();
    this._tempBoard.clear();
    for(var i = 0; i < this._points.length; i++){
        this._points[i].clear();
    }

    this._gridSteps.splice(0, this._gridSteps.length);
};

/** @function
* @description block
*/
SnapGrid.prototype.block = function() {
    for (var i = 0; i < this._activePoints.length; i++) {
        this._activePoints[i].block();
    }
};

/** @function
* @description unblock
*/
SnapGrid.prototype.unblock = function() {
    for (var i = 0; i < this._activePoints.length; i++) {
        this._activePoints[i].unblock();
    }
};

/** @function
* @description setArtwork
* @param coords {Object} coords
*/
SnapGrid.prototype.setArtwork = function(coords){
    coords = coords.replace(/\s+/g, "");
    var _coords = coords.split(',');
    var points = [];

    for(var i = 0; i < _coords.length; i++){
        var p = this._gridPoints[_coords[i]];

        if(!_coords[i + 1]) break;

        var fw = this._gridPoints[_coords[i + 1]];

        points.push({
            moveTo: {x: p.x, y: p.y},
            lineTo: {x: fw.x, y: fw.y}
        });
    }

    this._artwork = this._getImageFromCanvas(points);
};

/** @function
* @description _getImageFromCanvas
* @param points {Object} points
* @private
*/
SnapGrid.prototype._getImageFromCanvas = function(points){
    var artwork = document.createElement('canvas');
    artwork.width = Const.BASE_WIDTH;
    artwork.height = Const.BASE_HEIGHT;
    var ctx = artwork.getContext('2d');
    ctx.beginPath();
    ctx.strokeStyle = "black";
    ctx.lineCap = "round";
    ctx.lineJoin = "round";
    ctx.lineWidth = 5;

    for(var i = 0; i < points.length; i++){
        var mt = points[i].moveTo;
        var lt = points[i].lineTo;

        ctx.moveTo(mt.x, mt.y);
        ctx.lineTo(lt.x, lt.y);
    }

    ctx.stroke();

    var result = artwork.toDataURL();

    ctx.clearRect(0, 0, artwork.width, artwork.height);
    artwork = null;

    return result;
};

/** @function
* @description _verifyGrid
* @private
*/
SnapGrid.prototype._verifyGrid = function(){
    var self = this;
    var result = this._getImageFromCanvas(this._gridSteps);

    resemble(this._artwork).compareTo(result).onComplete(function(data){
            if (data.rawMisMatchPercentage !== 0) {
                self.emit('incorrect');
            }
            else {
                self.emit('correct');
            }
    });
};
