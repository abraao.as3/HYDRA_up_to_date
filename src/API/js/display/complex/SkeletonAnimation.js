var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var dragonBones = require('DragonBones');
var dragonBonesPixi = require('DragonBonesPixi');

function SkeletonAnimation() {

	ObjectBase.call(this);

	this._config = null;
	// this._path = path;
	this._animationContainers = [];
	this._animationIndex = {};
	this._loaded = false;
	this._screenObject = null;
	this._baseAnimationFrame = 1;

	this.visible = false;
}

SkeletonAnimation.prototype = Object.create(ObjectBase.prototype);
SkeletonAnimation.prototype.constructor = SkeletonAnimation;
module.exports = SkeletonAnimation;

Object.defineProperties(SkeletonAnimation.prototype, {
    // /**
    //  * @memberof module:display.complex.SkeletonAnimation
    //  * @name loaded
    //  * @type {Boolean}
    //  * @readonly
    //  */
    // loaded: {
    //     get: function() {
    //         return this._loaded;
    //     }
    // },
	/**
     * @memberof module:display.complex.SkeletonAnimation
     * @name loaded
     * @type {Boolean}
     * @readonly
     */
    animationNames: {
        get: function() {
			return Object.keys(this._animationIndex);
        }
    }
});

SkeletonAnimation.prototype.load = function(resource) {
	this._config = resource.config;
	this._baseAnimationFrame = this._config.baseAnimationFrame || this._baseAnimationFrame;
	this.visible = this._config.visible || this._visible;

	var dragonBonesData = dragonBones.PixiFactory.factory.parseDragonBonesData(resource.animationData.data);
	var textureAtlasData = dragonBones.PixiFactory.factory.parseTextureAtlasData(resource.textureData.data, resource.textureImg.texture);
	var armatureNames = dragonBonesData.armatureNames;

	armatureNames.forEach(function(armatureName) {
		var armatureDisplay = dragonBones.PixiFactory.factory.buildArmatureDisplay(armatureName);
		this.addChild(armatureDisplay);
		this._animationContainers.push({name: armatureName, display: armatureDisplay});

		armatureDisplay.animation.animationNames.forEach(function(animationName) {
			this._animationIndex[animationName] = {armatureName: armatureName};
		}, this);
	}, this);

	this._loaded = true;
	this.emit('animationLoaded', {target: this});
};

// SkeletonAnimation.prototype._loadCompleteHandler = function(loader, resource) {
// 	var dragonBonesData = dragonBones.PixiFactory.factory.parseDragonBonesData(resource.dragonBonesData.data);
//  var textureAtlasData = dragonBones.PixiFactory.factory.parseTextureAtlasData(resource.textureData.data, resource.textureImg.texture);
// 	var armatureNames = dragonBonesData.armatureNames;
//
// 	armatureNames.forEach(function(armatureName) {
// 		var armatureDisplay = dragonBones.PixiFactory.factory.buildArmatureDisplay(armatureName);
// 		this.addChild(armatureDisplay);
// 		this._animationContainers.push({name: armatureName, display: armatureDisplay});
//
// 		armatureDisplay.animation.animationNames.forEach(function(animationName) {
// 			this._animationIndex[animationName] = {armatureName: armatureName};
// 		}, this);
// 	}, this);
//
// 	this._loaded = true;
// 	this.emit('animationLoaded', {target: this});
// };

SkeletonAnimation.prototype.setScreen = function(screenObject) {
	this._screenObject = screenObject;
};

SkeletonAnimation.prototype._getControlObject = function(objectType, objectIndex) {
	var controlObject = null;
	switch(objectType) {
		case "screen": controlObject = [this._screenObject]; break;
		case "sound": controlObject = [this._screenObject.soundManager]; break;
		default: controlObject = this._screenObject.layersDisplay.getMap(objectType).filter(function(obj) {
			return objectIndex === undefined || objectIndex === null || objectIndex === obj.index;
		});
	}

	return controlObject;
};

SkeletonAnimation.prototype.addControlEvents = function() {
	if(this._config.controlSettings !== undefined && this._config.controlSettings !== null) {
		this._config.controlSettings.forEach(function(controlSettings) {
			var objectType = controlSettings.objectType;

			if(objectType !== undefined && objectType !== null) {
				controlSettings.objectSettings.forEach(function(objectSettings) {
					var controlObjects = this._getControlObject(objectType, objectSettings.index);

					objectSettings.eventSettings.forEach(function(eventSettings) {
						controlObjects.forEach(function(controlObject) {
							var eventListener = function(e) {
								switch(eventSettings.actionName) {
									case SkeletonAnimation.Actions.PLAY: this.play(eventSettings.animationNames, eventSettings.actionBehavior); break;
									case SkeletonAnimation.Actions.STOP: this.stop(eventSettings.animationNames); break;
									case SkeletonAnimation.Actions.STOPANDPLAY: this.cleanPlayQueue(); this.play(eventSettings.animationNames, eventSettings.actionBehavior); break;
									case SkeletonAnimation.Actions.CLEANPLAYQUEUE: this._screenObject.emit('cleanPlayQueue', {target: this}); break;
									default: console.warn("SkeletonAnimation::addControlEvents - actionName não encontrado: " + eventSettings.actionName);
								}
							};

							controlObject.on(eventSettings.eventName, eventListener, this);
						}, this);
					}, this);
				}, this);
			}
		}, this);
	}

	// if(!this._screenObject.listeners('cleanPlayQueue', true)) {console.log('add cleanPlayQueue listener');
		this._screenObject.on('cleanPlayQueue',	this.cleanPlayQueue, this);
	// }
};

SkeletonAnimation.prototype.cleanPlayQueue = function (e) {
	this._animationContainers.forEach(function(container) {
		container.display.removeListener(dragonBones.EventObject.COMPLETE);

		container.display.animation.animationNames.forEach(function(animationName) {
			if(container.display.animation.getState(animationName) !== undefined && container.display.animation.getState(animationName) !== null && container.display.animation.getState(animationName).isPlaying) {
				container.display.animation.gotoAndStopByFrame(animationName, this._baseAnimationFrame);
				container.display.animation.reset();
			}
		}, this);

		if(this._config.visible !== true && this.visible) {
			this.visible = false;
		}
	}, this);
};

SkeletonAnimation.prototype.play = function(animationNames, actionBehavior, playTimes, timeOutTime) {
	playTimes = playTimes || -1;
	timeOutTime = timeOutTime || 5;

	if(!this._loaded) {
		var timeOut = setTimeout(function(self) {
			self.removeListener('animationLoaded', animationLoadedListener);
			throw "Play Animation: Tempo limite excedido. Recursos não carregados.";
		}, timeOutTime*1000, this);

		var animationLoadedListener = function(e) {
			clearTimeout(timeOut);
			e.target.play(animationName, playTimes, timeOutTime);
		};

		this.once('animationLoaded', animationLoadedListener);
	} else {
		if(animationNames !== undefined && animationNames !== null) {
			animationNames.forEach(function(animationName, idx, names) {
				if((this._animationIndex[animationName] === null || this._animationIndex[animationName] === undefined)) {
					console.warn("SkeletonAnimation::play - Animação não encontrada: " + animationName);
				} else {
					var armatureName = this._animationIndex[animationName].armatureName;
					var aContainer = this._animationContainers.find(function(container) {
						return container.name == armatureName;
					}, this);

					var isPlaying = aContainer.display.animation.lastAnimationName === animationName ? aContainer.display.animation.getState(animationName).isPlaying : aContainer.display.animation.isPlaying;
					if(isPlaying) {
						var behavior = actionBehavior || SkeletonAnimation.ActionBehavior.NONE;
						if((idx > 0) && (animationNames.indexOf(aContainer.display.animation.lastAnimationName) >= 0)) { // Condição para sempre stackar as animações contidas no mesmo array
							behavior = SkeletonAnimation.ActionBehavior.STACK;
						}

						switch(behavior) {
							case SkeletonAnimation.ActionBehavior.STACK:
								if(aContainer.display.animation.lastAnimationState.playTimes > 0) {
									aContainer.display.once(dragonBones.EventObject.COMPLETE, function(e) {
										this.play([animationName], behavior, playTimes, timeOutTime);
									}, this);
								} else {
									if(aContainer.display.animation.lastAnimationName !== animationName) {
										aContainer.display.once(dragonBones.EventObject.LOOP_COMPLETE, function(e) {
											this.stop([aContainer.display.animation.lastAnimationName]);
											this.play([animationName], behavior, playTimes, timeOutTime);
										}, this);
									}
								}
								break;
							case SkeletonAnimation.ActionBehavior.REPLACE:
								if(aContainer.display.animation.lastAnimationName !== animationName) {
									this.stop([aContainer.display.animation.lastAnimationName]);
									this.play([animationName], behavior, playTimes, timeOutTime);
								}
								break;
							case SkeletonAnimation.ActionBehavior.NONE:
								// Do Nothing
								return;
							default: console.warn("SkeletonAnimation::play - actionBehavior não encontrado: " + eventSettings.actionName);

						}
					} else {

						this.visible = true;
						aContainer.display.animation.gotoAndPlayByFrame(animationName, this._baseAnimationFrame, playTimes);
					}
				}
			}, this);
		}
	}
};

SkeletonAnimation.prototype.stop = function(animationNames) {
	animationNames.forEach(function(animationName) {
		var armatureName = this._animationIndex[animationName].armatureName;
		var aContainer = this._animationContainers.find(function(container) {
			return container.name == armatureName;
		}, this);

		if(aContainer.display.animation.getState(animationName) !== null && aContainer.display.animation.getState(animationName).isPlaying) {
			aContainer.display.animation.gotoAndStopByFrame(animationName, this._baseAnimationFrame);
			aContainer.display.animation.reset();

			if(this._config.visible !== true) {
				this.visible = false;
			}
		}
	}, this);
};

SkeletonAnimation.Actions = {};
SkeletonAnimation.Actions.PLAY = "play";
SkeletonAnimation.Actions.STOP = "stop";
SkeletonAnimation.Actions.STOPANDPLAY = "stopAndPlay";
SkeletonAnimation.Actions.CLEANPLAYQUEUE = "cleanPlayQueue";

SkeletonAnimation.ActionBehavior = {};
SkeletonAnimation.ActionBehavior.STACK = "stack";
SkeletonAnimation.ActionBehavior.REPLACE = "replace";
SkeletonAnimation.ActionBehavior.NONE = "none";
