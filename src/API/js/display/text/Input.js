/**
 * @module display/text
 */
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
  * @classdesc Classe que define os Input e sua exibição
  * @memberof module:display/text
  * @extends Pixi.Container
  * @exports Input
  * @constructor
  * @param width {number} largura
  * @param height {number} altura
  */
function Input(width, height) {
    ObjectBase.call(this);

    this._displayType = "typeInput";

    this.field = new type.text.Input(width, height);
    this.addChild(this.field);
}

Input.prototype = Object.create(ObjectBase.prototype);
Input.prototype.constructor = Input;
module.exports = Input;

Object.defineProperties(Input.prototype, {

    /**
     * @memberof module:display/text.Input
     * @type {string}
     */
    tint: {
        get: function() {
            return this.field.tint;
        },
        set: function(cor) {
            this.field.tint = cor;
        }
    },

    /**
     * @memberof module:display/text.Input
     * @type {string}
     */
    textAlign: {
        get: function() {
            return this.field.align;
        },
        set: function(cor) {
            this.field.align = cor;
        }
    }


});

/**
 * atribui o foco ao input
 * @private
 */
Input.prototype._focus = function() {
	this.emit("focus", { target: this });
};

/**
 * atribui o texto ao input
 */
Input.prototype.setText = function(text, style){
    this.field.setText(text, style);
};

/**
 * atribui o estilo ao input
 */
Input.prototype.setStyle = function(style){
    this.field.setStyle(style);
};

/**
 * insere eventos ao input
 */
Input.prototype.addEvents = function(){
	this.field.addEvents();
	this.field.on("focus",this._focus, this);
};

/**
 * remove eventos do input
 */
Input.prototype.removeEvents = function(){
	var self = this;
    this.field.removeEvents();
	this.field.removeListener("focus",this._focus);
};

/**
 * exibe cursor
 */
Input.prototype.showCursor = function(){
    this.field.showCursor();
};

/**
 * esconde cursor
 */
Input.prototype.hideCursor = function(){
    this.field.hideCursor();
	this.field.showCursor = type.text.Input.prototype.showCursor;
};
