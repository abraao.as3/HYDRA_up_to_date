
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
  * @classdesc Classe que define os TextField e sua exibição
  * @memberof module:display/text
  * @extends Pixi.Container
  * @exports TextField
  * @constructor
  * @param width {number} largura
  * @param height {number} altura
  */
function TextField(width, height) {
    ObjectBase.call(this);

    this._displayType = "type";

    this.field = new type.text.TextField(width, height);
    this.addChild(this.field);
}

TextField.prototype = Object.create(ObjectBase.prototype);
TextField.prototype.constructor = TextField;
module.exports = TextField;

Object.defineProperties(TextField.prototype, {


    /**
     * @memberof module:display/text.TextField
     * @type {string}
     */
    tint: {
        get: function() {
            return this.field.tint;
        },
        set: function(cor) {
            this.field.tint = cor;
        }
    },

    /**
     * @memberof module:display/text.TextField
     * @type {string}
     */
    textAlign: {
        get: function() {
            return this.field.align;
        },
        set: function(align) {
            this.field.align = align;
        }
    }


});

/**
 * atribui o texto ao TextField
 */
TextField.prototype.setText = function(text, style){
    this.field.setText(text, style);
};

/**
 * atribui o estilo ao TextField
 */
TextField.prototype.setStyle = function(style){
    this.field.setStyle(style);
};
