var PIXI = require('PIXI');
var TweenMax = require('TweenMax');

/**
 * @classdesc Classe que define o LinePoint
 * @memberof module:display/simple
 * @extends Pixi.Container
 * @exports LinePoint
 * @constructor
 * @param ref {Object[]}
 */
function LinePoint(ref) {
    PIXI.Container.call(this);

    this.p = new PIXI.Graphics();
    this.p.beginFill(0x5a87c5);
    this.p.drawCircle(0, 0, ref.width / 2);
    this.p.endFill();

    this.c = new PIXI.Graphics();
    this.c.beginFill(0xa6b8df);
    this.c.lineStyle(ref.width / 2 * 0.25, 0x5a87c5);
    this.c.drawCircle(0, 0, ref.width / 2 + (ref.width / 2 * 0.5));
    this.c.endFill();

    this.addChild(this.c);
    this.c.scale.set(0);
    this.addChild(this.p);

    ref.visible = false;

    this.relation = null;
    this._targets = [];

    this.correct = false;

    this.full = false;
}

LinePoint.prototype = Object.create(PIXI.Container.prototype);
LinePoint.prototype.constructor = LinePoint;
module.exports = LinePoint;

/**
 * Metodo que inicia a linha
 */
LinePoint.prototype.open = function() {
    TweenMax.to(this.c.scale, 0.3, {
        x: 1,
        y: 1,
        ease: Back.easeOut
    });
};

/**
 * Metodo que encerra a linha
 */
LinePoint.prototype.close = function() {
    TweenMax.to(this.c.scale, 0.3, {
        x: 0,
        y: 0,
        ease: Quad.easeIn
    });
};

/**
 * Metodo que adiciona eventos no LinePoint
 */
LinePoint.prototype.addEvents = function() {
    this.interactive = true;
    this.on('mousedown', this._onDown, this);
    this.on('touchstart', this._onDown, this);

    this.on('mouseup', this._onUp, this);
    this.on('touchend', this._onUp, this);
    this.on('mouseupoutside', this._onUp, this);
    this.on('touchendoutside', this._onUp, this);

    this.on('mousemove', this._onMove, this);
    this.on('touchMove', this._onMove, this);
};

/**
 * Metodo que remove todos os eventos no LinePoint
 */
LinePoint.prototype.removeEvents = function() {
    this.interactive = false;
    this.removeListener('mousedown');
    this.removeListener('touchstart');

    this.removeListener('mouseup');
    this.removeListener('touchend');
    this.removeListener('mouseupoutside');
    this.removeListener('touchendoutside');

    this.removeListener('mousemove');
    this.removeListener('touchMove');
};

/**
 * Metodo que controla o down do mouse
 * @param e evento, é utilizado seu ponto de referecia para formação da linha
 * @private
 */
LinePoint.prototype._onDown = function(e) {
    this.open();
    this._isMove = true;
    this.emit('pointStart', e, this);
};

/**
 * Metodo que controla o up do mouse
 * @memberof LinePoint
 * @param e evento, é utilizado seu ponto de referecia para formação da linha
 * @private
 */
LinePoint.prototype._onUp = function(e) {
    this.close();
    this._isMove = false;
    this.emit('pointEnd', e, this);
};

/**
 * Metodo que controla o move do mouse
 * @memberof LinePoint
 * @param e evento, é utilizado seu ponto de referecia para formação da linha
 * @private
 */
LinePoint.prototype._onMove = function(e) {
    if (this._isMove) {
        this.emit('pointMove', e, this);
    }
};

/**
 *
 * @param target {Object}
 */
LinePoint.prototype.addTarget = function(target) {
    for(var z = 0; z < this._targets.length; z++){
        if(this._targets[z] === target) return;
    }

    if(this.relation === undefined || this.relation === null){
        return;
    }

    if (this._targets.length < this.relation.length) {
        this._targets.push(target);
    } else {
        return;
    }

    if (this._targets.length == this.relation.length) {
        this.full = true;
        this.removeEvents();

        for (var i = 0; i < this._targets.length; i++) {
            var v = false;
            for (var j = 0; j < this.relation.length; j++) {
                if (this._targets[i].id == this.relation[j]) {
                    v = true;
                    this.correct = true;
                    break;
                }
            }

            if (v === false) {
                this.correct = false;
                this.emit('pointFull');
                return;
            }
        }
        this.emit('pointFull');
    }
};

/**
* limpa linha
 */
LinePoint.prototype.clear = function(){
    this.correct = false;
    this.full = false;
    this._targets.splice(0, this._targets.length);
};

/**
 * bloqueia
 */
LinePoint.prototype.block = function(){
    this.interactive = false;
};

/**
 * desbloqueia 
 */
LinePoint.prototype.unblock = function(){
    this.interactive = true;
};
