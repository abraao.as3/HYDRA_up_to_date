var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var Dragable = require('../../behavior/Dragable');
var Clickable = require('../../behavior/Clickable');
var Bitmap = require('../core/Bitmap');

Scroll.prototype = Object.create(ObjectBase.prototype);
Scroll.prototype.constructor = Scroll;
module.exports = Scroll;

function Scroll() {
    ObjectBase.call(this);

    this._displayType = "scroll";

    this.addPlugin(new Dragable());

    //pequeno contorno para corrigir discrepancia de ponto de registro do drag para a tela fazendo o behavior funcionar para os 2
    this.on("positionUpdated", function(){
        this.x += this.width /2;
        this.y += this.height /2;
    });

}

/*
orientation: (string)
-vertical: "left", "right"(default)
-horizontal: "top", "bottom"(default)
*/

/*
config: (object)

//use the default configuration of scroll values
-default (bool)

//back layer of scroll
-scroll_barWidth
-scroll_barHeight
-scroll_barColor
-scroll_barAlpha

//scroll point of current porcentage
-point_scrollWidth
-point_scrollHeight
-point_scrollColor
-point_scrollAlpha

//scroll of viewed bar
-viewed_scrollWidth
-viewed_scrollHeight
-viewed_scrollColor
-viewed_scrollAlpha
*/
Scroll.prototype.CreateHorizontalScrollBar = function(fatherContainer, config, orientation){
    var self = this;

    //ainda sem funcionalidade
    var validate = orientation == "top" ? true : false;
    orientation = validate ? orientation : "bottom";

    //create scroll bar
    var sb = new PIXI.Graphics();
    sb.beginFill(0xBBBBBB, 1);
    sb.drawRoundedRect(0,0,config.scroll_barWidth , config.scroll_barHeight, config.scroll_barRadius);
    sb.endFill();

    var scroll_bar = new Bitmap(sb.generateCanvasTexture());
    scroll_bar.position.set(fatherContainer.initX,fatherContainer.initY + fatherContainer.sH);
    // scroll_bar.addPlugin(new behavior.Clickable());
    scroll_bar.addPlugin(new Clickable());
    scroll_bar._selectionEffect = false;
    scroll_bar.addEvents();

    //create scroll point
    var sp = new PIXI.Graphics();
    sp.beginFill(0xFFFFFF, 0.5);
    sp.drawRoundedRect(0,0,config.point_scrollWidth, config.point_scrollHeight,config.point_barRadius);
    sp.endFill();

    var scroll_point = new Bitmap(sp.generateCanvasTexture());
    scroll_point.position.set(fatherContainer.initX,fatherContainer.initY + fatherContainer.sH);
    scroll_point.addPlugin(new Dragable());
    scroll_point.addEvents();
    scroll_point._lockY = true;
    scroll_point.minX = fatherContainer.initX;
    scroll_point.maxX = fatherContainer.initX + config.scroll_barWidth - scroll_point.width;
    scroll_point.difX = scroll_point.maxX - scroll_point.minX;

    //adjust main scroll container endY and height
    fatherContainer.endY += config.scroll_barHeight;
    fatherContainer.sH += config.scroll_barHeight;
    fatherContainer.addChild(scroll_bar,scroll_point);

    //add scroll_point drag functions
    scroll_point.on("dragStart", self.HorizontalPointStart, this);
    scroll_point.on("dragReleased", self.HorizontalPointReleased, this);
    scroll_point.on("positionUpdated", self.HorizontalPointUpdate, {c_scroll:fatherContainer,sp:scroll_point,self:self});

};

Scroll.prototype.CreateVerticalScrollBar = function(fatherContainer, config, orientation){
    var self = this;

    //ainda sem funcionalidade
    var validate = orientation == "left" ? true : false;
    orientation = validate ? orientation : "right";

    var sb = new PIXI.Graphics();
    sb.beginFill(0xBBBBBB, 1);
    sb.drawRoundedRect(0,0,config.scroll_barWidth,config.scroll_barHeight - config.scroll_barWidth,config.scroll_barRadius);
    sb.endFill();

    var scroll_bar = new Bitmap(sb.generateCanvasTexture());
    scroll_bar.position.set(fatherContainer.initX + fatherContainer.sW,fatherContainer.initY);
    // scroll_bar.addPlugin(new behavior.Clickable());
    scroll_bar.addPlugin(new Clickable());
    scroll_bar._selectionEffect = false;
    scroll_bar.addEvents();

    //create scroll point
    var sp = new PIXI.Graphics();
    sp.beginFill(0xFFFFFF, 0.5);
    sp.drawRoundedRect(0,0,config.point_scrollWidth, config.point_scrollHeight,config.point_barRadius);
    sp.endFill();

    var scroll_point = new Bitmap(sp.generateCanvasTexture());
    scroll_point.position.set(fatherContainer.initX + fatherContainer.sW, fatherContainer.initY);
    scroll_point.addPlugin(new Dragable());
    scroll_point.addEvents();
    scroll_point._lockX = true;
    scroll_point.minY = fatherContainer.initY;
    scroll_point.maxY = fatherContainer.initY + config.scroll_barHeight - scroll_point.height*2;
    scroll_point.difY = scroll_point.maxY - scroll_point.minY;

    //readjust father container values
    fatherContainer.endX += config.scroll_barWidth;
    fatherContainer.sW += config.scroll_barWidth;
    fatherContainer.addChild(scroll_bar,scroll_point);

    //add scroll_point drag functions
    scroll_point.on("dragStart", self.VerticalPointStart, this);
    scroll_point.on("dragReleased", self.VerticalPointReleased, this);
    scroll_point.on("positionUpdated", self.VerticalPointUpdate, {c_scroll:fatherContainer,sp:scroll_point,self:self});

};

Scroll.prototype.HorizontalPointUpdate = function(e){
        var newX = this.sp.x;
        if(this.sp.x <= this.sp.minX){newX = this.sp.minX;}
        if(this.sp.x >= this.sp.maxX){newX = this.sp.maxX;}
        var porc = (newX - this.sp.minX) / this.sp.difX;
        this.sp.x = newX;
        this.self.SetPorcXScroll(this.c_scroll, porc);
};

Scroll.prototype.VerticalPointUpdate = function(e){
        var newY = this.sp.y;
        if(this.sp.y <= this.sp.minY){newY = this.sp.minY;}
        if(this.sp.y >= this.sp.maxY){newY = this.sp.maxY;}
        var porc = (newY - this.sp.minY) / this.sp.difY;
        this.sp.y = newY;
        this.self.SetPorcYScroll(this.c_scroll, porc);
};

Scroll.prototype.SetPorcXScroll = function(scroll, x){//change the horizontal scroll with the porcentage
    var difX = scroll.endX - scroll.initX - scroll.sW;//pixel diference
    var difPorc = scroll.porcX - x;//pixel porcentage difference

    scroll.porcX = x;
    var aux = (difX * difPorc);

    scroll.children[0].x += aux;
};

Scroll.prototype.SetPorcYScroll = function(scroll, y){//change the horizontal scroll with the porcentage
    var difY = scroll.endY - scroll.initY - scroll.sH;//pixel diference
    var difPorc = scroll.porcY - y;//pixel porcentage difference

    scroll.porcY = y;
    var aux = (difY * difPorc);

    scroll.children[0].y += aux;
};
