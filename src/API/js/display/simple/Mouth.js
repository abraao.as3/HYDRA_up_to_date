var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Classe responsável por criar bocas
 * @extends module:core.ObjectBase
 * @extends module:behavior.AlignSupport
 * @memberof module:display/simple
 * @exports Mouth
 * @constructor
 * @param type {string} tipos de bocas
 * @param stationary {number} numero do frame da boca parada
 */
function Mouth(type, stationary) {
    var self = this;

    ObjectBase.call(this);

    this.loader = new PIXI.loaders.Loader();

    this._stationary = stationary;
    this._movieClip = null;

    this._displayType = "mouth";

    this.addPlugin(new behavior.AlignSupport());
    this.objectToAlign = this._sprite;

    this.frames = [];

    var loader = PIXI.loader;

    this.loader.once('loaded', function(response) {
        self.defineMovieClip(response.framesTextures);
    });

    switch (type) {
        case 'focinhoAbel':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/focinhoAbel/focinhoAbel.json');
            break;
        case 'focinhoAnanasFrente':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/ananasFrente/ananasFrente.json');
            break;
        case 'focinhoAnanasLado':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/ananasLado/ananasLado.json');
            break;
        case 'focinhoAnanas34':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/ananas34/ananas34.json');
            break;
        case 'boca':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/boca/boca.json');
            break;
        case 'bocaAnaMaria':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaAnaMaria/bocaAnaMaria.json');
            break;
        case 'bocaClovis':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaClovis/bocaClovis.json');
            break;
        case 'bocaEdu':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaEdu/bocaEdu.json');
            break;
        case 'bocaJack':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaJack/bocaJack.json');
            break;
        case 'bocaLila':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaLila/bocaLila.json');
            break;
        case 'bocaLucia':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaLucia/bocaLucia.json');
            break;
        case 'bocaMichel':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaMichel/bocaMichel.json');
            break;
        case 'bocaNico':
            self.frames = self.generateFrames('./telas/commom/movieClips/mouths/bocaNico/bocaNico.json');
            break;

    }

    /**
     * Emitido quando a Boca é criada
     * @event event:mouthCreated
     * @memberof module:display/simple.Mouth#
     */

}

Mouth.prototype = Object.create(ObjectBase.prototype);
Mouth.prototype.constructor = Mouth;
module.exports = Mouth;

Object.defineProperties(Mouth.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @type {String}
     * @memberof module:display/simple.Mouth
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * numero que representa o frame estatico da boca
     * @type {Number}
     * @memberof module:display/simple.Mouth
     * @readonly
     */
    stationary: {
        get: function() {
            return this._stationary;
        }
    },

    /**
     * escalona a largura do video
     * @type {Number}
     * @memberof module:display/simple.Mouth
     * @readonly
     */
    rescaleMovieWidth: {
        get: function() {
            return this._movieClip.width;
        }
    },

    /**
     * escalona a altura do video
     * @type {Number}
     * @memberof module:display/simple.Mouth
     */
    rescaleMovieHeight: {
        get: function() {
            return this._movieClip.height;
        },

        set: function() {
            return this._movieClip.height;
        }
    }
});

/**
 * @desc Gera movie clip a partir de frames de entrada
 * @method module:display/simple.Mouth#defineMovieClip
 * @memberof module:display/simple.Mouth
 * @fires module:display/simple.Mouth#mouthCreated sinaliza a criação do movieClip
 * @listens module:display/simple.Mouth#event:mouthCreated sinaliza a criação do movieClip
 * @param frames {Array} Texturas do movieClip
 */
Mouth.prototype.defineMovieClip = function(frames) {
    var self = this;

    this._movieClip = new PIXI.extras.MovieClip(frames);

    this.addChild(this._movieClip);

    this._movieClip.pivot.set(this._movieClip.width / 2, this._movieClip.height / 2);
    this.gotoAndStop(this._stationary);

    this._movieClip.animationSpeed = 0.2;

    self.emit("mouthCreated");

};

/**
 * inicia o movimento da boca
 */
Mouth.prototype.play = function() {
    var self = this;

    this._movieClip.play();
};

/**
 * @desc vai para um frame especifico e para o video da boca
 * @param frame {Array} Texturas da boca
 */
Mouth.prototype.gotoAndStop = function(frame) {
    var self = this;

    this._movieClip.gotoAndStop(frame);
};

/**
 * Gera os frames especificos para essa boca
 * @param path {String} Caminho do json e sprites para pegar os frames
 */
Mouth.prototype.generateFrames = function(path) {


    this.loader.reset();
    var framesTextures = [];

    this.loader.add('framesRequested', path);

    //carrega os sprites da tela
    this.loader.load(function(loader, resource) {

        for (var j in resource.framesRequested.textures) {
            framesTextures.push(resource.framesRequested.textures[j]);
        }

        loader.emit('loaded', {
            framesTextures: framesTextures
        });
    });
};
