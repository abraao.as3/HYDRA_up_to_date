var PIXI = require('PIXI');
var CrossField = require('../../utils/Field');
require('Wordfind');
var PIXIConfig = require('../../core/PIXIConfig');

/**
 * @classdesc Classe que define o CrosWord e sua exibição
 * @memberof module:display/simple
 * @extends Pixi.Container
 * @exports CrossWord
 * @constructor
 * @param rows define o numero de linhas
 * @param cols define o numero de colunas
 * @param size define o tamanho do campo (PIXI.rectangle)
 * @param corrects define as letras do caça palavras
 */
function CrossWord(rows, cols, size, corrects) {
    PIXI.Container.call(this);

    this._rows = rows;
    this._cols = cols;
    this._size = size;
    this._fields = [];

    this._isMove = false;
    this._currentWord = "";
    this._currentFields = [];

    if (corrects === undefined || corrects === null || corrects.length === 0) {
        throw "CrossWord: Need set correctWords as Array in config.json";
    }
    this._corrects = corrects;

    this._feeds = [];

    this._feedStroke = 2;
    this.lacunasPreenchidas = [];

    this.x = this._size.x;
    this.y = this._size.y;

    this._generalContainer = new PIXI.Container();
    this.addChild(this._generalContainer);

    this._debug = false;
    this._lineColor = 0xa6b1d6;
    this._fontStyle = {
        fill: "#a6b1d6",
        fontSize: 20,
        fontFamily: "playtime"
    };

    this._upperCase = false;
    this.upperCase = this._upperCase;
    this._textBase = null;
}

CrossWord.prototype = Object.create(PIXI.Container.prototype);
CrossWord.prototype.constructor = CrossWord;
module.exports = CrossWord;

Object.defineProperties(CrossWord.prototype, {
    /**
     * @memberof module:display/simple.CrossWord
     * @name debug
     * @param value {Boolean}
     */
    debug: {
        get: function() {
            return this._debug;
        },

        set: function(value) {
            this._debug = value || false;
        }
    },

    /**
     * @memberof module:display/simple.CrossWord
     * @name lineColor
     * @param value {number}
     */
    lineColor: {
        get: function() {
            return this._lineColor;
        },

        set: function(value) {
            this._lineColor = value || 0xa6b1d6;
        }
    },

    /**
     * @memberof module:display/simple.CrossWord
     * @name fontStyle
     * @param value {Boolean}
     */
    fontStyle: {
        set: function(value) {
            if (value === undefined) return;

            for (var i in this._fontStyle) {
                if (value[i] !== undefined) {
                    this._fontStyle[i] = value[i];
                }
            }
        }
    },

    /**
     * @memberof module:display/simple.CrossWord
     * @name upperCase
     * @param value {Boolean}
     */
    upperCase: {
        set: function(value) {
            var v = value || false;
            this._upperCase = v;

            for (var i = 0; i < this._corrects.length; i++) {
                if (v) {
                    this._corrects[i] = this._corrects[i].toUpperCase();
                } else {
                    this._corrects[i] = this._corrects[i].toLowerCase();
                }
            }
        }
    },

    /**
     * @memberof module:display/simple.CrossWord
     * @name orientation
     * @param value {number}
     */
    orientation: {
        set: function(value) {
            this._orientation = value;
        }
    },

    /**
     * @memberof module:display/simple.CrossWord
     * @name textBase
     * @param value {string}
     */
    textBase: {
        set: function(value) {
            this._textBase = value;
        }
    }
});

/**
 * Metodo que cria o caça palavras
 */
CrossWord.prototype.create = function() {
    var puzzle = wordfind.newPuzzle(this._corrects, {
        width: this._cols,
        height: this._rows,
        orientations: this._orientation
    });

    if (puzzle.length > this._rows) this._rows = puzzle.length;
    if (puzzle[0].length > this._cols) this._cols = puzzle[0].length;

    this._fieldW = Math.round(this._size.w / this._cols);
    this._fieldH = Math.round(this._size.h / this._rows);

    this._size.w = this._fieldW * this._cols;
    this._size.h = this._fieldH * this._rows;

    var container = new PIXI.Container();
    this._fieldsContainer = new PIXI.Container();

    for (var i = 0; i < this._rows; i++) {
        for (var j = 0; j < this._cols; j++) {
            var f = new CrossField(this._fieldW, this._fieldH);
            f.x = f.width / 2 + (f.width * j);
            f.y = f.height / 2 + (f.height * i);
            f.index = this._cols * i + j;
            f.text = this._upperCase ? puzzle[i][j].toUpperCase() : puzzle[i][j].toLowerCase();
            f.debug = this._debug;

            var l = new PIXI.Graphics();
            l.lineStyle(2, this._lineColor);
            l.drawRect(0, 0, this._fieldW, this._fieldH);
            l.endFill();
            l.pivot.set(l.width / 2, l.height / 2);
            l.x = l.width / 2 + (l.width * j);
            l.y = l.height / 2 + (l.height * i);
            l.index = f.index;

            var t = new type.text.TextField(this._fieldW, 0);
            t.pivot.set(l.width / 2, l.height / 2);
            t.setText('<a>' + f.text + '</a>', {
                a: this._fontStyle
            });
            t.align = 'center';
            t.index = f.index;

            t.x = Math.round(l.x);
            t.y = Math.round(l.y + this._fieldH / 2 - t.height / 2);

            container.addChild(l);
            container.addChild(t);

            this._fields.push(f);
            this._fieldsContainer.addChild(f);
        }
    }

    // $('canvas').css('border', '1px solid red');

    var line = new PIXI.Graphics();
    line.lineStyle(4, this._lineColor);
    line.drawRect(0, 0, container.width, container.height);
    line.endFill();
    container.addChild(line);
    container.x = 2;
    container.y = 2;

    var temp = new PIXI.Container();
    temp.addChild(container);

    var rt = PIXI.RenderTexture.create(temp.width, temp.height);
    PIXIConfig.renderer.render(temp, rt);

    var sp = new PIXI.Sprite(rt);

    container.destroy();
    temp.destroy();

    var whiteBase = new PIXI.Graphics();
    whiteBase.beginFill(0xffffff);
    whiteBase.drawRect(0, 0, sp.width, sp.height);
    whiteBase.endFill();
    this._generalContainer.addChild(whiteBase);
    this._fieldsContainer.x = whiteBase.width / 2 - this._fieldsContainer.width / 2;
    this._fieldsContainer.y = whiteBase.height / 2 - this._fieldsContainer.height / 2;
    this._generalContainer.addChild(this._fieldsContainer);
    this._generalContainer.addChild(sp);

    this.addChild(this._generalContainer);

    puzzle = null;
};

/**
 * Metodo que adiciona eventos no caça palavras
 * utilizado para habilitar captura de letras
 */
CrossWord.prototype.addEvents = function() {
    this.interactive = true;

    this
        .on('mousedown', this._onDown)
        .on('touchstart', this._onDown)

    .on('mouseup', this._onUp)
        .on('touchend', this._onUp)
        .on('mouseupoutside', this._onUp)
        .on('touchendoutside', this._onUp)

    .on('mousemove', this._onMove)
        .on('touchmove', this._onMove);
};

/**
 * Metodo que remove todos os eventos no caça palavras
 */
CrossWord.prototype.removeEvents = function() {
    this.interactive = false;

    this
        .removeListener('mousedown', this._onDown)
        .removeListener('touchstart', this._onDown)

    .removeListener('mouseup', this._onUp)
        .removeListener('touchend', this._onUp)
        .removeListener('mouseupoutside', this._onUp)
        .removeListener('touchendoutside', this._onUp)

    .removeListener('mousemove', this._onMove)
        .removeListener('touchmove', this._onMove);
};

/**
 * Metodo que habilita a captura de letras ao tocar na tela
 *
 * @memberof CrossWord
 * @param e evento, é utilizado seu ponto de referecia para formação da palavra
 * @private
 */
CrossWord.prototype._onDown = function(e) {
    this._isMove = true;

    var p = e.data.getLocalPosition(this.parent);
    this._checkTouch(p);

    this.data = e.data;
    this.local = this.data.getLocalPosition(this);
};

/**
 * Metodo que desabilita a captura de letras e limpa a palavra formada
 *
 * @memberof CrossWord
 * @param e não é utilizado nenhuma caracteristica do evento
 * @private
 */
CrossWord.prototype._onUp = function(e) {
    this._isMove = false;

    this._clear();
    this._checkWord();
};

/**
 * Metodo que captura a posição do cursor e utiliza a mesma para fomar palavras no caça palavras
 *
 * @memberof CrossWord
 * @param e não é utilizada nenhuma caracteristica do evento
 * @private
 */
CrossWord.prototype._onMove = function(e) {
    if (this._isMove) {
        var newPosition = this.data.getLocalPosition(this.parent);

        this._checkTouch(newPosition);
    }
};

/**
 * Metodo que captura a posição do cursor e utiliza a mesma para fomar palavras no caça palavras
 *
 * @memberof CrossWord
 * @param point verifica a posição que está o cursor(dedo) e forma palavras empurrando letras em um array
 * @private
 */
CrossWord.prototype._checkTouch = function(point) {
    var p = point;

    for (var i = 0; i < this._fields.length; i++) {
        var g = this._fields[i].sensor.toGlobal(this._fields[i].sensor.position);
        var a = new PIXI.Rectangle(g.x, g.y, this._fields[i].sensor.width, this._fields[i].sensor.height);

        if (p.x > a.x && p.x < a.x + a.width && p.y > a.y && p.y < a.y + a.height && !this._fields[i].turnOn) {
            this._fields[i].turnOn = true;
            this._currentWord += this._fields[i].text;
            this._currentFields.push(this._fields[i]);

            console.log(this._currentWord);
        }
    }
};

/**
 * Metodo para desabilitar campos
 *
 * @memberof CrossWord
 * @private
 */
CrossWord.prototype._clear = function() {
    for (var i = 0; i < this._fields.length; i++) {
        this._fields[i].turnOn = false;
    }
};

/**
 * Metodo que verificar se a palavra forma é igual alguma das palavras corretas
 * depois emite os eventos correct e incorrect
 * @param nomePreenchido {string}
 */
CrossWord.prototype.verificaPreenchidos = function(nomePreenchido) {
    for (var i = 0; i < this.lacunasPreenchidas.length; i++) {
        if (nomePreenchido === this.lacunasPreenchidas[i]) {
            return true;
        }
    }
    return false;
};

/**
 * Metodo para validar palavra
 *
 * @memberof CrossWord
 * @private
 */
CrossWord.prototype._checkWord = function() {
    var isok = false;
    var self = this;
    console.log("current word");
    console.log(this._currentWord);

    for (var i = 0; i < this._corrects.length; i++) {
        console.log("corretas....");
        console.log(this._corrects[i]);
        if (this._corrects[i] == this._currentWord) {

            if (this.verificaPreenchidos(this._corrects[i])) {
                break;
            }
            this.lacunasPreenchidas.push(this._corrects[i]);

            if (this._textBase !== undefined && this._textBase !== null) {
                this._textBase.field.setWordStyle(this._textBase.field._text.match(new RegExp(this._corrects[i], "ig"))[0], {
                    fill: "#187815"
                });
            }

            this.emit("correct");

            this._drawFeed();

            isok = true;

            break;
        }
    }

    if (!isok) {
        this.emit("incorrect");
    }

    this._currentWord = "";
    this._currentFields.splice(0, this._currentFields.length);
};

/**
 * Metodo que deixa as respostas corretas marcadas
 *
 * @memberof CrossWord
 * @private
 */
CrossWord.prototype._drawFeed = function() {
    if (this._currentFields[0].x != this._currentFields[this._currentFields.length - 1].x && this._currentFields[0].y != this._currentFields[this._currentFields.length - 1].y) {
        this._drawDiagonal();
    } else {
        this._drawRect();
    }
};

/**
 * Preenche os fundo das letras formando um linha caso a resposta seja uma resposta correta
 *
 * @memberof CrossWord
 * @private
 */
CrossWord.prototype._drawRect = function() {
    var i = this._currentFields[0];
    var f = this._currentFields[this._currentFields.length - 1];

    var feed = new PIXI.Graphics();
    feed.beginFill(0x187815, 1);
    feed.drawRect(i.x - i.width / 2, i.y - i.height / 2, f.x - i.x + i.width, f.y - i.y + i.height);
    feed.endFill();

    this._feeds.push(feed);
    this._generalContainer.addChildAt(feed, 1);
    this._currentFields.splice(0, this._currentFields.length);
};

/**
 *  Preenche os fundo das letras formando uma diagonal caso a resposta seja uma resposta correta
 *
 * @memberof CrossWord
 * @private
 */
CrossWord.prototype._drawDiagonal = function() {
    var i = this._currentFields[0];
    var f = this._currentFields[this._currentFields.length - 1];

    var feed = new PIXI.Graphics();
    feed.beginFill(0x187815, 1);
    if (i.x < f.x) {
        feed.moveTo(i.x - i.width, i.y);
        feed.lineTo(f.x, f.y + f.height);
        feed.lineTo(f.x + f.width, f.y);
        feed.lineTo(i.x, i.y - i.height);
    } else {
        feed.moveTo(i.x + i.width, i.y);
        feed.lineTo(f.x, f.y + f.height);
        feed.lineTo(f.x - f.width, f.y);
        feed.lineTo(i.x, i.y - i.height);
    }

    feed.endFill();

    this._feeds.push(feed);
    this._generalContainer.addChildAt(feed, 1);
    this._currentFields.splice(0, this._currentFields.length);
};

/**
 * Método de dispose do caça palavras e suas dependencias para liberação da memória
 */
CrossWord.prototype.reset = function() {
    this.removeChildren();
    this.removeAllListeners();

    this._fieldsContainer.removeChildren();

    for (var i = 0; i < this._fields.length; i++) {
        this._fields[i].destroy();
        this._fields[i] = null;
    }

    this._fields.splice(0, this._fields.length);
    this._feeds.splice(0, this._feeds.length);

    for (i = this._generalContainer.children.length; i > 0; i--) {
        var c = this._generalContainer.removeChildAt(0);
        c.destroy();
        c = null;
    }

    this.lacunasPreenchidas.splice(0, this.lacunasPreenchidas.length);

    for (i = 0; i < this._corrects.length; i++) {
      this._textBase.field.setWordStyle(this._textBase.field._text.match(new RegExp(this._corrects[i], "ig"))[0], {
          fill: "#000000"
      });
    }
};
