var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Classe responsável por criar bocas
 * @extends module:core.ObjectBase
 * @extends module:behavior.AlignSupport
 * @memberof module:display/simple
 * @exports MovieClip
 * @constructor
 * @param texture {PIXI.Texture} Textura para formar a imagem
 */
function MovieClip(path, names, speed) {
    var self = this;

    ObjectBase.call(this);

    this.loader = new PIXI.loaders.Loader();

    this._movieClip = null;
    this._stationary = 0;

    this._displayType = "movieClip";

    this.addPlugin(new behavior.AlignSupport());
    this.objectToAlign = this._sprite;

    this.frames = [];

    var loader = PIXI.loader;

    this.loader.once('loaded', function(response) {

        self.defineMovieClip(response.framesTextures, speed);
    });


    self.frames = self.generateFrames(path, names);

    /**
     * Emitido quando a Boca é criada
     * @event MovieClipCreated
     * @memberof module:display/simple.MovieClip
     */

}

MovieClip.prototype = Object.create(ObjectBase.prototype);
MovieClip.prototype.constructor = MovieClip;
module.exports = MovieClip;

Object.defineProperties(MovieClip.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @type {String}
     * @memberof module:display/simple.MovieClip
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * numero que representa o frame estatico do movie clip
     * @type {Number}
     * @memberof module:display/simple.MovieClip
     * @readonly
     */
    stationary: {
        get: function() {
            return this._stationary;
        }
    },

    /**
     * escalona a largura do video
     * @type {Number}
     * @memberof module:display/simple.MovieClip
     */
    rescaleMovieWidth: {
        get: function() {
            return this._movieClip.width;
        },

        set: function(value) {
            this._movieClip.width = value;
        }
    },

    /**
     * escalona a altura do video
     * @type {Number}
     * @memberof module:display/simple.MovieClip
     */
    rescaleMovieHeight: {
        get: function() {
            return this._movieClip.height;
        },

        set: function(value) {
            this._movieClip.height = value;
        }
    },

    /**
     * variavel que armazena a função a ser executada ápos o fim do movie clip
     * @type {function}
     * @memberof module:display/simple.MovieClip
     */
    onComplete : {
        get: function() {
            return this._movieClip.onComplete;
        },

        set: function(value) {
            this._movieClip.onComplete = value;
        }
    },

    /**
     * variavel que armazena velocidade do movie clip
     * @type {number}
     * @memberof module:display/simple.MovieClip
     */
    animationSpeed: {
      get: function() {
        return this._movieClip.animationSpeed;
      },

      set: function(value) {
        this._movieClip.animationSpeed = value;
      }
    },

});

/**
 * Gera movie clip a partir de frames de entrada
 * @param frames {Array} Texturas do movieClip
 * @fires MovieClipCreated sinaliza a criação do movieClip
 */
MovieClip.prototype.defineMovieClip = function(frames, speed) {
    var self = this;

    this._movieClip = new PIXI.extras.MovieClip(frames);

    this.addChild(this._movieClip);

    this._movieClip.pivot.set(this._movieClip.width / 2, this._movieClip.height / 2);
    this.gotoAndStop(0);
    this._movieClip.loop = false;



    if (speed === undefined) {
        this._movieClip.animationSpeed = 0.2;
    } else{
        this._movieClip.animationSpeed = speed;
    }


    self.emit("MovieClipCreated");

};

/**
 * inicia o movimento do movie
 */
MovieClip.prototype.play = function() {
    var self = this;

    this._movieClip.play();
};

/**
 * inicia o movimento do movie uma vez
 */
MovieClip.prototype.playOnce = function() {
    var self = this;

    this._movieClip.gotoAndStop(self._stationary);
};

/**
 * vai para um frame especifico e para o video
 * @param frame {Object}
 */
MovieClip.prototype.gotoAndStop = function(frame) {
    var self = this;

    this._movieClip.gotoAndStop(frame);
};

/**
 * vai para um frame especifico e toca o video
 * @param frame {Object}
 */
MovieClip.prototype.gotoAndPlay = function(frame) {
    var self = this;

    this._movieClip.gotoAndPlay(frame);
};
/**
 * Gera os frames especificos para essa boca
 * @param path {String} Caminho do json e sprites para pegar os frames
 * @param names {Object[]}
 */
MovieClip.prototype.generateFrames = function(path, names) {

    var self = this;

    this.loader.reset();
    var framesTextures = [];

    for (var i = 0; i < names.length; i++) {
        this.loader.add(names[i], path + names[i] + ".json");
    }



    //carrega os sprites da tela
    this.loader.load(function(loader, resource) {

        for (i = 0; i < names.length; i++) {

            for (var j in resource[names[i]].textures) {
                framesTextures.push(resource[names[i]].textures[j]);
            }

        }

        self._stationary = framesTextures.length - 1;

        loader.emit('loaded', {
            framesTextures: framesTextures
        });
    });
};
