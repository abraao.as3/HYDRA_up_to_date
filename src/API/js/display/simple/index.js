var simple = module.exports = {

    AreaDrag: require('./AreaDrag'),

    Ballon: require('./Ballon'),

    Card: require('./Card'),

    Compass: require('./Compass'),

    CrossWord: require('./CrossWord'),

    Drag: require('./Drag'),

    LinePoint: require('./LinePoint'),

    Mouth: require('./Mouth'),

    MovieClip: require('./MovieClip'),

    SeteErros: require('./SeteErros'),

    Scroll: require('./Scroll')

};
