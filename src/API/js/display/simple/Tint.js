var PIXI = require('PIXI');
var TweenMax = require('TweenMax');
var ObjectBase = require('../../core/ObjectBase');

/**
  * @classdesc Classe que define os Tint e sua exibição
  * @memberof module:display/simple
  * @extends Pixi.Container
  * @exports Tint
  * @constructor
  * @param texture {PIXI.Texture} Textura para formar a imagem
  */
function Tint(texture) {
    ObjectBase.call(this);

    this._displayType = "tint";

    this._color = "";

    this._feed = new PIXI.Sprite(texture);
    this._feed.alpha = 0;
    this.addChild(this._feed);
}

Tint.prototype = Object.create(ObjectBase);
Tint.prototype.constructor = Tint;

Object.defineProperties(Tint.prototype, {
    /**
     * @memberof module:display/simple
     * @type {string}
     */
    blend: {
        set: function(value) {
            this.blendMode = value;
            this._feed.blendMode = value;
        }
    }
});

/**
 * Pinta o objeto da cor que vier de dentro do paramentro e mostra o objeto pintado
 *
 */
Tint.prototype.color = function(color) {
    var self = this;

    if (color !== null && color !== undefined) this._feed.tint = color;

    TweenMax.to(this._feed, 0.3, {
        alpha: 1
    });
};

/**
 * Esconde o objeto pintado.
 *
 */
Tint.prototype.uncolor = function() {
    var self = this;

    TweenMax.to(this._feed, 0.3, {
        alpha: 0
    });
};
