var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Define um objeto do tipo SeteErros usado em exericios
 * @memberof module:display/simple
 * @extends PIXI.Container
 * @exports SeteErros
 * @constructor
 */
function SeteErros() {
    ObjectBase.call(this);

	this._displayType = "seteErros";

    this._corrects = 0;

}

SeteErros.prototype = Object.create(ObjectBase.prototype);
SeteErros.prototype.constructor = SeteErros;
module.exports = SeteErros;

Object.defineProperties(SeteErros.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name _displayType
     * @type {String}
     * @memberof module:display/simple.SeteErros
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * Counter utilizado no calculo de quantos pontos foram encontrados
     * @name _corrects
     * @type {Number}
     * @memberof module:display/simple.SeteErros
     * @readonly
     */
    corrects: {
        get: function() {
            return this._corrects;
        },
        set: function(param) {
            this._corrects = param;
        }
    }

});

/**
 * Metodo que incia o Sete Erros
 * @memberof module:display/simple.SeteErros
 * @public
 */
SeteErros.prototype.initSeteErros = function (){

    var self = this;

    self._corrects = 0;

    for (var i = 0; i < self.config.erros.length; i++) {
        if (self.config.erros[i].hasOwnProperty('alpha')) {
            self.config.erros[i].alpha = 0;
        }
    }

};

/**
 * Metodo que adiciona eventos no Sete Erros
 * @memberof module:display/simple.SeteErros
 * @public
 */
SeteErros.prototype.addSeteErrosEvents = function (){

    var self = this;

    for (var i = 0; i < self.config.erros.length; i++) {
            if (!self.config.erros[i]._block) {
                self.config.erros[i].once('mousedown', self.addPoints, { ctx : self, erro : self.config.erros[i]});

            }

    }

};

/**
 * Metodo remove todos os eventos no Sete Erros
 * @memberof module:display/simple.SeteErros
 * @public
 */
SeteErros.prototype.removeSeteErrosEvents = function (){

    var self = this;

    for (var i = 0; i < self.config.erros.length; i++) {
            self.config.erros[i].off('mousedown');
    }

};

/**
 * Metodo adiciona pontos no Sete Erros
 * @memberof module:display/simple.SeteErros
 * @public
 */
SeteErros.prototype.addPoints = function (){

    var self = this;

    self.ctx._corrects++;
    self.erro._block = true;
    if (self.ctx._corrects == self.ctx.config.erros.length) {
        self.ctx.emit('exercicioCompleto');
    }

};
