
var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Define um objeto com propriedades de uma carta
 * @memberof module:display/simple
 * @extends PIXI.Container
 * @exports Card
 * @constructor
 */
function Card() {
    ObjectBase.call(this);
    this._displayType = "card";
    this._selectionEffect = true;
    this._block = false;
    this._cardStatus = 0;
    this.id = 0;
    this._start = false;
    this._hideCard = false;

}

Card.prototype = Object.create(ObjectBase.prototype);
Card.prototype.constructor = Card;
module.exports = Card;

Object.defineProperties(Card.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name _dispalyType
     * @type {String}
     * @memberof module:display/simple.Card
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    }
});

/**
 * Este método gira o cartao dependendo da posicao
 *
 * @param status [self._back.alpha = 1] {number} posicao do cartao verso
 * @param status [self._front.alpha = 1] {number} posicao do cartao frente
 */
Card.prototype.flipCard = function(status) {
    var self = this;
    this._cardStatus = status;
    switch (this._cardStatus) {
        case 1:
            self._back.alpha = 1;

            break;

        case 0:
            self._front.alpha = 1;
            if(self._texto !== undefined)
                self._texto.alpha = 1;
            break;
        default:

    }

    switch (this.config.type) {
        case "horizontal":
            TweenMax.to(this.scale,self.config.scaleTime,{x:0, onComplete: this.flipCardOnComplete, onCompleteParams: [this]});
            break;

        case "vertical":
            TweenMax.to(this.scale,self.config.scaleTime,{y:0, onComplete: this.flipCardOnComplete, onCompleteParams: [this]});
            break;

        default:
    }
};

/**
 * Este método gira o cartao dependendo da posicao
 *
 * @param self {Object[]}
 */
Card.prototype.flipCardOnComplete = function(self)
{
    switch (self._cardStatus) {
        case 1:
            self._front.alpha = 1;
            self._back.alpha = 0;

            if(self._texto !== undefined)
                self._texto.alpha = 1;

            break;

        case 0:
            self._front.alpha = 0;
            self._back.alpha = 1;

            if(self._texto !== undefined)
                self._texto.alpha = 0;
            break;
        default:

    }
    switch (self.config.type) {
        case "horizontal":
            TweenMax.to(self.scale,self.config.scaleTime,{x:1, onComplete:self.flippedCard()});
            break;

        case "vertical":
            TweenMax.to(self.scale,self.config.scaleTime,{y:1, onComplete:self.flippedCard()});
            break;

        default:
    }
};

Card.prototype.resetCard = function()
{
    var self = this;
    // self._front.alpha = 1;
    // self._back.alpha = 0;
    // TweenMax.killAll();
    clearInterval(this.addTimer);
    self.flipCard(1);
};

/**
 * Este método verifica se o cartao foi girado
 *
 */
Card.prototype.flippedCard  = function()
{

    if(this._cardStatus === 0)
    {
        this._cardStatus = 1;
    }else{
        this._cardStatus = 0;
    }

};

Card.prototype.removeFundoCard = function()
{
    var self = this;

    for (var i = 0; i < this.children.length; i++) {

        if(i === 0){
            this.children[i].alpha =0;
        }
    }
};

/**
 * Metodo que adiciona evento nos elementos injetados
 */
Card.prototype.addEvents = function() {
    var self = this;

    this.addTimer = setTimeout(function() {
        self.interactive = true;
        self.buttonMode = true;
        self.on('mousedown', self._onDown);
        self.on('touchstart', self._onDown);

        console.log("Cards", self.children);

        for (var i = 0; i < self.children.length; i++) {
            if(i === 1){
                self._back = self.children[i];
            }
            if(i === 2)
            {
                self._front = self.children[i];
                self._front.alpha = 0;
            }

            if(i === 3)
            {
                self._texto = self.children[i];
            }
        }

        if(!self._hideCard)
            self.flipCard(0);

    }, self.config.timeOpen*1000);

};

/**
 *
 * @memberof Card
 * @private
 */
Card.prototype._addUpEvents = function() {
    this.on('mouseup', this._onUp);
    this.on('touchend', this._onUp);

    this.on('mouseupoutside', this._onUp);
    this.on('touchendoutside', this._onUp);
};

/**
 * Metodo que remove evento nos elementos injetados
 */
Card.prototype.removeEvents = function() {
    this.interactive = false;
    this.buttonMode = false;

    this.removeListener('mousedown', this._onDown);
    this.removeListener('touchstart', this._onDown);

    this._removeUpEvents();

};

/**
 *
 * @memberof Card
 * @private
 */
Card.prototype._removeUpEvents = function() {
    this.removeListener('mouseup', this._onUp);
    this.removeListener('touchend', this._onUp);

    this.removeListener('mouseupoutside', this._onUp);
    this.removeListener('touchendoutside', this._onUp);
};

/**
 * Metodo que reduz o tamanho do elemento clickado
 * @private
 */
Card.prototype._onDown = function(e) {
    if(this._block) return;
    if (this._selectionEffect) {
        this.flipCard(this._cardStatus);
        this.emit('cardClicked');
    }

    this._addUpEvents();

};

/**
 * Metodo que retorna o tamanho do elemento clickado ao tamanho original
 * @param e evento
 * @private
 */
Card.prototype._onUp = function(e) {
    this._removeUpEvents();
    if (this._selectionEffect) {
    }

    //Dispara o evento indicando o click
    this.emit("clicked", e);
};
