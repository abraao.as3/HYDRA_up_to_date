var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Define um objeto que pode ser arrastado e atribuido a um receptor
 * @memberof module:display/simple
 * @extends PIXI.Container
 * @exports Drag
 * @constructor
 */
function Drag() {
    ObjectBase.call(this);

    this._displayType = "drag";

    this.addPlugin(new behavior.Dragable());

    //pequeno contorno para corrigir discrepancia de ponto de registro do drag para a tela fazendo o behavior funcionar para os 2
    this.on("positionUpdated", function(){
        this.x += this.width /2;
        this.y += this.height /2;
    });
}

Drag.prototype = Object.create(ObjectBase.prototype);
Drag.prototype.constructor = Drag;
module.exports = Drag;

Object.defineProperties(Drag.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @name _dispalyType
     * @type {String}
     * @memberof module:display/simple.Drag
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    }


});
