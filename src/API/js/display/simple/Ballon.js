var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');
var constantes = require('../../core/Const');

/**
 * @classdesc Define um objeto que pode ser rotacionado
 * @memberof module:display/simple
 * @extends PIXI.Container
 * @exports Ballon
 * @constructor
 */
function Ballon() {
    ObjectBase.call(this);

    this._displayType = "ballon";

    this._resetOnFail = true;

}

Ballon.prototype = Object.create(ObjectBase.prototype);
Ballon.prototype.constructor = Ballon;
module.exports = Ballon;

Object.defineProperties(Ballon.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @type {String}
     * @memberof module:display/simple.Ballon
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    }

});

/**
 * Este método verifica se aqui está o conteúdo na posição específica
 *
 * @param x {number} posicao x
 * @param y {number} posicao y
 * @param width {number} largura do conteudo
 * @param height {number} altura do conteudo
 * @return {Boolean} Retorna se este bitmap Contém algo na posição específica
 */
Ballon.prototype.hasContent = function(x, y, width, height) {

    for (var i = 0; i < this.children.length; i++) {
        if (this.children[i]._displayType == "bitmap") {

            if (this.children[i].hasContent((x - this.x), (y - this.y), width, height)) {
                return true;
            }
        }
    }
    return false;

};

/**
 * Este método é um setter da propriedade
 *
 * @param resetOnFail {Boolean}
 */
Ballon.prototype.setResetOnFail = function(resetOnFail) {
    this._resetOnFail = resetOnFail;
};

/**
 * Este método reinicia um balao na situacao inicial
 *
 * @param t_ballon {object}
 */
Ballon.prototype.resetBallon = function(t_ballon) {
    var self = this;
    var complete = function() {
        t_ballon.addDragEvents();
    };

    if (this._isOff) return;

    if (self._resetOnFail) {


        TweenMax.from(self.position, 1.5, {
            y: constantes.BASE_HEIGHT,
            ease: Back.easeOut,
            onComplete: complete
        });


    }

};
