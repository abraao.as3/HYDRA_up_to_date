/**
 * @module display/simple
 */
var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Base de todos os objetos
 * @memberof module:display/simple
 * @extends PIXI.Container
 * @exports AreaDrag
 * @constructor
 */
function AreaDrag() {
    ObjectBase.call(this);

    this._displayType = "areaDrag";

    this._containerDrags = [];
    this._maxChildren = 0;
    this._alignMode = "none";

    this._vspace = 0;
    this._hspace = 0;
    this._grid = [];

    this._index = null;

    this._corrects = [];

    this.addPlugin(new behavior.Dragable());


    /**
     * Emitido quando o areaDrag recebe um elemento e não o comporta
     * @event areafull
     * @memberof module:display/simple.AreaDrag
     */
    /**
     * Emitido quando o drag recebe um elemento
     * @event received
     * @memberof module:display/simple.AreaDrag
     */
    /**
     * Emitido quando o area Drag atinge seu limite de e não pode mais receber elementos
     * @event full
     * @memberof module:display/simple.AreaDrag
     */
}

AreaDrag.prototype = Object.create(ObjectBase.prototype);
AreaDrag.prototype.constructor = AreaDrag;
module.exports = AreaDrag;

Object.defineProperties(AreaDrag.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @type {String}
     * @memberof module:display/simple.AreaDrag
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * numero maximo de drag que podem ser recebidos neste elemento
     * @type {Number}
     * @memberof module:display/simple.AreaDrag
     */
    maxChildren: {
        get: function() {
            return this._maxChildren;
        },

        set: function(value) {
            this._maxChildren = value;
        }
    },

    /**
     * alias para align mode (forma de alinhamento dos drags)
     * @type {String}
     * @memberof module:display/simple.AreaDrag
     */
    align: {
        get: function() {
            return this._alignMode;
        },

        set: function(value) {
            this._alignMode = value;
        }
    },

    /**
     * espacamento vertical entre os objetos alinhados
     * @type {Number}
     * @memberof module:display/simple.AreaDrag
     */
    vspace: {
        get: function() {
            return this._vspace;
        },

        set: function(value) {
            this._vspace = value;
        }
    },

    /**
     * espacamento horizontal entre os objetos alinhados
     * @type {Number}
     * @memberof module:display/simple.AreaDrag
     */
    hspace: {
        get: function() {
            return this._hspace;
        },

        set: function(value) {
            this._hspace = value;
        }
    },

    /**
     * Pontos que serão utilizados no caso alinhamntos customizados
     * @type {Number}
     * @memberof module:display/simple.AreaDrag
     */
    points: {
        get: function() {
            return this._points;
        },

        set: function(value) {
            this._points = value;
        }
    },

    /**
     * forma de alinhamento dos drags
     * @type {String}
     * @memberof module:display/simple.AreaDrag
     */
    alignMode: {
        get: function() {
            return this._alignMode;
        },

        set: function(value) {
            this._alignMode = value;
        }
    },

    /**
     * indice deste elemento na tela
     * @type {Number}
     * @memberof module:display/simple.AreaDrag
     */
    index: {
        get: function() {
            return this._index;
        },

        set: function(value) {
            this._index = value;
        }
    },

    /**
     * Variavel que armazena a informação dos drags corretos para este elemento
     * @type {Array}
     * @memberof module:display/simple.AreaDrag
     */
    corrects: {
        get: function() {
            return this._corrects;
        },

        set: function(value) {
            this._corrects = value;
        }
    }
});

/**
 * Utilizado para atribuir um drag a este elemento
 * @param {module:display/simple.Drag}
 * @fires areafull Emitido quando o area drag já está cheio e recebe mais um elemento
 * @fires received enviado qndo um drag é recebido pelo areadrag
 * @fires full emitido qndo a area atinge seu limite e não comporta mais elementos
 */
AreaDrag.prototype.receiveDrag = function(drag) {

    if ((this._maxChildren > 0 && this._containerDrags.length >= this._maxChildren) || this._lockArea === true) {
        drag.resetPosition();
        this.emit('areaFull');
        return;
    }


    this._containerDrags.push(drag);
    this._align();

    this.emit('received');
    if (this.disableDragOnHit) drag._isOff = true;


    if (this._maxChildren > 0 && this._containerDrags.length >= this._maxChildren) {
        this.emit('full');
    }
};

/**
 * Remove elemento desta area
 * @param index {Number} indice do elemento que será removido do area drag
 */
AreaDrag.prototype.removeDrag = function(index) {
    this._containerDrags.splice(index, 1);
};

/**
 * Remove todos os elementos desta area
 */
AreaDrag.prototype.clear = function() {

    for (var i = 0; i < this._containerDrags.length; i++) {
        var d = this._containerDrags[i];
        d._isOff = false;
        d.resetPosition();
    }

    this._containerDrags.splice(0, this._containerDrags.length);
};

/**
 * Remove apenas os elementos incorretos
 */
AreaDrag.prototype.clearIncorrect = function() {

    for (var i = 0; i < this._containerDrags.length; i++) {
        var d = this._containerDrags[i];

        var v = false;
        for (var j = 0; j < this.corrects.length; j++) {
            if (this.corrects[j] == d.id) {
                v = true;
                break;
            }
        }

        if (!v) {
            d._isOff = false;
            d.resetPosition();
            var removido = this._containerDrags.splice(i, 1);

            i--;
        }
    }
};

/**
 * Metodo que cria guarda as referencias para posicionar os itens em grid
 * @param row {Number} numero de linhas do grid
 * @param col {Number} numero de colunas do grid
 */
AreaDrag.prototype.createGrid = function(row, col) {
    this._grid = {
        "row": row,
        "col": col
    };
};

/**
 * Realiza a tomada de decisão de como alinhas os elementos recebidos baseado no align mode
 * @private
 */
AreaDrag.prototype._align = function() {
    switch (this._alignMode) {
        case AreaDrag.AlineMode.VERTICAL:
            this._verticalAlign();
            break;
        case AreaDrag.AlineMode.HORIZONTAL:
            this._horizontalAlign();
            break;
        case AreaDrag.AlineMode.GRID:
            this._gridAlign();
            break;
        case AreaDrag.AlineMode.PERSON:
            this._personAlign();
            break;
    }
};

/**
 * manipula os elementos para que eles fiquem alinhados verticalmente e seu espacamentoé determinado por vspace
 * @private
 */
AreaDrag.prototype._verticalAlign = function() {
    for (var i = 0; i < this._containerDrags.length; i++) {
        var d = this._containerDrags[i];

        if (i === 0) {
            TweenMax.to(d, 0.3, {
                x: this.x,
                y: this.y - this.height / 2 + d.height / 2
            });
        } else {
            var dold = this._containerDrags[i - 1];

            TweenMax.to(d, 0.3, {
                x: this.x,
                y: dold.y + dold.height / 2 + this._vspace + d.height / 2
            });
        }
    }
};

/**
 * manipula os elementos para que eles fiquem alinhados horizontalmente e seu espacamento é determinado por hspace
 * @private
 */
AreaDrag.prototype._horizontalAlign = function() {
    for (var i = 0; i < this._containerDrags.length; i++) {
        var d = this._containerDrags[i];

        if (i === 0) {
            TweenMax.to(d, 0.3, {
                x: this.x - this.width / 2 + d.width / 2,
                y: this.y
            });
        } else {
            var dold = this._containerDrags[i - 1];

            TweenMax.to(d, 0.3, {
                x: dold.x + dold.width / 2 + this.hspace + d.width / 2,
                y: this.y
            });
        }
    }
};

/**
 * manipula os elementos para que eles fiquem alinhados com o grid preciamente criado
 * @private
 */
AreaDrag.prototype._gridAlign = function() {
    var self = this;
    for (var i = 0; i < this._containerDrags.length; i++) {
        var d = this._containerDrags[i];

        TweenMax.to(d, 0.3, {
            x: self.x - self.width / 2 + i % this._grid.col * self.width / this._grid.col + self.width / this._grid.col / 2,
            y: self.y - self.height / 2 + Math.floor(i / this._grid.row) * self.height / this._grid.row + self.height / this._grid.row / 2,
        });

    }
};

/**
 * move os elementos para as posições customizadas determinadas previamente
 * @private
 */
AreaDrag.prototype._personAlign = function() {
    for (var i = 0; i < this._containerDrags.length; i++) {
        var d = this._containerDrags[i];

        TweenMax.to(d, 0.3, {
            x: this._points[i].x,
            y: this._points[i].y
        });
    }
};

/**
 * Verifica todos os elementos recebidos se o indice deles é correto ou não
 * @param maxCheck {Number} define se a verifica~]ap será completa ou de apenas uma quantidade de elementos
 */
AreaDrag.prototype.checkCorrect = function(maxCheck) {

    var isOk = false;

    if (Array.isArray(this.corrects)) {

        var max = maxCheck || this._maxChildren;

        var count = 0;
        for (var i = 0; i < this._containerDrags.length; i++) {
            var v = false;
            for (var j = 0; j < this.corrects.length; j++) {
                if (this.corrects[j] == this._containerDrags[i].id) {
                    v = true;
                    break;
                }
            }

            if (v)
                count++;
            else
                break;
        }

        if (count == max)
            isOk = true;

        return isOk;

    } else {

        if (this._containerDrags.length == this.corrects) {
            isOk = true;
        }

        return isOk;
    }
};


/**
 * Constante que armazenará todas as formas de alinhamento
 * @const
 */
AreaDrag.AlineMode = {};

/**
 * Constante que representa nenhuma forma de alinhamento
 * @const
 */
AreaDrag.AlineMode.NONE = "none";

/**
 * Constante que representa alinhamnto vertical
 * @const
 */
AreaDrag.AlineMode.VERTICAL = "vertical";

/**
 * Constante que representa alinhamnto horizontal
 * @const
 */
AreaDrag.AlineMode.HORIZONTAL = "horizontal";

/**
 * Constante que representa alinhamnto por grid
 * @const
 */
AreaDrag.AlineMode.GRID = "grid";

/**
 * Constante que representa alinhamnto personlizado
 * @const
 */
AreaDrag.AlineMode.PERSON = "person";
