var PIXI = require('PIXI');
var ObjectBase = require('../../core/ObjectBase');
var behavior = require('../../behavior/index');

/**
 * @classdesc Define um objeto que pode ser rotacionado
 * @memberof module:display/simple
 * @extends PIXI.Container
 * @exports Compass
 * @constructor
 */
function Compass() {
    ObjectBase.call(this);

	this._displayType = "compass";

    this.addPlugin(new behavior.Rotable());
}

Compass.prototype = Object.create(ObjectBase.prototype);
Compass.prototype.constructor = Compass;
module.exports = Compass;

Object.defineProperties(Compass.prototype, {

    /**
     * retorna o tipo de objeto entre os elementos da tela
     * @type {String}
     * @memberof module:display/simple.Compass
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    }


});
