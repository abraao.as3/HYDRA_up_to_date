var core = module.exports = {

    Bitmap: require('./Bitmap'),

    LayerMoveReference: require('./LayerMoveReference'),

    Layers: require('./Layers'),

    ObjectFactory: require('./ObjectFactory')

};
