/**
 * @module display/core
 */
var PIXI = require('PIXI');
var PIXIConfig = require('../../core/PIXIConfig');
var ObjectBase = require('../../core/ObjectBase');
// var behavior = require('../../behavior/index');
var AlignSupport = require('../../behavior/AlignSupport');

/**
 * @classdesc Classe responsável por criar as imagens
 * @extends module:core.ObjectBase
 * @extends module:behavior.AlignSupport
 * @memberof module:display/core
 * @exports Bitmap
 * @constructor
 * @param texture {PIXI.Texture} Textura para formar a imagem
 */
function Bitmap(texture) {
    ObjectBase.call(this);

    this._displayType = "bitmap";

    /**
     * @member {PIXI.Texture}
     * @private
     */
    this._texture = texture;

    /**
     * Cria Sprite baseado na texture
     * @member {PIXI.Sprite}
     * @private
     */
    this._sprite = new PIXI.Sprite(texture);
    this.addChild(this._sprite);

    // this.addPlugin(new behavior.AlignSupport());
    this.addPlugin(new AlignSupport());
    this.objectToAlign = this._sprite;
}

Bitmap.prototype = Object.create(ObjectBase.prototype);
Bitmap.prototype.constructor = Bitmap;
module.exports = Bitmap;

Object.defineProperties(Bitmap.prototype, {
    /**
     * @memberof module:display/core.Bitmap
     * @name sprite
     * @type {PIXI.Sprite}
     * @readonly
     */
    sprite: {
        get: function() {
            return this._sprite;
        }
    },

    /**
     * @memberof module:display/core.Bitmap
     * @name displayType
     * @type {PIXI.displayType}
     * @readonly
     */
    displayType: {
        get: function() {
            return this._displayType;
        }
    },

    /**
     * @memberof module:display/core.Bitmap
     * @name tint
     * @type {String}
     */
    tint: {
        get: function() {
            return this._sprite.tint;
        },
        set: function(cor) {
            this._sprite.tint = cor;
        }
    }
});

/**
 * Este método verifica se aqui está o conteúdo na posição específica
 *
 * @param x {number} posicao x
 * @param y {number} posicao y
 * @param width {number} largura do conteudo
 * @param height {number} altura do conteudo
 * @return {Boolean} Return if this bitmap Contains something in the specific position
 */
Bitmap.prototype.hasContent = function(x, y, width, height) {

    var bx = this.x - this.width / 2;
    var by = this.y - this.height / 2;
    var bw = this.width;
    var bh = this.height;

    bx = Math.floor(bx);
    by = Math.floor(by);
    bw = Math.floor(bw);
    bh = Math.floor(bh);

    x = Math.floor(x);
    y = Math.floor(y);



    // find the top left and bottom right corners of overlapping area
    var xMin = Math.max(x, bx),
        yMin = Math.max(y, by),
        xMax = Math.min(x + width, bx + bw),
        yMax = Math.min(y + height, by + bh);


    // Sanity collision check, we ensure that the top-left corner is both
    // above and to the left of the bottom-right corner.
    if (xMin >= xMax || yMin >= yMax) {
        return false;
    }

    var xDiff = xMax - xMin,
        yDiff = yMax - yMin;


    var pixels = PIXIConfig.renderer.extract.pixels(this);

    // if the area is really small,
    // then just perform a normal image collision check
    if (xDiff < 4 && yDiff < 4) {
        for (pixelX = xMin; pixelX < xMax; pixelX++) {
            for (pixelY = yMin; pixelY < yMax; pixelY++) {
                if (pixels[((pixelX - bx) + (pixelY - by) * bw) * 4 + 3] !== 0 && pixels[((pixelX - bx) + (pixelY - by) * bw) * 4 + 3] !== undefined) {
                    return true;
                }
            }
        }
    } else {
        /* What is this doing?
         * It is iterating over the overlapping area,
         * across the x then y the,
         * checking if the pixels are on top of this.
         *
         * What is special is that it increments by incX or incY,
         * allowing it to quickly jump across the image in large increments
         * rather then slowly going pixel by pixel.
         *
         * This makes it more likely to find a colliding pixel early.
         */

        // Work out the increments,
        // slither of an area for the last iteration (using fast ceil).
        var incX = xDiff / 5.0,
            incY = yDiff / 5.0;
        incX = (~~incX === incX) ? incX : (incX + 1 | 0);
        incY = (~~incY === incY) ? incY : (incY + 1 | 0);

        for (var offsetY = 0; offsetY < incY; offsetY++) {
            for (var offsetX = 0; offsetX < incX; offsetX++) {
                for (pixelY = yMin + offsetY; pixelY < yMax; pixelY += incY) {
                    for (pixelX = xMin + offsetX; pixelX < xMax; pixelX += incX) {
                        if (pixels[((pixelX - bx) + (pixelY - by) * bw) * 4 + 3] !== 0 &&
                            pixels[((pixelX - bx) + (pixelY - by) * bw) * 4 + 3] !== undefined) {
                            return true;
                        }
                    }
                }
            }
        }
    }

    return false;


};
