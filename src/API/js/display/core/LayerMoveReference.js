/**
 * @classdesc Classe que Representa a relatividade de movimento das layers
 * @memberof module:display/core
 * @exports LayerMoveReference
 * @param x {number} posicao x
 * @param y {number} posicao y
 * @constructor
 */
function LayerMoveReference(x, y) {


    /**
     * Variavel que irá conter os Objetos desta camada estes se moverão acompanhando o moviemtno da layer
     * @member
     * @private
     */
    this._layerComponent = [];

    this.xRelativity = x || 1;

    this.yRelativity = y || 1;


}

LayerMoveReference.prototype.constructor = LayerMoveReference;
module.exports = LayerMoveReference;

Object.defineProperties(LayerMoveReference.prototype, {

    /**
     * retorna os compoentes pertencentes a esta layer
     * @name layerComponent
     * @type {Array}
     * @readonly
     */
    layerComponent: {
        get: function() {
            return this._layerComponent;
        }
    },


    /**
     * variable that moves the layer horizontaly
     * @name x
     * @type {Integer}
     * @memberof module:display/core.LayerMoveReference
     */
    x: {
        get: function() {
            return this._layerComponent;
        },
        set: function(amount) {
            for (var i = 0; i < this._layerComponent.length; i++) {
                this._layerComponent[i].x = this._layerComponent[i]._parallaxX + amount * (this.xRelativity - 1);
            }
        }
    },

    /**
     * variable that moves the layer verticaly
     * @name y
     * @type {Integer}
     * @memberof module:display/core.LayerMoveReference
     */
    y: {
        get: function() {
            return this._layerComponent;
        },
        set: function(amount) {
            for (var i = 0; i < this._layerComponent.length; i++) {
                this._layerComponent[i].y = this._layerComponent[i]._parallaxY + amount * (this.yRelativity - 1);
            }
        }
    }

});


/**
* Método que adiciona um componente nesta tela
* @param elemento {Object} Elemento a ser adicionado na layer
*/
LayerMoveReference.prototype.addElement = function(elemento) {

    this._layerComponent.push(elemento);

};
