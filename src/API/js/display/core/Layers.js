var PIXI = require('PIXI');

/**
 * @classdesc Classe que Representa as Layer da Tela
 * @memberof module:display/core
 * @extends EventEmitter
 * @exports Layers
 * @constructor
 */
function Layers() {
    PIXI.Container.call(this);

    /**
     * Variavel que irá conter todas as Layers
     * @member
     * @private
     */
    this._layers = {};

    this.particleElements = [];

    this._checkAndCreate('background');
    this._checkAndCreate('images');
    this._checkAndCreate('mouths');
    this._checkAndCreate('interactions');
    this._checkAndCreate('text');

    this._map = {
        background: [],
        images: [],
        mouths: [],
        interactions: [],
        text: []
    };
}

Layers.prototype = Object.create(PIXI.Container.prototype);
Layers.prototype.constructor = Layers;
module.exports = Layers;

/**
 * Método que gera e retorna uma Layer
 * @param name {String} Nome da Layer
 * @param index {Integer} Indice que a layer será adicionada
 * @return {Object} Layer criada
 * @private
 */
Layers.prototype._checkAndCreate = function(name, index) {
    if (this._layers[name] === undefined || this._layers[name] === null) {
        this._layers[name] = new PIXI.Container();
        var i = index || this.getListLayers().length - 1;
        this.addChildAt(this._layers[name], i);
    }

    return this._layers[name];
};

/**
 * criando um catalogo de um tipo
 * @param name {String} Nome da Layer
 * @param obj {Object} Indice que a layer será adicionada
 */
Layers.prototype.addMap = function(name, obj){
    if(this._map[name] === undefined){
        this._map[name] = [];
    }

    this._map[name].push(obj);
};

/**
 * obtendo catalogo
 * @param mapName {String} Nome da Layer
 * @return {Object} Layer criada
 */
Layers.prototype.getMap = function(mapName){
    if(this._map[mapName] === undefined){
        console.warn("Layer::getMap - não existe mapa com o nome " + mapName);
        return null;
    }

    return this._map[mapName];
};

/**
 * Método que lista todas as layers
 * @return names {Array} Array contendo todas as Layers
 */
Layers.prototype.getListLayers = function() {
    var names = [];
    for (var i in this._layers) {
        names.push(i);
    }

    return names;
};

/**
 * Método que retornar ou cria uma layer especifica caso nao exista
 * @param name {String} Nome da Layer
 * @return {object} layer(s)
 */
Layers.prototype.getLayer = function(name) {
    return this._checkAndCreate(name);
};

/**
 * Método para retornar objetos de um determinado tipo de uma layer
 * @param type {String} tipo do objeto
 * @return result {Array} layer(s)
 */
Layers.prototype.getObjects = function(type) {
    var result = [];
    var _default = ['images', 'text', 'mouths', 'interactions', 'background'];

    if (_default.indexOf(type) !== -1) {
        result = this.getLayer(type).children;
    } else {
        var children = this.getLayer('interactions').children;
        for (var i = 0; i < children.length; i++) {
            var ob = children[i];
            if (ob.displayType == type) result.push(ob);
        }
    }

    return result;
};

/**
 * Método para retornar um Children
 * @return result {Array} layer(s)
 */
Layers.prototype.getChildren = function() {
    var result = [];
    for (var i = 0; i < this.children.length; i++) {
        var arr = this.children[i].children;
        for(var j = 0; j < arr.length; j++){
            result.push(arr[j]);
        }
    }

    return result;
};

/**
 *   Metodo que percorre o mapa passado e pega o ID que e configurado pelo json
  * @param map {String}
  * @param id {number}
  * @return {Array}
*/
Layers.prototype.getMapByID = function(map, id) {

    var result = [];

    var _default = ['images', 'text', 'mouths', 'interactions', 'background'];

    if (_default.indexOf(map) !== -1) {

        result = this.getMap(map);

    } else {

        var children = this.getMap('interactions').children;

        for (var i = 0; i < children.length; i++) {

            var ob = children[i];
            if (ob.displayType == map) result.push(ob);

        }

    }
    for (var x = 0; x < result.length; x++) {

        if (result[x].index === id) {

            return result[x];

        }

    }

    return 0;

};
