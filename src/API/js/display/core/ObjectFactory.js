//importa os elementos core
var EventEmitter = require("EventEmitter");
var constantes = require('../../core/Const');
var behavior = require('../../behavior/index');

//importa os objetos da pasta display
var display = require('../index');

//ui

var Paint = require('../../ui/paint/Paint');
var Keyboard = require('../../ui/keyboard/Keyboard');
var Calculator = require('../../ui/calculator/Calculator');
var VideoPlayer = require('../../ui/videoplayer/VideoPlayer');
var AnalogController = require('../../ui/controller/AnalogController');
var DigitalController = require('../../ui/controller/DigitalController');
var DataBoard = require('../../ui/d3/DataBoard');

var PIXIConfig = require('../../core/PIXIConfig');

/**
 * @classdesc Classe que Cria os elementos da Tela
 * @memberof module:display/core
 * @extends EventEmitter
 * @exports ObjectFactory
 * @constructor
 */
function ObjectFactory() {
    EventEmitter.call(this);
}

ObjectFactory.prototype = Object.create(EventEmitter.prototype);
ObjectFactory.prototype.constructor = ObjectFactory;
module.exports = ObjectFactory;

Object.defineProperties(ObjectFactory.prototype, {


});


/**
 * Método que gera e retorna as layers contendo objetos
 * @static
 * @param sprites {Object} Json Contendo os objetos a serem criados
 * @param tmpCfg {Object}
 * @param telaPath {String} caminho da tela
 * @return {Array} Array All layers to be added to screen
 */
ObjectFactory.generateObjects = function(sprites, tmpCfg, telaPath) {
    var self = require('./ObjectFactory');

    var component = null;
    var txtContador = 0;

    var screenOffSet = 0;

    var controleParallax = -1;
    var indiceParallax = 0;
    var layers = {};

    var layerDisplay = new display.core.Layers();
    var layerDisplacement = [];

    layerDisplacement.push(new display.core.LayerMoveReference());

    this.contadorImg = 0;
    this.contadorTxt = 0;

    for (var i = 0; i < sprites.length; i++) {
		for (var element in sprites[i]) {
			if (element.indexOf("parallax") !== -1) {

                relativeValue = element.indexOf("parallax") + 8;

                if (controleParallax !== element.substr(relativeValue, 2)) {

                    var l = new display.core.LayerMoveReference();
                    l.xRelativity = 0.1 * element.substr(relativeValue, 2); // multiplica 0.1 pelo numero da layer pra obter a referencia de x
                    layerDisplacement.push(l);

                    controleParallax = element.substr(relativeValue, 2);

                }

                indiceParallax = layerDisplacement.length - 1;

                } else {
                indiceParallax = 0;

            }

            if (element.indexOf("screen") !== -1) {
                screenOffSet = sprites[i][element].config.x;
            }

            if (element.indexOf("txt") !== -1) {

                var inputCfg = null;
				if(tmpCfg.inputs !== undefined) {
					for(var iptContador = 0; iptContador < tmpCfg.inputs.length && inputCfg === null; iptContador++) {
						if(tmpCfg.inputs[iptContador].text == txtContador) {
							inputCfg = tmpCfg.inputs[iptContador];
						}
					}
				}

                if (inputCfg === null) {

                    component = new display.text.TextField(sprites[i][element].config.w, sprites[i][element].config.h);

                    component.index = this.contadorTxt;
                    this.contadorTxt++;

                    if (tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].style !== undefined) {
						component.style = tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].style;
                        component.setText(tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].text, tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].style);
                    } else {
                        component.setText(tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].text);
                    }

                    if (tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].align !== undefined && tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].align !== null) {
                        //alinhamento default
                        component.textAlign = tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].align;
                    } else {
                        //alinhamento default
                        component.textAlign = 'center';
                    }


                    //posiciona o elemento usando o sprites json

                    component.x = sprites[i][element].config.x - screenOffSet;
                    component.y = sprites[i][element].config.y + sprites[i][element].config.h / 2 - component.height / 2;
                    //corrige o alinhamento vertical do texto em relação à caixa de texto

                    if (element.indexOf('_t') !== -1) {
                        component.y = sprites[i][element].config.y;
                    }

                    if (element.indexOf('_b') !== -1) {
                        component.y = sprites[i][element].config.y + sprites[i][element].config.h - component.height;
                    }

                    component.x = Math.round(component.x);
                    component.y = Math.round(component.y);

                    component._parallaxX = component.x;
                    component._parallaxY = component.y;

                    layerDisplay.getLayer("text").addChild(component);
                    layerDisplay.addMap("text", component);
                    if (indiceParallax !== 0) layerDisplacement[indiceParallax].addElement(component); // se for diferente de 0 coloca em layer de deslocamento

                    txtContador++;
                } else {
					if((inputCfg.correct === null || inputCfg.correct === undefined) && (inputCfg.maxLength === null || inputCfg.maxLength === undefined) && (inputCfg.text === null || inputCfg.text === undefined)) {
						throw "Input sem conteudo";
					} else if((inputCfg.correct === null || inputCfg.correct === undefined) && (inputCfg.maxLength === null || inputCfg.maxLength === undefined)){
						throw "Input sem limitação de caracteres. Defina a propriedade \"correct\" ou \"maxLength\"";
					}

                    component = new display.text.Input(sprites[i][element].config.w, sprites[i][element].config.h);

                    component.index = this.contadorTxt;
                    this.contadorTxt++;

                    if (tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].style !== undefined) {
                        component.setText(tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].text, tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].style);
                    } else {
                        component.setText(tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].text);
                    }

                    if (tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].align !== undefined && tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].align !== null) {
                        //alinhamento default
                        component.textAlign = tmpCfg.text[constantes.APPConfig.LANGUAGE][txtContador].align;
                    } else {
                        //alinhamento default
                        component.textAlign = 'center';
                    }


                    //posiciona o elemento usando o sprites json

                    component.x = sprites[i][element].config.x - screenOffSet;


                    component.y = sprites[i][element].config.y + sprites[i][element].config.h / 2 - component.height / 2;
                    //corrige o alinhamento vertical do texto em relação à caixa de texto

                    if (element.indexOf('_t') !== -1) {
                        component.y = sprites[i][element].config.y;
                    }

                    if (element.indexOf('_b') !== -1) {
                        component.y = sprites[i][element].config.y + sprites[i][element].config.h - component.height;
                    }

                    component.x = Math.round(component.x);
                    component.y = Math.round(component.y);

                    component._parallaxX = component.x;
                    component._parallaxY = component.y;

					component.config = inputCfg;

                    layerDisplay.getLayer("interactions").addChild(component);
                    layerDisplay.addMap("interactions", component);
                    layerDisplay.addMap("inputs", component);
                    if (indiceParallax !== 0) layerDisplacement[indiceParallax].addElement(component); // se for diferente de 0 coloca em layer de deslocamento

                    txtContador++;
                }
            }

            if (element.indexOf("img") !== -1) {

                component = new display.Bitmap(sprites[i][element].texture);

                component.index = this.contadorImg;
                this.contadorImg++;

                component.x = sprites[i][element].config.x - screenOffSet;
                component.y = sprites[i][element].config.y;

                //corrige posição considerando pivot no centro
                component.x += sprites[i][element].config.w / 2;
                component.y += sprites[i][element].config.h / 2;

                component.x = Math.round(component.x);
                component.y = Math.round(component.y);

                component._parallaxX = component.x;
                component._parallaxY = component.y;


                component.align('center', 'center');

                layerDisplay.getLayer("images").addChild(component);
                layerDisplay.addMap("images", component);
                if (indiceParallax !== 0) layerDisplacement[indiceParallax].addElement(component); // se for diferente de 0 coloca em layer de deslocamento

            }

            if (element.indexOf("boca") !== -1) {

                component = null;

                if (element.indexOf("padrao") !== -1) component = new display.simple.Mouth("boca", 7);
                if (element.indexOf("anamaria") !== -1) component = new display.simple.Mouth("bocaAnaMaria", 3);
                if (element.indexOf("clovis") !== -1) component = new display.simple.Mouth("bocaClovis", 2);
                if (element.indexOf("edu") !== -1) component = new display.simple.Mouth("bocaEdu", 5);
                if (element.indexOf("jack") !== -1) component = new display.simple.Mouth("bocaJack", 2);
                if (element.indexOf("lila") !== -1) component = new display.simple.Mouth("bocaLila", 0);
                if (element.indexOf("lucia") !== -1) component = new display.simple.Mouth("bocaLucia", 0);
                if (element.indexOf("michel") !== -1) component = new display.simple.Mouth("bocaMichel", 0);
                if (element.indexOf("nico") !== -1) component = new display.simple.Mouth("bocaNico", 2);

                if (component === null) {
                    throw "boca do sprites.png está com a nomenclatura errada";
                }

                component.x = sprites[i][element].config.x - screenOffSet;
                component.y = sprites[i][element].config.y;


                //corrige posição considerando pivot no centro
                component.x += sprites[i][element].config.w / 2;
                component.y += sprites[i][element].config.h / 2;

                component.x = Math.round(component.x);
                component.y = Math.round(component.y);

                component._parallaxX = component.x;
                component._parallaxY = component.y;

                component.w = sprites[i][element].config.w;
                component.h = sprites[i][element].config.h;

                component.once('mouthCreated', function() {
                    this.width = this.w;
                    this.height = this.h;

                    this.rescaleMovieWidth = this.w;
                    this.rescaleMovieHeight = this.h;
                });

                layerDisplay.getLayer("mouths").addChild(component);
                layerDisplay.addMap("mouths", component);
                if (indiceParallax !== 0) layerDisplacement[indiceParallax].addElement(component); // se for diferente de 0 coloca em layer de deslocamento

            }

            if (element.indexOf("focinho") !== -1) {

                component = null;

                if (element.indexOf("abel") !== -1) component = new display.simple.Mouth('focinhoAbel', 10);

                if (element.indexOf("ananas") !== -1) {
                    if (element.indexOf("frente") !== -1) component = new display.simple.Mouth('focinhoAnanasFrente', 1);
                    if (element.indexOf("lado") !== -1) component = new display.simple.Mouth('focinhoAnanasLado', 4);
                    if (element.indexOf("34") !== -1) component = new display.simple.Mouth('focinhoAnanas34', 4);
                }


                if (component === null) {
                    throw "Focinho do sprites.png está com a nomenclatura errada";
                }

                component.x = sprites[i][element].config.x - screenOffSet;
                component.y = sprites[i][element].config.y;

                component.w = sprites[i][element].config.w;
                component.h = sprites[i][element].config.h;

                component.once('mouthCreated', function() {
                    this.width = this.w;
                    this.height = this.h;

                    this.rescaleMovieWidth = this.w;
                    this.rescaleMovieHeight = this.h;
                });


                //corrige posição considerando pivot no centro
                component.x += sprites[i][element].config.w / 2;
                component.y += sprites[i][element].config.h / 2;

                component.x = Math.round(component.x);
                component.y = Math.round(component.y);

                component._parallaxX = component.x;
                component._parallaxY = component.y;

                layerDisplay.getLayer("mouths").addChild(component);
                layerDisplay.addMap("mouths", component);
                if (indiceParallax !== 0) layerDisplacement[indiceParallax].addElement(component); // se for diferente de 0 coloca em layer de deslocamento
            }

			if(element.indexOf('animacoes') !== -1) {
				var arrAnimacoes = sprites[i][element];
				for(var r = 0; r < arrAnimacoes.length; r++) {
					var animacaoResources = arrAnimacoes[r];

					var skeletonAnimation = new display.complex.SkeletonAnimation();
					skeletonAnimation.load(animacaoResources);


					var imgBase = layerDisplay.getMapByID('images', animacaoResources.config.imageBase);
					skeletonAnimation.x = imgBase.x;
					skeletonAnimation.y = imgBase.y;

					imgBase.visible = false;
					imgBase.alpha = 0;

					layerDisplay.getLayer('animations').addChild(skeletonAnimation);
				}
			}
        }

    }


    for (component in tmpCfg) {
        if (component == "button") {
            for (var j = 0; j < tmpCfg[component].length; j++) {

                var button = new display.buttons.Button();
                button.index = j;

                self.composeElement(button, layerDisplay, tmpCfg[component][j]);

                layerDisplay.getLayer("interactions").addChild(button);
                layerDisplay.addMap("interactions", button);
                layerDisplay.addMap("button", button);

                indiceParallax = tmpCfg[component][j].layerDisplacement;
                if (indiceParallax !== undefined && indiceParallax !== 0 && indiceParallax < layerDisplacement.length) layerDisplacement[indiceParallax].addElement(button); // se for diferente de 0 coloca em layer de deslocamento
                else layerDisplacement[0].addElement(button);
            }
        }

        if (component == "drags") {

            for (var k = 0; k < tmpCfg[component].length; k++) {

                if ((tmpCfg[component][k].img === undefined || tmpCfg[component][k].img.length === 0) &&
                    (tmpCfg[component][k].text === undefined || tmpCfg[component][k].text.length === 0)) throw "drag sem conteudo";

                var drag = new display.simple.Drag();

                if (tmpCfg[component][k].color === undefined) drag.color = tmpCfg[component][k].color;

                self.composeElement(drag, layerDisplay, tmpCfg[component][k]);

                layerDisplay.getLayer("interactions").addChild(drag);
                layerDisplay.addMap("interactions", drag);
                layerDisplay.addMap("drags", drag);

                //variaveis que irão armazenar a posição inicial dos drags
                drag._initialX = drag._parallaxX;
                drag._initialY = drag._parallaxY;

                indiceParallax = tmpCfg[component].layerDisplacement;
                if (indiceParallax !== undefined && indiceParallax !== 0 && indiceParallax < layerDisplacement.length) layerDisplacement[indiceParallax].addElement(drag); // se for diferente de 0 coloca em layer de deslocamento
                else layerDisplacement[0].addElement(drag);

                if (tmpCfg[component][k].obstacles !== undefined) {
                    if (tmpCfg[component][k].obstacles.img === undefined) throw "obstacles sem conteudo";

                    for (var o = 0; o < tmpCfg[component][k].obstacles.img.length; o++) {
                        for (var z = 0; z < layerDisplay.getLayer("images").children.length; z++) {
                            if (layerDisplay.getLayer("images").children[z].index == tmpCfg[component][k].obstacles.img[o]) {

                                c = layerDisplay.getLayer("images").children[z];
                                if (!tmpCfg[component][k].obstacles.show) {
                                    c = layerDisplay.getLayer("images").removeChildAt(z);
                                }
                                c.align('left', 'top');
                                c.x -= Math.round(c.width / 2);
                                c.y -= Math.round(c.height / 2);

                                drag._obstacles.push(c);
                            }
                        }
                    }

                }

            }

        }

        if (component == "areaDrags") {


            for (var m = 0; m < tmpCfg[component].length; m++) {

                if ((tmpCfg[component][m].img === undefined || tmpCfg[component][m].img.length === 0) &&
                    (tmpCfg[component][m].text === undefined || tmpCfg[component][m].text.length === 0)) throw "drag sem conteudo";

                var areaDrag = new display.simple.AreaDrag();

                self.composeElement(areaDrag, layerDisplay, tmpCfg[component][m]);

                layerDisplay.getLayer("interactions").addChild(areaDrag);
                layerDisplay.addMap("interactions", areaDrag);
                layerDisplay.addMap("areaDrags", areaDrag);

                areaDrag.alpha = tmpCfg[component][m].alpha;

                indiceParallax = tmpCfg[component].layerDisplacement;
                if (indiceParallax !== undefined && indiceParallax !== 0 && indiceParallax < layerDisplacement.length) layerDisplacement[indiceParallax].addElement(areaDrag); // se for diferente de 0 coloca em layer de deslocamento
                else layerDisplacement[0].addElement(areaDrag);

            }
        }

        if (component == "movieClips") {

            for (var n = 0; n < tmpCfg[component].length; n++) {

                if (tmpCfg[component][n].files === undefined || tmpCfg[component][n].files.length === 0) throw "movieClip sem conteudo";

                var movieClip = new display.simple.MovieClip(telaPath + tmpCfg[component][n].path, tmpCfg[component][n].files, tmpCfg[component][n].animationSpeed);
				movieClip.index = n;

                movieClip.x = tmpCfg[component][n].x;
                movieClip.y = tmpCfg[component][n].y;

                movieClip.w = tmpCfg[component][n].w;
                movieClip.h = tmpCfg[component][n].h;



                movieClip.once('MovieClipCreated', function() {
                    this.width = this.w;
                    this.height = this.h;

                    this.rescaleMovieWidth = this.w;
                    this.rescaleMovieHeight = this.h;

                });

                layerDisplay.getLayer("interactions").addChild(movieClip);
                layerDisplay.addMap("interactions", movieClip);
                layerDisplay.addMap("movieClips", movieClip);
                //if (tmpCfg[component][n].indiceParallax !== undefined && tmpCfg[component][n].indiceParallax !== 0 && tmpCfg[component][n].indiceParallax < layerDisplacement.length) layerDisplacement[tmpCfg[component][n].indiceParallax].addElement(movieClip); // se for diferente de 0 coloca em layer de deslocamento
                //else layerDisplacement[0].addElement(movieClip);
            }
        }

        // if (component == "tint") {
        //
        //     for (var r = 0; r < tmpCfg[component].length; r++) {
        //
        //         if (tmpCfg[component][r].img === undefined && tmpCfg[component][r].text === undefined) throw "drag sem conteudo";
        //
        //         var texture = null;
        //
        //         if (tmpCfg[component][r].img !== undefined) {
        //
        //             for (var h = 0; h < layerDisplay.getLayer("images").children.length; h++) {
        //                 if (layerDisplay.getLayer("images").children[h].index == tmpCfg[component][r].img) {
        //                     texture = PIXIConfig.renderer.generateTexture(layerDisplay.getLayer("images").children[h]);
        //                     c.align('left', 'top');
        //                     c.x -= c.width / 2;
        //                     c.y -= c.height / 2;
        //                 }
        //             }
        //
        //         } else {
        //             PIXIConfig.renderer.generateTexture();
        //         }
        //
        //
        //
        //         //var tint = new display.simple.Tint();
        //
        //
        //         layerDisplay.getLayer("interactions").addChild(tint);
        //
        //         indiceParallax = tmpCfg[component].layerDisplacement;
        //         if (indiceParallax !== undefined && indiceParallax !== 0 && indiceParallax < layerDisplacement.length) layerDisplacement[indiceParallax].addElement(tint); // se for diferente de 0 coloca em layer de deslocamento
        //         else layerDisplacement[0].addElement(tint);
        //
        //     }
        // }

        if(component == "zoom"){
            for (var c = 0; c < tmpCfg[component].length; c++) {
                if(tmpCfg[component][c].type == "zoomIn"){
                    var imgOriginal = layerDisplay.getMapByID("images",tmpCfg[component][c].imgOriginal);
                    var imgZoom = layerDisplay.getMapByID("images",tmpCfg[component][c].imgZoom);

                    imgOriginal.addPlugin(new behavior.Zoom(imgZoom));

                    imgOriginal.zoomIn(tmpCfg[component][c]);
                } else if (tmpCfg[component][c].type == "zoomOut"){

                    var imgOrigin = layerDisplay.getMapByID("images",tmpCfg[component][c].imgOrigin);

                    imgOrigin.addPlugin(new behavior.Zoom());

                    imgOrigin.zoomOut(tmpCfg[component][c]);
                }
            }
        }


        if (component == "particles") {

            for (var x = 0; x < tmpCfg[component].length; x++) {

                if (tmpCfg[component][x].layerObject === undefined) throw "definir layerObject para pegar o objeto";
                if (tmpCfg[component][x].numberObject === undefined) throw "definir numberObject para pegar o objeto";

                var objectArray = layerDisplay.getObjects(tmpCfg[component][x].layerObject);

                var object = null;

                for (var t = 0; t < objectArray.length; t++) {

                    if (objectArray[t].index == tmpCfg[component][x].numberObject) {
                        object = objectArray[t];
                    }

                }

                if (object === null)
                    throw "particle object não encontrado";

                var ___img = layerDisplay.getMapByID("images", tmpCfg[component][x].numberObject);
                ___img.y = -___img.height;
                object.addPlugin(new behavior.Particles(object, tmpCfg[component][x].config, ___img.sprite.texture.clone()));

                layerDisplay.particleElements.push(object);

            }
        }

		if (component == "compassess") {
			for (var y = 0; y < tmpCfg[component].length; y++) {
				if ((tmpCfg[component][y].img === undefined || tmpCfg[component][y].img.length === 0) && (tmpCfg[component][y].text === undefined || tmpCfg[component][y].text.length === 0)) throw "compass sem conteudo";

				var compass = new display.simple.Compass();

                self.composeElement(compass, layerDisplay, tmpCfg[component][y]);

				compass.initObjectsToRotate();

                layerDisplay.getLayer("interactions").addChild(compass);
                layerDisplay.addMap("interactions", compass);
                layerDisplay.addMap("compasses", compass);
			}
		}

    if (component == "ballons") {
      for (var b = 0; b < tmpCfg[component].length; b++) {
        if (tmpCfg[component][b].img === undefined && tmpCfg[component][b].text === undefined && tmpCfg[component][b].correct === undefined){
          throw "ballon sem conteudo";
        }
        var ballon = new display.simple.Ballon();
        self.composeElement(ballon, layerDisplay, tmpCfg[component][b]);
        layerDisplay.getLayer("interactions").addChild(ballon);
        layerDisplay.addMap("interactions", ballon);
        layerDisplay.addMap("ballons", ballon);
      }
    }

    if (component == "clocks") {
			for (var clockCount = 0; clockCount < tmpCfg[component].length; clockCount++) {
                //O clock possui pelo menos um corpo e dois ponteirros ?
				if ((tmpCfg[component][clockCount].img === undefined || (tmpCfg[component][clockCount].pointerH === undefined || tmpCfg[component][clockCount].pointerM === undefined)))
                    throw "clock sem conteudo";

                    var countImages = layerDisplay.getLayer("images").children.length;

                    //itera sobre childs das images para pegar as usadas no clock
                    for (var imgClock = 0; imgClock < countImages; imgClock++) {

                        if (tmpCfg[component][clockCount].img === layerDisplay.getLayer("images").getChildAt(imgClock).index) {
                            tmpCfg[component][clockCount].img = layerDisplay.getLayer("images").getChildAt(imgClock);

                        } else if (tmpCfg[component][clockCount].pointerH === layerDisplay.getLayer("images").getChildAt(imgClock).index) {
                            tmpCfg[component][clockCount].pointerH = layerDisplay.getLayer("images").getChildAt(imgClock);

                        } else if (tmpCfg[component][clockCount].pointerM === layerDisplay.getLayer("images").getChildAt(imgClock).index) {
                            tmpCfg[component][clockCount].pointerM = layerDisplay.getLayer("images").getChildAt(imgClock);
                        }

                    }
				var clock = new display.complex.Clock(tmpCfg[component][clockCount]);

                self.composeElement(clock, layerDisplay, tmpCfg[component][clockCount]);

                layerDisplay.getLayer("interactions").addChild(clock);
                layerDisplay.addMap("interactions", clock);
                layerDisplay.addMap("clocks", clock);
			}
		}

        if (component == "seteErros") {
			for (var errosCount = 0; errosCount < tmpCfg[component].length; errosCount++) {
                //O SeteErros tem uma img e a array de erros nao esta vazia ?
				if ((!!tmpCfg[component][errosCount].img) || (!!tmpCfg[component][errosCount].erros)){
                    var countImgErros = layerDisplay.getLayer("images").children.length;
                    var countExErros = tmpCfg[component][errosCount].erros.length;

                    //itera sobre childs das images para pegar as usadas no clock
                    for (var imgErros = 0; imgErros < countExErros; imgErros++) {

                            tmpCfg[component][errosCount].erros[imgErros] = layerDisplay.getMapByID("images", tmpCfg[component][errosCount].erros[imgErros]);

                    }

                    tmpCfg[component][errosCount].image = layerDisplay.getMapByID("images", tmpCfg[component][errosCount].image);

    				var seteErros = new display.simple.SeteErros(tmpCfg[component][errosCount]);

                    self.composeElement(seteErros, layerDisplay, tmpCfg[component][errosCount]);

                    layerDisplay.getLayer("interactions").addChild(seteErros);
                    layerDisplay.addMap("interactions", seteErros);
                    layerDisplay.addMap("seteErros", seteErros);

                } else {
                throw "seteErros sem conteudo"; }
			}
		}

        if (component == "cards")
        {
            for (var c = 0; c < tmpCfg[component].cardsDefinition.length; c++)
            {
				if ((tmpCfg[component].cardsDefinition[c].img === undefined || tmpCfg[component].cardsDefinition[c].img.length === 0) && (tmpCfg[component].cardsDefinition[c].text === undefined || tmpCfg[component].cardsDefinition[c].text.length === 0)) throw "card sem conteudo";

				var card = new display.simple.Card();
                self.composeElement(card, layerDisplay, tmpCfg[component].cardsDefinition[c]);

                layerDisplay.getLayer("interactions").addChild(card);
			}
        }

		if(component == "viewStates") {
			for (var s = 0; s < tmpCfg[component].length; s++) {
				var viewState = tmpCfg[component][s];
				var objMap = layerDisplay.getMap(viewState.mapName);
				if(objMap !== null) {
					for(var u = 0; u < viewState.mapObjects.length; u++) {
						var mapObject = objMap.find(function(mapObj) {
							return mapObj.index === viewState.mapObjects[u].index;
						}, this);

						if(mapObject !== undefined) {
							mapObject.addPlugin(new behavior.ViewState(viewState.mapObjects[u].states, layerDisplay));
							// mapObject.loadStates();
							layerDisplay.addMap('statefullView', mapObject);
						}
					}
				}
			}
		}
    }

    layers = {
        layersDisplay: layerDisplay,
        layerDisplacement: layerDisplacement
    };

    return layers;

};


/**
 * Método que gera e retorna o player
 * @static
 * @param textures {Array} Array Contendo as textureas do player
 * @return {Object} Keyboard
 */
ObjectFactory.generateVideoPlayer = function(textures) {

    var player = new VideoPlayer(textures);
    return player;

};

/**
 * Método que gera e retorna o Paint
 * @static
 * @param textures {Array} Array Contendo as texturas do Paint
 * @return {Object} Paint
 */
ObjectFactory.generatePaint = function(textures) {

    var paint = new Paint(textures);
    return paint;

};

ObjectFactory.generateDataBoard = function(){
    var dataBoard = new DataBoard();
    return dataBoard;
};

/**
 * Método que gera e retorna o balao
 * @static
 * @return ballon {Object} balao
 */
ObjectFactory.generateBallon = function() {

    var ballon = new display.simple.MovieClip('telas/commom/MovieClips/ballon/',['ballon']);
    return ballon;

};

/**
 * Método que gera e retorna o teclado
 * @static
 * @param textures {Array} Array Contendo as texturas do teclado
 * @return Keyboard {Object} teclado
 */
ObjectFactory.generateKeyboard = function(textures) {

    var keyboard = new Keyboard(textures);
    return keyboard;

};

/**
 * Método que gera e retorna o controle analogico
 * @static
 * @param textures {Array} Array Contendo as texturas do controle
 * @return analogController {Object} controle analogico
 */
ObjectFactory.generateAnalogController = function(textures) {

    var analogController = new AnalogController(textures);

    return analogController;

};

/**
 * Método que gera e retorna o controle digital
 * @static
 * @param textures {Array} Array Contendo as texturas do controle
 * @return digitalController {Object} controle digital
 */
ObjectFactory.generateDigitalController = function(textures) {

    var digitalController = new DigitalController(textures);

    return digitalController;

};

/**
 * Método que gera e retorna calculadora
 * @static
 * @param textures {Array} Array Contendo as texturas da calculadora
 * @return generateCalculator {Object} calculadora
 */
ObjectFactory.generateCalculator = function(textures) {

    var calculator = new Calculator(textures);
    return calculator;

};

/**
 * Método que constroe elementos compostos a partir de elementos simples
 * @static
 * @param component {Object} Objeto que irá ser composto por elementos basicos
 * @param layerDisplay {module:display/core.Layers} Camada de exibição que será utilizada para coletar os elementos base e inserir o novo
 * @param config {Json} Array Informações do elemento a ser composto
 */
ObjectFactory.composeElement = function(component, layerDisplay, config) {

    var minX = null;
    var minY = null;

    component.config = config;

    // if (config.defineArea !== undefined && config.defineArea !== null) {
    //     component.width = config.defineArea;
    //     component.height = config.defineArea;
    // }

    if (config.img !== undefined) {
        for (i = 0; i < config.img.length; i++) {
            var c = null;
            for (var z = 0; z < layerDisplay.getLayer("images").children.length; z++) {
                if (layerDisplay.getLayer("images").children[z].index == config.img[i]) {
                    c = layerDisplay.getLayer("images").removeChildAt(z);
                    c.align('left', 'top');
                    c.x -= Math.round(c.width / 2);
                    c.y -= Math.round(c.height / 2);
                }
            }

            if (c === null) {
                console.log("Ei estou aqui....");
                throw "indice de componente duplicado ou inexistente: " + config.img;
            }

            if (minX === null || minX > c.x) minX = c.x;
            if (minY === null || minY > c.y) minY = c.y;

            component.addChild(c);
            //console.log(component);
            this.contadorImg++;
        }
    }

    if (config.text !== undefined) {

        for (i = 0; i < config.text.length; i++) {

            var ct = null;
            for (var x = 0; x < layerDisplay.getLayer("text").children.length; x++) {
                if (layerDisplay.getLayer("text").children[x].index == config.text[i]) {
                    ct = layerDisplay.getLayer("text").removeChildAt(x);
                }
            }

            if (ct === null) {
                throw "indice de componente duplicado ou inexistente: " + config.text;
            }

            if (minX === null || minX > ct.x) minX = ct.x;
            if (minY === null || minY > ct.y) minY = ct.y;

            component.addChild(ct);

            this.contadorTxt++;

        }
    }

    component.x = Math.round(minX);
    component.y = Math.round(minY);

    for (i = 0; i < component.children.length; i++) {
        component.children[i].x = component.children[i].x - component.x;
        component.children[i].y = component.children[i].y - component.y;
        component.children[i]._parallaxX = component.children[i].x;
        component.children[i]._parallaxY = component.children[i].y;
    }


    component.pivot.set(Math.round(component.width / 2), Math.round(component.height / 2));
    component.x += component.width / 2;
    component.y += component.height / 2;

	component.x = Math.round(component.x);
    component.y = Math.round(component.y);


    component._parallaxX = component.x;
    component._parallaxY = component.y;

    if (config.sensor) {
        component.addPlugin(new behavior.Sensor(config.sensor.x, config.sensor.y, config.sensor.width, config.sensor.height));
        if (config.sensor.show) {
            component.drawSensor();
        }
    }

    if (config.debug === true) {
        var graphics = new PIXI.Graphics();

        graphics.beginFill("0x00ff00", 0.5);
        graphics.drawRect(0, 0, component.width, component.height);
        graphics.endFill();
        graphics.x = 0;
        graphics.y = 0;
        component.addChild(graphics);

    }
};
