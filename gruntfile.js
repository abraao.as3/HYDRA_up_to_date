module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-chokidar');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-rename');
    grunt.loadNpmTasks('grunt-ffmpeg');
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-json-minification');
    grunt.loadNpmTasks('grunt-tinyimg');
    grunt.loadNpmTasks('grunt-contrib-requirejs');
    grunt.loadNpmTasks('grunt-contrib-compass');

    var path = 'bin/curso/';
    var lib = '../lib/';
    var packageName = 'testando';

    var config = function() {
        this.clean = {
            folders: ['bin/*'],
            temp: ['temp'],
            roteiroTemplate: ['src/roteiroTemplate/bin'],
            roteiro: ['roteiro']
        };

        this.requirejs = {
            compile: {
                options: {
                    name: lib + 'almond',
                    baseUrl: "src/",
                    include: ['API/js/Config'],
                    insertRequire: ["API/js/Config"],
                    out: path + "js/script.js",
                    cjsTranslate: true,
                    useStrict: true,
                    paths: {

                        Font: lib + 'Font',
                        jquery: lib + 'jquery/jquery-3.1.0.min',

                        EventEmitter: lib + 'eventemitter3',
                        PIXI: lib + 'pixi/pixi.min',
                        TweenMax: lib + 'TweenMax.min',

                        Particles: lib + 'pixi/pixi-particles',

                        D3: lib + 'd3.min',

                        Sketch: lib + 'sketch.min',

                        Wordfind: lib + 'wordfind',

                        scormApi: lib + 'SCORM_API_wrapper',

                        resemble: lib + 'resemble',

                        Scream: lib + 'Scream',

						DragonBones: lib + 'dragonbones/dragonBones.5.0',
						DragonBonesPixi: lib + 'dragonbones/dragonBonesPixi.5.0'
                    },
					deps: [lib + 'type'],
                    optimize: 'none'
                }
            }
        };

        this.tinyimg = {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/API/',
                    src: ['**/*.{png,jpg,svg}'],
                    dest: path
                }]
            },

            dynamic2: {
                files: [{
                    expand: true,
                    cwd: 'src/telas/',
                    src: ['**/*.{png,jpg,svg}'],
                    dest: path + 'telas/'
                }]
            },

            roteiro: {
                files: [{
                    expand: true,
                    cwd: 'src/roteiroTemplate/src',
                    src: ['**/*.{png,jpg,svg}'],
                    dest: 'src/roteiroTemplate/bin/'
                }]
            }
        };

        this.json_minification = {
            dev: {
                files: [{
                    expand: true,
                    cwd: 'src/telas',
                    src: ['**/*.json'],
                    dest: path + 'telas/',
                    ext: '.json'
                }]
            },

            separate: {
                files: [{
                    expand: true,
                    cwd: path + 'telas/',
                    src: ['**/*.json'],
                    dest: path + 'telas/',
                    ext: '.json'
                }]
            },

            outhers: {
                files: [{
                    expand: true,
                    cwd: 'src/telas/',
                    src: ['**/*.json', '!**/config.json'],
                    dest: path + 'telas/',
                    ext: '.json'
                }]
            },

            api: {
                files: [{
                    expand: true,
                    cwd: 'src/API/',
                    src: ['**/*.json'],
                    dest: path,
                    ext: '.json'
                }]
            },
        };

        this.ffmpeg = {
            audio: {
                options: {
                    audioBitrate: '64k',
                },
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['*.mp3', '**/*.mp3', '!API/**/*.mp3'],
                    dest: path,
                    ext: '.mp3'
                },{
                    expand: true,
                    cwd: 'src/API',
                    src: ['*.mp3', '**/*.mp3'],
                    dest: path,
                    ext: '.mp3'
                }]
            },

            video: {
                options: {
                    videoBitrate: '700k',
                    audioBitrate: '64k',
                    size: '974x524'
                },
                files: [{
                    expand: true,
                    cwd: 'src/telas/',
                    src: ['**/*.mp4'],
                    dest: path + 'telas/',
                    ext: '.mp4'
                }]
            }
        };

        this.compress = {
            main: {
                options: {
                    archive: 'temp/' + packageName + '.zip'
                },
                files: [{
                    expand: true,
                    cwd: path.replace('curso/', ''),
                    src: ['**/*'],
                    filter: 'isFile'
                }]
            }
        };

        this.copy = {
            weclass: {
                files: [{
                    expand: true,
                    flatten: true,
                    src: 'temp/*',
                    dest: 'bin/',
                    filter: 'isFile'
                }]
            },

            scorm: {
                expand: true,
                cwd: 'src/scorm1.2/',
                src: ['**'],
                dest: path.replace('curso/', '')
            },

            fonts: {
                expand: true,
                cwd: 'src/fonts/',
                src: ['**'],
                dest: path + 'fonts/'
            },

            roteiroTemplate: {
                expand: true,
                cwd: 'src/roteiroTemplate/src/html/',
                src: ['*.html'],
                dest: 'src/roteiroTemplate/bin/',
                isFile: true
            },

            fontsRoteiro: {
                expand: true,
                cwd: 'src/fonts/',
                src: ['**'],
                dest: 'src/roteiroTemplate/bin/fonts/'
            },

            roteiro: {
                expand: true,
                cwd: 'src/roteiroTemplate/bin/',
                src: ['**'],
                dest: './roteiro/'
            }
        };

        this.compass = {
            dev: {
                options:{
                    config: 'config.rb'
                }
            }
        };

        this.chokidar = {
            requirejs: {
                files: ['src/**/*.js', 'lib/Scream.js'],
                tasks: ['requirejs']
            },

            json: {
                files: ['src/telas/**/*.json'],
                tasks: ['json_minification']
            },

            imagens: {
                files: ['src/telas/**/*.png', 'src/telas/**/*.jpg'],
                tasks: ['tinyimg:dynamic2']
            },

            audio: {
                files: ['src/telas/**/*.mp3'],
                tasks: ['ffmpeg']
            },

            roteiro: {
                files: ['src/roteiroTemplate/**'],
                tasks: ['compileRoteiro']
            }
        };

        this.jsdoc = {
            dist : {
                src: ['src/**/*.js', 'README.md'],
                options: {
                    destination : 'documentation',
                       template : "node_modules/docdash",
                      configure : "jsdoc.conf.json"
                }
            }
        };
    };

    grunt.initConfig(new config());

    grunt.registerTask('default', ['chokidar']);

    grunt.registerTask('initialize', [
        'clean:folders',
        'copy:fonts',
        'index',
        'requirejs',
        'json_minification:dev',
        'json_minification:api',
        'tinyimg:dynamic',
        'tinyimg:dynamic2',
        'ffmpeg:audio',
        'ffmpeg:video',
        // 'manifest',
        'jsdoc'
    ]);

    var weclass = ['compress', 'clean:folders', 'copy:weclass', 'clean:temp'];
    var portugues = [
        'debug',
        'clean:folders',
        'port',
        'copy:scorm',
        'copy:fonts',
        'index',
        'requirejs',
        'json_minification:api',
        'json_minification:separate',
        'json_minification:outhers',
        'tinyimg:dynamic',
        'tinyimg:dynamic2',
        'ffmpeg:audio',
        'ffmpeg:video',
        'manifest',
        'compress'
    ];

    grunt.registerTask('portuguesW', portugues.concat(weclass));
    grunt.registerTask('portugues', portugues);
    //grunt.registerTask('portugues', portugues);

    function turnOffDebug() {
        var p = 'src/telas/appConfig.json';

        var json = grunt.file.read(grunt.file.expand(p));
        var pattern = /\s*\"debug\"\s*:\s*true/g;
        var result = json.replace(pattern, '\"debug\": false');

        grunt.file.write(p, result);
    }

    function isolateLanguage(lang) {
        var dirs = grunt.file.expand('src/telas/**/');
        dirs.shift();
        for (var i = 0; i < dirs.length; i++) {
            var json = grunt.file.expand(dirs[i] + 'config.json');
            grunt.log.writeln(json);
            if (json.length !== 0) {
                var conf = grunt.file.readJSON(json);
                if (config.textBox !== undefined) {
                    for (var j in conf.textBox) {
                        if (j != lang) {
                            delete conf.textBox[j];
                        }
                    }
                }
                grunt.log.writeln(path + json.toString().replace('src/', ''));
                grunt.file.write(path + json.toString().replace('src/', ''), JSON.stringify(conf, null, 4));
            }
        }

        grunt.config.merge({
            ffmpeg: {
                video: {
                    files: [{
                        cwd: 'src/telas/video/' + lang.replace('-', '') + '/',
                        dest: path + 'telas/video/' + lang.replace('-', '') + '/'
                    }]
                }
            }
        });
    }

    grunt.registerTask('debug', 'desliga o debug', function() {
        turnOffDebug();
    });

    grunt.registerTask('port', 'separa portugues', function() {
        isolateLanguage('pt-BR');
    });

    grunt.registerTask('eng', 'separa portugues', function() {
        isolateLanguage('en-US');
    });

    grunt.registerTask('index', 'cria o index', function() {
        var index = '';
        index += '<!DOCTYPE html>';
        index += '<html lang="en">';
        index += '<head>';
        index += '<meta charset="UTF-8">';
        index += '<meta name="viewport" content="width=device-width, initial-scale=1.0">';
        index += '<meta http-equiv="X-UA-Compatible" content="ie=edge">';
        index += '<title>Document</title>';

		index += '<script src="js/script.js" charset="utf-8"></script>';
        index += '<title>Profuturo</title>';
        index += '</head><body></body></html>';

        grunt.file.write(path + 'index.html', index);
    });

    grunt.registerTask('manifest', 'Cria o manifesto', function() {
        var config = grunt.file.readJSON(grunt.file.expand('src/telas/manifest.json'));

        var xml = "";
        xml += '<?xml version="1.0" encoding="UTF-8"?>';
        xml += '<manifest xmlns="http://www.imsproject.org/xsd/imscp_rootv1p1p2" xmlns:imsmd="http://www.imsglobal.org/xsd/imsmd_rootv1p2p1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:adlcp="http://www.adlnet.org/xsd/adlcp_rootv1p2" identifier="MANIFEST01" xsi:schemaLocation="http://www.imsproject.org/xsd/imscp_rootv1p1p2 imscp_rootv1p1p2.xsd http://www.imsglobal.org/xsd/imsmd_rootv1p2p1 imsmd_rootv1p2p1.xsd http://www.adlnet.org/xsd/adlcp_rootv1p2 adlcp_rootv1p2.xsd">';
        xml += '<metadata>';
        xml += '<schema>ADL SCORM</schema>';
        xml += '<schemaversion>1.2</schemaversion>';
        xml += '<adlcp:location>imsmetadata.xml</adlcp:location>';
        xml += '</metadata>';
        xml += '<organizations default="AE_01">';
        xml += '<organization identifier="AE_01">';

        xml += '<title>' + config.title + '</title>';

        var itens = "";
        var resources = "";

        Object.keys(config.division).forEach(function(e, i) {
            var count = i < 10 ? '0' + i : i;

            itens += '<item identifier="ITEM_' + count + '" identifierref="RES_' + count + '" isvisible="true">';
            itens += '<title>' + e + '</title>';
            itens += '</item>';

            resources += '<resource identifier="RES_' + count + '" type="webcontent" adlcp:scormtype="sco" href="curso/index.html?select_module=' + config.division[e] + '" />';
        });

        xml += itens;
        xml += '</organization>';
        xml += '</organizations>';
        xml += '<resources>';
        xml += resources;
        xml += '</resources>';
        xml += '</manifest>';

        grunt.file.write(path.replace('curso/', '') + 'imsmanifest.xml', xml);
    });

    grunt.registerTask('compileRoteiro', ['compass', 'copy:roteiroTemplate', 'tinyimg:roteiro']);

    grunt.registerTask('createRoteiro', function(){
        var language = grunt.option('lang');
        var validLanguages = ['pt-BR', 'en-US', 'es-ES', 'fr-FR'];
        console.log('linguagem selecionada: ' + language);
        if(validLanguages.indexOf(language) == -1){
            grunt.log.error("por favor digite um valor para language: ex: --lang=pt-BR\nvalores validos: " + validLanguages);
        }

        var _manifest = grunt.file.readJSON('src/telas/manifest.json');
        var configsURL = grunt.file.expand('src/telas/**/config.json');

        var htmlSrt = grunt.file.read('src/roteiroTemplate/bin/index.html');

        var jsdom = require('jsdom').jsdom;
        var serialize = require('jsdom').serializeDocument;
        var w = jsdom(htmlSrt).defaultView;

        var $ = require('jQuery')(w);

        var title = $('body').find('#roteiro #cabecalho .cabecalho-title h3');
        title.text('Unidade ' + _manifest.unidade + ' ' + _manifest.title);

        var telaTitleTemplate = $('body').find('#roteiro .tela .tela-title').clone();
        var articleNameTemplate = $('body').find('#roteiro .tela .tela-article .article-name').clone();
        var articleTextTemplate = $('body').find('#roteiro .tela .tela-article .article-text').clone();
        var articleFootTemplate = $('body').find('#roteiro .tela .tela-article .article-foot').clone();

        $('body').find('#roteiro .tela').remove();
        var currentNamePattern = /(src\/telas\/)|(\/config\.json)/ig;

        for(var i = 0; i < configsURL.length; i++){
        // for(var i = 0; i < 3; i++){
            var config = grunt.file.readJSON(configsURL[i]);
            var tela = $('<div/>',{class: 'tela'});
            var _title = $($.parseHTML(telaTitleTemplate.prop('outerHTML')));
            _title.find('p').text(configsURL[i].replace(currentNamePattern, ''));

            tela.append(_title);
            if(config.template.config.text === undefined) continue;

            if(config.template.config.text[language] === undefined) continue;

            for(var j = 0; j < config.template.config.text[language].length; j++){

                var article = $('<div/>', {class: 'tela-article'});

                var obj = config.template.config.text[language][j];

                if(obj.audioID !== undefined){
                    var person = obj.person ? obj.person : 'O personagem não foi definido';
                    var _name = $($.parseHTML(articleNameTemplate.prop('outerHTML')));
                    _name.find('p').text(person);
                    article.append(_name);

                    for(var k = 0; k < config.template.config.audio[language].length; k++){
                        var audioEL = config.template.config.audio[language][k];
                        if(obj.audioID == audioEL.id){
                            var _audio = $($.parseHTML(articleFootTemplate.prop('outerHTML')));
                            _audio.find('.foot-audio').text(audioEL.name + '.mp3');
                            article.append(_audio);
                            break;
                        }
                    }
                }

                var txt = obj.text ? obj.text : obj.observation;
                if(txt === undefined) continue;

                var _text = $($.parseHTML(articleTextTemplate.prop('outerHTML')));
                _text.find('p').text(txt.replace(/<\w+>|<\/\w+>/ig, ''));
                article.append(_text);
                article.find('.article-name').after(_text);
                if(obj.audioID === undefined){
                    _text.addClass('no-audio');
                }
                if(obj.observation){
                    article.addClass('observation');
                }

                tela.append(article);
            }

            for(var l = 0; l < config.template.config.audio[language].length; l++){
                var audioObj = config.template.config.audio[language][l];
                if(audioObj.person !== undefined){
                    var audioArticle = $('<div/>', {class: 'tela-article'});
                    tela.append(audioArticle);

                    var __name = $($.parseHTML(articleNameTemplate.prop('outerHTML')));
                    __name.find('p').text(audioObj.person);
                    audioArticle.append(__name);

                    var __audio = $($.parseHTML(articleFootTemplate.prop('outerHTML')));
                    __audio.find('.foot-audio').text(audioObj.name + '.mp3');
                    __audio.append($('<p/>', {class: 'foot-text', text: audioObj.text}));
                    audioArticle.append(__audio);
                }
            }

            tela.find('.tela-article').last().addClass('last');

            $('body').find('#roteiro').append(tela);
        }
        grunt.file.write('roteiro/index.html', w.document.documentElement.outerHTML);
    });

    grunt.registerTask('roteiro', ['clean:roteiroTemplate', 'clean:roteiro', 'compileRoteiro', 'copy:fontsRoteiro', 'copy:roteiro', 'createRoteiro', 'clean:roteiroTemplate']);
};
